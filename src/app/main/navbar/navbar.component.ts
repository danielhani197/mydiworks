import { Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { Subscription } from 'rxjs';

import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { navigation } from 'app/navigation/navigation';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { FuseSidebarComponent } from '@fuse/components/sidebar/sidebar.component';
import { Pet } from '../content/model/pet/pet';
import { User } from '../content/model/user';

import { WallPetService } from 'app/main/content/services/pet/wall-pet.service';
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service';
import { AhliPetService } from '../content/services/pet/ahliPet.service';
import { AhliPet } from '../content/model/pet/ahliPet';
import { PetAuthorities } from '../content/model/pet/petAuthorities';


@Component({
    selector     : 'fuse-navbar',
    templateUrl  : './navbar.component.html',
    styleUrls    : ['./navbar.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FuseNavbarComponent implements OnInit, OnDestroy
{
    private fusePerfectScrollbar: FusePerfectScrollbarDirective;

    currentUser: User;
    id         : string;
    roles      : any []; 
    ahliPet: AhliPet[];
    data:any;
    petAuth: PetAuthorities;

    projectList: Pet[] = [];
    eventList  : Pet[] = [];
    teamList   : Pet[] = [];

    //START OF NAVIGATION
    secAns: any = {
        'id'   : 'security_ans',
        'title': 'Jawapan Keselamatan',
        'translate': 'NAV.SETTING.ANSWER',
        'type' : 'item',
        'icon' : 'security',
        'url'  : 'setting/security_ans'
    }

    roleApply: any =
    {
        'id'   : 'role_apply',
        'title': 'Permohonan Peranan',
        'translate': 'NAV.SETTING.ROLE',
        'type' : 'item',
        'icon' : 'group_add',
        'url'  : 'setting/role/apply'
    };

    navSetting: any = {
        'id'   : 'setting',
        'title': 'Tetapan',
        'translate': 'NAV.SETTING.MANAGE',
        'type' : 'collapse',
        'icon' : 'settings',
        'children' : [
            {
                'id'   : 'security_ques',
                'title': 'Senarai Soalan Keselamatan',
                'translate': 'NAV.SETTING.SECURITY',
                'type' : 'item',
                'url'  : 'setting/question_list'
            },
            {
                'id'   : 'term',
                'title': 'Terma & Syarat',
                'translate': 'NAV.TERM.TITLE',
                'type' : 'item',
                //'icon' : 'view_list',
                'url'  : 'terms/list'
            },
            {
                'id'   : 'ministryList',
                'title': 'Senarai Kementerian',
                'translate': 'NAV.GENERAL.MINISTRY.MINISTRYLIST',
                'type' : 'item',
                //'icon' : 'people',
                'url'  : 'kementerian/list'
            },
            {
                'id'   : 'agencyList',
                'title': 'Senarai Agensi',
                'translate': 'NAV.GENERAL.AGENSI.AGENCYLIST',
                'type' : 'item',
                //'icon' : 'supervisor_account',
                'url'  : 'agensi/list'
            },
            {
                'id'   : 'skemaList',
                'title': 'Senarai Skema',
                'translate': 'NAV.GENERAL.SKEMA.SKEMALIST',
                'type' : 'item',
                //'icon' : 'star_border',
                'url'  : 'skema/list'
            },
            {
                'id'   : 'grade',
                'title': 'Senarai Grade',
                'translate': 'NAV.GENERAL.GRADE.GRADELIST',
                'type' : 'item',
                //'icon' : 'star',
                'url'  : 'grade/list'
            }
        ]
    }

    navWall: any = {
        'id'   : 'mywall',
        'title': 'My Wall',
        'translate': 'NAV.MYWALL.TITLE',
        'type' : 'item',
        'icon' : 'widgets',
        'url'  : '/wall'
    }

    navBox: any = {
        'id'   : 'mybox',
        'title': 'My Box',
        'translate': 'NAV.MYBOX.TITLE',
        'type' : 'item',
        'icon' : 'storage',
        'url'  : 'file-manager/mybox'
    }

    navSearch: any = {

        'id'   : 'carian-admin',
        'title': 'Carian',
        'type' : 'item',
        'translate': 'NAV.CARIAN.CARIAN',
        'icon' : 'search',
        'url'  : '/carian/post'
    
    }

    navPetMain: any = {
        
        'id'   : 'pet',
        'title': 'Mengurus PET',
        'translate': 'NAV.PET.TITLE',
        'type' : 'collapse',
        'icon' : 'business_center',
    }

    nav = {
        pet : {
            user: {
                join: {
                    'id'   : 'request',  
                    'title': 'Permohonan PET',  
                    'translate':'NAV.PET.REQUEST',  
                    'type' : 'item',  
                    'url'  :'pet/request' 
                },
                create: {
                    'id'   : 'petCreateForm',  
                    'title': 'Mohon Bina PET',  
                    'translate':'NAV.PET.CREATEPET',  
                    'type' : 'item',  
                    'url'  :'pet/form/create'
                },
            },
            admin: {
                list: {
                    'id'   : 'list',
                    'title': 'Senarai PET',
                    'translate': 'NAV.PET.LIST',
                    'type' : 'item',
                    'url'  : 'pet/senarai'
                },
                approvaljoin: {
                    'id'   : 'approval',  
                    'title': 'Melulus PET',  
                    'translate':'NAV.PET.APPROVAL',  
                    'type' : 'item',  
                    'url'  :'pet/approval'
                },
                approvalcreate: {
                    'id'   : 'approvalCreate',  
                    'title': 'Melulus Bina PET',  
                    'translate':'NAV.PET.APPROVALCREATE',  
                    'type' : 'item',  
                    'url'  :'pet/list/approve/createPet'
                },
                approvalagensicreate: {
                    'id'   : 'approvalAgensiCreate',  
                    'title': 'Melulus Agensi Bina PET',  
                    'translate':'NAV.PET.APPROVALAGENSICREATE',  
                    'type' : 'item',  
                    'url'  :'pet/list/approve/agensi/createPet'
                },
                approvaldeactive :{
                    'id'    : 'approvalDeactive',
                    'title' : 'Permohonan Nyahaktif',
                    'translate' : 'NAV.PET.DEACTIVE',
                    'type'  : 'item',
                    'url'   : 'pet/deactive'
                }
            }
        }
    }

    navManageUser: any = {
        'id'   : 'user',
        'title': 'Pengurusan Pengguna',
        'translate': 'NAV.USER.MANAGE',
        'type' : 'collapse',
        'icon' : 'contacts',
        'children' : [
            {
                'id'   : 'list',
                'title': 'Senarai Pengguna',
                'translate': 'NAV.USER.LIST',
                'type' : 'item',
                'url'  : 'user/list'
            },
            {
                'id'   : 'approval',
                'title': 'Pengesahan Pengguna',
                'translate': 'NAV.USER.APPROVAL',
                'type' : 'item',
                'url'  : 'user/new'
            },
            {
                'id'   : 'role',
                'title': 'Peranan Pengguna',
                'translate': 'NAV.USER.ROLE',
                'type' : 'item',
                'url'  : 'user/role_list'
            },
        ]
    }

    navManageUserAdmin: any = {
        'id'   : 'adminlist',
        'title': 'Pengurusan Pengguna',
        'translate': 'NAV.USER.MANAGE',
        'type' : 'collapse',
        'icon' : 'search',
        'children' : [
            {
                'id'   : 'agensisenarai',
                'title': 'Senarai Pengguna',
                'translate': 'NAV.USER.LIST',
                'type' : 'item',
                'url'  : 'agensi/pengguna/senarai'
            },
            {
                'id'   : 'agensipenggunabaru',
                'title': 'Pengesahan Pengguna',
                'translate': 'NAV.USER.APPROVAL',
                'type' : 'item',
                'url'  : 'agensi/pengguna/baru'
            },
            {
                'id'   : 'agensipenggunaperanan',
                'title': 'Peranan Pengguna',
                'translate': 'NAV.USER.ROLE',
                'type' : 'item',
                'url'  : 'agensi/pengguna/peranan'
            },
        ]
    }

    navAdvSearch: any = {
        
        'id'   : 'carian-admin',
        'title': 'Carian Admin',
        'translate': 'NAV.CARIAN.CARIANADMIN',
        'type' : 'item',
        'icon' : 'search',
        'url'  : '/post/search'
        
    }

    navManageAgency: any = {
        
        'id'   : 'profileKementerian',
        'title': 'Mengurus Kementerian/ Agensi',
        'translate': 'NAV.GENERAL.AGENSI.MANAGEAGENCY',
        'type' : 'collapse',
        'icon' : 'business',
        'children' : [
            {
                'id'   : 'agensiprofil',
                'title': 'Profil',
                'translate': 'NAV.PROFILE_kEMENTERIAN.TITLE',
                'type' : 'item',
                'url'  : 'kementerianProfile/profileKementerian'
            },
            {
                'id'   : 'bahagianList',
                'title': 'Senarai Bahagian',
                'translate': 'NAV.GENERAL.BAHAGIAN.BAHAGIANLIST',
                'type' : 'item',
                //'icon' : 'people_outline',
                'url'  : 'bahagian/list'
            },
            {
                'id'   : 'agensisenarai',
                'title': 'Senarai Pengguna',
                'translate': 'NAV.USER.LIST',
                'type' : 'item',
                'url'  : 'agensi/pengguna/senarai'
            },
            {
                'id'   : 'agensipenggunabaru',
                'title': 'Pengesahan Pengguna',
                'translate': 'NAV.USER.APPROVAL',
                'type' : 'item',
                'url'  : 'agensi/pengguna/baru'
            },
            {
                'id'   : 'agensipenggunaperanan',
                'title': 'Peranan Pengguna',
                'translate': 'NAV.USER.ROLE',
                'type' : 'item',
                'url'  : 'agensi/pengguna/peranan'
            },
        ]
        

    }

    navPetList: any = {
        'id'   : 'petList',
        'title': 'PET',
        'translate': 'NAV.PET.PET',
        'type' : 'collapse',
        //'icon' : 'apps',
        'children' :[
            {
                'id'   : 'project',
                'title': 'Project',
                'type' : 'collapse',
                'icon' : 'person_pin',
                'children': [
                ] 
            },
            {  
                'id'   : 'event',  
                'title': 'Event',    
                'type' : 'collapse',
                'icon' : 'event',
                'children': [
                ]  
            },  
            {  
                'id'   : 'team',  
                'title': 'Team', 
                'type' : 'collapse',
                'icon' : 'people',
                'children': [
                ] 
            }
        ]
    }

    //END OF NAVIGATION

    @ViewChild(FusePerfectScrollbarDirective) set directive(theDirective: FusePerfectScrollbarDirective)
    {
        if ( !theDirective )
        {
            return;
        }

        this.fusePerfectScrollbar = theDirective;

        this.navigationServiceWatcher =
            this.navigationService.onItemCollapseToggled.subscribe(() => {
                this.fusePerfectScrollbarUpdateTimeout = setTimeout(() => {
                    this.fusePerfectScrollbar.update();
                }, 310);
            });
    }

    @Input() layout;
    navigation: any;
    navigationServiceWatcher: Subscription;
    fusePerfectScrollbarUpdateTimeout;

    constructor(
        private sidebarService: FuseSidebarService,
        private navigationService: FuseNavigationService,
        private router: Router,
        private _pet: WallPetService,
        private _auth: AuthenticationService,
        private _ahliPet: AhliPetService
    )
    {
        // Navigation data
        this.navigation = navigation;

        // Default layout
        this.layout = 'vertical';
    }

    ngOnInit()
    {
        this.router.events.subscribe(
            (event) => {
                if ( event instanceof NavigationEnd )
                {
                    if ( this.sidebarService.getSidebar('navbar') )
                    {
                        this.sidebarService.getSidebar('navbar').close();
                    }
                }
            }
        );

        this.generateSideBar();

        
    }

    ngOnDestroy()
    {
        if ( this.fusePerfectScrollbarUpdateTimeout )
        {
            clearTimeout(this.fusePerfectScrollbarUpdateTimeout);
        }

        if ( this.navigationServiceWatcher )
        {
            this.navigationServiceWatcher.unsubscribe();
        }
    }

    toggleSidebarOpened()
    {
        this.sidebarService.getSidebar('navbar').toggleOpen();
    }

    toggleSidebarFolded()
    {
        this.sidebarService.getSidebar('navbar').toggleFold();
    }

    generateSideBar(){

        this.currentUser = this._auth.getCurrentUser();
        
        if(this.currentUser){

            this.addNavItemDefault();

            this.roles = this.currentUser.authorities;
            var roleArr = new Array();
            
            for(let roleObj of this.roles){
                let role = roleObj.authority;

                roleArr.push(role);
                
                // if(role == 'ROLE_USER'){
                //     this.addNavItemUser();
                // }if(role == 'ROLE_ADMIN'){
                //     this.addNavItemAdmin();
                // }if(role == 'ROLE_SUPERADMIN'){
                //     this.addNavItemSAM();
                // }
            }

            if(roleArr.includes('ROLE_SUPERADMIN')){
                this.addNavItemSAM();
                this.addNavItemAdmin();
            }else if(roleArr.includes('ROLE_ADMIN')){
                this.addNavItemAdmin();
            }else if(roleArr.includes('ROLE_USER')){
                this.addNavItemUser();
            }

            this.addPetList();
        }
    }

    addNavItemSAM(){

        // Get the applications nav item
        const applicationsNavItem = this.navigation[0];

        // Add the new nav item at the beginning of the applications
        applicationsNavItem.children.unshift(this.navAdvSearch);
        applicationsNavItem.children.unshift(this.navManageUser);
        applicationsNavItem.children.unshift(this.navSetting);
    }

    addNavItemAdmin(){

        // Get the applications nav item
        const applicationsNavItem = this.navigation[0];

        // Add the new nav item at the beginning of the applications
        applicationsNavItem.children.unshift(this.navManageAgency);
        //applicationsNavItem.children.unshift(this.navManageUserAdmin);

        this.addChildPetAdmin();
    }

    addNavItemUser(){
        const applicationsNavItem = this.navigation[0];
        this.addChildPetUser();
        applicationsNavItem.children.unshift(this.roleApply);

        
    }

    addNavItemDefault(){

        // Get the applications nav item
        const applicationsNavItem = this.navigation[0];

        // Add the new nav item at the beginning of the applications
        applicationsNavItem.children.unshift(this.navPetMain);
        applicationsNavItem.children.unshift(this.navBox);
        applicationsNavItem.children.unshift(this.navSearch);
        applicationsNavItem.children.unshift(this.navWall);
        applicationsNavItem.children.unshift(this.secAns);

        //default
        const petNav = this.navigation[0].children.find( nav => nav.id === 'pet');
        petNav.type = 'collapse';
        petNav.children = [];
    }

    addChildPetUser(){
        this.id = this.currentUser.id;
        this._ahliPet.getByUserId(this.id).subscribe(
            data=>{
                const petNav = this.navigation[0].children.find( nav => nav.id === 'pet');
        
                petNav.children.push(this.nav.pet.user.join);
                petNav.children.push(this.nav.pet.user.create);
        
                this.ahliPet = data;
                for(let i=0; i< this.ahliPet.length; i++){
                    if(this.ahliPet[i].petAuthorities){
                        this.petAuth =this.ahliPet[i].petAuthorities;
                        if(this.petAuth.id == "SECURITY-PET-ADMIN"){
                            this.data = this.ahliPet[i].tempPet.id;
                        }
                    }
                    
                }
                if(this.data){
                    const newNavItem = {
                        id   : 'approvalAdminPet',
                        title: 'Melulus Ahli',
                        translate:'NAV.PET.APPROVAL',
                        type : 'item',
                        url  : 'pet/approvals/'+this.data
                    };
                    petNav.children.push(newNavItem);
                }
            }
        )
        
    }

    addChildPetAdmin(){
        const petNav = this.navigation[0].children.find( nav => nav.id === 'pet');

        petNav.children.push(this.nav.pet.admin.list);
        petNav.children.push(this.nav.pet.admin.approvaljoin);
        petNav.children.push(this.nav.pet.admin.approvalcreate);
        petNav.children.push(this.nav.pet.admin.approvalagensicreate);
        petNav.children.push(this.nav.pet.admin.approvaldeactive);


    }

    addPetList(){
        this.id = this.currentUser.id;

            const petNav = this.navigation[0].children.find( nav => nav.id === 'pet');
            petNav.children.push(this.navPetList);

            if(this.id){
                
                this._pet.getProjectByUser(this.id).subscribe(
                    project=>{
                        this.addIntoNav('project', project)
                    }
                );
        
                this._pet.getEventByUser(this.id).subscribe(
                    event=>{
                        this.addIntoNav('event', event)
                    }
                );
        
                this._pet.getTeamByUser(this.id).subscribe(
                    team=>{
                        this.addIntoNav('team', team)
                    }
                );
            }
    }

    addIntoNav(type: string, petList: Pet[]){

        const parent = this.navigation[0].children.find( 
            nav => {
                return nav.id === 'pet'
            }
        )

        const result = parent.children.find(
            res=>{
                return res.id === 'petList' 
            }
        )

        if( type == "project"){
            const toInsert = result.children.find( nav => nav.id === 'project')
            for( let obj of petList){
                const newNavItem = {
                    id   : obj.id,
                    title: obj.nama,
                    type : 'item',
                    url  : '/pet/woc/'+obj.id
                };

                toInsert.children.push(newNavItem);
            }
        }
        if( type == "event"){
            const toInsert = result.children.find( nav => nav.id === 'event')
            for( let obj of petList){
                const newNavItem = {
                    id   : obj.id,
                    title: obj.nama,
                    type : 'item',
                    url  : '/pet/woc/'+obj.id
                };

                toInsert.children.push(newNavItem);
            }
        }
        if( type == "team"){
            const toInsert = result.children.find( nav => nav.id === 'team')
            for( let obj of petList){
                const newNavItem = {
                    id   : obj.id,
                    title: obj.nama,
                    type : 'item',
                    url  : '/pet/woc/'+obj.id
                };

                toInsert.children.push(newNavItem);
            }
        }
    }
}
