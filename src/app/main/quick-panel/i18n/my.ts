export const locale = {
    lang: 'my',
    data: {
        'NOTIFIKASI': {
            'TITLE': 'Notifikasi',
            'TODAY': 'Hari Ini',
            'INFOR': 'Informasi',
            'DATE': 'Tarikh',
            'SEARCH': 'Carian',
            'USER':'Pengguna'
        }
    }
};