export const locale = {
    lang: 'en',
    data: {
            'NOTIFIKASI': {
                'TITLE': 'Notifications',
                'TODAY': 'Today',
                'INFOR': 'Information',
                'DATE': 'Date',
                'SEARCH': 'Search',
                'USER': 'User'
            }
    }
};