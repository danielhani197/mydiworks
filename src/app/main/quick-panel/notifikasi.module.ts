import { NgModule } from '@angular/core'; 
import { FormsModule } from '@angular/forms'; 
import { ListNotifikasiModule } from './list-notifikasi/list-notifikasi.module'; 
import { FuseQuickPanelModule } from './quick-panel.module'; 
 
@NgModule({ 
    imports: [ 
        // Auth 
        ListNotifikasiModule, 
        FuseQuickPanelModule, 
        FormsModule 
    ] 
}) 
export class NotifikasiModule 
{ 
     
} 