import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Notifikasi } from 'app/main/content/model/notifikasi';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
 
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { Page } from 'app/main/content/model/util/page';
 
import { locale as english } from './i18n/en';
import { locale as malay } from './i18n/my';
 
import * as Stomp from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import { environment } from '../../../environments/environment';
import { NotifikasiService } from 'app/main/content/services/setting/notifikasi.service';
 
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { AuthenticationService } from '../content/services/authentication/authentication.service';
import { User } from '../content/model/user';
//import { NotificationRouteService } from './noti-route.service';
 
 
@Component({
    selector     : 'fuse-quick-panel',
    templateUrl  : './quick-panel.component.html',
    styleUrls    : ['./quick-panel.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations : fuseAnimations
})
export class FuseQuickPanelComponent implements OnInit
{
    notifiForm :FormGroup;
    date: Date;

    selected: any;
    pathArr: string[];
 
    notifiFormError: any;
    settings: any;
    id: string;
 
    notes = [];
    events = [];
    notifikasi : Notifikasi[];
 
    user: User;

    rows: any[];
    page = new Page();

  
    loadingIndicator = true;
    reorderable = true;

    navi:String;
    
    private stompSelfSub;
    constructor(
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private _router: Router,
        private _user: AuthenticationService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private notifikasiService : NotifikasiService,
        //private _notiRoute: NotificationRouteService
     )
    {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortField = "createddate";
        this.page.sort = "desc";
        this.fuseTranslationLoader.loadTranslations(malay, english);

        this.fuseTranslationLoader.loadTranslations(malay, english);

        this.date = new Date();
        this.settings = {
            notify: true,
            cloud : false,
            retro : true
        };
        
    }
    ngOnInit(){
        // this.notifiForm = this.formBuilder.group({
        //     maklumat           : ['', Validators.required],
        //     createby           : ['', Validators.required],
        // });
        this.user = this._user.getCurrentUser()
        if(this.user){
            this.id = this.user.id;
            this.notifikasiService.loadNoti(this.notifikasi)
            this.latersnoti(this.id);
            
        }
        
        let ws = new SockJS(`${environment.apiUrl}/socket`);
        this.stompSelfSub = Stomp.over(ws);
        let that = this
        that.stompSelfSub.debug = () => {};
        that.stompSelfSub.connect({}, function (frame) {
            that.stompSelfSub.subscribe("/topic/private.noti." + that.user.id, (chatlive) => {
                if(chatlive){
                    
                    that.notifikasiService.laters5Noti(this.id).subscribe(
                        list => {
                            this.notifikasi = list;
                        }
                    )
                }
            });
        });
    }
    latersnoti(id :string){
        this.notifikasiService.laters5Noti(id).subscribe(
            list => {
                this.notifikasi = list;
            }
        )
    }

    getNoti(id)
    {

        let noti: Notifikasi = new Notifikasi();
        noti.setModifiedby(this.user),
        noti.setId(id)
        this.notifikasiService.updateStatusReadNoti(noti).subscribe(
            success=>{
                this.notifikasiService.getNotiId(id).subscribe(
                    data =>{
                        this._router.navigate([data.tindakan]);
                    }
                )
            }
        )
    }
    listNoti(){
        this._router.navigate(['/notification/listnoti']);
    }

    loadNoti(){
        this.notifikasiService.laters5Noti(this.id).subscribe(
            list => {
                
                this.notifikasi = list;
                //this.notifikasiService.loadNoti(this.notifikasi);
            }
        )    
    }
}
 