import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { AuthenticationService } from '../../content/services/authentication/authentication.service';

import { TokenService } from 'app/main/content/services/util/token.service';
import { Notifikasi } from '../../content/model/notifikasi';
import { User } from '../../content/model/user';
import {environment} from '../../../../environments/environment';

@Injectable()
export class NotificationRouteService implements Resolve<any>
{
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    
    private UrlView = `${environment.apiUrl}/api/notification/view-id/`;

    _id: string;
    user: User;
    file: any;

    constructor(
        private _auth: AuthenticationService,
        private http: HttpClient,
        private _token: TokenService,
    )
    {
        this.user = this._auth.getCurrentUser();
        this._id = this.user.id;
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getNoti(this._id)
            ]).then(
                ([Notifikasi]) => {
                    resolve();
                },
                reject
            );
        });
    }

    getNoti(id: string): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get<Notifikasi[]>(this.UrlView+id, this._token.jwtBearer())
                .subscribe((response: any) => {
                    this.file=response;
                    this.onFilesChanged.next(response);
                    this.onFileSelected.next(response[0]);
                    resolve(response);
                }, reject);
        });
    }

}
