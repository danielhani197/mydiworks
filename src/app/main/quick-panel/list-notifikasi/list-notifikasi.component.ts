
import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs';

import { NotificationRouteService } from '../list-notifikasi/noti-route.service';
import { fuseAnimations } from '@fuse/animations/index';
import { NotifikasiService } from '../../content/services/setting/notifikasi.service';
import { User } from '../../content/model/user';
import { AuthenticationService } from '../../content/services/authentication/authentication.service';
import { Notifikasi } from '../../content/model/notifikasi';
import { Router } from '@angular/router';

@Component({
    selector     : 'list-notifikasi',
    templateUrl  : './list-notifikasi.component.html',
    styleUrls    : ['./list-notifikasi.component.scss'],
    animations : fuseAnimations
})
export class ListNotifikasiComponent implements OnInit
{

    pathArr: string[];
    files: any;
    dataSource: FilesDataSource | null;
    user: User;
    id: any;
    // displayedColumns = ['icon', 'name', 'type', 'owner', 'size', 'modified', 'detail-button'];
    displayedColumns = ['maklumat', 'createdby', 'createddate'];
    selected: any;
    list: any;
    _id:any;

    constructor(
        private _notiRoute: NotificationRouteService,
        private _noti: NotifikasiService,
        private _user: AuthenticationService,
        private _router: Router,
    )
    {
        this._notiRoute.onFilesChanged.subscribe(files => {
            this.files = files;
        });
        this._notiRoute.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
        this.user = this._user.getCurrentUser();
        this._id = this.user.id;
    }

    ngOnInit()
    {        
        this.dataSource = new FilesDataSource(this._notiRoute);

        this._noti.getNotiByUserId(this._id).subscribe(
            noti => {
                this.list = noti;  
            }
        )
        
    }

    onSelect(selected)
    {
        this._notiRoute.onFileSelected.next(selected);
        
        let noti: Notifikasi = new Notifikasi();
        noti.setModifiedby(this.user),
        noti.setId(selected.id)
        this._noti.updateStatusReadNoti(noti).subscribe(
            success=>{
                this._noti.getNotiId(selected.id).subscribe(
                    data =>{
                        this._router.navigate([data.tindakan]);
                    }
                )
                
            },
            error=>{
                console.log(error)
            }
        )
    }
}

export class FilesDataSource extends DataSource<any>
{
    constructor(
        private _notiRoute: NotificationRouteService
    )
    {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        return this._notiRoute.onFilesChanged;
    }

    disconnect()
    {
    }
}
