import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, 
    MatCheckboxModule,
    MatIconModule, 
    MatInputModule,
    MatChipsModule,
    MatFormFieldModule,
    MatMenuModule,
    MatDialogModule,
    MatTableModule} from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxEditorModule } from 'ngx-editor';
 
import { MatDividerModule, MatListModule, MatSlideToggleModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { ListNotifikasiComponent } from './list-notifikasi.component';
import { CdkTableModule } from '@angular/cdk/table';
import { NotificationRouteService } from '../list-notifikasi/noti-route.service';
const routes = [
    {
        path     : 'listnoti',
        component: ListNotifikasiComponent,
        children:[],
        resolve  : {
            files: NotificationRouteService
        }
    }
];
 
@NgModule({
    declarations: [
        ListNotifikasiComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        
        MatDividerModule,
        MatListModule,
        MatSlideToggleModule,
        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatInputModule,
        NgxDatatableModule,
        NgxEditorModule,
        CdkTableModule,
        MatTableModule,
        MatFormFieldModule,
        MatChipsModule,
        MatMenuModule,
        MatDialogModule,
        FuseSharedModule,
    ],
    exports: [
        ListNotifikasiComponent
    ],
    providers   : [
        NotificationRouteService
    ]
    
})
export class ListNotifikasiModule
{
}