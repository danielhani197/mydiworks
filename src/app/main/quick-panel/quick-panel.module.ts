import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatDividerModule, MatListModule, MatSlideToggleModule, MatFormFieldModule, MatInputModule, MatIconModule, MatCheckboxModule, MatTableModule, MatSidenavModule, MatRippleModule, MatButtonModule, MatCard } from '@angular/material';
import { ListNotifikasiComponent } from './list-notifikasi/list-notifikasi.component';
import { FuseSharedModule } from '@fuse/shared.module';
import {MatChipsModule} from '@angular/material/chips';
import { FuseQuickPanelComponent } from 'app/main/quick-panel/quick-panel.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CdkTableModule } from '@angular/cdk/table';

 
@NgModule({
    declarations: [
        FuseQuickPanelComponent,
    ],
    imports     : [
        RouterModule,
        MatChipsModule,
        MatDividerModule,
        MatListModule,
        MatSlideToggleModule,
        NgxDatatableModule,
        CdkTableModule,
        MatButtonModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatCheckboxModule,
        MatIconModule,
        NgxDatatableModule,
        MatInputModule,
        MatFormFieldModule,
         
        FuseSharedModule,
    ],
    exports: [
        FuseQuickPanelComponent
    ]
})
export class FuseQuickPanelModule
{
}
