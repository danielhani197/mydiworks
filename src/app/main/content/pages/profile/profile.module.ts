import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ScrollEventModule } from 'ngx-scroll-event';
import { CdkTableModule } from '@angular/cdk/table';

import { MatButtonModule, MatTableModule, MatDividerModule, MatIconModule, MatTabsModule, MatFormFieldModule, MatChipsModule, MatSnackBarModule, MatMenuModule, MatDialogModule, MatCheckboxModule, MatOptionModule, MatSelectModule, MatDatepickerModule, MatInputModule, MatSlideToggleModule, MatToolbarModule, MatRadioModule } from '@angular/material';
import { MatAutocompleteModule} from '@angular/material/autocomplete';

import { FuseSharedModule } from '@fuse/shared.module';

import { FuseProfileComponent } from './profile.component';
import { FuseProfileTimelineComponent } from './tabs/timeline/timeline.component';
import { FuseProfileAboutComponent } from './tabs/about/about.component';
import { FuseProfilePhotosVideosComponent } from './tabs/photos-videos/photos-videos.component';
import { TranslateModule } from '@ngx-translate/core';
import { DeleteKomenDialog } from './tabs/timeline/deleteKomenDialog.component';
import { EditDialogComponent } from './tabs/timeline/editDialog.component';
import { UploadFileDialogComponent } from './tabs/timeline/uploadFileDialog.component';
import { MyBoxUploadDialogComponent } from './tabs/timeline/myBoxUploadDialog.component';
import { DataService } from 'app/main/content/services/wall3/data.service';
import { FuseCalendarComponent } from './tabs/calendar/calendar.component';
import { CalendarModule } from 'angular-calendar';
import { CalendarService } from '../../services/calendar/calendar.service';
import { FuseCalendarEventFormDialogComponent } from './tabs/calendar/event-form/event-form.component';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

const routes = [
    {
        path     : '',
        component: FuseProfileComponent
    }
];

@NgModule({
    declarations: [
        FuseProfileComponent,
        FuseProfileTimelineComponent,
        FuseProfileAboutComponent,
        FuseProfilePhotosVideosComponent,
        DeleteKomenDialog,
        EditDialogComponent,
        UploadFileDialogComponent,
        MyBoxUploadDialogComponent,
        FuseCalendarComponent,
        FuseCalendarEventFormDialogComponent

    ],
    entryComponents: [
        DeleteKomenDialog,
        EditDialogComponent,
        UploadFileDialogComponent,
        MyBoxUploadDialogComponent,
        FuseCalendarEventFormDialogComponent

    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        ScrollEventModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatTabsModule,
        MatFormFieldModule,
        MatChipsModule,
        MatSnackBarModule,
        MatMenuModule,
        MatDialogModule,
        MatCheckboxModule,
        MatOptionModule,
        MatSelectModule,
        MatDatepickerModule,
        MatInputModule,
        MatTableModule,
        CdkTableModule,
        MatAutocompleteModule,
        MatSlideToggleModule,
        MatToolbarModule,
        MatRadioModule,
        CalendarModule.forRoot(),
        SweetAlert2Module,
        
        FuseSharedModule
    ],
    providers   : [
        DataService,
        CalendarService
    ]
})
export class ProfileModule
{
}
