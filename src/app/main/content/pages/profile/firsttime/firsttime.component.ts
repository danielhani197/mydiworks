import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { MatSnackBar, MatStepper, MatStepLabel } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

import { Agensi } from 'app/main/content/model/general/agensi';
import { AgensiService } from 'app/main/content/services/agency/agensi.service';
import { Kementerian } from 'app/main/content/model/general/kementerian';
import { KementerianService } from 'app/main/content/services/agency/kementerian.service';

import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { locale as secEnglish } from 'app/main/content/pages/setting/i18n/en';
import { locale as secMalay } from 'app/main/content/pages/setting/i18n/my';
import { locale as userEnglish } from 'app/main/content/pages/pengguna/i18n/en';
import { locale as userMalay } from 'app/main/content/pages/pengguna/i18n/my';
import { locale as regEnglish } from 'app/main/content/pages/authentication/register-2/i18n/en';
import { locale as regMalay } from 'app/main/content/pages/authentication/register-2/i18n/my';

import { Bandar } from '../../../model/ref/bandar';
import { Negeri } from '../../../model/ref/negeri';
import { BandarNegeriService } from '../../../services/references/bandarNegeri.service';
import { FuseConfigService } from '@fuse/services/config.service';
import { Bahagian } from '../../../model/general/bahagian';
import { Skema } from '../../../model/general/skema';
import { Grade } from '../../../model/general/grade';
import { BahagianService } from '../../../services/agency/bahagian.service';
import { SkemaService } from '../../../services/general/skema.service';
import { Observable } from 'rxjs';
import { User } from '../../../model/user';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { GradeService } from '../../../services/general/grade.service';
import { SoalanKeselamatanService } from '../../../services/setting/soalanKeselamatan.service';
import { SoalanKeselamatan } from '../../../model/setting/soalanKeselamatan';
import { JawapanKeselamatanTemp } from '../../../model/setting/jawapanKeselamatanTemp';
import { PenggunaService } from '../../../services/pengguna/pengguna.service';
import { JawapanKeselamatan } from '../../../model/setting/jawapanKeselamatan';
import { JawapanKeselamatanService } from '../../../services/setting/jawapanKeselamatan.service';
import { PasswordValidation } from '../../../common/utilities/password-validation';
import { MessageService } from 'ngx-editor/app/ngx-editor/common/services/message.service';

@Component({
    selector   : 'firsttime',
    templateUrl: './firsttime.component.html',
    styleUrls  : ['./firsttime.component.scss'],
    animations : fuseAnimations,
})
export class FirsttimeComponent implements OnInit
{   

    agencyForm: FormGroup;
    agencyFormErrors: any;

    personalForm: FormGroup;
    personalFormErrors: any;

    securityForm: FormGroup;
    securityFormErrors: any;

    passForm: FormGroup;
    passFormErrors: any;

    currentUser: User;
    agensiForm: FormGroup;
    agensiFormErrors: any;

    successTxt: string;
    existTxt: string;
    formErrTxt: string;

    state: Negeri;
    city: Bandar;
    agency: Agensi;
    ministry: Kementerian;
    department$: Observable<Bahagian>;
    scheme: Skema;
    grade: Grade;

    stateLs$: Observable<Negeri[]> 
    cityLs$: Observable<Bandar[]>
    agencyLs$: Observable<Agensi[]> 
    departmentLs$: Observable<Bahagian[]> 
    schemeLs$: Observable<Skema[]> 
    gradeLs$: Observable<Grade[]>
    soalanLs$: Observable<any[]> 

    selectedQuestion: any[] = new Array();
    jawapanLs : JawapanKeselamatan[] = new Array();

    ministryId: string;
    agencyId: string;
    bahagianId: string;
    schemeId: string;
    gradeId: string;

    errEmailExist: boolean;
    errEmailPersonalExist: boolean;
    errWrongPassword: boolean;
    personalHeaderErr: boolean;
    passHeaderErr: boolean;

    @ViewChild('stepper') stepper: MatStepper;

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _kementerian: KementerianService,
        private _agensi: AgensiService,
        private _router: Router,
        private _toastr: ToastrService,
        private _translate: TranslateService,
        private _bandar: BandarNegeriService,
        private _bahagian: BahagianService,
        private _scheme: SkemaService,
        public snackBar: MatSnackBar,
        private _auth: AuthenticationService,
        private _grade: GradeService,
        private _soalanKeselamatan: SoalanKeselamatanService,
        private _jawapanKeselamatan: JawapanKeselamatanService,
        private _pengguna: PenggunaService
    )
    {
        this.fuseConfig.setConfig({
            layout: {
                navigation: 'none',
                toolbar   : 'none',
                footer    : 'none',
                chat     : 'none',
            }
        });

        this.fuseTranslationLoader.loadTranslations(
            malay, english,
            secMalay, secEnglish,
            userMalay, userEnglish,
            regMalay, regEnglish
        );

        this._translate.get('AGENSI.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('USER.MSG.GENERAL-SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
        this._translate.get('REG.ERROR').subscribe((res: string) => {
            this.formErrTxt = res;
        });

        this.initFormError();
        this.currentUser = this._auth.getCurrentUser();

    }

    ngOnInit()
    {
        this.checkIsFirstLogin();

        this.initForms();

        this.stateLs$ = this._bandar.getState();
        this.ministry = this.currentUser.kementerian;
        this.agency = this.currentUser.agensi;

        if(this.agency){
            this.departmentLs$ = this._bahagian.getBahagiansByAgensiId(this.agency.id);
        }else if(this.ministry){
            this.departmentLs$ = this._bahagian.getBahagiansByKementerianId(this.ministry.id);
        }

        this.schemeLs$ = this._scheme.getSchema();
        this.gradeLs$ = this._grade.getGrade();
        this.soalanLs$ = this._soalanKeselamatan.getSoalanKeselamatan();
    
    }

    onSubmit(){
        if(this.currentUser.id){

            //reset field error
            this.resetError();

            var negeriId = this.personalForm.controls['negeri'].value;
            var bandarId = this.personalForm.controls['bandar'].value;

            let user : User = new User();
            user.setId(this.currentUser.id);
            user.setName(this.personalForm.controls['name'].value);
            user.setPhoneNo(this.personalForm.controls['phone'].value);
            user.setEmail(this.personalForm.controls['emailoffice'].value);
            user.setUserEmail(this.personalForm.controls['emailpersonal'].value);
            user.setAddress(this.personalForm.controls['address'].value);
            user.setPostcode(this.personalForm.controls['postcode'].value);

            let negeri: Negeri = new Negeri(
                negeriId, null
            );

            let bandar : Bandar = new Bandar(
                bandarId, "", negeriId
            );

            user.setBandar(bandar);
            user.setNegeri(negeri);

            let bahagian: Bahagian = this.agencyForm.controls['bahagian'].value;
            if(bahagian){
                user.setBahagian(bahagian);
            }

            user.setSkema(this.agencyForm.controls['scheme'].value);
            user.setPosition(this.agencyForm.controls['position'].value);
            user.setGrade(this.agencyForm.controls['grade'].value);
            user.setOldPassword(this.passForm.controls['oldpass'].value);
            user.setNewPassword(this.passForm.controls['newpass'].value);
            user.setFirstLogin(false);
            
            this._pengguna.updateUserProfil(user).subscribe(
                userObj=>{

                    let newUser: User = new User();
                    newUser.setId(userObj.id)

                    let soalan1: SoalanKeselamatan = new SoalanKeselamatan ();
                    soalan1.setId(this.securityForm.controls['q1'].value)

                    let soalan2: SoalanKeselamatan = new SoalanKeselamatan ();
                    soalan2.setId(this.securityForm.controls['q2'].value)

                    let soalan3: SoalanKeselamatan = new SoalanKeselamatan ();
                    soalan3.setId(this.securityForm.controls['q3'].value)

                    let soalan4: SoalanKeselamatan = new SoalanKeselamatan ();
                    soalan4.setId(this.securityForm.controls['q4'].value)

                    let soalan5: SoalanKeselamatan = new SoalanKeselamatan ();
                    soalan5.setId(this.securityForm.controls['q5'].value)

                    let jawapan1: JawapanKeselamatan = new JawapanKeselamatan ();
                    jawapan1.setJawapan(this.securityForm.controls['a1'].value);
                    jawapan1.setSoalanKeselamatan(soalan1);
                    jawapan1.setRank("1");
                    jawapan1.setUser(newUser);

                    let jawapan2: JawapanKeselamatan = new JawapanKeselamatan ();
                    jawapan2.setJawapan(this.securityForm.controls['a2'].value);
                    jawapan2.setSoalanKeselamatan(soalan2);
                    jawapan2.setRank("2");
                    jawapan2.setUser(newUser);

                    let jawapan3: JawapanKeselamatan = new JawapanKeselamatan ();
                    jawapan3.setJawapan(this.securityForm.controls['a3'].value);
                    jawapan3.setSoalanKeselamatan(soalan3);
                    jawapan3.setRank("3");
                    jawapan3.setUser(newUser);

                    let jawapan4: JawapanKeselamatan = new JawapanKeselamatan ();
                    jawapan4.setJawapan(this.securityForm.controls['a4'].value);
                    jawapan4.setSoalanKeselamatan(soalan4);
                    jawapan4.setRank("4");
                    jawapan4.setUser(newUser);

                    let jawapan5: JawapanKeselamatan = new JawapanKeselamatan ();
                    jawapan5.setJawapan(this.securityForm.controls['a5'].value);
                    jawapan5.setSoalanKeselamatan(soalan5);
                    jawapan5.setRank("5");
                    jawapan5.setUser(newUser);

                    this.jawapanLs.push(jawapan1);
                    this.jawapanLs.push(jawapan2);
                    this.jawapanLs.push(jawapan3);
                    this.jawapanLs.push(jawapan4);
                    this.jawapanLs.push(jawapan5);

                    this._jawapanKeselamatan.createJawapanKeselamatan(this.jawapanLs).subscribe(
                        success=>{
                            this.snackBar.open(this.successTxt, "OK", {
                                panelClass: ['blue-snackbar']
                            });
                            this._router.navigate(['/wall']).then(()=>{window.location.reload();});
                        }
                    )
                },
                error=>{
                    var messages = error.error;

                    for(let errMessage of messages){
                        console.log(errMessage)
                        if(errMessage == "ERR_EXIST_EMAIL"){
                            this.errEmailExist = true;
                            this.stepper.selectedIndex = 0;
                            this.personalHeaderErr = true;
                        }else if(errMessage == "ERR_EXIST_EMAIL_PERSONAL"){
                            this.errEmailPersonalExist = true;
                            this.stepper.selectedIndex = 0;
                            this.personalHeaderErr = true;
                        }else if(errMessage == "ERR_EXIST_EMAIL_BOTH"){
                            this.errEmailExist = true;
                            this.errEmailPersonalExist = true;
                            this.stepper.selectedIndex = 0;
                            this.personalHeaderErr = true;
                        }else if(errMessage == "ERR_WRONGPASSWORD"){
                            this.errWrongPassword = true;
                            this.passHeaderErr = true;
                        }
                    }
                    this.snackBar.open(this.formErrTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });
                }
            )
        }
    }

    resetError(){
        this.errEmailExist = false;
        this.errEmailPersonalExist = false;
        this.errWrongPassword = false;
        this.passHeaderErr = false;
        this.personalHeaderErr = false;
    }

    setSBState(stateId){
        this.cityLs$ = this._bandar.getCity(stateId);
    }

    initForms(){
        this.personalForm = this.formBuilder.group({
            name: [this.currentUser.name, Validators.required],
            phone: ['', Validators.required],
            emailoffice: [this.currentUser.email, [Validators.required, Validators.email]],
            emailpersonal: ['', [Validators.required, Validators.email]],
            address: ['', Validators.required],
            postcode: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.maxLength(5), Validators.minLength(5)]],
            bandar: ['', Validators.required],
            negeri: ['', Validators.required]
        });

        this.personalForm.valueChanges.subscribe(() => {
            this.onPersonalFormValuesChanged();
        });

        this.agencyForm = this.formBuilder.group({
            bahagian:[''],
            scheme:[ '' ,Validators.required],
            position:[ '' ,Validators.required],
            grade:[ '',Validators.required ]
        })

        this.securityForm = this.formBuilder.group({
            q1      : ['', Validators.required],
            q2      : ['', Validators.required],
            q3      : ['', Validators.required],
            q4      : ['', Validators.required],
            q5      : ['', Validators.required],
            a1      : ['', Validators.required],
            a2      : ['', Validators.required],
            a3      : ['', Validators.required],
            a4      : ['', Validators.required],
            a5      : ['', Validators.required],
        });

        this.passForm = this.formBuilder.group({
            oldpass: ['', Validators.required],
            newpass: ['', [Validators.required, Validators.pattern(PasswordValidation.pattern)]],
            confirmpass: ['', Validators.required]
        },
        {
            validator: PasswordValidation.MatchPassword
        })
    }

    initFormError(){
        // Reactive form errors
        this.personalFormErrors = {
            name: {},
            phone:{},
            emailoffice: {},
            emailpersonal: {},
            address: {},
            postcode:{},
            bandar:{},
            negeri:{}
        };
    
        this.agencyFormErrors = {
            bahagian: {},
            position:{},
            scheme: {},
            grade:{},
        };

        this.securityFormErrors = {
            q1      : {},
            q2      : {},
            q3      : {},
            q4      : {},
            q5      : {},
            a1      : {},
            a2      : {},
            a3      : {},
            a4      : {},
            a5      : {},
        };

        this.passFormErrors = {
            oldpass: {},
            newpass: {},
            confirmpass: {}
        }
    }

    onPersonalFormValuesChanged()
    {
        for ( const field in this.personalFormErrors )
        {
            if ( !this.personalFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.personalFormErrors[field] = {};

            // Get the control
            const control = this.personalForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.personalFormErrors[field] = control.errors;
            }
        }
    }

    onChangeQuestion(){

        let q1 = this.securityForm.controls['q1'].value;
        let q2 = this.securityForm.controls['q2'].value;
        let q3 = this.securityForm.controls['q3'].value;
        let q4 = this.securityForm.controls['q4'].value;
        let q5 = this.securityForm.controls['q5'].value;

        //reset array value
        this.selectedQuestion.splice(0,this.selectedQuestion.length)

        if(q1){
            this.selectedQuestion.push(q1);
        }
        if(q2){
            this.selectedQuestion.push(q2);
        }
        if(q3){
            this.selectedQuestion.push(q3);
        }
        if(q4){
            this.selectedQuestion.push(q4);
        }
        if(q5){
            this.selectedQuestion.push(q5);
        }
        
    }

    checkIsFirstLogin(){
        var isFirstLogin = this.currentUser.isFirstLogin;
        if(!isFirstLogin){
            //this._router.navigate(['wall']);
        }
    }

}
