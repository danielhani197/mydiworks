import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatIconModule, MatSelectModule, MatStepperModule, MatSnackBarModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { FirsttimeComponent } from './firsttime.component';

const routes = [
    {
        path     : 'update',
        component: FirsttimeComponent
    }
];

@NgModule({
    declarations: [
        FirsttimeComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,

        MatIconModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatSnackBarModule,

        FuseSharedModule
    ],
})
export class FirsttimeModule
{
}
