import { Component } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';
import { TranslateService } from '@ngx-translate/core';
import { Skema } from '../../../../../model/general/skema';
import { Grade } from '../../../../../model/general/grade';
import { PenggunaService } from '../../../../../services/pengguna/pengguna.service';
import { SkemaService } from '../../../../../services/general/skema.service';
import { GradeService } from '../../../../../services/general/grade.service';
import { User } from '../../../../../model/user';
import { MatSnackBar } from '@angular/material';
import { KementerianService } from '../../../../../services/agency/kementerian.service';
import { AgensiService } from '../../../../../services/agency/agensi.service';
import { BahagianService } from '../../../../../services/agency/bahagian.service';
import { Kementerian } from '../../../../../model/general/kementerian';
import { Agensi } from '../../../../../model/general/agensi';
import { Bahagian } from '../../../../../model/general/bahagian';
import { AuthenticationService } from '../../../../../services/authentication/authentication.service';

@Component({
    selector   : 'profile-agensi',
    templateUrl: './agensi.component.html',
    styleUrls  : ['./agensi.component.scss'],
    animations : fuseAnimations
})
export class AgensiComponent
{
    _id: string;
    successTxt: string;
    existTxt: string;

    agensiForm: FormGroup;
    agensiFormErrors: any;
   
    currentSkema: any[]
    skemaLs: any[]
    skema: Skema;

    //FILTER SEARCH STUFF
    kementerianLs:       any[];
    agensiLs:            any[];
    kementerian:         Kementerian;
    agensi:              Agensi;
    bahagianLs:          any[];
    bahagian:            Bahagian;

    currentGrade: any[]
    gradeLs: any[];
    grade: Grade;
    constructor(    
        private formBuilder: FormBuilder,
        private _router: Router,
        private _authentication: AuthenticationService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _translate: TranslateService,
        private _pengguna: PenggunaService,
        private _skema: SkemaService,
        private _grade: GradeService,
        private _kementerian: KementerianService,
        private _agensi: AgensiService,
        private _bahagian: BahagianService,
        private snackBar: MatSnackBar
    )
    {
        this._id = this._authentication.getCurrentUser().id;
        this.fuseTranslationLoader.loadTranslations(malay, english);

        // Reactive form errors
        this.agensiFormErrors = {
            position : {},
            skema: {},
            grade : {},
            kementerian : {},
            agensi : {},
            bahagian : {}
        };

        this._translate.get('USER.EDIT.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('USER.EDIT.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
     
    } 
    
    ngOnInit(){

        this.agensiForm = this.formBuilder.group({
            kementerian    : ({value: '', disabled: true}),
            agensi    : ({value: '', disabled: true}),
            bahagian    : ['',[Validators.required]],
            position    : ['', [Validators.required]],
            skema    : ['', [Validators.required]],
            grade    : ['', [Validators.required]],
        });

        this.agensiForm.valueChanges.subscribe(() => {
            this.onAgensiFormValuesChanged();
        });
        
        this._skema.getSchema().subscribe(
            skema=>{
                this.skemaLs = skema;
                this._grade.getGrade().subscribe(
                    grade=>{
                        this.gradeLs = grade;
                        this.loadPengguna();
                        this._kementerian.getKementerians().subscribe(
                            kementerian=>{
                                this.kementerianLs = kementerian;
                            }
                        )
                    }
                )
            }
        )
        
    }

    onAgensiFormValuesChanged(){
        for ( const field in this.agensiFormErrors )
        {
            if ( !this.agensiFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.agensiFormErrors[field] = {};

            // Get the control
            const control = this.agensiForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.agensiFormErrors[field] = control.errors;
            }
        }
    }

    loadPengguna(){
        this.kementerian = null;
        this.agensi = null;
        this.bahagian = null;
        this._pengguna.getUserById(this._id).subscribe(
            data=>{

                let userSkema = data.skema;
                let userGrade = data.grade;
                let userKementerian = data.kementerian;
                let userAgensi = data.agensi;
                let userBahagian = data.bahagian;

                if(userSkema){
                    this.setSBSkema(userSkema.id)
                    this.agensiForm.patchValue({
                        skema: userSkema.id
                    })
                }
                if(userGrade){
                    this.setSBGrade(userGrade.id)
                    this.agensiForm.patchValue({
                        grade: userGrade.id
                    })
                }
                if(userKementerian){
                    this.kementerian = userKementerian
                    this.agensiForm.patchValue({
                        kementerian: userKementerian.name
                    })
                    this._agensi.getAllAgenciesByMinistryId(userKementerian.id).subscribe(
                        agencies=>{
                            this.agensiLs = agencies;
                            if(userAgensi){
                                this.agensi = userAgensi;
                                this.agensiForm.patchValue({
                                    agensi: userAgensi.name
                                })
                                this._bahagian.getBahagiansByAgensiId(userAgensi.id).subscribe(
                                    bahagian=>{
                                        this.bahagianLs = bahagian;
                                        if(userBahagian){
                                            this.bahagian = userBahagian;
                                            this.agensiForm.patchValue({
                                                bahagian: userBahagian.id
                                            })
                                        }
                                    }
                                )
                            }else{
                                this._bahagian.getBahagiansByKementerianId(userKementerian.id).subscribe(
                                    bahagian=>{
                                        this.bahagianLs = bahagian;
                                        if(userBahagian){
                                            this.bahagian = userBahagian;
                                            this.agensiForm.patchValue({
                                                bahagian: userBahagian.id
                                            })
                                        }
                                    }
                                )
                            }
                        }
                    )
                    
                }
                
                
                this.agensiForm.patchValue({
                    position: data.position
                })
            }
        )
    }

    hantar(){
        if(this._id){
            let user : User = new User();
            user.setId(this._id);
            user.setPosition((this.agensiForm.controls['position'].value));
            user.setSkema(this.skema);
            user.setGrade(this.grade);
            user.setKementerian(this.kementerian);
            user.setAgensi(this.agensi);
            user.setBahagian(this.bahagian);
            this._pengguna.adminkemaskiniAgensi(user).subscribe(
                success=>{
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                    });
                    this.loadPengguna();
                },
                error=>{
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });
                }
            )
        }
    }

    setSBSkema(id: any): void {
        
        this.currentSkema = this.skemaLs.filter(value =>  value.id === id);
        this.skema = this.currentSkema[0];
    }

    setSBGrade(id: any): void {

        this.currentGrade = this.gradeLs.filter(value => value.id === id);
        this.grade = this.currentGrade[0];
        
    }
    
    backButton(){
        this._router.navigate(['/wall']);
    }
    
    //ONCHANGE KEMENTERIAN SELECTION
    setKementerian(id: any): void {

        
        // GET KEMENTERIAN BASED ON ID
        this._kementerian.getKementerianById(id).subscribe(
            kementerian=>{
                this.kementerian = kementerian
                this.agensi = null
                this.bahagian = null
            }
        )
        
        //load agensi based on kementerian
        this._agensi.getAllAgenciesByMinistryId(id).subscribe(
            agensiLs=>{
                this.agensiLs = agensiLs;
            }
        )

        this._bahagian.getBahagiansByKementerianId(id).subscribe(
            data=>{
                this.bahagianLs = data
            }
        )

        this.agensiForm.patchValue({
            agensi: '',
            bahagian: ''
        })
            
        
    }

    setSBAgency(id: any): void {

        this._agensi.getAgencyById(id).subscribe(
            data=>{
                this.agensi = data;
                this.bahagian = null
            }
        )

        this._bahagian.getBahagiansByAgensiId(id).subscribe(
            data=>{
                this.bahagianLs = data
            }
        )

        this.agensiForm.patchValue({
            bahagian: ''
        })

    }

    setSBBahagian(id: any): void {
        this._bahagian.getBahagianById(id).subscribe(
            data=>{
                this.bahagian = data;
            }
        )
    }
}
