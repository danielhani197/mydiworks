import { Component, ViewChild } from '@angular/core';
import { User } from '../../../model/user';
import { PenggunaService } from '../../../services/pengguna/pengguna.service';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { Router } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as malay } from '../i18n/my';
import { locale as english } from '../i18n/en';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'edit-profile',
    templateUrl: './edit-profile.component.html',
    styleUrls: ['./edit-profile.component.scss'],
    animations: fuseAnimations
})
export class EditProfileAboutComponent {

    _id: any;

    successTxt: string;
    existTxt: string;

    selectedFile: File;
    imageSrc: string = '';
    loaded: boolean = false;
    imageLoaded: boolean = false;
    picForm: FormGroup;

    picImage: any;

    //document upload
    @ViewChild('image') uploadImage;

    constructor(
        private penggunaService: PenggunaService,
        private authenticationService: AuthenticationService,
        private route: Router,
        private translate: TranslateService,
        private translateLoader: FuseTranslationLoaderService,
        public snackBar: MatSnackBar
    ) {
        this.translateLoader.loadTranslations(malay, english);

        this.translate.get('WALL.ABOUT.FAIL').subscribe((res: string) => {
            this.existTxt = res;
        });
        this.translate.get('WALL.ABOUT.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });

        this._id = this.authenticationService.getCurrentUser().id;
    }
    ngOnInit() {


        this.picForm = new FormGroup({
            image: new FormControl()
        });


        this.penggunaService.getUserById(this._id).subscribe(
            user => {

                if (user.image) {
                    this.imageSrc = "data:image/JPEG;base64," + user.image;
                }
                if (!user.image) {
                    this.imageSrc = "assets/images/avatars/profile.jpg";
                }

            }
        )

    }


    savePic() {

        if (this._id) {
            let user: User = new User();
            let input = new FormData();
            user.setId(this._id);
            input.append('image', this.picForm.get('image').value);
            input.append('info', new Blob([JSON.stringify(user)],
                {
                    type: "application/json"
                }));

            const formModel = input;
            this.penggunaService.updateUserProfilPic(formModel).subscribe(
                success => {
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                    });
                },
                error => {
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });
                }
            )
        }
    }

    back() {
        this.route.navigate(['/wall']);
    }

    handleInputChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

        var pattern = /image-*/;
        var reader = new FileReader();

        if (!file.type.match(pattern)) {
            //toastr.danger(message.global.invalidFormatImage);
            return;
        }

        this.loaded = false;

        this.picForm.get('image').setValue(file);
        //console.log(this.picForm.get('image').value);

        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        //save direct to db
    }
    handleImageLoad() {
        this.imageLoaded = true;
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    }


}