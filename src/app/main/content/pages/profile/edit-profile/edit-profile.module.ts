import { EditProfileAboutComponent } from "./edit-profile.component";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule, 
    MatCheckboxModule,
    MatIconModule, 
    MatInputModule,
    MatChipsModule,
    MatFormFieldModule,
    MatMenuModule,
    MatSelectModule,
    MatDialogModule,
    MatStepperModule,
    MatSnackBarModule, 
    MatTabsModule} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaklumatComponent } from "./tabs/maklumat/maklumat.component";
import { PerhubunganComponent } from "./tabs/perhubungan/perhubungan.component";
import { KatalaluanComponent } from "./tabs/katalaluan/katalaluan.component";
import { AgensiComponent } from "./tabs/agensi/agensi.component";


const routes = [
    {
        path     : 'edit',
        component: EditProfileAboutComponent
    }
];

@NgModule({
    declarations: [
        EditProfileAboutComponent,
        MaklumatComponent,
        PerhubunganComponent,
        KatalaluanComponent,
        AgensiComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatInputModule,
        MatTabsModule,
        MatStepperModule,
        MatFormFieldModule,
        MatChipsModule,
        MatMenuModule,
        MatDialogModule,
        MatSnackBarModule,
        MatSelectModule,

        FuseSharedModule
    ],
    exports     : [
        EditProfileAboutComponent
    ]
})
export class EditProfileModule
{
}