export const locale = {
    lang: 'my',
    data: {
        'WALL': {
            'ERROR': 'Maklumat Tidak Berjaya Dihantar.',
            'SUCCESS': 'Maklumat Telah Berjaya Dihantar.',
            'CREATEPOST': 'Tulis Sesuatu',
            'PRIVATE': 'Peribadi',
            'POST': 'Hantar',
            'FileError': 'Format fail salah atau Saiz fail melebihi had.',
            'EDIT': {
                'BACK': 'Kembali',
                'EDIT': 'Kemaskini',
                'KOMEN': {
                    'EDIT': 'Kemaskini Komen',
                    'ERROR': 'Komen Tidak Berjaya Dikemaskini.',
                    'SUCCESS': 'Komen Telah Berjaya Dikemaskini.'
                },
                'POST': {
                    'EDIT': 'Kemaskini Post',
                    'ERROR': 'Post Tidak Berjaya Dikemaskini.',
                    'SUCCESS': 'Post Telah Berjaya Dikemaskini.'
                },
                'ERROR':{
                    'NAME': 'Nama diperlukan!',
                    'ADDRESS': 'Alamat diperlukan!',
                    'STATE': 'Negeri diperlukan!',
                    'CITY': 'Bandar diperlukan!',
                    'POSTCODE': 'Poskod diperlukan!',
                    'P_EMAIL': 'Email Peribadi diperlukan!',
                    'C_EMAIL': 'Email Rasmi diperlukan!',
                    'TEL': 'Sila masukkan minimun 10 angka!',
                    'POSITION': 'Jawatan diperlukan!',
                    'SKIM': 'Skim diperlukan!',
                    'GRADE': 'Gred diperlukan!',
                    'OLD_PASS': 'Kata Laluan Lama diperlukan!',
                    'NEW_PASS': 'Format tidak sah!',
                    'CON_PASS': 'Kata laluan tidak sama!'
                }
            },
            'DELETE': {
                'BACK': 'Kembali',
                'DELETE': 'Padam',
                'KOMEN':{
                    'CONFIRM': 'Adakah anda pasti untuk memadam komen ini ?',
                    'DELETE': 'Padam Komen',
                    'ERROR': 'Komen Tidak Berjaya Dipadam.',
                    'SUCCESS': 'Komen Telah Berjaya Dipadam.'
                },
                'POST':{
                    'CONFIRM': 'Adakah anda pasti untuk memadam post ini ?',
                    'DELETE': 'Padam Post',
                    'ERROR': 'Post Tidak Berjaya Dipadam.',
                    'SUCCESS': 'Post Telah Berjaya Dipadam.',
                }
            },
            'KOMEN': 'Komen',
            'CREATEKOMEN': 'Tambah komen',
            'ABOUT':{
                'NAME': 'Nama',
                'EMAIL': {
                    'COMPANY': 'Email Rasmi',
                    'PERSONAL': 'Email Peribadi'
                },
                'ADDRESS':'Alamat',
                'TEL':'Telefon',
                'CONTACT': 'Hubungi',
                'AGENSI': 'Agensi',
                'BAHAGIAN': 'Bahagian',
                'KEMENTERIAN': 'Kementerian',
                'NEGERI': 'Negeri',
                'POSITION': 'Jawatan',
                'BANDAR': 'Bandar',
                'POSTCODE': 'Poskod',
                'PREVIOUS':'Kembali',
                'NEXT': 'Seterusnya',
                'SAVE': 'Simpan',
                'FILL_NAME': 'Isikan maklumat anda',
                'FILL_ADDRESS': 'Isikan Maklumat Untuk Dihubungi',
                'FILL_AGENSI': 'Isikan maklumat agensi anda',
                'UPLOAD_PHOTO': 'Muat Naik Gambar Profile',
                'DONE': 'Selesai',
                'FAIL':'Gagal Simpan Data',
                'SUCCESS': 'Maklumat Berjaya Simpan',
                'SKEMA': 'Skim',
                'GRADE': 'Gred',
                'IMAGE': 'Gambar',
                'EDIT_PROFILE': 'Kemaskini Profile',
                'SUBMIT': 'Hantar',
                'BACK': 'Kembali Ke Profile',
                'SAVE_PICTURE':'Simpan Gambar',
                'EDIT':'Update Profile',
                'PERSONAL': 'Maklumat Peribadi',
                'RESET_PASS' : 'Kemaskini Kata Laluan',
                'OLD_PASS': 'Kata Laluan Lama',
                'NEW_PASS': 'Kata Laluan Baru',
                'CON_PASS': 'Sahkan Kata Laluan'
            },

            'WALLATTACH': {
                'UPLOAD': 'Muat Naik Fail Dari PC',
                'TYPE': 'Jenis Kandungan',
                'DOCUMENT': 'Dokumen',
                'PHOTO': 'Foto',
                'VIDEO': 'Video',
                'AUDIO': 'Audio',
                'CHOOSE': 'Sila Pilih',
                'MAX': 'Maksima saiz setiap fail adalah',
                'TAJUK': 'Tajuk',
                'KETERANGAN': 'Keterangan',
                'PERISTIWA': 'Peristiwa',
                'TARIKH': 'Tarikh Peristiwa',
                'KETERANGANPERISTIWA': 'Keterangan Peristiwa',
                'LOKASI': 'Lokasi Peristiwa',
                'KEMBALI': 'Kembali',
                'SIMPAN': 'Simpan',
                'TAKS': 'Pilih Taksonomi',
                'TAKSNULL': 'Taksonomi wajib diisi'
            }
        }
    }
};
