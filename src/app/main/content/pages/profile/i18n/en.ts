export const locale = {
    lang: 'en',
    data: {
        'WALL': {
            'ERROR': 'Information Was Unsuccessfully Delivered.',
            'SUCCESS': 'Information Was Successfully Delivered.',
            'CREATEPOST': 'Write Something..',
            'PRIVATE': 'Private',
            'POST': 'Post',
            'FileError': 'File Format is incorrect or File size limit is exceed.',
            'EDIT': {
                'BACK': 'Back',
                'EDIT': 'Edit',
                'KOMEN': {
                    'EDIT': 'Edit Comment',
                    'ERROR': 'Comment Was Unsuccessfully Edited.',
                    'SUCCESS': 'Comment Was Successfully Edited.'
                },
                'POST': {
                    'EDIT': 'Edit Post',
                    'ERROR': 'Post Was Unsuccessfully Edited.',
                    'SUCCESS': 'Post Was Successfully Edited.'
                },
                'ERROR':{
                    'NAME': 'Name is required!',
                    'ADDRESS': 'Address is required!',
                    'STATE': 'State is required!',
                    'CITY': 'City is required!',
                    'POSTCODE': 'Postcode is required!',
                    'P_EMAIL': 'Personal Email is required!',
                    'C_EMAIL': 'Formal Email is required!',
                    'TEL': 'Please input minimun 10 digit!',
                    'POSITION': 'Position is required!',
                    'SKIM': 'Skim is required!',
                    'GRADE': 'Grade is required!',
                    'OLD_PASS': 'Old password is required!',
                    'NEW_PASS': 'Invalid format!',
                    'CON_PASS': 'Password should be same!'
                }

            },
            'DELETE': {
                'BACK': 'Back',
                'DELETE': 'Delete',
                'KOMEN':{
                    'CONFIRM': 'Are you sure you want to delete this comment ?',
                    'DELETE': 'Delete Comment',
                    'ERROR': 'Comment Was Unsuccessfully Deleted.',
                    'SUCCESS': 'Comment Was Successfully Deleted.'
                },
                'POST':{
                    'CONFIRM': 'Are you sure you want to delete this post ?',
                    'DELETE': 'Delete Post',
                    'ERROR': 'Post Was Unsuccessfully Deleted.',
                    'SUCCESS': 'Post Was Successfully Deleted.',
                }
            },
            'KOMEN': 'Comment(s)',
            'CREATEKOMEN': 'Add a comment',
            'ABOUT':{
                'NAME': 'Name',
                'EMAIL': {
                    'COMPANY': 'Formal Email ',
                    'PERSONAL': 'Personal Email'
                },
                'ADDRESS':'Address',
                'TEL':'Phone Number',
                'CONTACT': 'Agency',
                'AGENSI': 'Agensi',
                'BAHAGIAN': 'Section',
                'KEMENTERIAN': 'Ministry',
                'NEGERI': 'State',
                'SKEMA': 'Skema',
                'POSITION': 'Position',
                'POSTCODE':'Postcode',
                'PREVIOUS':'Previous',
                'NEXT': 'Next',
                'SAVE': 'Save',
                'FILL_NAME':'Fill out your information',
                'FILL_ADDRESS': 'Fill out your contact',
                'FILL_AGENSI': 'Fill out your agency information',
                'UPLOAD_PHOTO': 'Upload Profile Picture',
                'DONE': 'Done',
                'FAIL':'Fail Save Profile',
                'SUCCESS': 'Save Success',
                'GRADE': 'Grade',
                'BANDAR': 'City',
                'IMAGE': 'Picture',
                'EDIT_PROFILE': 'Update Profile',
                'SUBMIT': 'Submit',
                'BACK': 'Back To Profile',
                'SAVE_PICTURE': 'Save Picture',
                'EDIT':'Kemaskini Profile',
                'PERSONAL': 'Personal Information',
                'RESET_PASS' : 'Reset Password',
                'OLD_PASS': 'Old Password',
                'NEW_PASS': 'New Password',
                'CON_PASS': 'Confirmation Password'
            },

            'WALLATTACH': {
                'UPLOAD': 'Upload File From PC',
                'TYPE': 'Content Type',
                'DOCUMENT': 'Document',
                'PHOTO': 'Photo',
                'VIDEO': 'Video',
                'AUDIO': 'Audio',
                'CHOOSE': 'Please Choose',
                'MAX': 'File limit is',
                'TAJUK': 'Title',
                'KETERANGAN': 'Description',
                'PERISTIWA': 'Event',
                'TARIKH': 'Date Event',
                'KETERANGANPERISTIWA': 'Event Description',
                'LOKASI': 'Event Venue',
                'KEMBALI': 'Back',
                'SIMPAN': 'Save',
                'TAKS': 'Choose Taxonomy',
                'TAKSNULL': 'Taksonomi is required'
            }
        }
    }
};
