import { Component, ViewEncapsulation, ViewChild } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { ScrollEvent } from 'ngx-scroll-event';
import { FuseProfileTimelineComponent } from './tabs/timeline/timeline.component';
import { User } from '../../model/user';
import { PenggunaService } from '../../services/pengguna/pengguna.service';
import { AuthenticationService } from '../../services/authentication/authentication.service';


@Component({
    selector     : 'fuse-profile',
    templateUrl  : './profile.component.html',
    styleUrls    : ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class FuseProfileComponent
{
    @ViewChild(FuseProfileTimelineComponent) timeLine: FuseProfileTimelineComponent;

    handleScroll(event: ScrollEvent) {

        if (event.isReachingBottom) {
            this.timeLine.getNext();
        }
    
    }

    user: User;
    imageSrc:any;
    name: any;

    constructor( 
        private penggunaService: PenggunaService,
        private authenticationService: AuthenticationService,
    )
    {

    }

    ngOnInit(){
        const userObj = this.authenticationService.getCurrentUser();

        this.penggunaService.getUserById(userObj.id).subscribe(
            user => {
                this.user = user;
                this.imageSrc =  user.image;
                this.name = user.name;
            }
        )

    }
}
