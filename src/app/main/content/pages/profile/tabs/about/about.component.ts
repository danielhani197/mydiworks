import { Component } from '@angular/core';
import { User } from '../../../../model/user';
import { PenggunaService } from '../../../../services/pengguna/pengguna.service';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { Router } from '@angular/router';
import { AhliPetService } from '../../../../services/pet/ahliPet.service';
import { Pet } from '../../../../model/pet/pet';
import { AhliPet } from '../../../../model/pet/ahliPet';
import { PetService } from '../../../../services/pet/pet.service';


@Component({
    selector   : 'fuse-profile-about',
    templateUrl: './about.component.html',
    styleUrls  : ['./about.component.scss'],
    animations : fuseAnimations
})
export class FuseProfileAboutComponent
{
    about: any;
    profileForm: any;
    pengguna: User;
    address: any;
    email: any;
    nama: any;
    phone: any;
    position: any;
    postcode:any ;
    agensi:any;
    bandar: any;
    kementerian: any;
    negeri: any;
    bahagian:any ;
    skema:any ;
    grade: any;
    image: any;
    userEmail: any;

    tempPet: any;
    countPet: any;
    pet: any[];

    ahliPetLs: any[];

    userForm :FormGroup;
    userFormErrors: any;

    constructor(    private penggunaService: PenggunaService,
                    private formBuilder: FormBuilder,
                    private authenticationService: AuthenticationService,
                    private route: Router,
                    private ahlipet: AhliPetService,
                    private _pet:PetService,
        )
    {
     
    } 
    
    ngOnInit(){
        const pengguna = this.authenticationService.getCurrentUser();
        if (pengguna.id){
            this.penggunaService.getUserById(pengguna.id).subscribe(
                pengguna => {
                     this.address = pengguna.address;
                     this.email = pengguna.email;
                     this.nama = pengguna.name;
                     this.phone = pengguna.phoneNo;
                     this.position = pengguna.position;
                     this.postcode = pengguna.postcode;
                     if(pengguna.kementerian){
                     this.kementerian = pengguna.kementerian.name;
                     }
                     if(pengguna.agensi){
                     this.agensi = pengguna.agensi.name;
                     }
                     if(pengguna.bandar){
                     this.bandar = pengguna.bandar.name;
                     }
                     if(pengguna.negeri){
                     this.negeri = pengguna.negeri.name;
                     }
                     if(pengguna.bahagian){
                     this.bahagian = pengguna.bahagian.name;
                     }
                     if(pengguna.skema){
                     this.skema = pengguna.skema.name;
                     }
                     if(pengguna.grade){
                        this.grade = pengguna.grade.name;
                    }
                     this.image = pengguna.image;
                     this.userEmail = pengguna.userEmail;
                     this.ahlipet.getByUserId(pengguna.id).subscribe(
                        ahliPetLs=> {
                            this.ahliPetLs= ahliPetLs;
                            this.pet = this.ahliPetLs;
                            this.countPet = this.ahliPetLs.length;
                        }
                    )
                }
                    
            )
        } 
    }
     
    editForm(){
        this.route.navigate(['/profileEdit/edit']);
    }
}
