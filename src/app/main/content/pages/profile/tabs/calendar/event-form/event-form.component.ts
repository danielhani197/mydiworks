import { Component, Inject, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { DatePipe } from '@angular/common';
import { MatColors } from '@fuse/mat-colors';
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service'
import { User } from '../../../../../model/user';
import { Calendar } from 'app/main/content/model/calendar/calendar';
import { CalendarService } from 'app/main/content/services/calendar/calendar.service';
import { Event } from '../../../../../model/calendar/event';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { PenggunaService } from '../../../../../services/pengguna/pengguna.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector     : 'fuse-calendar-event-form-dialog',
    templateUrl  : './event-form.component.html',
    styleUrls    : ['./event-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseCalendarEventFormDialogComponent implements OnInit
{

    eventFormErrors: any;
    event: Event;
    dialogTitle: string;
    eventForm: FormGroup;
    action: string;
    presetColors = MatColors.presets;
    currentPerson: any[];
    personsToDisplay: any[];
    persons: any[];
    pic: User;
    successTxt: string;
    existTxt: string;
    errPICNULL = false;
    searchTerm: FormControl = new FormControl();

    user: User;
    myKalendar: Calendar[];
    thisKalendar: Calendar;
    jenisKalendar: string;
    namaKalendar: string;
    cantsave = false;
    allDay = false;

    constructor(
        public snackBar: MatSnackBar,
        public dialogRef: MatDialogRef<FuseCalendarEventFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private _calendar: CalendarService,
        private _authentication: AuthenticationService,
        private _user: PenggunaService,
        private datePipe: DatePipe,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _translate: TranslateService,
    )

    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
        this._translate.get('CALENDAR.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('CALENDAR.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });

        this.eventFormErrors = {
            title : {},
            start_datetime: {},
            end_datetime: {},
            starttime: {},
            endtime: {},
            location: {},
            notes: {}
        };

        this.event = data.event;
        this.action = data.action;
        this.user = this._authentication.getCurrentUser();

    }

    onEventFormValuesChange(){

        for ( const field in this.eventFormErrors )
        {
            if ( !this.eventFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.eventFormErrors[field] = {};

            // Get the control
            const control = this.eventForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.eventFormErrors[field] = control.errors;
            }
        }
    }

    allDayfn(){
        this.allDay = this.eventForm.controls['allDay'].value
        if(this.allDay){
            this.eventForm.get('starttime').clearValidators();
            this.eventForm.get('starttime').updateValueAndValidity();
            this.eventForm.get('endtime').clearValidators();
            this.eventForm.get('endtime').updateValueAndValidity();
        }else{
            this.eventForm.get('starttime').setValidators([Validators.required]);
            this.eventForm.get('endtime').setValidators([Validators.required]);
        }
    }

    getCombinedDateTime(date, time){
        var datePart = this.datePipe.transform(date,"yyyy-MM-dd")
        var timePart = " "+time+":00"
        return new Date(datePart + timePart);
    }

    ngOnInit(){

        this.eventForm = this.formBuilder.group({
            title: ["", Validators.required],
            start_datetime: ["", Validators.required],
            end_datetime: ["", Validators.required],
            allDay: [false],
            starttime: ["", Validators.required],
            endtime: ["", Validators.required],
            location: ["", Validators.required],
            notes: ["", Validators.required],
        });

        this.eventForm.valueChanges.subscribe(() => {
            this.onEventFormValuesChange();
        }); 

        if(this.action == 'edit'){
            this.patchFormValues();
        }

        this._user.getAllUser().subscribe(
            data=>{
                this.persons = data;
            }
        )

        this.searchTerm.valueChanges.subscribe(
            term => {
                if (term !=''){
                    this._user.keyUpSearch(term).subscribe(
                      data=> {
                        this.personsToDisplay = data;
                      }
                    )
                }else {
                    this.personsToDisplay = this.persons
                }
            }
        )

    }

    send(){
        
        if(this.pic){
            if(this.action == 'edit'){

                let event : Event = new Event();
                event.setTitle(this.eventForm.controls['title'].value);
                if(this.allDay){
                    event.setStart(this.getCombinedDateTime(this.eventForm.controls['start_datetime'].value, "00:00"))
                    event.setEnd(this.getCombinedDateTime(this.eventForm.controls['end_datetime'].value, "00:00"))
                }else{
                    event.setStart(this.getCombinedDateTime(this.eventForm.controls['start_datetime'].value, this.eventForm.controls['starttime'].value))
                    event.setEnd(this.getCombinedDateTime(this.eventForm.controls['end_datetime'].value, this.eventForm.controls['endtime'].value))
                }
                event.setAllDay(this.allDay)
                event.setLocation(this.eventForm.controls['location'].value);
                event.setNote(this.eventForm.controls['notes'].value);
                event.setUser(this.user);
                event.setId(this.event.id);
                event.setPic(this.pic);
                
                this._calendar.editEvent(event).subscribe(
                    data=>{
                        this._translate.get('CALENDAR.EDIT.SUCCESS').subscribe((res: string) => {
                            this.successTxt = res;
                            this.snackBar.open(this.successTxt, "OK", {
                                panelClass: ['blue-snackbar']
                            });
                            this.dialogRef.close();
                        });
                        
                        
                    },
                    error=>{
                        this._translate.get('CALENDAR.EDIT.FAILED').subscribe((res: string) => {
                            this.existTxt = res;
                            this.snackBar.open(this.existTxt, "OK", {
                                panelClass: ['red-snackbar']
                            });
                        });
                        
                    }
                )
    
            }else{
    
                let event : Event = new Event();
                event.setTitle(this.eventForm.controls['title'].value);
                if(this.allDay){
                    event.setStart(this.getCombinedDateTime(this.eventForm.controls['start_datetime'].value, "00:00"))
                    event.setEnd(this.getCombinedDateTime(this.eventForm.controls['end_datetime'].value, "00:00"))
                }else{
                    event.setStart(this.getCombinedDateTime(this.eventForm.controls['start_datetime'].value, this.eventForm.controls['starttime'].value))
                    event.setEnd(this.getCombinedDateTime(this.eventForm.controls['end_datetime'].value, this.eventForm.controls['endtime'].value))
                }
                event.setAllDay(this.allDay)
                event.setLocation(this.eventForm.controls['location'].value);
                event.setNote(this.eventForm.controls['notes'].value);
                event.setUser(this.user);
                event.setKind("USR");
                event.setPic(this.pic);
                
                this._calendar.createEvent(event).subscribe(
                    data=>{
                        this._translate.get('CALENDAR.NEW.SUCCESS').subscribe((res: string) => {
                            this.successTxt = res;
                            this.snackBar.open(this.successTxt, "OK", {
                                panelClass: ['blue-snackbar']
                            });
                            this.dialogRef.close();
                        });
                        
                        
                    },
                    error=>{
                        this._translate.get('CALENDAR.NEW.FAILED').subscribe((res: string) => {
                            this.existTxt = res;
                            this.snackBar.open(this.existTxt, "OK", {
                                panelClass: ['red-snackbar']
                            });
                        });
                        
                    }
                )
            }
        }else{
            this.errPICNULL = true;
        }
        
    }

    delete(){
        this._calendar.deleteEventById(this.event.id).subscribe(
            success=>{

                this._translate.get('CALENDAR.FORM.DELETE.SUCCESS').subscribe((res: string) => {
                    this.successTxt = res;
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                    });
                    this.dialogRef.close();
                });
                
                
            },
            error=>{
                this._translate.get('CALENDAR.FORM.DELETE.FAILED').subscribe((res: string) => {
                    this.existTxt = res;
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });
                });
                
            }
        )
    }

    patchFormValues(){
        
        if(this.event.kind == "USR"){
            this._translate.get('CALENDAR.PERSONALEVENT').subscribe((res: string) => {
                this.jenisKalendar = res;
            });
            this.namaKalendar = this.event.user.name;
        }else if(this.event.kind == "PET"){
            this._translate.get('CALENDAR.PETEVENT').subscribe((res: string) => {
                this.jenisKalendar = res;
            });
            this.namaKalendar = this.event.pet.nama;
            this.searchTerm.disable();
        }

        this.eventForm.patchValue({
            title: this.event.title,
            start_datetime: new Date(this.event.start),
            end_datetime: new Date(this.event.end),
            kind: this.event.kind,
            allDay: this.event.allDay,
            location: this.event.location,
            notes: this.event.notes,
        })


        if(!this.event.allDay){
            var start = this.datePipe.transform(this.event.start,"HH:mm")
            var end = this.datePipe.transform(this.event.end,"HH:mm")
            this.eventForm.patchValue({
                starttime: start,
                endtime: end
            })
        }

        this.searchTerm.patchValue(this.event.pic.name)
        this.pic = this.event.pic;

        if((this.event.user.id != this.user.id)&&(this.event.kind != "USR")){
            this.eventForm.disable();
            this.cantsave = true;
        }

        this.allDayfn();
    }

    setPic(id: any) {
        this._user.getUserById(id).subscribe(
            data=>{
                this.pic = data;
                this.errPICNULL = false;
            }
        )
    }
}
