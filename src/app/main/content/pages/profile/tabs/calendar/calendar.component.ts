import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Subject } from 'rxjs';
import { startOfDay, isSameDay, isSameMonth, isThursday } from 'date-fns';
import { CalendarEventAction, CalendarEventTimesChangedEvent, CalendarMonthViewDay } from 'angular-calendar';
import { ToastrService } from 'ngx-toastr';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { locale as english } from './i18n/my';
import { locale as malay } from './i18n/my';
import { Calendar } from 'app/main/content/model/calendar/calendar';
import { CalendarService} from 'app/main/content/services/calendar/calendar.service';
import { CalendarEvent } from 'app/main/content/model/calendar/calendar-utils'
import { Event } from 'app/main/content/model/calendar/event';
import { FuseCalendarEventFormDialogComponent } from './event-form/event-form.component';
import { User } from '../../../../model/user';
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service'
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector     : 'fuse-calendar',
    templateUrl  : './calendar.component.html',
    styleUrls    : ['./calendar.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class FuseCalendarComponent implements OnInit
{
    view: string;
    successTxt: string;
    existTxt: string;
    myKalendarId: string;
    activeDayIsOpen: boolean;
    dialogRef: any;
    selectedDay: any;
    refresh: Subject<any> = new Subject();
    viewDate: Date;
    events: CalendarEvent[];
    event: CalendarEvent;
    user: User;
    myKalendar: Calendar[];
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    public actions: CalendarEventAction[];

    constructor(
        public dialog: MatDialog,
        public calendarService: CalendarService,
        private _authentication: AuthenticationService,
        private _toastr: ToastrService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _translate: TranslateService,
    )
    {
        this.user = this._authentication.getCurrentUser();
        this.view = 'month';
        this.viewDate = new Date();
        this.activeDayIsOpen = true;
        this.selectedDay = {date: startOfDay(new Date())};

        this.fuseTranslationLoader.loadTranslations(malay, english);
        this._translate.get('CALENDAR.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('CALENDAR.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
    }

    ngOnInit()
    {
        this.setEvents();
    }

    setEvents()
    {
        this.calendarService.getUserEvents().subscribe(
            data=>{
                this.calendarService.update();
                this.events = data
            }
        )
    }

    /**
     * Before View Renderer
     * @param {any} header
     * @param {any} body
     */
    beforeMonthViewRender({header, body})
    {
        
        /**
         * Get the selected day
         */
        const _selectedDay = body.find((_day) => {
            return _day.date.getTime() === this.selectedDay.date.getTime();
        });

        if ( _selectedDay )
        {
            /**
             * Set selectedday style
             * @type {string}
             */
            _selectedDay.cssClass = 'mat-elevation-z3';
        }

    }

    /**
     * Day clicked
     * @param {MonthViewDay} day
     */
    dayClicked(day: CalendarMonthViewDay): void
    {
        const date: Date = day.date;
        // to review
        const events: CalendarEvent[] = day.events;

        if ( isSameMonth(date, this.viewDate) )
        {
            if ( (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0 )
            {
                this.activeDayIsOpen = false;
            }
            else
            {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
        this.selectedDay = day;
        this.refresh.next();
    }

    /**
     * Event times changed
     * Event dropped or resized
     * @param {CalendarEvent} event
     * @param {Date} newStart
     * @param {Date} newEnd
     */
    eventTimesChanged({event, newStart, newEnd}: CalendarEventTimesChangedEvent): void
    {
        event.start = newStart;
        event.end = newEnd;
        
        this.refresh.next(true);
    }

    

    /**
     * Edit Event
     * @param {string} action
     * @param {CalendarEvent} event
     */
    editEvent(event: Event)
    {
        
        this.dialogRef = this.dialog.open(FuseCalendarEventFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data      : {
                event : event,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed().subscribe(
            _=>{
                this.setEvents();
            }
        )
    }


    addEvent(): void
    {
        this.dialogRef = this.dialog.open(FuseCalendarEventFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data      : {
                action: 'new'
            }
        });
        this.dialogRef.afterClosed().subscribe(
            _=>{
                this.setEvents();
            }
        )
    }
}


