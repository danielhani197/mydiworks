export const locale = {
    lang: 'my',
    data: {
        'CALENDAR': {
            'PERSONALEVENT': 'Aktiviti Peribadi',
            'PETEVENT': 'Aktiviti Pet',
            'NEW': {
                'TITLE': 'Tambah Aktiviti',
                'SUCCESS': 'Aktiviti telah berjaya disimpan',
                'FAILED': 'Aktiviti tidak berjaya disimpan'
            },
            'EDIT': {
                'TITLE': 'Lihat/Kemaskini Aktiviti',
                'SUCCESS': 'Aktiviti telah berjaya dikemaskini',
                'FAILED': 'Aktiviti tidak berjaya dikemaskini'
            },
            'FORM': {
                'TITLE': {
                    'TITLE': 'Nama Aktiviti',
                    'REQUIRED': 'Nama aktiviti wajib diisi'
                },
                'ALLDAY': {
                    'ALLDAY': 'Sepanjang Hari'
                },
                'STARTDATE': {
                    'STARTDATE': 'Tarikh Mula',
                    'REQUIRED': 'Tarikh mula wajib diisi'
                },
                'ENDDATE': {
                    'ENDDATE': 'Tarikh Tamat',
                    'REQUIRED': 'Tarikh tamat wajib diisi'
                },
                'STARTTIME': {
                    'STARTTIME': 'Masa Mula',
                    'REQUIRED': 'Masa mula wajib diisi'
                },
                'ENDTIME': {
                    'ENDTIME': 'Masa Tamat',
                    'REQUIRED': 'Masa tamat wajib diisi'
                },
                'LOCATION': {
                    'LOCATION': 'Tempat Aktiviti',
                    'REQUIRED': 'Tempat aktiviti wajib diisi'
                },
                'NOTES': {
                    'NOTES': 'Nota',
                    'REQUIRED': 'Nota wajib diisi'
                },
                'PIC': {
                    'PIC': 'Orang Yang Bertanggungjawab',
                    'ERRPICNULL': 'Sila pilih orang yang bertanggungjawab'
                },
                'SAVE': 'Hantar',
                'DELETE': {
                    'DELETE': 'Padam Aktiviti',
                    'CONFIRMATION': 'Adakah anda pasti untuk padam aktiviti ini',
                    'BTNDELETE': 'Padam',
                    'BTNCANCEL': 'Kembali',
                    'SUCCESS': 'Aktiviti telah berjaya dipadam',
                    'FAILED': 'Aktiviti tidak dapat dipadam',
                }
            }
        }
    }
};
