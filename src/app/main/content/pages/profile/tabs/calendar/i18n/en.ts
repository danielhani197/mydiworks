export const locale = {
    lang: 'en',
    data: {
        'CALENDAR': {
            'PERSONALEVENT': 'Personal Activity',
            'PETEVENT': 'Pet Activity',
            'NEW': {
                'TITLE': 'Add Activity',
                'SUCCESS': 'Activity has been successfully created',
                'FAILED': 'Activity has been unsuccessfully created'
            },
            'EDIT': {
                'TITLE': 'View/Edit Activity',
                'SUCCESS': 'Activity has been successfully edited',
                'FAILED': 'Activity has been unsuccessfully edited'
            },
            'FORM': {
                'TITLE': {
                    'TITLE': 'Activity Name',
                    'REQUIRED': 'Activity name is required'
                },
                'ALLDAY': {
                    'ALLDAY': 'All Day'
                },
                'STARTDATE': {
                    'STARTDATE': 'Start Date',
                    'REQUIRED': 'Start date is required'
                },
                'ENDDATE': {
                    'ENDDATE': 'End Date',
                    'REQUIRED': 'End date is required'
                },
                'STARTTIME': {
                    'STARTTIME': 'Start time',
                    'REQUIRED': 'Start time is required'
                },
                'ENDTIME': {
                    'ENDTIME': 'End Time',
                    'REQUIRED': 'End time is required'
                },
                'LOCATION': {
                    'LOCATION': 'Activity Location',
                    'REQUIRED': 'Activity location is required'
                },
                'NOTES': {
                    'NOTES': 'Notes',
                    'REQUIRED': 'Notes is required'
                },
                'PIC': {
                    'PIC': 'Person in Charge',
                    'ERRPICNULL': 'Please select person in charge'
                },
                'SAVE': 'Submit',
                'DELETE': {
                    'DELETE': 'Delete Activity',
                    'CONFIRMATION': 'Are you sure want to delete this activity',
                    'BTNDELETE': 'Delete',
                    'BTNCANCEL': 'Back',
                    'SUCCESS': 'Activity has been successfully deleted',
                    'FAILED': 'Activity has been unsuccessfully deleted',
                }
            }
        }
    }
};
