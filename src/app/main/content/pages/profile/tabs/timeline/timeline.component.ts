import { Component, OnInit, ViewChild, HostListener} from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';

import { ScrollEvent } from 'ngx-scroll-event';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { MyWallService } from 'app/main/content/services/wall3/myWall.service';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { User } from '../../../../model/user';
import { WallKandungan } from '../../../../model/wall3/wallKandungan';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Hashtag } from '../../../../model/wall3/hashtag';
import { WallKomen } from '../../../../model/wall3/wallKomen';
import { DeleteKomenDialog } from './deleteKomenDialog.component';
import { EditDialogComponent } from './editDialog.component';
import { FileStorageService } from 'app/main/content/services/wall3/filestorage.service'
import { WallAttachment } from '../../../../model/wall3/wallAttachment';
import { DataService } from 'app/main/content/services/wall3/data.service';
import { UploadFileDialogComponent } from './uploadFileDialog.component';
import { MyBoxUploadDialogComponent } from './myBoxUploadDialog.component';

import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';
import { PostCollection } from '../../../../model/wall3/postCollection';
import { AdvancedPage } from '../../../../model/util/advanced-page';

import { TermService } from 'app/main/content/services/setting/term.service'
import { TermViewLoginComponent } from 'app/main/content/common/term-view-login.component';

//kalendar
import { CalendarService } from 'app/main/content/services/calendar/calendar.service';
import { Event } from 'app/main/content/model/calendar/event';

//pengumuman
import { PengumumanService } from 'app/main/content/services/pengumuman/pengumuman.service';
import { Pengumuman } from 'app/main/content/model/pengumuman/pengumuman';
import { CalendarEvent } from 'calendar-utils';

@Component({
    selector   : 'fuse-profile-timeline',
    templateUrl: './timeline.component.html',
    styleUrls  : ['./timeline.component.scss'],
    animations : fuseAnimations
})
export class FuseProfileTimelineComponent
{
    @ViewChild('private') pvt;

    page: AdvancedPage = new AdvancedPage;

    timeline: PostCollection[]= new Array<PostCollection>();
    user: User;
    hashtagList: Hashtag[] = new Array<Hashtag>();
    tagList: string[] = new Array<string>();
    file2: any;
    
    successTxt: string;
    existTxt: string;

    show: boolean = false;
    postForm: FormGroup;
    postFormErrors: any;
    length: any;

    latersDate:Date;

    showLoadingBar: boolean;
    
    //kalendar
    kalendarEvents: Event[] = new Array<Event>();
    //pengumuman
    pengumumanLs : Pengumuman[] = new Array();
    pengumumanForm: FormGroup;
    wa : boolean = false;
    namaFail: string;

    constructor(
        private myWallService: MyWallService,
        private _auth: AuthenticationService,
        private formBuilder: FormBuilder,
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _filestorage: FileStorageService,
        private _dataservice: DataService,
        public snackBar: MatSnackBar,
        public _dialog: MatDialog,
        private router: Router,
        public calendarService: CalendarService,
        private _term: TermService,
        public pengumumanService: PengumumanService

    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
        this._translate.get('WALL.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('WALL.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });

        this.postFormErrors = {
            post : {}
        };

        this.user = this._auth.getCurrentUser();
        this.page.search = this.user.id;
        this.page.pageNumber = 0;
        this.page.size = 5;
        
        router.events.subscribe(
            (event) => {
                if ( event instanceof NavigationStart )
                {
                    this.showLoadingBar = true;
                }
                if ( event instanceof NavigationEnd )
                {
                    this.showLoadingBar = false;
                }
        });

        this.calendarService.personalListen().subscribe(
            (_)=>{
                this.updateCalendar();
            }
        )

        this._dataservice.listen().subscribe(
            (e:any)=>{                
                setTimeout(()=>{ 
                    let uploadedFile: WallAttachment = this._dataservice.getInfo();
                    if(uploadedFile!=null){
                        this.wa = true;
                        this.namaFail = uploadedFile.filename;
                    }    
                }, 1000)                           
            }
        )                
    }

    updateCalendar(){
        this.calendarService.getCurrentWeekEvents(this.user.id).subscribe(
            list => {
                this.kalendarEvents = list;
            }
        )
    }

    ngOnInit(){

        this.termPopup();
        
        this.postForm = this.formBuilder.group({
            post: ['', [Validators.required]],
            tagInput: ['']
        })
        this.getLatestTimeline();
        this.updateCalendar();
        //calendar
        // this.user = this._auth.getCurrentUser();
        
        //load pengumuman
        this.pengumumanService.getAllPengumuman().subscribe(
            pengumuman=>{
                this.pengumumanLs = pengumuman;

            }

        )

    }

    toggleComment(id: string){
        var documentid = 'komen-'+id
        var icon1id = 'icon-arrow1-'+id
        var icon2id = 'icon-arrow2-'+id
        var previewid = 'preview-'+id

        var x = document.getElementById(documentid);
        var y = document.getElementById(icon1id);
        var z = document.getElementById(icon2id);
        var w = document.getElementById(previewid);

        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }

        if (y.style.display === "none") {
            y.style.display = "block";
        } else {
            y.style.display = "none";
        }

        if (z.style.display === "none") {
            z.style.display = "block";
        } else {
            z.style.display = "none";
        }

        w.style.display = 'none'

    }
    

    

    addTag(event: any){
        const input = event.input;
        const value = event.value;

        // Add tag
        if ( value )
        {
            this.tagList.push(value);
        }

        // Reset the input value
        if ( input )
        {
            input.value = '';
        }
    }

    removeTag(tag)
    {
        const index = this.tagList.indexOf(tag);
        if ( index >= 0 )
        {
            this.tagList.splice(index, 1);
        }
    }

    onCommentEvent(post: any){
        if(this.user){
            let documentId = post.wallKandungan.id;
            var comment = (<HTMLInputElement>document.getElementById(documentId)).value;
            if(comment){
                let wallKomen: WallKomen = new WallKomen();
                wallKomen.setCreateddate(new Date);
                wallKomen.setKeterangan(comment);
                wallKomen.setCreatedby(this.user);
                wallKomen.setId(documentId)
                this.myWallService.createKomen(wallKomen).subscribe(
                    success=>{
                        this.getLatestTimeline();
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                          });
                        
                          
                    },
                    error=>{
                        
                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['red-snackbar']
                          });
                    }
                )
            }
        }
    }

    


    onPostEvent(){
        if(this.user){

            if(this.pvt.checked){

                for(let obj of this.tagList){
                    let hashtag: Hashtag = new Hashtag();
                    hashtag.setName(obj);
                    this.hashtagList.push(hashtag);
                }
                let wallKandungan: WallKandungan = new WallKandungan();
                wallKandungan.setKeterangan(this.postForm.controls['post'].value);
                wallKandungan.setCreateddate(new Date);
                wallKandungan.setCreatedby(this.user);
                wallKandungan.setCategory("PRIVATE");
                wallKandungan.setHashtagLs(this.hashtagList);
                wallKandungan.setUserid(this.user.id);
                this.myWallService.createPost(wallKandungan).subscribe(
                    wallKandungan=>{

                        let input = new FormData();               
                        var mode = localStorage.getItem("MODE-UPLOAD");

                        if(mode == "PC"){
                            let wa: WallAttachment = this._dataservice.getInfo();
                        
                            if( wa != null){
                                wa.wallKandungan = wallKandungan;
                            
                                input.append('fileUpload', this._dataservice.getData());
                                input.append('info', new Blob([JSON.stringify(wa)],
                                    {
                                        type: "application/json"
                                    }));   
        
                                const formModel = input;
                                this._filestorage.saveFile(formModel).subscribe(
                                    success=>{
                                        localStorage.removeItem('info');
                                        this.getLatestTimeline();
                                        this._dataservice.clearInfo()
                                    }
                                );
                            }
                        }else if (mode == "MYBOX"){
                            let wa: WallAttachment = this._dataservice.getInfo();
                            if( wa != null){
                                wa.wallKandungan = wallKandungan;
                            
                                input.append('info', new Blob([JSON.stringify(wa)],
                                    {
                                        type: "application/json"
                                    }));      
                                const formModel = input;
                                this._filestorage.saveMyBoxFile(formModel).subscribe(
                                    success=>{
                                        localStorage.removeItem('info');
                                        this.getLatestTimeline();
                                        this._dataservice.clearInfo()
                                    }
                                );
                            }
                        }
                        
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                        });
                        this.getLatestTimeline();
                        this.postForm.reset();
                        this.tagList = [];
                        this.hashtagList = [];
                        this.pvt.checked = false;
                    },
                    error=>{
                        
                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['red-snackbar']
                        });
                    }
                )

            }else{

                for(let obj of this.tagList){
                    let hashtag: Hashtag = new Hashtag();
                    hashtag.setName(obj);
                    this.hashtagList.push(hashtag);
                }
                let wallKandungan: WallKandungan = new WallKandungan();
                wallKandungan.setKeterangan(this.postForm.controls['post'].value);
                wallKandungan.setCreateddate(new Date);
                wallKandungan.setCreatedby(this.user);
                wallKandungan.setCategory("BAHAGIAN");
                wallKandungan.setHashtagLs(this.hashtagList);
                wallKandungan.setBahagianid(this.user.bahagian.id);
                this.myWallService.createPost(wallKandungan).subscribe(
                    wallKandungan=>{

                        let input = new FormData();               
                        var mode = localStorage.getItem("MODE-UPLOAD");

                        if(mode == "PC"){
                            let wa: WallAttachment = this._dataservice.getInfo();
                            //JSON.parse(localStorage.getItem('info'));
                        
                            if( wa != null){
                                wa.wallKandungan = wallKandungan;
                            
                                input.append('fileUpload', this._dataservice.getData());
                                input.append('info', new Blob([JSON.stringify(wa)],
                                    {
                                        type: "application/json"
                                    }));   
        
                                const formModel = input;
                                this._filestorage.saveFile(formModel).subscribe(
                                    success=>{
                                        localStorage.removeItem('info');
                                        this.getLatestTimeline();
                                        this._dataservice.clearInfo()
                                    }
                                );
                            }
                        }else if (mode == "MYBOX"){
                            let wa: WallAttachment = this._dataservice.getInfo();
                            if( wa != null){
                                wa.wallKandungan = wallKandungan;
                            
                                input.append('info', new Blob([JSON.stringify(wa)],
                                    {
                                        type: "application/json"
                                    }));      
                                const formModel = input;
                                this._filestorage.saveMyBoxFile(formModel).subscribe(
                                    success=>{
                                        localStorage.removeItem('info');
                                        this.getLatestTimeline();
                                        this._dataservice.clearInfo()
                                    }
                                );
                            }
                        }
                         
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                        });
                        this.getLatestTimeline();
                        this.wa = false;
                        this.postForm.reset();
                        this.tagList = [];
                        this.hashtagList = [];                                               
                    },
                    error=>{
                        
                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['red-snackbar']
                        });
                    }
                )
            }
        }
    }

    deletePost(id: string){
        let dialogRef = this._dialog.open(DeleteKomenDialog, {
            width: '500px',
            data: { 
                id: id,
                type: "post"
            }
        });
        dialogRef.afterClosed().subscribe(
            data=>{
                this.getLatestTimeline();
            }
        )
    }

    editPost(id: string){
        let dialogRef = this._dialog.open(EditDialogComponent, {
            width: '750px',
            height: '400px',
            data: { 
                id: id,
                type: "post"
            }
        });
        dialogRef.afterClosed().subscribe(
            data=>{
                this.getLatestTimeline();
            }
        )
    }

    deleteComment(id: string){
        let dialogRef = this._dialog.open(DeleteKomenDialog, {
            width: '500px',
            data: { 
                id: id,
                type: "komen"
            }
        });
        dialogRef.afterClosed().subscribe(
            data=>{
                this.getLatestTimeline();
            }
        )
    }

    editComment(id: string){
        let dialogRef = this._dialog.open(EditDialogComponent, {
            width: '750px',
            height: '350px',
            data: { 
                id: id,
                type: "komen"
            }
        });
        dialogRef.afterClosed().subscribe(
            data=>{
                this.getLatestTimeline();
            }
        )
    }

    getLatestTimeline(){
        
        this.myWallService.getNewPostAll(this.page).subscribe(
            data=>{
                this.timeline = data.data;
                this.length = data.advancedPage.totalElements
            }
        );
    }

    getNext(){
        if(this.length > this.timeline.length){

            this.page.size += 5
            this.myWallService.getNewPostAll(this.page).subscribe(
                data=>{
                    this.timeline = data.data;
                }
            );
        }
        
    }
    
    formUpload(){
        localStorage.setItem("MODE-UPLOAD","PC")
        let dialogRef = this._dialog.open(UploadFileDialogComponent,{
          width: '670px',
          height: '500px'
        });
    }

    myBoxUpload(){
        localStorage.setItem("MODE-UPLOAD","MYBOX")
        let dialogRef = this._dialog.open(MyBoxUploadDialogComponent,{
          width: '900px',
          height: '600px'
        });
    }

    downloadFile(id){

        this._filestorage.getFilesById(id).subscribe(

            data => {
                this.file2 = data;
               
                var dataFile = this.base64ToArrayBuffer(this.file2.content);
                var blob = new Blob([dataFile], {type: data.formatType});
                var url= window.URL.createObjectURL(blob);
                window.open(url);
            }
        );
    }

    streamFile(attachment){

        let a = document.createElement("a");
        document.body.appendChild(a);
        //a.style = "display: none"; 
        var dataFile = this.base64ToArrayBuffer(attachment.content);
        var blob = new Blob([dataFile], {type: attachment.formatType});
        var url= window.URL.createObjectURL(blob);
        a.href = url;
        a.download = attachment.filename;
        a.click();
        window.URL.revokeObjectURL(url);
    }

    base64ToArrayBuffer(base64) {
        var binaryString = window.atob(base64);
        var binaryLen = binaryString.length;
        var bytes = new Uint8Array(binaryLen);
        for (var i = 0; i < binaryLen; i++) {
           var ascii = binaryString.charCodeAt(i);
           bytes[i] = ascii;
        }
        return bytes;
    }

    gotoKalendar() {
        this.router.navigate(['kalendar/']);
    }

    termPopup(){
        let currentUser = this._auth.getCurrentUser();
        var cuTermDate = currentUser.termdate;

        this._term.latersTerm().subscribe(
            data => {
                var convertDate = data.date;
                this.latersDate = new Date(Date.parse(convertDate));

                if(cuTermDate){
                    if(this.latersDate > cuTermDate){
                        setTimeout(
                            () => this._dialog.open(TermViewLoginComponent, {
                                width: '900px',
                            })
                        ) 
                    }
                }else{
                    setTimeout(
                        () => this._dialog.open(TermViewLoginComponent, {
                            width: '900px',
                        })
                    )
                }
            }
        )
    }
}
