import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Router } from "@angular/router";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { MyWallService } from '../../../../services/wall3/myWall.service';

import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';


@Component({
    selector: 'delete-komen-dialog',
    templateUrl: 'deleteKomenDialog.component.html',
    animations : fuseAnimations
    })
    export class DeleteKomenDialog implements OnInit
    {
        _id: string;
        _type: string;

        successTxt: string;
        existTxt: string;

        constructor(
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _myWall: MyWallService,
        private _router: Router,
        public snackBar: MatSnackBar,
        public _dialogRef: MatDialogRef<DeleteKomenDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any
        ) { 
            this.fuseTranslationLoader.loadTranslations(malay, english);
            if(data.type=='komen'){
                this._translate.get('WALL.DELETE.KOMEN.ERROR').subscribe((res: string) => {
                    this.existTxt = res;
                });
                this._translate.get('WALL.DELETE.KOMEN.SUCCESS').subscribe((res: string) => {
                    this.successTxt = res;
                });
            }else{
                this._translate.get('WALL.DELETE.POST.ERROR').subscribe((res: string) => {
                    this.existTxt = res;
                });
                this._translate.get('WALL.DELETE.POST.SUCCESS').subscribe((res: string) => {
                    this.successTxt = res;
                });
            }
            
            
        }
        

        ngOnInit(){
            this._id = this.data.id;
            this._type = this.data.type;
        }
        confirmDelete(): void {
            if(this._type == "komen"){
                this._myWall.deleteKomenById(this._id).subscribe(
                    success=>{
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                        });
                        
                        this._dialogRef.close();
                        
                    },
                    error=>{
                        
                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['red-snackbar']
                        });
                        this._dialogRef.close();
                    }
                )
            }else{
                this._myWall.deletePostById(this._id).subscribe(
                    success=>{
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                        });
                        
                        this._dialogRef.close();
                        
                    },
                    error=>{
                        
                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['red-snackbar']
                        });
                        this._dialogRef.close();
                    }
                )
            }
        }

    }