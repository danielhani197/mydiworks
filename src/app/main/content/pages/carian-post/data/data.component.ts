import { Component } from '@angular/core';
import { PageEvent } from '@angular/material';
import { SearchService } from 'app/main/content/services/search/search.service';
import { Page } from 'app/main/content/model/util/page';
import { Hashtag } from 'app/main/content/model/wall3/hashtag';
import { AdvancedPage } from '../../../model/util/advanced-page';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { BahagianService } from '../../../services/agency/bahagian.service';
import { Bahagian } from 'app/main/content/model/general/bahagian';
import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { User } from '../../../model/user';

@Component({
    selector   : 'carian-post-data',
    templateUrl: './data.component.html',
    styleUrls  : ['./data.component.scss']
})
export class CarianPostDataComponent
{
    page = new AdvancedPage();
    length = 100;
    pageSize = 5;
    pageSizeOptions: number[] = [5, 10, 15, 20];
    pageEvent: PageEvent;
    classic: any;
    post: Hashtag;
    user: User;
    bahagianId: string;

    constructor(
        private searchService: SearchService,
        private _auth: AuthenticationService,
        private _bahagian: BahagianService,
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
        this.user = this._auth.getCurrentUser();
        
        if (this.user.bahagian == null){
            this.page.user = this.user;
            this.page.bahagian = null;
            this.page.pageNumber = 0;
            this.page.size = 5;
    
            this.searchService.getPost(this.page).subscribe(
                classic => {
                    
                    this.post = classic.data;
                    this.length = classic.advancedPage.totalElements;
                }
            );
        }else{
            this._bahagian.getBahagianById(this.user.bahagian.id).subscribe(
                data=>{
                    this.page.user = this.user;
                    this.page.bahagian = data;
                    this.page.pageNumber = 0;
                    this.page.size = 5;
            
                    this.searchService.getPost(this.page).subscribe(
                        classic => {
                            
                            this.post = classic.data;
                            this.length = classic.advancedPage.totalElements;
                        }
                    );
                }
            )
        }
        
        

        
        
    }

    updateFilter(event: any) {
        const val = event.target.value.toLowerCase();
        this.page.search = val
    
        this.searchService.getPost(this.page).subscribe(
            data => {
                this.post = data.data;
                this.length = data.advancedPage.totalElements;
            }
        );
    }

    onPaginateChange(event: any){
        this.page.size = event.pageSize;
        this.page.pageNumber = event.pageIndex;

        this.searchService.getPost(this.page).subscribe(
            data => {
                this.post = data.data;
                this.length = data.advancedPage.totalElements;
            }
        )
        
    }

    deletePost(id: string){

    }
}
