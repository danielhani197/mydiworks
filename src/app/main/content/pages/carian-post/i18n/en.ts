export const locale = {
    lang: 'en',
    data: {
        'CARIAN': {
            'SEARCH': 'Search',
            'KEMENTERIAN': 'Ministry',
            'AGENSI': 'Agency',
            'BAHAGIAN': 'Division',
            'ITEMSPERPAGE': 'Items per page',
            'OF': 'of',
            'TIADA': 'none',
            'RESULT': 'Results'
        }
    }
};
