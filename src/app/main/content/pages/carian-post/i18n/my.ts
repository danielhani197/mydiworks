export const locale = {
    lang: 'my',
    data: {
        'CARIAN': {
            'SEARCH': 'Carian',
            'KEMENTERIAN': 'Kementerian',
            'AGENSI': 'Agensi',
            'BAHAGIAN': 'Bahagian',
            'ITEMSPERPAGE': 'Item per halaman',
            'OF': 'daripada',
            'TIADA': 'tiada',
            'RESULT': 'Hasil Carian'
        }
    }
};
