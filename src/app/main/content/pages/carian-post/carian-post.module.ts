import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { 
    MatButtonModule, 
    MatFormFieldModule, 
    MatIconModule, 
    MatInputModule, 
    MatTableModule, 
    MatTabsModule,
    MatPaginatorModule, 
    MatChipsModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { SearchService } from 'app/main/content/services/search/search.service';
import { CarianPostComponent } from './carian-post.component';
import { CarianPostDataComponent } from './data/data.component';
import { TranslateModule } from '@ngx-translate/core';

const routes = [
    {
        path     : 'post',
        component: CarianPostComponent
    }
];

@NgModule({
    declarations: [
        CarianPostComponent,
        CarianPostDataComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,

        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatTableModule,
        MatTabsModule,
        MatPaginatorModule,
        MatChipsModule,

        FuseSharedModule
    ],
    providers   : [
        SearchService
    ]
})
export class CarianPostModule
{
}
