import { Component, ViewChild } from '@angular/core';
import { CarianPostDataComponent } from './data/data.component';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from './i18n/en';
import { locale as malay } from './i18n/my';

@Component({
    selector   : 'carian-post',
    templateUrl: './carian-post.component.html',
    styleUrls  : ['./carian-post.component.scss']
})
export class CarianPostComponent
{
    @ViewChild(CarianPostDataComponent) _search:CarianPostDataComponent;
    constructor(
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
    }

    updateFilter(event: any){
        this._search.updateFilter(event);
    }
}
