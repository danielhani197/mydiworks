import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

import { Kementerian } from 'app/main/content/model/general/kementerian';
import { KementerianService } from 'app/main/content/services/agency/kementerian.service';
import { Bandar } from '../../../../model/ref/bandar';
import { Negeri } from '../../../../model/ref/negeri';
import { BandarNegeriService } from '../../../../services/references/bandarNegeri.service';

import { locale as english } from './i18n/en';
import { locale as malay } from './i18n/my';
import { MatSnackBar } from '@angular/material';

@Component({
    selector   : 'tetapan-kementerian',
    templateUrl: './tetapan-kementerian.component.html',
    styleUrls  : ['./tetapan-kementerian.component.scss'],
    animations : fuseAnimations
})
export class TetapanKementerianComponent implements OnInit
{   
    _id: string;
    kementerianForm: FormGroup;
    kementerianFormErrors: any;

    successTxt: string;
    existTxt: string;

    stateLs : any[];
    currentState: any;

    cities : any[];
    currentCity: any;
    city: Bandar;
    state: Negeri;

    errNameExist = false;
    errCodeExist = false;

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _kementerian: KementerianService,
        private _router: Router,
        private _toastr: ToastrService,
        private _translate: TranslateService,
        private _bandar: BandarNegeriService,
        public snackBar: MatSnackBar
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);

        // Reactive form errors
        this.kementerianFormErrors = {
            name : {},
            code  : {},
            phoneNo : {},
            address : {},
            postcode : {},
            city : {},
            state : {},
            urlPortal : {}
        };

        this._translate.get('KEMENTERIAN.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('KEMENTERIAN.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });

    }

    ngOnInit()
    {
        this.kementerianForm = this.formBuilder.group({
            name    : ['', [Validators.required]],
            code    : ['', Validators.required],
            phoneNo    : ['', [Validators.required, Validators.pattern("[^a-zA-Z]{7,16}")]],
            address    : ['', [Validators.required]],
            postcode    : ['', [Validators.required, Validators.pattern("[0-9]{5}")]],
            city    : ['', [Validators.required]],
            state    : ['', [Validators.required]],
            urlPortal    : ['', [Validators.required, Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')]]
        });

        this.kementerianForm.valueChanges.subscribe(() => {
            this.onKementerianFormValuesChanged();
        });

        this._bandar.getState().subscribe(
            data =>{
                this.stateLs = data;
                this._id = this.route.snapshot.paramMap.get('id');
                if(this._id){

                    this._kementerian.getKementerianById(this._id).subscribe(
                        kementerian=>{
                            var state =  kementerian.state;
                            var city =  kementerian.city;
                            var stateid = "";
                            var cityid = "";

                            if(state){
                                stateid = state.id;
                                this._bandar.getCity(stateid).subscribe(
                                    data => {
                                        this.cities = data;
                                        if(city){
                                            cityid = city.id;
                                            if(stateid){
                                                this.currentState = this.stateLs.filter(value => parseInt(value.id) === parseInt(stateid));
                                                this.state = this.currentState[0];
                                                if(cityid){
                                                    this.currentCity = this.cities.filter(value => parseInt(value.id) === parseInt(cityid));
                                                    this.city = this.currentCity[0];
                                                    
                                                    this.kementerianForm.setValue({
                                                        name: kementerian.name,
                                                        code: kementerian.code,
                                                        phoneNo: kementerian.phoneNo,
                                                        address: kementerian.address,
                                                        postcode: kementerian.postcode,
                                                        city: kementerian.city.id,
                                                        state: kementerian.state.id,
                                                        urlPortal: kementerian.urlPortal
                                                    });
                                                }
                                            }
                                        }
                                    }
                                );
                            }                            
                        }
                    )
                    
                }
            }
        );


    }

    hantar(){

        if(this._id){

            let kementerian: Kementerian = new Kementerian ();
            kementerian.setName(this.kementerianForm.controls['name'].value),
            kementerian.setCode(this.kementerianForm.controls['code'].value),
            kementerian.setPhoneNo(this.kementerianForm.controls['phoneNo'].value),
            kementerian.setAddress(this.kementerianForm.controls['address'].value),
            kementerian.setPostcode(Number(this.kementerianForm.controls['postcode'].value)),
            kementerian.setCity(this.city),
            kementerian.setState(this.state),
            kementerian.setUrlPortal(this.kementerianForm.controls['urlPortal'].value),
            kementerian.setId(this._id)

            this._kementerian.updateKementerian(kementerian).subscribe(
                success=>{
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                    });
                    this._router.navigate(['/kementerian/list']);
                },
                error=>{
                    
                    var messages = error.error.errorMessage;

                        
                    if(messages == "ERR_EXIST_AGENCY_NAME"){

                        this.errNameExist = true;

                    }else if(messages == "ERR_EXIST_AGENCY_CODE"){

                        this.errCodeExist = true;

                    }else if(messages == "ERR_EXIST_AGENCY_BOTH"){

                        this.errNameExist = true;
                        this.errCodeExist = true;

                    }
                    
                    
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });

                }
            )

        }else{
        
            let kementerian: Kementerian = new Kementerian ();
            kementerian.setName(this.kementerianForm.controls['name'].value),
            kementerian.setCode(this.kementerianForm.controls['code'].value),
            kementerian.setPhoneNo(this.kementerianForm.controls['phoneNo'].value),
            kementerian.setAddress(this.kementerianForm.controls['address'].value),
            kementerian.setPostcode(Number(this.kementerianForm.controls['postcode'].value)),
            kementerian.setCity(this.city),
            kementerian.setState(this.state),
            kementerian.setUrlPortal(this.kementerianForm.controls['urlPortal'].value)

            this._kementerian.createKementerian(kementerian).subscribe(
                success=>{
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                      });
                      this._router.navigate(['/kementerian/list']);
                },
                error=>{

                    var messages = error.error.errorMessage;

                        
                    if(messages == "ERR_EXIST_AGENCY_NAME"){

                        this.errNameExist = true;

                    }else if(messages == "ERR_EXIST_AGENCY_CODE"){

                        this.errCodeExist = true;

                    }else if(messages == "ERR_EXIST_AGENCY_BOTH"){

                        this.errNameExist = true;
                        this.errCodeExist = true;

                    }
                    
                    
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });
                }
            )
        }
    }

    onKementerianFormValuesChanged(){
        this.errNameExist = false;
        this.errCodeExist = false;
        for ( const field in this.kementerianFormErrors )
        {
            if ( !this.kementerianFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.kementerianFormErrors[field] = {};

            // Get the control
            const control = this.kementerianForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.kementerianFormErrors[field] = control.errors;
            }
        }
    }

    setSBState(id: any): void {
        // Match the selected ID with the ID's in array
        this.currentState = this.stateLs.filter(value => parseInt(value.id) === parseInt(id));
        this.state = this.currentState[0];
        
        //load city based on state
        this._bandar.getCity(id).subscribe(
          data => {
            this.cities = data;
          }
        );
    }
  
    setSBCity(id: any): void {

        // Match the selected ID with the ID's in array
        this.currentCity = this.cities.filter(value => parseInt(value.id) === parseInt(id));
        this.city = this.currentCity[0];

    }

    kembali() {
        this._router.navigate(['/kementerian/list']);
    }
}
