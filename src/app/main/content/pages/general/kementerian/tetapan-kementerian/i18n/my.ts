export const locale = {
    lang: 'my',
    data: {
        'KEMENTERIAN': {
            'TITLE': 'Kementerian',
            'LIST': 'Senarai Kementerian',
            'NEW': 'Tambah Kementerian',
            'DETAILS': 'Maklumat Kementerian',
            'NAME': 'Nama',
            'CODE': 'Nama Ringkas',
            'PHONENO': 'Nombor Telefon',
            'ADDRESS': 'Alamat',
            'POSTCODE': 'Poskod',
            'STATE': 'Negeri',
            'CITY': 'Bandar',
            'URLPORTAL': 'Url Portal',
            'BTN' : {
                'SUBMIT' : 'Hantar',
                'BACK'  : 'Kembali'
            },
            'FORMERROR' : {
                'NAME': 'Nama wajib diisi',
                'CODE': 'Nama ringkas wajib diisi',
                'PHONENO': 'Nombor telefon wajib diisi',
                'PHONENOPATTERN': 'Sila masukkan format yang betul. Cth: 0122344567',
                'ADDRESS': 'Alamat wajib diisi',
                'POSTCODE': {
                    'REQUIRED': 'Poskod wajib diisi',
                    'PATTERN' : 'Sila masukkan format poskod yang sah'
                },
                'STATE': 'Negeri wajib diisi',
                'CITY': 'Bandar wajib diisi',
                'URLPORTAL': 'Url Portal wajib diisi',
                'URLPATTERN': 'Sila masukkan format url yang sah',
                'NAMEEXIST': 'Nama kementerian telah wujud',
                'CODEEXIST': 'Nama ringkas kementerian telah wujud'
            },
            'ERROR': 'Maklumat Kementerian Tidak Berjaya Dihantar',
            'SUCCESS': 'Maklumat Kementerian Telah Berjaya Dihantar'
        }
    }
};
