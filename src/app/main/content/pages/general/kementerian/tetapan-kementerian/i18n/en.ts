export const locale = {
    lang: 'en',
    data: {
        'KEMENTERIAN': {
            'TITLE': 'Ministry',
            'LIST': 'Ministries List',
            'NEW': 'Add Ministry',
            'DETAILS': 'Ministry Details',
            'NAME': 'Name',
            'CODE': 'Short Name',
            'PHONENO': 'Phone No',
            'ADDRESS': 'Address',
            'POSTCODE': 'Postcode',
            'STATE': 'State',
            'CITY': 'City',
            'URLPORTAL': 'Portal Url',
            'BTN' : {
                'SUBMIT' : 'Submit',
                'BACK'  : 'Back'
            },
            'FORMERROR' : {
                'NAME': 'Name is required',
                'CODE': 'Code is required',
                'PHONENO': 'Phone number is required',
                'PHONENOPATTERN': 'Please enter the valid format. Cth: 0122344567',
                'ADDRESS': 'Address is required',
                'POSTCODE': {
                    'REQUIRED': 'Address is required',
                    'PATTERN' : 'Please enter a valid postcode format'
                },
                'STATE': 'State is required',
                'CITY': 'City is required',
                'URLPORTAL': 'Portal url is required',
                'URLPATTERN': 'Please enter a valid url format',
                'NAMEEXIST': 'Ministry Name Already Exists',
                'CODEEXIST': 'Ministry Code Already Exists'
            },
            'ERROR': 'Ministry Details Are Unsuccessfully Submitted',
            'SUCCESS': 'Ministry Details Are Successfully Submitted'
        }
    }
};
