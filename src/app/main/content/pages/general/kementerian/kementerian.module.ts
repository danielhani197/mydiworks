import { NgModule } from '@angular/core';
import { SenaraiKementerianComponent } from './senarai-kementerian/senarai-kementerian.component';
import { TetapanKementerianComponent } from './tetapan-kementerian/tetapan-kementerian.component';
import { SenaraiKementerianDeleteDialogComponent } from './senarai-kementerian/senarai-kementerian-delete-dialog.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule, MatCheckboxModule, MatIconModule, MatFormFieldModule, MatInputModule, MatChipsModule, MatMenuModule, MatDialogModule, MatSnackBarModule, MatSelectModule, MatStepperModule } from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FuseSharedModule } from '@fuse/shared.module';


const routes = [
    {
        path     : 'list',
        component: SenaraiKementerianComponent
    },
    {
        path     : 'tetapan',
        component: TetapanKementerianComponent
    },
    {
        path    : 'tetapan/:id',
        component: TetapanKementerianComponent
    }
];

@NgModule({
    declarations: [
        SenaraiKementerianComponent,
        SenaraiKementerianDeleteDialogComponent,
        TetapanKementerianComponent
    ],
    entryComponents: [
        SenaraiKementerianDeleteDialogComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatMenuModule,
        MatDialogModule,
        MatSnackBarModule,
        MatSelectModule,
        MatStepperModule,
        NgxDatatableModule,

        FuseSharedModule,
        
    ],
    exports: [

    MatDialogModule,

    ]
})
export class KementerianModule
{

}