export const locale = {
    lang: 'en',
    data: {
        'KEMENTERIAN': {
            'SEARCH': 'Search',
            'TITLE': 'Ministry',
            'LIST': 'Ministries List',
            'NEW': 'Add Ministry',
            'DETAILS': 'Ministry Details',
            'NAME': 'Name',
            'CODE': 'Short Name',
            'PHONENO': 'Phone No',
            'ADDRESS': 'Address',
            'POSTCODE': 'Postcode',
            'STATE': 'State',
            'CITY': 'City',
            'URLPORTAL': 'Portal Url',
            'BTN' : {
                'DELETE' : 'Delete',
                'EDIT' : 'Edit',
                'SUBMIT' : 'Submit'
            },
            'DELETEKEMENTERIAN' : {
                'DELETEKEMENTERIAN' : 'Delete Ministry',
                'CONFIRM' : 'Are you sure to delete this ministry details?',
                'DELETE' : 'Delete',
                'BACK' : 'Back',
                'ERROR' : 'Please Delete Agency Under This Ministry First',
                'SUCCESS' : 'Ministry Details Was Successfully Deleted'
            },
            'ERROR': 'Error Somewhere',
            'SUCCESS': 'Ministry Details Are Successfully Submitted'
        }
    }
};
