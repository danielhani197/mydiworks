export const locale = {
    lang: 'my',
    data: {
        'KEMENTERIAN': {
            'SEARCH': 'Carian',
            'TITLE': 'Kementerian',
            'LIST': 'Senarai Kementerian',
            'NEW': 'Tambah Kementerian',
            'DETAILS': 'Maklumat Kementerian',
            'NAME': 'Nama',
            'CODE': 'Nama Ringkas',
            'PHONENO': 'Nombor Telefon',
            'ADDRESS': 'Alamat',
            'POSTCODE': 'Poskod',
            'STATE': 'Negeri',
            'CITY': 'Bandar',
            'URLPORTAL': 'Url Portal',
            'BTN' : {
                'DELETE' : 'Padam',
                'EDIT' : 'Kemaskini',
                'SUBMIT' : 'Hantar'
            },
            'DELETEKEMENTERIAN' : {
                'DELETEKEMENTERIAN' : 'Padam Kementerian',
                'CONFIRM' : 'Adakah anda pasti untuk memadam maklumat kementerian ini?',
                'DELETE' : 'Padam',
                'BACK' : 'Kembali',
                'ERROR' : 'Sila Padam Maklumat Agensi Untuk Kementerian Ini Terlebih Dahulu',
                'SUCCESS' : 'Maklumat Kementerian Telah Berjaya Dipadam'
            },
            'ERROR': 'Maklumat Tercalar',
            'SUCCESS': 'Maklumat Kementerian Telah Berjaya Dihantar'
        }
    }
};
