import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { 
    MatButtonModule, 
    MatCheckboxModule, 
    MatIconModule, 
    MatFormFieldModule, 
    MatInputModule,
    MatChipsModule,
    MatMenuModule,
    MatDialogModule,
    MatSnackBarModule
} from '@angular/material';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { SenaraiAgensiComponent } from './senarai-agensi.component';
import { SenaraiAgensiDeleteDialogComponent } from './senarai-agensi-delete-dialog.component';

const routes = [
    {
        path     : 'list',
        component: SenaraiAgensiComponent
    }
];

@NgModule({
    declarations: [
        SenaraiAgensiComponent,
        SenaraiAgensiDeleteDialogComponent
    ],
    entryComponents: [
        SenaraiAgensiDeleteDialogComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatMenuModule,
        MatDialogModule,
        MatSnackBarModule,

        NgxDatatableModule,

        FuseSharedModule,
    ],
    exports: [

    MatDialogModule,

    ]
})
export class SenaraiAgensiModule
{
}
