import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { MatSnackBar } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

import { Agensi } from 'app/main/content/model/general/agensi';
import { AgensiService } from 'app/main/content/services/agency/agensi.service';
import { Kementerian } from 'app/main/content/model/general/kementerian';
import { KementerianService } from 'app/main/content/services/agency/kementerian.service';

import { Bandar } from '../../../../model/ref/bandar';
import { Negeri } from '../../../../model/ref/negeri';
import { BandarNegeriService } from '../../../../services/references/bandarNegeri.service';

import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';

@Component({
    selector   : 'tetapan-agensi',
    templateUrl: './tetapan-agensi.component.html',
    styleUrls  : ['./tetapan-agensi.component.scss'],
    animations : fuseAnimations
})
export class TetapanAgensiComponent implements OnInit
{   
    _id: string;
    agensiForm: FormGroup;
    agensiFormErrors: any;

    successTxt: string;
    existTxt: string;

    stateLs : any[];
    currentState: any;

    kementerianLs : any[];
    currentKementerian : any;

    cities : any[];
    currentCity: any;

    city: Bandar;
    state: Negeri;
    kementerian: Kementerian;

    errNameExist = false;
    errCodeExist = false;

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _kementerian: KementerianService,
        private _agensi: AgensiService,
        private _router: Router,
        private _toastr: ToastrService,
        private _translate: TranslateService,
        private _bandar: BandarNegeriService,
        public snackBar: MatSnackBar
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);

        // Reactive form errors
        this.agensiFormErrors = {
            name : {},
            code  : {},
            phoneNo : {},
            address : {},
            postcode : {},
            city : {},
            state : {},
            kementerian : {},
            urlPortal : {}
        };

        this._translate.get('AGENSI.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('AGENSI.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });

    }

    ngOnInit()
    {
        this.agensiForm = this.formBuilder.group({
            name    : ['', [Validators.required]],
            code    : ['', Validators.required],
            phoneNo    : ['', [Validators.required, Validators.pattern("[^a-zA-Z]{7,16}")]],
            address    : ['', [Validators.required]],
            postcode    : ['', [Validators.required, Validators.pattern("[0-9]{5}")]],
            city    : ['', [Validators.required]],
            state    : ['', [Validators.required]],
            kementerian    : ['', [Validators.required]],
            urlPortal    : ['', [Validators.required, Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')]]
        });

        this.agensiForm.valueChanges.subscribe(() => {
            this.onAgensiFormValuesChanged();
        });

        this._bandar.getState().subscribe(
            states =>{
                this.stateLs = states;
                this._kementerian.getKementerians().subscribe(
                    data => {
                        this.kementerianLs = data;
                        this._id = this.route.snapshot.paramMap.get('id');
                        if(this._id){
                            this._agensi.getAgencyById(this._id).subscribe(
                                agency=> {
                                    var state =  agency.state;
                                    var city =  agency.city;
                                    var kementerian = agency.ministry;
                                    var stateid = "";
                                    var cityid = "";
                                    var kementerianid = "";

                                    if(state&&city&&kementerian){
                                        stateid = state.id;
                                        cityid = city.id;
                                        kementerianid = kementerian.id;

                                        this._bandar.getCity(stateid).subscribe(
                                            bandar=>{
                                                this.cities = bandar;
                                                
                                                if(stateid&&cityid&&kementerianid){
                                                    this.currentState = this.stateLs.filter(value => parseInt(value.id) === parseInt(stateid));
                                                    this.state = this.currentState[0];

                                                    this.currentCity = this.cities.filter(value => parseInt(value.id) === parseInt(cityid));
                                                    this.city = this.currentCity[0];

                                                    this._kementerian.getKementerianById(kementerianid).subscribe(
                                                        ref => {

                                                            this.kementerian = ref;
                                                            this.agensiForm.setValue({
                                                                name: agency.name,
                                                                code: agency.code,
                                                                phoneNo: agency.phoneNo,
                                                                address: agency.address,
                                                                postcode: agency.postcode,
                                                                city: agency.city.id,
                                                                state: agency.state.id,
                                                                kementerian: agency.ministry.id,
                                                                urlPortal: agency.urlPortal
                                                            });
                                                        }
                                                    )
                                                }
                                            }
                                        )
                                    }
                                }
                            )
                        }
                    }
                )
            }
        );        
    }

    hantar(){

        if(this._id){
            let agensi: Agensi = new Agensi ();
            agensi.setName(this.agensiForm.controls['name'].value),
            agensi.setCode(this.agensiForm.controls['code'].value),
            agensi.setPhoneNo(this.agensiForm.controls['phoneNo'].value),
            agensi.setAddress(this.agensiForm.controls['address'].value),
            agensi.setPostcode(Number(this.agensiForm.controls['postcode'].value)),
            agensi.setCity(this.city),
            agensi.setState(this.state),
            agensi.setMinistry(this.kementerian),
            agensi.setUrlPortal(this.agensiForm.controls['urlPortal'].value),
            agensi.setId(this._id)

            this._agensi.updateAgensi(agensi).subscribe(
                success=>{
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                      });
                      this._router.navigate(['/agensi/list']);
                },
                error=>{
                    
                    var messages = error.error.errorMessage;

                        
                    if(messages == "ERR_EXIST_AGENCY_NAME"){

                        this.errNameExist = true;

                    }else if(messages == "ERR_EXIST_AGENCY_CODE"){

                        this.errCodeExist = true;

                    }else if(messages == "ERR_EXIST_AGENCY_BOTH"){

                        this.errNameExist = true;
                        this.errCodeExist = true;

                    }

                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                      });
                }
            )

        }else{
            let agensi: Agensi = new Agensi ();
            agensi.setName(this.agensiForm.controls['name'].value),
            agensi.setCode(this.agensiForm.controls['code'].value),
            agensi.setPhoneNo(this.agensiForm.controls['phoneNo'].value),
            agensi.setAddress(this.agensiForm.controls['address'].value),
            agensi.setPostcode(Number(this.agensiForm.controls['postcode'].value)),
            agensi.setCity(this.city),
            agensi.setState(this.state),
            agensi.setMinistry(this.kementerian),
            agensi.setUrlPortal(this.agensiForm.controls['urlPortal'].value)
            
            this._agensi.createAgensi(agensi).subscribe(
                success=>{
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                      });
                      this._router.navigate(['/agensi/list']);
                },
                error=>{
                    

                    var messages = error.error.errorMessage;

                        
                    if(messages == "ERR_EXIST_AGENCY_NAME"){

                        this.errNameExist = true;

                    }else if(messages == "ERR_EXIST_AGENCY_CODE"){

                        this.errCodeExist = true;

                    }else if(messages == "ERR_EXIST_AGENCY_BOTH"){

                        this.errNameExist = true;
                        this.errCodeExist = true;

                    }

                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                      });
                }
            )
        }
    }

    onAgensiFormValuesChanged(){
        this.errNameExist = false;
        this.errCodeExist = false;
        for ( const field in this.agensiFormErrors )
        {
            if ( !this.agensiFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.agensiFormErrors[field] = {};

            // Get the control
            const control = this.agensiForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.agensiFormErrors[field] = control.errors;
            }
        }
    }

    setSBState(id: any): void {
        // Match the selected ID with the ID's in array
        this.currentState = this.stateLs.filter(value => parseInt(value.id) === parseInt(id));
        this.state = this.currentState[0];
        
        //load city based on state
        this._bandar.getCity(id).subscribe(
          data => {
            this.cities = data;
          }
        );
    }
  
    setSBCity(id: any): void {

        // Match the selected ID with the ID's in array
        this.currentCity = this.cities.filter(value => parseInt(value.id) === parseInt(id));
        this.city = this.currentCity[0];

    }

    setSBKementerian(id: any): void {

        this._kementerian.getKementerianById(id).subscribe(
            data=>{
                this.kementerian = data;
            }
        )

    }

    redirectAgencyPage() {
        this._router.navigate(['/agensi/list']);
    }
}
