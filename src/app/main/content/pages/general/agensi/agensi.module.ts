import { NgModule } from '@angular/core';

import { SenaraiAgensiModule } from './senarai-agensi/senarai-agensi.module';
import { TetapanAgensiModule } from './tetapan-agensi/tetapan-agensi.module';

@NgModule({
    imports: [
        // Auth
        SenaraiAgensiModule,
        TetapanAgensiModule
        
    ]
})
export class AgensiModule
{

}