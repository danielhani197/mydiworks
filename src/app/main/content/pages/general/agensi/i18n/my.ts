export const locale = {
    lang: 'my',
    data: {
        'AGENSI': {
            'SEARCH': 'Carian',
            'TITLE': 'Agensi',
            'LIST': 'Senarai Agensi',
            'NEW': 'Tambah Agensi',
            'DETAILS': 'Maklumat Agensi',
            'NAME': 'Nama',
            'CODE': 'Nama Ringkas',
            'PHONENO': 'Nombor Telefon',
            'ADDRESS': 'Alamat',
            'POSTCODE': 'Poskod',
            'STATE': 'Negeri',
            'CITY': 'Bandar',
            'URLPORTAL': 'Url Portal',
            'KEMENTERIAN': 'Kementerian',
            'BTN' : {
                'DELETE' : 'Padam',
                'EDIT' : 'Kemaskini',
                'SUBMIT' : 'Hantar',
                'BACK'  : 'Kembali'
            },
            'FORMERROR' : {
                'NAME': 'Nama wajib diisi',
                'CODE': 'Nama ringkas wajib diisi',
                'PHONENO': 'Nombor telefon wajib diisi',
                'PHONENOPATTERN': 'Sila masukkan format yang betul. Cth: 0122344567',
                'ADDRESS': 'Alamat wajib diisi',
                'POSTCODE': {
                    'REQUIRED': 'Poskod wajib diisi',
                    'PATTERN' : 'Sila masukkan format poskod yang sah'
                },
                'STATE': 'Negeri wajib diisi',
                'CITY': 'Bandar wajib diisi',
                'URLPORTAL': 'Url Portal wajib diisi',
                'URLPATTERN': 'Sila masukkan format url yang sah',
                'KEMENTERIAN': 'Kementerian wajib diisi',
                'NAMEEXIST': 'Nama agensi telah wujud',
                'CODEEXIST': 'Nama ringkas agensi telah wujud'
            },
            'DELETEAGENCY' : {
                'DELETEAGENCY' : 'Padam Agensi',
                'CONFIRM' : 'Adakah anda pasti untuk memadam maklumat agensi ini?',
                'DELETE' : 'Padam',
                'BACK' : 'Kembali',
                'ERROR' : 'Sila Padam Maklumat Bahagian Untuk Agensi Ini Terlebih Dahulu',
                'SUCCESS' : 'Maklumat Agensi Telah Berjaya Dipadam'
            },
            'ERROR': 'Maklumat Agensi Tidak Dapat Disimpan',
            'SUCCESS': 'Maklumat Agensi Telah Berjaya Dihantar'
        }
    }
};
