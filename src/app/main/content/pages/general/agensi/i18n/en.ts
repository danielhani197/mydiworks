export const locale = {
    lang: 'en',
    data: {
        'AGENSI': {
            'SEARCH': 'Search',
            'TITLE': 'Agency',
            'LIST': 'Agencies List',
            'NEW': 'Add Agency',
            'DETAILS': 'Agency Details',
            'NAME': 'Name',
            'CODE': 'Short Name',
            'PHONENO': 'Phone No',
            'ADDRESS': 'Address',
            'POSTCODE': 'Postcode',
            'STATE': 'State',
            'CITY': 'City',
            'URLPORTAL': 'Portal Url',
            'KEMENTERIAN': 'Ministry',
            'BTN' : {
                'DELETE' : 'Delete',
                'EDIT' : 'Edit',
                'SUBMIT' : 'Submit',
                'BACK'  : 'Back'
            },
            'FORMERROR' : {
                'NAME': 'Name is required',
                'CODE': 'Code is required',
                'PHONENO': 'Phone number is required',
                'PHONENOPATTERN': 'Please enter the valid format. Cth: 0122344567',
                'ADDRESS': 'Address is required',
                'POSTCODE': {
                    'REQUIRED': 'Address is required',
                    'PATTERN' : 'Please enter a valid postcode format'
                },
                'STATE': 'State is required',
                'CITY': 'City is required',
                'URLPORTAL': 'Portal url is required',
                'URLPATTERN': 'Please enter a valid url format',
                'KEMENTERIAN': 'Ministry is required',
                'NAMEEXIST': 'Agency Name Already Exists',
                'CODEEXIST': 'Agency Code Already Exists'
            },
            'DELETEAGENCY' : {
                'DELETEAGENCY' : 'Delete Agency',
                'CONFIRM' : 'Are you sure to delete this agency details?',
                'DELETE' : 'Delete',
                'BACK' : 'Back',
                'ERROR' : 'Please Delete Divisions Under This Agency First',
                'SUCCESS' : 'Agency Details Was Successfully Deleted'
            },
            'ERROR': 'Agency Details Are Unsuccessfully Submitted',
            'SUCCESS': 'Agency Details Are Successfully Submitted'
        }
    }
};
