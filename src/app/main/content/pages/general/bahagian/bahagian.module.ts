import { NgModule } from '@angular/core';

import { SenaraiBahagianModule } from './senarai-bahagian/senarai-bahagian.module';
import { TetapanBahagianModule } from './tetapan-bahagian/tetapan-bahagian.module';

@NgModule({
    imports: [
        // Auth
        SenaraiBahagianModule,
        TetapanBahagianModule
        
    ]
})
export class BahagianModule
{

}