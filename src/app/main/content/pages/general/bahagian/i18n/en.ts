export const locale = {
    lang: 'en',
    data: {
        'BAHAGIAN': {
            'SEARCH': 'Search',
            'TITLE': 'Division',
            'LIST': 'Divisions List',
            'NEW': 'Add Division',
            'DETAILS': 'Division Details',
            'NAME': 'Name',
            'CODE': 'Short Name',
            'AGENCY': 'Agency',
            'KEMENTERIAN': 'Ministry',
            'BTN' : {
                'DELETE' : 'Delete',
                'EDIT' : 'Edit',
                'SUBMIT' : 'Submit',
                'BACK'  : 'Back'
            },
            'FORMERROR' : {
                'NAME': 'Name is required',
                'CODE': 'Code is required',
                'KEMENTERIAN': 'Ministry is required',
                'NAMEEXIST': 'Division name already exists',
                'CODEEXIST': 'Division code already exists'
            },
            'DELETEBAHAGIAN' : {
                'DELETEBAHAGIAN' : 'Delete Division',
                'CONFIRM' : 'Are you sure to delete this division details?',
                'DELETE' : 'Delete',
                'BACK' : 'Back',
                'ERROR' : 'Division Details Was Unsuccessfully Deleted',
                'SUCCESS' : 'Division Details Was Successfully Deleted'
            },
            'ERROR': 'Division Details Are Unsuccessfully Submitted',
            'SUCCESS': 'Division Details Are Successfully Submitted'
        }
    }
};
