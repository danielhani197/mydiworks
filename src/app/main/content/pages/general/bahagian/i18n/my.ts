export const locale = {
    lang: 'my',
    data: {
        'BAHAGIAN': {
            'SEARCH': 'Carian',
            'TITLE': 'Bahagian',
            'LIST': 'Senarai Bahagian',
            'NEW': 'Tambah Bahagian',
            'DETAILS': 'Maklumat Bahagian',
            'NAME': 'Nama',
            'CODE': 'Nama Ringkas',
            'AGENCY': 'Agensi',
            'KEMENTERIAN': 'Kementerian',
            'BTN' : {
                'DELETE' : 'Padam',
                'EDIT' : 'Kemaskini',
                'SUBMIT' : 'Hantar',
                'BACK'  : 'Kembali'
            },
            'FORMERROR' : {
                'NAME': 'Nama wajib diisi',
                'CODE': 'Nama ringkas wajib diisi',
                'KEMENTERIAN': 'Kementerian wajib diisi',
                'NAMEEXIST': 'Nama bahagian telah wujud',
                'CODEEXIST': 'Nama ringkas bahagian telah wujud'
            },
            'DELETEBAHAGIAN' : {
                'DELETEBAHAGIAN' : 'Padam Bahagian',
                'CONFIRM' : 'Adakah anda pasti untuk memadam maklumat bahagian ini?',
                'DELETE' : 'Padam',
                'BACK' : 'Kembali',
                'ERROR' : 'Maklumat Bahagian Tidak Dapat Dipadam',
                'SUCCESS' : 'Maklumat Bahagian Telah Berjaya Dipadam'
            },
            'ERROR': 'Maklumat Bahagian Tidak Dapat Disimpan',
            'SUCCESS': 'Maklumat Bahagian Telah Berjaya Disimpan'
        }
    }
};
