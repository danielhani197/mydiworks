import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { 
    MatButtonModule, 
    MatCheckboxModule, 
    MatIconModule, 
    MatFormFieldModule, 
    MatInputModule,
    MatChipsModule,
    MatMenuModule,
    MatDialogModule,
    MatSnackBarModule
} from '@angular/material';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { SenaraiBahagianComponent } from './senarai-bahagian.component';
import { SenaraiBahagianDeleteDialogComponent } from './senarai-bahagian-delete-dialog.component';

const routes = [
    {
        path     : 'list',
        component: SenaraiBahagianComponent
    }
];

@NgModule({
    declarations: [
        SenaraiBahagianComponent,
        SenaraiBahagianDeleteDialogComponent
    ],
    entryComponents: [
        SenaraiBahagianDeleteDialogComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatMenuModule,
        MatDialogModule,
        MatSnackBarModule,

        NgxDatatableModule,

        FuseSharedModule,
    ],
    exports: [

    MatDialogModule,

    ]
})
export class SenaraiBahagianModule
{
}
