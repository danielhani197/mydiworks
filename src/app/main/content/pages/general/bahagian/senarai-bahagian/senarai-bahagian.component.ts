import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { Page } from 'app/main/content/model/util/page';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

import { Bahagian } from 'app/main/content/model/general/bahagian';
import { BahagianService } from 'app/main/content/services/agency/bahagian.service';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { SenaraiBahagianDeleteDialogComponent } from './senarai-bahagian-delete-dialog.component';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { PenggunaService } from '../../../../services/pengguna/pengguna.service';

@Component({
    selector   : 'senarai-bahagian',
    templateUrl: './senarai-bahagian.component.html',
    styleUrls  : ['./senarai-bahagian.component.scss']
})
export class SenaraiBahagianComponent implements OnInit
{
    rows: any[];
    temp = [];
    page = new Page();

    _idToDelete : string;
    loadingIndicator = true;
    reorderable = true;
    _userType : string;
    _userKementerianId : string;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private http: HttpClient,
        private _bahagian: BahagianService,
        private _router: Router,
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        public _dialog: MatDialog,
        public _auth: AuthenticationService,
        public _pengguna: PenggunaService
    )
    {
        this.page.pageNumber = 0;
        this.page.size = 10;
        this.page.sortField = "name";
        this.page.sort = "asc";
        this.fuseTranslationLoader.loadTranslations(malay, english);
        
    }

    ngOnInit()
    {
        this._pengguna.getUserById(this._auth.getCurrentUser().id).subscribe(
            data=>{
                if (data.agensi){
                    this._userType = 'agensi';
                    this._userKementerianId = data.agensi.id
                }else {
                    this._userType = 'kementerian';
                    this._userKementerianId = data.kementerian.id
                }
                this.setPage({ offset: 0 });
            }
        )
        
    }

    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._bahagian.getBahagianList(this.page, this._userType, this._userKementerianId).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._bahagian.getBahagianList(this.page, this._userType, this._userKementerianId).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;
    
        this._bahagian.getBahagianList(this.page, this._userType, this._userKementerianId).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    editBahagian(id: string){
        this._router.navigate(['bahagian/tetapan/'+id]);
    }

    deleteBahagian(id: string){
        this._idToDelete = id;
        let dialogRef = this._dialog.open(SenaraiBahagianDeleteDialogComponent, {
            width: '500px',
            data: { id: this._idToDelete }
        });

        dialogRef.afterClosed().subscribe(result => {
            this.setPage({ offset: 0 });
        });
    }

    tambah(){
        this._router.navigate(['bahagian/tetapan']);
    }

    
}