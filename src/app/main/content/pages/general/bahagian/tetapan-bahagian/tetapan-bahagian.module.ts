import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatIconModule, MatSelectModule, MatStepperModule, MatSnackBarModule  } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { TetapanBahagianComponent } from './tetapan-bahagian.component';

const routes = [
    {
        path     : 'tetapan',
        component: TetapanBahagianComponent
    },
    {
        path     : 'tetapan/:id',
        component: TetapanBahagianComponent
    }
];

@NgModule({
    declarations: [
        TetapanBahagianComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,

        MatIconModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatSnackBarModule,

        FuseSharedModule
    ],
})
export class TetapanBahagianModule
{
}
