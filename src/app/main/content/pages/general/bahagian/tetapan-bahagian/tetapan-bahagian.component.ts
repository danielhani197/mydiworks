import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { MatSnackBar } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

import { Agensi } from 'app/main/content/model/general/agensi';
import { AgensiService } from 'app/main/content/services/agency/agensi.service';
import { Bahagian } from 'app/main/content/model/general/bahagian';
import { BahagianService } from 'app/main/content/services/agency/bahagian.service';

import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { Kementerian } from '../../../../model/general/kementerian';
import { KementerianService } from '../../../../services/agency/kementerian.service';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { PenggunaService } from '../../../../services/pengguna/pengguna.service';

@Component({
    selector   : 'tetapan-bahagian',
    templateUrl: './tetapan-bahagian.component.html',
    styleUrls  : ['./tetapan-bahagian.component.scss'],
    animations : fuseAnimations
})
export class TetapanBahagianComponent implements OnInit
{   
    _id: string;
    _userId: string;
    bahagianForm: FormGroup;
    bahagianFormErrors: any;

    successTxt: string;
    existTxt: string;

    //FILTER SEARCH STUFF
    kementerianLs:       any[];
    agensiLs:            any[];
    currentKementerian:  any;
    currentAgensi:       any;
    kementerian:         Kementerian;
    agensi:              Agensi;

    errNameExist = false;
    errCodeExist = false;

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _bahagian: BahagianService,
        private _agensi: AgensiService,
        private _router: Router,
        private _toastr: ToastrService,
        private _translate: TranslateService,
        public snackBar: MatSnackBar,
        private _kementerian: KementerianService,
        private _auth: AuthenticationService,
        private _user: PenggunaService

    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);

        // Reactive form errors
        this.bahagianFormErrors = {
            name : {},
            code  : {},
            kementerian : {}
        };

        this._userId = this._auth.getCurrentUser().id;
        this._id = this.route.snapshot.paramMap.get('id');
                
        this._translate.get('BAHAGIAN.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('BAHAGIAN.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });

    }

    ngOnInit()
    {
        this.bahagianForm = this.formBuilder.group({
            name    : ['', [Validators.required]],
            code    : ['', Validators.required],
            kementerian    : ({value: '', disabled: true}),
            agency    : ({value: '', disabled: true}),
        });

        this.bahagianForm.valueChanges.subscribe(() => {
            this.onBahagianFormValuesChanged();
        }); 


        if(this._userId){
            this._user.getUserById(this._userId).subscribe(
                user=>{
                    if(user.kementerian){
                        this.kementerian = user.kementerian;
                        this.bahagianForm.patchValue({
                            kementerian: user.kementerian.name
                        })
                    }
                    if(user.agensi){
                        this.agensi = user.agensi;
                        this.bahagianForm.patchValue({
                            agency: user.agensi.name
                        })
                    }
                }
            )
        }
        if(this._id){
            this._bahagian.getBahagianById(this._id).subscribe(
                bahagian=>{

                    this.bahagianForm.patchValue({
                        name: bahagian.name,
                        code: bahagian.code,
                    });
                }
            )
        }

                
    }

    hantar(){

        if(this.kementerian){
            if(this._id){
                let bahagian: Bahagian = new Bahagian ();
                bahagian.setName(this.bahagianForm.controls['name'].value),
                bahagian.setCode(this.bahagianForm.controls['code'].value),
                bahagian.setAgensi(this.agensi),
                bahagian.setKementerian(this.kementerian),
                bahagian.setId(this._id)
    
                this._bahagian.updateBahagian(bahagian).subscribe(
                    success=>{
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                          });
                          this._router.navigate(['/bahagian/list']);
                    },
                    error=>{
                        
                        var messages = error.error.errorMessage;
    
                            
                        if(messages == "ERR_EXIST_AGENCY_NAME"){
    
                            this.errNameExist = true;
    
                        }else if(messages == "ERR_EXIST_AGENCY_CODE"){
    
                            this.errCodeExist = true;
    
                        }else if(messages == "ERR_EXIST_AGENCY_BOTH"){
    
                            this.errNameExist = true;
                            this.errCodeExist = true;
    
                        }
    
                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['red-snackbar']
                        });
                    }
                )
    
            }else{
                let bahagian: Bahagian = new Bahagian ();
                bahagian.setName(this.bahagianForm.controls['name'].value),
                bahagian.setCode(this.bahagianForm.controls['code'].value),
                bahagian.setAgensi(this.agensi),
                bahagian.setKementerian(this.kementerian)
                
                this._bahagian.createBahagian(bahagian).subscribe(
                    success=>{
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                          });
                          this._router.navigate(['/bahagian/list']);
                    },
                    error=>{
                        
                        var messages = error.error.errorMessage;
    
                            
                        if(messages == "ERR_EXIST_AGENCY_NAME"){
    
                            this.errNameExist = true;
    
                        }else if(messages == "ERR_EXIST_AGENCY_CODE"){
    
                            this.errCodeExist = true;
    
                        }else if(messages == "ERR_EXIST_AGENCY_BOTH"){
    
                            this.errNameExist = true;
                            this.errCodeExist = true;
    
                        }
    
                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['red-snackbar']
                          });
                    }
                )
            }
        }else{
            this.snackBar.open('Sila pastikan anda mempunyai kementerian atau agensi', "OK", {
                panelClass: ['red-snackbar']
            });
        }
    }

    onBahagianFormValuesChanged(){
        this.errNameExist = false;
        this.errCodeExist = false;
        for ( const field in this.bahagianFormErrors )
        {
            if ( !this.bahagianFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.bahagianFormErrors[field] = {};

            // Get the control
            const control = this.bahagianForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.bahagianFormErrors[field] = control.errors;
            }
        }
    }

    //ONCHANGE KEMENTERIAN SELECTION
    setKementerian(id: any): void {

        // GET KEMENTERIAN BASED ON ID
        this._kementerian.getKementerianById(id).subscribe(
            kementerian=>{
                this.kementerian = kementerian
                this.agensi = null
            }
        )
        
        //load agensi based on kementerian
        this._agensi.getAllAgenciesByMinistryId(id).subscribe(
            agensiLs=>{
                this.agensiLs = agensiLs;
            }
        )
            
        
    }

    setSBAgency(id: any): void {

        this._agensi.getAgencyById(id).subscribe(
            data=>{
                this.agensi = data;
            }
        )

    }

    redirectBahagianPage() {
        this._router.navigate(['/bahagian/list']);
    }
}
