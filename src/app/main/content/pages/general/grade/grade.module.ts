import { NgModule } from '@angular/core';

import { SenaraiGradeModule } from './senarai-grade/senarai-grade.module';

@NgModule({
    imports: [
        
        SenaraiGradeModule
        
    ]
})
export class GradeModule
{

}