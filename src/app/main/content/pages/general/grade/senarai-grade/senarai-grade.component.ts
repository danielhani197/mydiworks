import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { Page } from 'app/main/content/model/util/page';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Grade } from 'app/main/content/model/general/grade';
import { GradeService } from 'app/main/content/services/general/grade.service';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { SenaraiGradeDelDialogComponent } from './senarai-grade-del-dialog.component';
import { SenaraiGradeTetDialogComponent } from './senarai-grade-tet-dialog.component';

@Component({
    selector   : 'senarai-grade',
    templateUrl: './senarai-grade.component.html',
    styleUrls  : ['./senarai-grade.component.scss']
})
export class SenaraiGradeComponent implements OnInit
{
    rows: any[];
    temp = [];
    page = new Page();

    _idToDelete : string;
    loadingIndicator = true;
    reorderable = true;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private http: HttpClient,
        private _grade: GradeService,
        private _router: Router,
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        public _dialog: MatDialog
    )
    {
        this.page.pageNumber = 0;
        this.page.size = 10;
        this.page.sortField = "name";
        this.page.sort = "asc";
        this.fuseTranslationLoader.loadTranslations(malay, english);
    }

    ngOnInit()
    {
        this.setPage({ offset: 0 });
    }

    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._grade.getGradeList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._grade.getGradeList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;
    
        this._grade.getGradeList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    editGrade(id: string){
        
        let dialogRef = this._dialog.open(SenaraiGradeTetDialogComponent, {
            width: '500px',
            data: { id: id }
        });

        dialogRef.afterClosed().subscribe(result => {
            this.setPage({ offset: 0 });
        });
    }

    deleteGrade(id: string){
        this._idToDelete = id;
        let dialogRef = this._dialog.open(SenaraiGradeDelDialogComponent, {
            width: '500px',
            data: { id: this._idToDelete }
        });

        dialogRef.afterClosed().subscribe(result => {
            this.setPage({ offset: 0 });
        });
    }

    tambah(){
        let dialogRef = this._dialog.open(SenaraiGradeTetDialogComponent, {
            width: '500px',
            data: { id: null }
        });

        dialogRef.afterClosed().subscribe(result => {
            this.setPage({ offset: 0 });
        });
    }

    
}