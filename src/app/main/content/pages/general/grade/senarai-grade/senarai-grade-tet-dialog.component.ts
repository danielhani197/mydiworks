import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';

import { Grade } from 'app/main/content/model/general/grade';
import { GradeService } from 'app/main/content/services/general/grade.service';

@Component({
    selector: 'senarai-grade-tet-dialog',
    templateUrl: 'senarai-grade-tet-dialog.component.html',
    animations : fuseAnimations
    })
    export class SenaraiGradeTetDialogComponent implements OnInit
    {
        _id: string;

        gradeCn: any[];

        successTxt: string;
        existTxt: string;
        titleTxt: string;
        gradeForm: FormGroup;
        gradeFormErrors: any;

        errNameExist = false;

        constructor(
        private _translate: TranslateService,
        private formBuilder: FormBuilder,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _grade: GradeService,
        private _router: Router,
        public snackBar: MatSnackBar,
        public _dialogRef: MatDialogRef<SenaraiGradeTetDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
        ) { 
            this._translate.get('GRADE.ERROR').subscribe((res: string) => {
                this.existTxt = res;
            });
            this._translate.get('GRADE.SUCCESS').subscribe((res: string) => {
                this.successTxt = res;
            });
            this.fuseTranslationLoader.loadTranslations(malay, english);

            this.gradeFormErrors = {
                name : {},
                seniority  : {}
            };
        }
        

        ngOnInit(){

            this._id = this.data.id;
            

            this.gradeForm = this.formBuilder.group({
                name    : ['', [Validators.required]],
                seniority    : ['', Validators.required]
            });

            this.gradeForm.valueChanges.subscribe(() => {
                this.onGradeFormValuesChanged();
            });

            if (this._id){
                this._grade.getGradeCnEdit().subscribe(
                    data=>{
                        this.gradeCn = data;
                    }
                )
                this._translate.get('GRADE.DETAILS').subscribe((res: string) => {
                    this.titleTxt = res;
                    this._grade.getGradeById(this._id).subscribe(
                        grade=>{
                            this.gradeForm.setValue({
                                name: grade.name,
                                seniority: grade.seniority
                            });
                        }
                    )
                });

                

            }else{
                this._grade.getGradeCn().subscribe(
                    data=>{
                        this.gradeCn = data;
                    }
                )
                this._translate.get('GRADE.NEW').subscribe((res: string) => {
                    this.titleTxt = res;
                });
            }
            
        }

        onGradeFormValuesChanged(){
            this.errNameExist = false;
            for ( const field in this.gradeFormErrors )
            {
                if ( !this.gradeFormErrors.hasOwnProperty(field) )
                {
                    continue;
                }
    
                // Clear previous errors
                this.gradeFormErrors[field] = {};
    
                // Get the control
                const control = this.gradeForm.get(field);
    
                if ( control && control.dirty && !control.valid )
                {
                    this.gradeFormErrors[field] = control.errors;
                }
            }
        }

        hantar(): void {

            if(this._id){
                let grade: Grade = new Grade ();
                grade.setName(this.gradeForm.controls['name'].value);
                grade.setSeniority(this.gradeForm.controls['seniority'].value);
                grade.setId(this._id)
                this._grade.updateGrade(grade).subscribe(
                    success=>{
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                          });
                          this._dialogRef.close()
                    },
                    error=>{

                        var messages = error.error.errorMessage;

                        
                        if(messages == "ERR_EXIST_AGENCY_NAME"){
    
                            this.errNameExist = true;
    
                        }
                        
                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['red-snackbar']
                        });
                    }
                )
            }else{
                let grade: Grade = new Grade ();
                grade.setName(this.gradeForm.controls['name'].value);
                grade.setSeniority(this.gradeForm.controls['seniority'].value);
                
                this._grade.createGrade(grade).subscribe(
                    success=>{
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                          });
                          this._dialogRef.close()
                    },
                    error=>{
                        
                        var messages = error.error.errorMessage;

                        
                        if(messages == "ERR_EXIST_AGENCY_NAME"){
    
                            this.errNameExist = true;
    
                        }

                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['red-snackbar']
                        });
                    }
                )
            }

            
            
        }

    }