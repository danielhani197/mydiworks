import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { 
    MatButtonModule, 
    MatCheckboxModule, 
    MatIconModule, 
    MatFormFieldModule, 
    MatInputModule,
    MatChipsModule,
    MatMenuModule,
    MatSelectModule, 
    MatStepperModule,
    MatDialogModule,
    MatSnackBarModule
} from '@angular/material';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { SenaraiGradeComponent } from './senarai-grade.component';
import { SenaraiGradeDelDialogComponent } from './senarai-grade-del-dialog.component';
import { SenaraiGradeTetDialogComponent } from './senarai-grade-tet-dialog.component';

const routes = [
    {
        path     : 'list',
        component: SenaraiGradeComponent,
        
    }
];

@NgModule({
    declarations: [
        SenaraiGradeComponent,
        SenaraiGradeDelDialogComponent,
        SenaraiGradeTetDialogComponent
    ],
    entryComponents : [
        SenaraiGradeDelDialogComponent,
        SenaraiGradeTetDialogComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatMenuModule,
        MatSelectModule, 
        MatStepperModule,
        MatDialogModule,
        MatSnackBarModule,

        NgxDatatableModule,

        FuseSharedModule,
    ],
    exports: [

    MatDialogModule,

    ]
})
export class SenaraiGradeModule
{
}
