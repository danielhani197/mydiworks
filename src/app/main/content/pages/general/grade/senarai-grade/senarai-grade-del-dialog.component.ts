import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';

import { Grade } from 'app/main/content/model/general/grade';
import { GradeService } from 'app/main/content/services/general/grade.service';

@Component({
    selector: 'senarai-grade-del-dialog',
    templateUrl: 'senarai-grade-del-dialog.component.html',
    animations : fuseAnimations
    })
    export class SenaraiGradeDelDialogComponent implements OnInit
    {
        _id: string;

        successTxt: string;
        existTxt: string;

        constructor(
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _grade: GradeService,
        private _router: Router,
        public _dialogRef: MatDialogRef<SenaraiGradeDelDialogComponent>,
        public snackBar: MatSnackBar,
        @Inject(MAT_DIALOG_DATA) public data: any
        ) { 
            this._translate.get('GRADE.DELETEGRADE.ERROR').subscribe((res: string) => {
                this.existTxt = res;
            });
            this._translate.get('GRADE.DELETEGRADE.SUCCESS').subscribe((res: string) => {
                this.successTxt = res;
            });
            this.fuseTranslationLoader.loadTranslations(malay, english);
        }
        

        ngOnInit(){
            this._id = this.data.id;
        }
        confirmDelete(): void {
            
            this._grade.deleteGradeById(this._id).subscribe(
                success=>{
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                      });
                      this._dialogRef.close()
                },
                error=>{
                    
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                      });
                      this._dialogRef.close()
                }
            )
            
        }

    }