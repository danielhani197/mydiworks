export const locale = {
    lang: 'my',
    data: {
        'GRADE': {
            'SEARCH': 'Carian',
            'TITLE': 'Gred',
            'LIST': 'Senarai Gred',
            'NEW': 'Tambah Gred',
            'DETAILS': 'Maklumat Gred',
            'NAME': 'Nama',
            'SENIORITY' : 'Kekananan',
            'GRADE': 'Gred',
            'BTN' : {
                'DELETE' : 'Padam',
                'EDIT' : 'Kemaskini',
                'SUBMIT' : 'Hantar',
                'BACK'  : 'Kembali'
            },
            'FORMERROR' : {
                'NAME': 'Nama wajib diisi',
                'SENIORITY': 'Kekananan wajib diisi',
                'NAMEEXIST': 'Nama gred telah wujud'
            },
            'DELETEGRADE' : {
                'DELETEGRADE' : 'Padam Gred',
                'CONFIRM' : 'Adakah anda pasti untuk memadam maklumat gred ini?',
                'DELETE' : 'Padam',
                'BACK' : 'Kembali',
                'ERROR' : 'Maklumat Gred Tidak Dapat Dipadam',
                'SUCCESS' : 'Maklumat Gred Telah Berjaya Dipadam'
            },
            'ERROR': 'Maklumat Gred Tidak Dapat Disimpan',
            'SUCCESS': 'Maklumat Gred Telah Berjaya Dihantar'
        }
    }
};
