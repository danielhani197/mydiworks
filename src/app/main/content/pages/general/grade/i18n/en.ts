export const locale = {
    lang: 'en',
    data: {
        'GRADE': {
            'SEARCH': 'Search',
            'TITLE': 'Grade',
            'LIST': 'Grade List',
            'NEW': 'Add Grade',
            'DETAILS': 'Grade Details',
            'NAME': 'Name',
            'SENIORITY' : 'Seniority',
            'GRADE': 'Grade',
            'BTN' : {
                'DELETE' : 'Delete',
                'EDIT' : 'Edit',
                'SUBMIT' : 'Submit',
                'BACK'  : 'Back'
            },
            'FORMERROR' : {
                'NAME': 'Name is required',
                'SENIORITY': 'Seniority is required',
                'NAMEEXIST': 'Grade name already exists'
            },
            'DELETEGRADE' : {
                'DELETEGRADE' : 'Delete Grade',
                'CONFIRM' : 'Are you sure to delete this grade details?',
                'DELETE' : 'Delete',
                'BACK' : 'Back',
                'ERROR' : 'Grade Details Was Unsuccessfully Deleted',
                'SUCCESS' : 'Grade Details Was Successfully Deleted'
            },
            'ERROR': 'Grade Details Was Unsuccessfully Submitted',
            'SUCCESS': 'Grade Details Was Successfully Submitted'
        }
    }
};
