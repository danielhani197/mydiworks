export const locale = {
    lang: 'en',
    data: {
        'SKEMA': {
            'SEARCH': 'Search',
            'TITLE': 'Scheme',
            'LIST': 'Scheme List',
            'NEW': 'Add Scheme',
            'DETAILS': 'Scheme Detail',
            'NAME': 'Name',
            'KETERANGAN': 'Details',
            'SENIORITY' : 'Seniority',
            'SKEMA': 'Scheme',
            'BTN' : {
                'DELETE' : 'Delete',
                'EDIT' : 'Edit',
                'SUBMIT' : 'Submit',
                'BACK'  : 'Back'
            },
            'FORMERROR' : {
                'NAME': 'Name is required',
                'SENIORITY': 'Seniority is required',
                'NAMEEXIST': 'Scheme name already exists',
                'KETERANGAN': 'Detail is required'
            },
            'DELETESKEMA' : {
                'DELETESKEMA' : 'Delete Scheme',
                'CONFIRM' : 'Are you sure to delete this scheme details?',
                'DELETE' : 'Delete',
                'BACK' : 'Back',
                'ERROR' : 'Scheme Details Was Unsuccessfully Deleted',
                'SUCCESS' : 'Scheme Details Was Successfully Deleted'
            },
            'ERROR': 'Scheme Details Was Unsuccessfully Submitted',
            'SUCCESS': 'Scheme Details Was Successfully Submitted'
        }
    }
};
