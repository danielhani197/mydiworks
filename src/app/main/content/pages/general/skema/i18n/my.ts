export const locale = {
    lang: 'my',
    data: {
        'SKEMA': {
            'SEARCH': 'Carian',
            'TITLE': 'Skim',
            'LIST': 'Senarai Skim',
            'NEW': 'Tambah Skim',
            'DETAILS': 'Maklumat Skim',
            'NAME': 'Nama',
            'KETERANGAN': 'Keterangan',
            'SENIORITY' : 'Kekananan',
            'SKEMA': 'Skim',
            'BTN' : {
                'DELETE' : 'Padam',
                'EDIT' : 'Kemaskini',
                'SUBMIT' : 'Hantar',
                'BACK'  : 'Kembali'
            },
            'FORMERROR' : {
                'NAME': 'Nama wajib diisi',
                'SENIORITY': 'Kekananan wajib diisi',
                'NAMEEXIST': 'Nama skim telah wujud',
                'KETERANGAN': 'Keterangan wajib diisi'
            },
            'DELETESKEMA' : {
                'DELETESKEMA' : 'Padam Skim',
                'CONFIRM' : 'Adakah anda pasti untuk memadam maklumat skim ini?',
                'DELETE' : 'Padam',
                'BACK' : 'Kembali',
                'ERROR' : 'Maklumat Skim Tidak Dapat Dipadam',
                'SUCCESS' : 'Maklumat Skim Telah Berjaya Dipadam'
            },
            'ERROR': 'Maklumat Skim Tidak Dapat Disimpan',
            'SUCCESS': 'Maklumat Skim Telah Berjaya Dihantar'
        }
    }
};
