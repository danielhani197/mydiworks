import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';

import { Skema } from 'app/main/content/model/general/skema';
import { SkemaService } from 'app/main/content/services/general/skema.service';

@Component({
    selector: 'senarai-skema-tet-dialog',
    templateUrl: 'senarai-skema-tet-dialog.component.html',
    animations : fuseAnimations
    })
    export class SenaraiSkemaTetDialogComponent implements OnInit
    {
        _id: string;

        skemaCn: any[];

        successTxt: string;
        existTxt: string;
        titleTxt: string;
        skemaForm: FormGroup;
        skemaFormErrors: any;

        errNameExist = false;

        constructor(
        private _translate: TranslateService,
        private formBuilder: FormBuilder,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _skema: SkemaService,
        private _toastr: ToastrService,
        private _router: Router,
        public _dialogRef: MatDialogRef<SenaraiSkemaTetDialogComponent>,
        public snackBar: MatSnackBar,
        @Inject(MAT_DIALOG_DATA) public data: any
        ) { 
            this._translate.get('SKEMA.ERROR').subscribe((res: string) => {
                this.existTxt = res;
            });
            this._translate.get('SKEMA.SUCCESS').subscribe((res: string) => {
                this.successTxt = res;
            });
            this.fuseTranslationLoader.loadTranslations(malay, english);

            this.skemaFormErrors = {
                name : {},
                keterangan: {},
                seniority  : {}
            };
        }
        

        ngOnInit(){

            this._id = this.data.id;
            

            this.skemaForm = this.formBuilder.group({
                name    : ['', [Validators.required]],
                keterangan    : ['', [Validators.required]],
                seniority    : ['', Validators.required]
            });

            this.skemaForm.valueChanges.subscribe(() => {
                this.onSkemaFormValuesChanged();
            });

            if (this._id){

                this._skema.getSchemaCnEdit().subscribe(
                    data=>{
                        this.skemaCn = data;
                    }
                )
                this._translate.get('SKEMA.DETAILS').subscribe((res: string) => {
                    this.titleTxt = res;
                    this._skema.getSchemaById(this._id).subscribe(
                        skema=>{
                            this.skemaForm.setValue({
                                name: skema.name,
                                keterangan: skema.keterangan,
                                seniority: skema.seniority
                            });
                        }
                    )
                });

                

            }else{
                this._skema.getSchemaCn().subscribe(
                    data=>{
                        this.skemaCn = data;
                    }
                )
                this._translate.get('SKEMA.NEW').subscribe((res: string) => {
                    this.titleTxt = res;
                });
            }
            
        }

        onSkemaFormValuesChanged(){
            this.errNameExist = false;
            for ( const field in this.skemaFormErrors )
            {
                if ( !this.skemaFormErrors.hasOwnProperty(field) )
                {
                    continue;
                }
    
                // Clear previous errors
                this.skemaFormErrors[field] = {};
    
                // Get the control
                const control = this.skemaForm.get(field);
    
                if ( control && control.dirty && !control.valid )
                {
                    this.skemaFormErrors[field] = control.errors;
                }
            }
        }

        hantar(): void {

            if(this._id){
                let skema: Skema = new Skema ();
                skema.setName(this.skemaForm.controls['name'].value);
                skema.setKeterangan(this.skemaForm.controls['keterangan'].value);
                skema.setSeniority(this.skemaForm.controls['seniority'].value);
                skema.setId(this._id)
                this._skema.updateSchema(skema).subscribe(
                    success=>{
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                          });
                          this._dialogRef.close()
                    },
                    error=>{

                        var messages = error.error.errorMessage;

                        
                        if(messages == "ERR_EXIST_AGENCY_NAME"){
    
                            this.errNameExist = true;
    
                        }
                        
                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['red-snackbar']
                          });
                        
                    }
                )
            }else{
                let skema: Skema = new Skema ();
                skema.setName(this.skemaForm.controls['name'].value);
                skema.setKeterangan(this.skemaForm.controls['keterangan'].value);
                skema.setSeniority(this.skemaForm.controls['seniority'].value);
                
                this._skema.createSchemas(skema).subscribe(
                    success=>{
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                          });
                          this._dialogRef.close()
                    },
                    error=>{
                        
                        var messages = error.error.errorMessage;

                        
                        if(messages == "ERR_EXIST_AGENCY_NAME"){
    
                            this.errNameExist = true;
    
                        }

                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['red-snackbar']
                          });
                    }
                )
            }

            
            
        }

    }