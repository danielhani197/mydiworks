import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar} from '@angular/material';
import { ToastrService } from 'ngx-toastr';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';

import { Skema } from 'app/main/content/model/general/skema';
import { SkemaService } from 'app/main/content/services/general/skema.service';

@Component({
    selector: 'senarai-skema-del-dialog',
    templateUrl: 'senarai-skema-del-dialog.component.html',
    animations : fuseAnimations
    })
    export class SenaraiSkemaDelDialogComponent implements OnInit
    {
        _id: string;

        successTxt: string;
        existTxt: string;

        constructor(
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _skema: SkemaService,
        private _toastr: ToastrService,
        private _router: Router,
        public _dialogRef: MatDialogRef<SenaraiSkemaDelDialogComponent>,
        public snackBar: MatSnackBar,
        @Inject(MAT_DIALOG_DATA) public data: any
        ) { 
            this._translate.get('SKEMA.DELETESKEMA.ERROR').subscribe((res: string) => {
                this.existTxt = res;
            });
            this._translate.get('SKEMA.DELETESKEMA.SUCCESS').subscribe((res: string) => {
                this.successTxt = res;
            });
            this.fuseTranslationLoader.loadTranslations(malay, english);
        }
        

        ngOnInit(){
            this._id = this.data.id;
        }
        confirmDelete(): void {
            
            this._skema.deleteSchemaById(this._id).subscribe(
                success=>{
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                      });
                      this._dialogRef.close()
                },
                error=>{
                    
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                      });
                      this._dialogRef.close()
                }
            )
            
        }

    }