import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { 
    MatButtonModule, 
    MatCheckboxModule, 
    MatIconModule, 
    MatFormFieldModule, 
    MatInputModule,
    MatChipsModule,
    MatMenuModule,
    MatSelectModule, 
    MatStepperModule,
    MatDialogModule,
    MatSnackBarModule
} from '@angular/material';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { SenaraiSkemaComponent } from './senarai-skema.component';
import { SenaraiSkemaDelDialogComponent } from './senarai-skema-del-dialog.component';
import { SenaraiSkemaTetDialogComponent} from './senarai-skema-tet-dialog.component';

const routes = [
    {
        path     : 'list',
        component: SenaraiSkemaComponent
    }
];

@NgModule({
    declarations: [
        SenaraiSkemaComponent,
        SenaraiSkemaDelDialogComponent,
        SenaraiSkemaTetDialogComponent
    ],
    entryComponents: [
        SenaraiSkemaDelDialogComponent,
        SenaraiSkemaTetDialogComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatMenuModule,
        MatSelectModule, 
        MatStepperModule,
        MatDialogModule,
        MatSnackBarModule,

        NgxDatatableModule,

        FuseSharedModule,
    ],
    exports: [

    MatDialogModule,

    ]
})
export class SenaraiSkemaModule
{
}
