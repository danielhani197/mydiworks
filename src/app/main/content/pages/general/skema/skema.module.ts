import { NgModule } from '@angular/core';

import { SenaraiSkemaModule } from './senarai-skema/senarai-skema.module';

@NgModule({
    imports: [
        
        SenaraiSkemaModule
        
    ]
})
export class SkemaModule
{

}