import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MatButtonModule, MatFormFieldModule, MatInputModule, MatSnackBarModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { FuseForgotPassword2Component } from './forgot-password-2.component';
import { TranslateModule } from '@ngx-translate/core';

const routes = [
    {
        path     : 'reset',
        component: FuseForgotPassword2Component
    }
];

@NgModule({
    declarations: [
        FuseForgotPassword2Component
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,

        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatSnackBarModule, 

        FuseSharedModule,
    ]
})
export class ForgotPassword2Module
{
}
