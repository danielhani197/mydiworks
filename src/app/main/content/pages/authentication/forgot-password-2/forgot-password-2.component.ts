import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material'; 

import { locale as english } from './i18n/en';
import { locale as malay } from './i18n/my';

import { User } from 'app/main/content/model/user';

import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service'
import { JawapanKeselamatan } from 'app/main/content/model/setting/jawapanKeselamatan';
import { JawapanKeselamatanService } from 'app/main/content/services/setting/jawapanKeselamatan.service'
import { ToastrService } from 'ngx-toastr';

@Component({
    selector   : 'fuse-forgot-password-2',
    templateUrl: './forgot-password-2.component.html',
    styleUrls  : ['./forgot-password-2.component.scss'],
    animations : fuseAnimations
})
export class FuseForgotPassword2Component implements OnInit
{
    userid: string;
    //user: User;
    id1: string;
    id2: string;
    id3: string;
    id4: string;
    id5: string;
    answer : JawapanKeselamatan[] = new Array();

    soalan1: string;
    soalan2: string;
    soalan3: string;
    soalan4: string;
    soalan5: string;

    forgotPasswordForm: FormGroup;
    soalanForm: FormGroup;
    forgotPasswordFormErrors: any;
    soalanFormErrors: any;
    successTxt: string;
    existTxt: string;
    ansTxt: string;

    ls: JawapanKeselamatan[];

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private _authentication: AuthenticationService,
        private _router: Router,
        public snackBar: MatSnackBar, 
        private route: ActivatedRoute,
        private translate: TranslateService,
        private jawapanKeselamatanService: JawapanKeselamatanService,
        private _toastr: ToastrService,

    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);

        this.fuseConfig.setConfig({
            layout: {
                navigation: 'none',
                toolbar   : 'none',
                footer    : 'none'
            }
        });

        this.forgotPasswordFormErrors = {
            email: {}
        };

        this.soalanFormErrors = {
            jawapan: {},
            jawapan2: {},
            jawapan3: {},
            jawapan4: {},
            jawapan5: {},

        };

        this.translate.get('RESET.EXIST').subscribe((res: string) => {
            this.existTxt = res;
        });
        this.translate.get('RESET.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
        this.translate.get('RESET.ANS').subscribe((res: string) => {
            this.ansTxt = res;
        });
    }

    ngOnInit()
    {
        this.forgotPasswordForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
        
        this.soalanForm = this.formBuilder.group({
            jawapan: ['', Validators.required],
            jawapan2: ['', Validators.required],
            jawapan3: ['', Validators.required],
            jawapan4: ['', Validators.required],
            jawapan5: ['', Validators.required],

        });

        this.forgotPasswordForm.valueChanges.subscribe(() => {
            this.onForgotPasswordFormValuesChanged();
        });

        this.soalanForm.valueChanges.subscribe(() => {
            this.onSoalanFormValuesChanged();
        });
    }

    checkEmail(){

        let user: User = new User ();
        user.setEmail(this.forgotPasswordForm.controls['email'].value);      
        
        this._authentication.checkEmail(user).subscribe(
            data => {
                
                this.ls = data;
                this.soalan1 = data[0].soalanKeselamatan.keterangan;
                this.soalan2 = data[1].soalanKeselamatan.keterangan;
                this.soalan3 = data[2].soalanKeselamatan.keterangan;
                this.soalan4 = data[3].soalanKeselamatan.keterangan;
                this.soalan5 = data[4].soalanKeselamatan.keterangan;

                this.userid = data[0].user.id;

                this.id1 = this.ls[0].id;
                this.id2 = this.ls[1].id;
                this.id3 = this.ls[2].id;
                this.id4 = this.ls[3].id;
                this.id5 = this.ls[4].id;

                document.getElementById('ifYes').style.display = 'block';
                document.getElementById('emailValid').style.display = 'block';
                document.getElementById('cekEmail').style.display = 'none';
                document.getElementById('emailuser').style.display = 'none';
                document.getElementById('btnLogin').style.display = 'none';

                },
            
            error => {
                document.getElementById('ifYes').style.display = 'none';
                document.getElementById('cekEmail').style.display = 'block';
                this._toastr.error(this.existTxt, null, {
                    positionClass: 'toast-top-right'
                });
            });
        }

    forgot() {
  
        let jawapan: JawapanKeselamatan = new JawapanKeselamatan ();
        let jawapan2: JawapanKeselamatan = new JawapanKeselamatan ();
        let jawapan3: JawapanKeselamatan = new JawapanKeselamatan ();
        let jawapan4: JawapanKeselamatan = new JawapanKeselamatan ();
        let jawapan5: JawapanKeselamatan = new JawapanKeselamatan ();

        //set value jawapan
        jawapan.setJawapan(this.soalanForm.controls['jawapan'].value);
        jawapan2.setJawapan(this.soalanForm.controls['jawapan2'].value);
        jawapan3.setJawapan(this.soalanForm.controls['jawapan3'].value);
        jawapan4.setJawapan(this.soalanForm.controls['jawapan4'].value);
        jawapan5.setJawapan(this.soalanForm.controls['jawapan5'].value);

        //get value id jawapan
        jawapan.setId(this.id1);
        jawapan2.setId(this.id2);
        jawapan3.setId(this.id3);
        jawapan4.setId(this.id4);
        jawapan5.setId(this.id5);

        //push array jawapan
        this.answer.push(jawapan);
        this.answer.push(jawapan2);
        this.answer.push(jawapan3);
        this.answer.push(jawapan4);
        this.answer.push(jawapan5);

        this._authentication.forgot(this.answer, this.userid).subscribe(
            success => {
                
                this.snackBar.open(this.successTxt, "OK", { 
                    panelClass: ['blue-snackbar'] 
                }); 
                this._router.navigate(['login/']);

            },
            error => {
                this._toastr.error(this.ansTxt, null, {
                    positionClass: 'toast-top-right'
                }); 
            });
    }

    onForgotPasswordFormValuesChanged()
    {
        for ( const field in this.forgotPasswordFormErrors )
        {
            if ( !this.forgotPasswordFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.forgotPasswordFormErrors[field] = {};

            // Get the control
            const control = this.forgotPasswordForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.forgotPasswordFormErrors[field] = control.errors;
            }
        }
    }

    onSoalanFormValuesChanged()
    {
        for ( const field in this.soalanFormErrors )
        {
            if ( !this.soalanFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.soalanFormErrors[field] = {};

            // Get the control
            const control = this.soalanForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.soalanFormErrors[field] = control.errors;
            }
        }
    }
}
