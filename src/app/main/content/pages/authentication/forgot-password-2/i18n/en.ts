export const locale = {
    lang: 'en',
    data: {
        'RESET': {
            'FORGOT': 'FORGOT PASSWORD?',
            'EXIST': 'Email is invalid.',
            'ANS': 'Answer in incomplete. Please answer correctly with at least 3',
            'SUCCESS': 'Thank You. Please check your email for your temporary password.',
            'LOGIN': 'Login',
            'PLACEHOLDER': 'Email',
            'WRONG': 'Email is required',
            'WRONG1': 'Email is incorrect',
            'WRONG2': 'Answer is required',
            'WRONG3': 'Please insert your answer correctly'

            ,
            'BTN' : {
                'RESET' : 'RESET PASSWORD'
            }
        }
    }
};
