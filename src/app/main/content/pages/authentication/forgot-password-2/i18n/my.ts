export const locale = {
    lang: 'my',
    data: {
        'RESET': {
            'FORGOT': 'Lupa Kata Laluan?',
            'EXIST': 'Email tidak wujud.',
            'ANS': 'Jawapan tidak lengkap. Perlu betul sekurangnya 3 soalan',
            'SUCCESS': 'Terima Kasih. Sila periksa emel anda untuk kata laluan sementara.',
            'LOGIN': 'Log Masuk',
            'PLACEHOLDER': 'Emel',
            'WRONG': 'Emel diperlukan',
            'WRONG1': 'Sila masukkan alamat emel yang sah',
            'WRONG2': 'Jawapan diperlukan',
            'WRONG3': 'Sila masukkan jawapan dengan betul'


            ,
            'BTN' : {
                'RESET' : 'SET SEMULA KATA LALUAN'
            }
        }
    }
};