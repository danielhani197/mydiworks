import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LogoutComponent } from './logout.component';
import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path     : 'logout',
        component: LogoutComponent
    }
];

@NgModule({
    declarations: [
        LogoutComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        FuseSharedModule
    ]
})
export class LogoutModule
{
}
