import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";

import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service'
import { FuseConfigService } from "@fuse/services/config.service";

@Component({
    selector   : 'logout',
    templateUrl: './logout.component.html',
    styleUrls  : ['./logout.component.scss']
})

export class LogoutComponent implements OnInit {
    
    constructor(private _router: Router,
        private _authentication: AuthenticationService,
        private fuseConfig: FuseConfigService,) {
            this.fuseConfig.setConfig({
                layout: {
                    navigation: 'none',
                    toolbar   : 'none',
                    footer    : 'none',
                    chat     : 'none',
                }
            });
    }

    ngOnInit(): void {
        // reset login status
        this._authentication.logout();
        //this._router.navigate(['/login']).then(()=>{window.location.reload()});
        this._router.navigate(['/login']);
    }
}