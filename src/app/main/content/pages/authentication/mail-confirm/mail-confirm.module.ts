import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MatIconModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { FuseMailConfirmComponent } from './mail-confirm.component';
import { TranslateModule } from '@ngx-translate/core';

const routes = [
    {
        path     : 'mailConfirm/:email',
        component: FuseMailConfirmComponent
    }
];

@NgModule({
    declarations: [
        FuseMailConfirmComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatIconModule,

        FuseSharedModule,
        TranslateModule
    ]
})
export class MailConfirmModule
{
}
