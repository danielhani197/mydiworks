export const locale = {
    lang: 'my',
    data: {
        'MAIL': {
            'THANK': 'Terima Kasih kerana mendaftar',
            'CONFIRM': 'Emel pengesahan akan dihantar kepada ',
            'INBOX': 'Kata laluan sementara akan dihantar selepas Pentadbir meluluskan permohonan anda',
            'BACK': 'Kembali ke halaman log masuk',
        }
    }
};