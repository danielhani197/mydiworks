export const locale = {
    lang: 'en',
    data: {
        'MAIL': {
            'THANK': 'Thank You for Registration',
            'CONFIRM': 'A confirmation e-mail will be sent to ',
            'INBOX': 'Temporary password will be sent to your email address, once your application approved by Administrator',
            'BACK': 'Go back to login page',
        }
    }
};
