import { Component } from '@angular/core';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as malay } from './i18n/my';

import { ActivatedRoute } from "@angular/router";

@Component({
    selector   : 'fuse-mail-confirm',
    templateUrl: './mail-confirm.component.html',
    styleUrls  : ['./mail-confirm.component.scss'],
    animations : fuseAnimations
})
export class FuseMailConfirmComponent
{
    email: string;

    constructor(
        private fuseConfig: FuseConfigService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private route: ActivatedRoute
    )
    {
        this.fuseConfig.setConfig({
            layout: {
                navigation: 'none',
                toolbar   : 'none',
                footer    : 'none',
                chat     : 'none',
            }
        });

        this.fuseTranslationLoader.loadTranslations(malay, english);

        this.email = this.route.snapshot.paramMap.get('email');
    }
}
