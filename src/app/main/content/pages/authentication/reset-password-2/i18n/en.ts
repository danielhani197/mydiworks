export const locale = {
    lang: 'en',
    data: {
        'RESETPWD': {
            'RESET': 'Update Password',
            'EXIST': 'Email is invalid.',
            'SUCCESS': 'Thank You. Your new password has changed.',
            'LOGIN': 'Login',
            'PLACEHOLDER': 'Password',
            'WRONG1': 'Password confirmation is required'

            ,
            'BTN' : {
                'RESET' : 'RESET PASSWORD'
            }
        }
    }
};
