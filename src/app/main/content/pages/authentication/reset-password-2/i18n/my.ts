export const locale = {
    lang: 'my',
    data: {
        'RESETPWD': {
            'RESET': 'Kemaskini Kata Laluan',
            'EXIST': 'Email tidak wujud.',
            'SUCCESS': 'Terima Kasih. Kata laluan baru telah ditukar.',
            'LOGIN': 'Log Masuk',
            'PLACEHOLDER': 'Kata Laluan',
            'WRONG': 'Pengesahan Kata Laluan diperlukan'

            ,
            'BTN' : {
                'RESET' : 'SET SEMULA KATA LALUAN'
            }
        }
    }
};