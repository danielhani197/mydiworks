import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material'; 

import { locale as english } from './i18n/en';
import { locale as malay } from './i18n/my';

import { User } from 'app/main/content/model/user';

import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service'
import { ToastrService } from 'ngx-toastr';

@Component({
    selector   : 'fuse-reset-password-2',
    templateUrl: './reset-password-2.component.html',
    styleUrls  : ['./reset-password-2.component.scss'],
    animations : fuseAnimations
})
export class FuseResetPassword2Component implements OnInit
{
    id: string;

    resetPasswordForm: FormGroup;
    resetPasswordFormErrors: any;
    successTxt: string;
    existTxt: string;

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private _authentication: AuthenticationService,
        private _router: Router,
        private route: ActivatedRoute,
        public snackBar: MatSnackBar, 
        private translate: TranslateService,
        private _toastr: ToastrService,
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);

        this.fuseConfig.setConfig({
            layout: {
                navigation: 'none',
                toolbar   : 'none',
                footer    : 'none',
                chat     : 'none',
            }
        });

        this.resetPasswordFormErrors = {
            password       : {}
        };

        this.translate.get('RESETPWD.EXIST').subscribe((res: string) => {
            this.existTxt = res;
        });
        this.translate.get('RESETPWD.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
    }

    ngOnInit()
    {
        this.resetPasswordForm = this.formBuilder.group({
            password       : ['', Validators.required],
        });

        this.resetPasswordForm.valueChanges.subscribe(() => {
            this.onResetPasswordFormValuesChanged();
        });

        this.id = this.route.snapshot.paramMap.get('id');
        this._authentication.getUser(this.id).subscribe(
            data => {
                var user = user.id;
            }
        );
    }

    res() {
        
    if (this.id) {

        let user: User = new User ();
        user.setOldPassword(this.resetPasswordForm.controls['password'].value);
        user.setId(this.id);

                this._authentication.res(user).subscribe(
                    success => {
                        this.snackBar.open(this.successTxt, "OK", { 
                            panelClass: ['blue-snackbar'] 
                            });
                        this._router.navigate(['user/list/']);
                    },
                    error => {
                        this._toastr.error(this.existTxt, null, {
                            positionClass: 'toast-top-right'
                        });
                    }
                );
            }
    }

    onResetPasswordFormValuesChanged()
    {
        for ( const field in this.resetPasswordFormErrors )
        {
            if ( !this.resetPasswordFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.resetPasswordFormErrors[field] = {};

            // Get the control
            const control = this.resetPasswordForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.resetPasswordFormErrors[field] = control.errors;
            }
        }
    }
}

function confirmPassword(control: AbstractControl)
{
    if ( !control.parent || !control )
    {
        return;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return;
    }

    if ( passwordConfirm.value === '' )
    {
        return;
    }

    if ( password.value !== passwordConfirm.value )
    {
        return {
            passwordsNotMatch: true
        };
    }
}
