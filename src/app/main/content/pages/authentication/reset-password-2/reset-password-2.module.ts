import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MatButtonModule, MatFormFieldModule, MatInputModule, MatSnackBarModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { FuseResetPassword2Component } from './reset-password-2.component';
import { TranslateModule } from '@ngx-translate/core';

const routes = [
    {
        path     : 'auth/reset-password-2',
        component: FuseResetPassword2Component
    },
    {
        path     : 'auth/reset-password-2/:id',
        component: FuseResetPassword2Component
    }
];

@NgModule({
    declarations: [
        FuseResetPassword2Component
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,

        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatSnackBarModule, 

        FuseSharedModule
    ]
})
export class ResetPassword2Module
{
}
