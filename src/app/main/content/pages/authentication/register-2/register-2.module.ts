import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { 
    MatButtonModule, 
    MatCheckboxModule, 
    MatFormFieldModule, 
    MatInputModule,
    MatSelectModule,
    MatStepperModule 
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { FuseRegister2Component } from './register-2.component';

import { TranslateModule } from '@ngx-translate/core';

const routes = [
    {
        path     : 'register',
        component: FuseRegister2Component
    }
];

@NgModule({
    declarations: [
        FuseRegister2Component,
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,

        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,

        FuseSharedModule
    ]
})
export class Register2Module
{
}
