import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormControl, ValidationErrors } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as malay } from './i18n/my';
import { locale as userEnglish } from 'app/main/content/pages/pengguna/i18n/en';
import { locale as userMalay } from 'app/main/content/pages/pengguna/i18n/my';
import { locale as secEnglish } from 'app/main/content/pages/setting/i18n/en';
import { locale as secMalay } from 'app/main/content/pages/setting/i18n/my';

import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

import { Router, ActivatedRoute } from "@angular/router";
import { MatDialog} from '@angular/material';

import { User } from 'app/main/content/model/user';

import { of, empty, Observable } from 'rxjs';

import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service'
import { Kementerian } from 'app/main/content/model/general/kementerian';
import { KementerianService } from 'app/main/content/services/agency/kementerian.service';
import { AgensiService } from 'app/main/content/services/agency/agensi.service';
import { PermohonanPengguna } from 'app/main/content/model/permohonan-pengguna';
import { Agensi } from 'app/main/content/model/general/agensi';
import { PenggunaService } from 'app/main/content/services/pengguna/pengguna.service'

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TermViewLoginComponent } from 'app/main/content/common/term-view-login.component';
import { TermService } from 'app/main/content/services/setting/term.service';
import { SoalanKeselamatanService } from 'app/main/content/services/setting/soalanKeselamatan.service';
import { SoalanKeselamatan } from '../../../model/setting/soalanKeselamatan';
import { JawapanKeselamatanService } from '../../../services/setting/jawapanKeselamatan.service';
import { JawapanKeselamatanTemp } from '../../../model/setting/jawapanKeselamatanTemp';

@Component({
    selector   : 'fuse-register-2',
    templateUrl: './register-2.component.html',
    styleUrls  : ['./register-2.component.scss'],
    animations : fuseAnimations,
    //host: { '[@slideInBottom]': '' }
})
export class FuseRegister2Component implements OnInit
{
    registerForm: FormGroup;
    registerFormErrors: any;
    successTxt: string;
    existTxt: string;
    kementerianLs: Kementerian[];
    agensiLs: Agensi[];

    errUsernameExist: boolean = false;
    errEmailExist: boolean = false;

    agencyExist: boolean = false;

    // Vertical Stepper
    termForm: FormGroup;
    verticalStepperStep2: FormGroup;
    termFormErrors: any;
    verticalStepperStep2Errors: any;

    syarat: any;
    soalanLs: any[];
    selectedQuestion: any[] = new Array();
    jawapanLs : JawapanKeselamatanTemp[] = new Array();

    //testid: string = '6e8d43311d13499da57205599f66bbeb'

    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private _authentication: AuthenticationService,
        private _toastr: ToastrService,
        private translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _kementerian: KementerianService,
        private _agensi: AgensiService,
        private _pengguna: PenggunaService,
        private http: HttpClient,
        private _router: Router,
        private _dialog: MatDialog,
        public _term: TermService,
        public _soalanKeselamatan: SoalanKeselamatanService,
        private _jawapanKeselamatan : JawapanKeselamatanService,
        
    )

    {
        this.fuseTranslationLoader.loadTranslations(
            malay, english, 
            userMalay, userEnglish,
            secMalay, secEnglish
        );

        this.fuseConfig.setConfig({
            layout: {
                navigation: 'none',
                toolbar   : 'none',
                footer    : 'none',
                chat     : 'none',
            }
        });

        this.registerFormErrors = {
            name           : {},
            email          : {},
            tujuan          :{},
            username          : {},
            kementerian          : {},
            agensi : {},
            
        };

        this.translate.get('REG.EXIST').subscribe((res: string) => {
            this.existTxt = res;
        });
        this.translate.get('REG.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });

        // Vertical Stepper form error
        this.termFormErrors = {
            term: {},
        };

        this.verticalStepperStep2Errors = {
            address: {}
        };

    }

    ngOnInit()
    {
        this.registerForm = this.formBuilder.group({
            name           : ['', Validators.required],
            email          : ['', [Validators.required, Validators.email]],
            tujuan          : ['', [Validators.required]],
            username           : ['', [
                                        Validators.required, 
                                        Validators.pattern("^[0-9]*$"),
                                        Validators.maxLength(12),
                                        Validators.minLength(12)
                                      ]
                                ],
            kementerian           : ['', Validators.required],
            agensi : [''],
            
        });

        this.registerForm.valueChanges.subscribe(() => {
            this.onRegisterFormValuesChanged();
        });

        // this.termForm.valueChanges.subscribe(() => {
        //     this.onRegisterFormValuesChanged();
        // });

        this._kementerian.getKementerians().subscribe(
            data => {
                this.kementerianLs = data;
            }
        )     

        // Vertical Stepper form stepper
        this.termForm = this.formBuilder.group({
            term : ['', Validators.required],
        });

        this.verticalStepperStep2 = this.formBuilder.group({
            address: ['', Validators.required]
        });

        this._term.latersTerm().subscribe(
            term => {
                this.syarat = term.syarat;
            }
        )
    }

    onChangeKemnterian(id: string){
        if(id){
            this._agensi.getAllAgenciesByMinistryId(id).subscribe(
                data => {
                    this.agensiLs = data;
                    if(this.agensiLs.length > 0){
                        this.agencyExist = true;
                    }else{
                        this.agencyExist = false;
                    }
                }
            )
        }else{
            this.agencyExist = false;
        }
    }

    onRegisterFormValuesChanged()
    {
        for ( const field in this.registerFormErrors )
        {
            if ( !this.registerFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.registerFormErrors[field] = {};

            // Get the control
            const control = this.registerForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.registerFormErrors[field] = control.errors;
            }
        }
    }

    signup(){

        this.resetCustomError();

        let user: PermohonanPengguna = new PermohonanPengguna ();
        user.setUsername(this.registerForm.controls['username'].value);
        user.setName(this.registerForm.controls['name'].value);
        user.setEmail(this.registerForm.controls['email'].value);
        user.setTujuan(this.registerForm.controls['tujuan'].value)

        let objKem: Kementerian = new Kementerian ();
        objKem.setId(this.registerForm.controls['kementerian'].value);
        user.setKementerian(objKem);

        var agencyId = this.registerForm.controls['agensi'].value;
        if(agencyId){
            let objAgensi: Agensi = new Agensi ();
            objAgensi.setId(agencyId);
            user.setAgensi(objAgensi);
        }
        
        this._authentication.signUp(user).subscribe(
            user => {

                this._toastr.success(this.successTxt, null, {
                    positionClass: 'toast-top-right'
                });
                this._router.navigate(['mailConfirm/'+user.email]);
                
            },
            error => {

                var message = error.error.errorMessage;
                
                if(message == "ERR_EXIST_EMAIL_USERNAME"){
                    this.errUsernameExist = true;
                    this.errEmailExist = true;
                }else if(message == "ERR_EXIST_EMAIL"){
                    this.errEmailExist = true;
                }else if(message == "ERR_EXIST_USERNAME"){
                    this.errUsernameExist = true;
                }

                this._toastr.error(this.existTxt, null, {
                    positionClass: 'toast-top-right'
                });
            }
        );
    }

    resetCustomError(){
        this.errUsernameExist = false;
        this.errEmailExist = false;
    }

    view(){
        let dialogRef = this._dialog.open(TermViewLoginComponent, {
            width: '900px',
        });
    }
}

