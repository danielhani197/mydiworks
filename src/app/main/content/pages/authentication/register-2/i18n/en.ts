export const locale = {
    lang: 'en',
    data: {
        'REG': {
            'EXIST': 'User already exist.',
            'SUCCESS': 'Thank You. Please check your email for completion of registration.',
            'REGISTER': 'Sign Up',
            'LOGIN': 'Log In',
            'TERM' : 'By clicking Sign Up you agree to our ',
            'TERMLINK' : 'Terms & Conditions',
            'ACC': {
                'EXIST': 'Already have an account?'
            },
            'BTN': {
                'SIGNUP': 'Sign Up',
                'NEXT': 'Next',
                'PREVIOUS': 'Previous'
            },
            'STEP1' : 'Please fill in User Details',
            'STEP2' : 'Please fill in Term & Condition',
            'STEP3' : 'Please fill in Security Answers',
            'STEP4' : 'Done',
            'THANK' : 'Thank You for Registration',
            'AGREE' : 'I am agree with this term & conditions',
            'ERROR' : 'Data has been unsuccessfully saved. There is error/s in your form.'
        }
    }
};
