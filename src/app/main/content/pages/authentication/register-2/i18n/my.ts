export const locale = {
    lang: 'my',
    data: {
        'REG': {
            'EXIST': 'Pengguna telah wujud.',
            'SUCCESS': 'Terima Kasih. Untuk melengkapkan pendaftaran anda, sila periksa emel anda.',
            'REGISTER': 'Daftar',
            'LOGIN': 'Log Masuk',
            'TERM' : 'Sekiranya anda klik pada butang Daftar, anda setuju dengan ',
            'TERMLINK' : 'Terma & Syarat',
            'ACC': {
                'EXIST': 'Akaun telah wujud?'
            },
            'BTN': {
                'SIGNUP': 'Daftar',
                'NEXT': 'Seterusnya',
                'PREVIOUS': 'Sebelumnya'
            },
            'STEP1' : 'Sila isi Maklumat Peribadi',
            'STEP2' : 'Sila isi Terma & Syarat',
            'STEP3' : 'Sila isi Soalan Keselamatan',
            'STEP4' : 'Selesai',
            'THANK' : 'Terima Kasih atas pendaftaran',
            'AGREE' : 'Saya setuju dengan terma & syarat tersebut.',
            'ERROR' : 'Data tidak berjaya disimpan. Sila semak ralat yang berkenaan.'
            
        }
    }
};
