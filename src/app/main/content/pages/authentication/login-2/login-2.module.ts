import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { 
    MatButtonModule, 
    MatCheckboxModule, 
    MatFormFieldModule, 
    MatInputModule,
    MatDialogModule 
} 
    from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { FuseLogin2Component } from './login-2.component';
import { TranslateModule } from '@ngx-translate/core';

const routes = [
    {
        path     : 'login',
        component: FuseLogin2Component,
    },
];

@NgModule({
    declarations: [
        FuseLogin2Component,
    ], 
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,

        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        MatDialogModule,

        FuseSharedModule
    ]
})
export class Login2Module
{
}
