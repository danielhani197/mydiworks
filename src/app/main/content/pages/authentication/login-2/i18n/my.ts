export const locale = {
    lang: 'my',
    data: {
        'LOGIN': {
            'LOGIN': 'Log Masuk',
            'FORGOT': 'Lupa Katalaluan?',
            'REMEMBER': 'Ingat Saya',
            'WRONG': 'Katanama / Katalaluan yang anda masukkan tidak betul ',
            'TERM' : 'Sekiranya anda klik pada butang Log Masuk, anda setuju dengan ',
            'TERMLINK' : 'Terma & Syarat',
            'ACC': {
                'DONT': 'Tiada akaun?',
                'CREATE': 'Daftar Baru'
            },
            'BTN' : {
                'LOGIN' : 'Log Masuk'
            },
            'SESSION': 'Sesi anda telah tamat',
            'EXPIRED': 'Sesi anda telah tamat. Sila masukkan kata laluan untuk meneruskan sesi ini.',
            'UNLOCK': 'Log Masuk',
            'AREYOU': 'Anda bukan',
        }
    }
};