export const locale = {
    lang: 'en',
    data: {
        'LOGIN': {
            'LOGIN': 'Log In',
            'FORGOT': 'Forgot Password?',
            'REMEMBER': 'Remember Me',
            'WRONG': ' Incorrect username/password',
            'TERM' : 'By clicking Login you agree to our ',
            'TERMLINK' : 'Terms & Conditions',
            'ACC': {
                'DONT': 'Don\'t have an account?',
                'CREATE': 'Create an account'
            },
            'BTN' : {
                'LOGIN' : 'Log In'
            },
            'SESSION': 'Your Session Is Locked',
            'EXPIRED': 'Due to inactivity, your session is locked. Enter your password to continue.',
            'UNLOCK': 'Unlock',
            'AREYOU': 'Are You Not',
        }
    }
};
