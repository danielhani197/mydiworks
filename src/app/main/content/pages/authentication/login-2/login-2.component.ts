import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog} from '@angular/material';
import { TermViewLoginComponent } from 'app/main/content/common/term-view-login.component';

import { locale as english } from './i18n/en';
import { locale as malay } from './i18n/my';
import { locale as userEnglish } from 'app/main/content/pages/pengguna/i18n/en';
import { locale as userMalay } from 'app/main/content/pages/pengguna/i18n/my';

import { User } from 'app/main/content/model/user';

import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service';

@Component({
    selector   : 'fuse-login-2',
    templateUrl: './login-2.component.html',
    styleUrls  : ['./login-2.component.scss'],
    animations : fuseAnimations
})
export class FuseLogin2Component implements OnInit
{
    loginForm: FormGroup;
    loginFormErrors: any;

    existTxt: string;

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private _authentication: AuthenticationService,
        private _router: Router,
        private _toastr: ToastrService,
        private _translate: TranslateService,
        public _dialog: MatDialog
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english, userEnglish, userMalay);

        this.fuseConfig.setConfig({
            layout: {
                navigation: 'none',
                toolbar   : 'none',
                footer    : 'none',
                chat     : 'none',
            }
        });

        this.loginFormErrors = {
            username   : {},
            password: {}
        };

        this._translate.get('LOGIN.WRONG').subscribe((res: string) => {
            this.existTxt = res;
        });
    }

    ngOnInit()
    {
        this.loginForm = this.formBuilder.group({
            username   : ['', [Validators.required]],
            password: ['', Validators.required]
        });

        this.loginForm.valueChanges.subscribe(() => {
            this.onLoginFormValuesChanged();
        });
    }

    login(){
        let user: User = new User();
        user.setUsername(this.loginForm.controls['username'].value);
        user.setOldPassword(this.loginForm.controls['password'].value);

        this._authentication.login(user).subscribe(
            success => {
                this._authentication.getUserDetail().subscribe(
                    data => {
                        this._authentication.setCurrentUser(data);
                        var userIsTemp = data.firstLogin;
                        var roles = data.authorities;
                        var isSam = false;

                        for(let roleObj of roles){
                            let role = roleObj.authority;
                            if(role === 'ROLE_SUPERADMIN'){
                                isSam = true;
                                break;
                            }
                        }

                        if(isSam){
                            this._router.navigate(['/wall']).then(()=>{window.location.reload();});
                        }else if(userIsTemp){
                            this._router.navigate(['/account/update']).then(()=>{window.location.reload();});
                        }else{
                            this._router.navigate(['/wall']).then(()=>{window.location.reload();});
                            
                        }
                        
                    }
                );
            },
            error => {
                console.log(error)
                this._toastr.error(this.existTxt, null, {
                    positionClass: 'toast-top-right'
                });
            }
        )
    }

    view(){
        
        let dialogRef = this._dialog.open(TermViewLoginComponent, {
            width: '900px',
           
        });
    }

    onLoginFormValuesChanged()
    {
        for ( const field in this.loginFormErrors )
        {
            if ( !this.loginFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.loginFormErrors[field] = {};

            // Get the control
            const control = this.loginForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.loginFormErrors[field] = control.errors;
            }
        }
    }
}
