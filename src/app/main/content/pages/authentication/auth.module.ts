import { NgModule } from '@angular/core';

import { Register2Module } from './register-2/register-2.module';
import { Login2Module } from './login-2/login-2.module';
import { ForgotPassword2Module } from './forgot-password-2/forgot-password-2.module';
import { LogoutModule } from './logout/logout.module';
import { ResetPassword2Module } from './reset-password-2/reset-password-2.module';
import { MailConfirmModule } from './mail-confirm/mail-confirm.module';
import { LockModule } from './lock/lock.module';


@NgModule({
    imports: [
        // Auth
        Register2Module,
        Login2Module,
        ForgotPassword2Module,
        LogoutModule,
        ResetPassword2Module,
        MailConfirmModule,
        LockModule
        
    ]
})
export class AuthModule
{

}
