import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { User } from '../../../model/user';
import { RouterConfigLoader } from '@angular/router/src/router_config_loader';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';

import { locale as english } from '../login-2/i18n/en';
import { locale as malay } from '../login-2/i18n/my';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

@Component({
    selector   : 'fuse-lock',
    templateUrl: './lock.component.html',
    styleUrls  : ['./lock.component.scss'],
    animations : fuseAnimations
})
export class FuseLockComponent implements OnInit
{
    lockForm: FormGroup;
    lockFormErrors: any;
    currentUser: User;

    existTxt: string;

    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private _auth: AuthenticationService,
        private _router: Router,
        private _translate: TranslateService,
        private _toastr: ToastrService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
    )
    {
        this.fuseConfig.setConfig({
            layout: {
                navigation: 'none',
                toolbar   : 'none',
                footer    : 'none',
                chat     : 'none',
            }
        });

        this.fuseTranslationLoader.loadTranslations(malay, english);

        this._translate.get('LOGIN.WRONG').subscribe((res: string) => {
            this.existTxt = res;
        });

        this.lockFormErrors = {
            username: {},
            password: {}
        };

        this.currentUser = _auth.getCurrentUser();
    }

    ngOnInit()
    {
        this.lockForm = this.formBuilder.group({
            username: [
                {
                    value   : this.currentUser.username,
                    disabled: true
                }, Validators.required
            ],
            password: ['', Validators.required]
        });

        this.lockForm.valueChanges.subscribe(() => {
            this.onLockFormValuesChanged();
        });
    }

    login(){
        let user: User = new User();
        user.setUsername(this.lockForm.controls['username'].value);
        user.setOldPassword(this.lockForm.controls['password'].value);

        this._auth.login(user).subscribe(
            success => {
                this._auth.getUserDetail().subscribe(
                    data => {
                        this._auth.setCurrentUser(data);
                        var userIsTemp = data.firstLogin;
                        var roles = data.authorities;
                        var isSam = false;

                        for(let roleObj of roles){
                            let role = roleObj.authority;
                            if(role === 'ROLE_SUPERADMIN'){
                                isSam = true;
                                break;
                            }
                        }

                        if(isSam){
                            this._router.navigate(['/wall']).then(()=>{window.location.reload();});
                        }else if(userIsTemp){
                            this._router.navigate(['/account/update']).then(()=>{window.location.reload();});
                        }else{
                            this._router.navigate(['/wall']).then(()=>{window.location.reload();});
                            
                        }
                        
                    }
                );
            },
            error => {
                console.log(error)
                this._toastr.error(this.existTxt, null, {
                    positionClass: 'toast-top-center'
                });
            }
        )
    }

    onLockFormValuesChanged()
    {
        for ( const field in this.lockFormErrors )
        {
            if ( !this.lockFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.lockFormErrors[field] = {};

            // Get the control
            const control = this.lockForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.lockFormErrors[field] = control.errors;
            }
        }
    }
}
