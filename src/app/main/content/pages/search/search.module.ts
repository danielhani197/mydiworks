import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { 
    MatButtonModule, 
    MatFormFieldModule, 
    MatIconModule, 
    MatInputModule, 
    MatTableModule, 
    MatTabsModule,
    MatPaginatorModule, 
    MatOptionModule,
    MatSelectModule,
    MatChipsModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatDialogModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { SearchService } from 'app/main/content/services/search/search.service';
import { FuseSearchClassicComponent } from './tabs/classic/classic.component';
import { FuseSearchComponent } from './search.component';
import { TranslateModule } from '@ngx-translate/core';
import { DeleteDialogComponent } from '../search/tabs/classic/delete-dialog.component';

const routes = [
    {
        path     : 'search',
        component: FuseSearchComponent
    }
];

@NgModule({
    declarations: [
        FuseSearchComponent,
        FuseSearchClassicComponent,
        DeleteDialogComponent
    ],
    entryComponents: [
        DeleteDialogComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),
        CdkTableModule,
        TranslateModule,

        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatTableModule,
        MatTabsModule,
        MatPaginatorModule,
        MatOptionModule,
        MatSelectModule,
        MatChipsModule,
        MatSidenavModule,
        MatSlideToggleModule,
        MatDialogModule,


        FuseSharedModule
    ],
    providers   : [
        SearchService
    ]
})
export class SearchModule
{
}
