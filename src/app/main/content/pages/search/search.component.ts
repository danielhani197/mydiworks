import { Component, ViewChild } from '@angular/core';
import { FuseSearchClassicComponent } from './tabs/classic/classic.component';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from './i18n/en';
import { locale as malay } from './i18n/my';

@Component({
    selector   : 'fuse-search',
    templateUrl: './search.component.html',
    styleUrls  : ['./search.component.scss']
})
export class FuseSearchComponent
{
    @ViewChild(FuseSearchClassicComponent) _search:FuseSearchClassicComponent;
    constructor(
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
    }

    updateFilter(event: any){
        this._search.updateFilter(event);
    }
}
