import { Component, ViewChild } from '@angular/core';
import { PageEvent, MatSnackBar, MatDialog } from '@angular/material';
import { SearchService } from 'app/main/content/services/search/search.service';
import { Hashtag } from '../../../../model/wall3/hashtag';
import { AdvancedPage } from '../../../../model/util/advanced-page';
import { KementerianService } from '../../../../services/agency/kementerian.service';
import { AgensiService } from '../../../../services/agency/agensi.service';
import { BahagianService } from '../../../../services/agency/bahagian.service';
import { Kementerian } from '../../../../model/general/kementerian';
import { Agensi } from '../../../../model/general/agensi';
import { Bahagian } from '../../../../model/general/bahagian';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';
import { WallKandungan } from 'app/main/content/model/wall3/wallKandungan';
import { MyWallService } from 'app/main/content/services/wall3/myWall.service';
import { DeleteDialogComponent } from '../classic/delete-dialog.component';

@Component({
    selector: 'fuse-search-classic',
    templateUrl: './classic.component.html',
    styleUrls: ['./classic.component.scss']
})
export class FuseSearchClassicComponent {
    @ViewChild('kementerian') kmnt;
    @ViewChild('agensi') agn;
    @ViewChild('bhg') bhg;

    //PAGINATION STUFF !!!! {{ WARNING: HANDLE WITH CARE !!}}
    page = new AdvancedPage();
    length = 100;
    pageSize = 5;
    pageSizeOptions: number[] = [5, 10, 15, 20];
    pageEvent: PageEvent;
    post: Hashtag;

    //FILTER SEARCH STUFF
    kementerianLs: any[];
    agensiLs: any[];
    bahagianLs: any[];
    currentKementerian: any;
    currentAgensi: any;
    currentBahagian: any;
    kementerian: Kementerian;
    agensi: Agensi;
    bahagian: Bahagian;
    isChecked: boolean = false;

    constructor(
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private searchService: SearchService,
        private _kementerian: KementerianService,
        private _agensi: AgensiService,
        private _bahagian: BahagianService,
        private _wall: MyWallService,
        public snackBar: MatSnackBar,
        public _dialog: MatDialog,

    ) {



        this.fuseTranslationLoader.loadTranslations(malay, english);
        this.page.pageNumber = 0;
        this.page.size = 5;

        this._kementerian.getKementerians().subscribe(
            data => {
                this.kementerianLs = data;
                this.reloadData();
            }
        )

    }

    toggle() {
        if (this.isChecked) {
            this.isChecked = false;
            this.agn.value = "";
            this.bhg.value = "";
            this.kmnt.value = "";
            this.page.kementerian = null;
            this.page.agensi = null;
            this.page.bahagian = null;
            this.reloadData();

        } else {
            this.isChecked = true;
        }

    }

    //FILTER BY SEARCH BAR 
    updateFilter(event: any) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;
        this.reloadData();
    }

    //CHANGE PAGE TOP RIGHT
    onPaginateChange(event: any) {
        this.page.size = event.pageSize;
        this.page.pageNumber = event.pageIndex;
        this.reloadData();
    }

    //ONCHANGE KEMENTERIAN SELECTION
    setKementerian(id: any): void {

        if (id) {
            // GET KEMENTERIAN BASED ON ID
            this._kementerian.getKementerianById(id).subscribe(
                kementerian => {
                    this.page.kementerian = kementerian;
                    this.reloadData()
                }
            )

            //load agensi based on kementerian
            this._agensi.getAllAgenciesByMinistryId(id).subscribe(
                agensiLs => {
                    this.agensiLs = agensiLs;
                }
            )
        } else {
            this.agensiLs = null;
            this.bahagianLs = null;
            this.agn.value = "";
            this.bhg.value = "";
            this.page.kementerian = null;
            this.page.agensi = null;
            this.page.bahagian = null;
            this.reloadData();
        }

    }

    //ONCHANGE AGENSI SELECTION
    setAgensi(id: any): void {

        if (id) {
            //GET AGENSI BASED ON ID
            this._agensi.getAgencyById(id).subscribe(
                agensi => {
                    this.page.agensi = agensi;
                    this.reloadData()
                }
            )

            //load bahagian based on agensi
            this._bahagian.getBahagiansByAgensiId(id).subscribe(
                bhgLs => {
                    this.bahagianLs = bhgLs;
                }
            )
        } else {
            this.bahagianLs = null;
            this.bhg.value = "";
            this.page.agensi = null;
            this.page.bahagian = null;
            this.reloadData();
        }

    }

    //ONCHANGE BAHAGIAN SELECTION
    setBahagian(id: any): void {

        if (id) {
            //GET BAHAGIAN BASED ON ID
            this._bahagian.getBahagianById(id).subscribe(
                bhg => {
                    this.page.bahagian = bhg;
                    this.reloadData()
                }
            )

        } else {
            this.page.bahagian = null;
            this.reloadData();
        }
    }

    reloadData() {
        this.searchService.getClassic(this.page).subscribe(
            data => {
                this.post = data.data;
                this.length = data.advancedPage.totalElements;
            }
        )
    }

    deletePost(id: string) {

        let dialogRef = this._dialog.open(DeleteDialogComponent, {
            width: '500px',
            data: { 
                id: id,
            }
        });
        dialogRef.afterClosed().subscribe(
            data=>{
                this.reloadData();
            }
        )
        
    }
}
