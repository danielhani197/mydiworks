import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Router } from "@angular/router";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { MyWallService } from '../../../../services/wall3/myWall.service';

import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';


@Component({
    selector: 'delete-dialog',
    templateUrl: 'delete-dialog.component.html',
    animations : fuseAnimations
    })
    export class DeleteDialogComponent implements OnInit{

    successTxt: string;
    existTxt: string;
    _id: string;

        constructor(
            private _translate: TranslateService,
            private fuseTranslationLoader: FuseTranslationLoaderService,
            private _myWall: MyWallService,
            private _router: Router,
            public snackBar: MatSnackBar,
            public _wall: MyWallService,
            public _dialogRef: MatDialogRef<DeleteDialogComponent>,
            @Inject(MAT_DIALOG_DATA) public data: any
        ){
        this._translate.get('CARIAN.DELETE.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('CARIAN.DELETE.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
        }

        ngOnInit(){
            this._id = this.data.id;
        }

        confirmDelete(){
            this._wall.deletePostById(this._id).subscribe(
            success => {
                this.snackBar.open(this.successTxt, "OK", {
                    panelClass: ['blue-snackbar']
                });
                this._dialogRef.close();
            },
            error => {
                this.snackBar.open(this.existTxt, "OK", {
                    panelClass: ['red-snackbar']
                });
            }
        )
        }
    }