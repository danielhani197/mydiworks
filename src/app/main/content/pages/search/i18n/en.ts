export const locale = {
    lang: 'en',
    data: {
        'CARIAN': {
            'SEARCH': 'Search',
            'KEMENTERIAN': 'Ministry',
            'AGENSI': 'Agency',
            'BAHAGIAN': 'Division',
            'ITEMSPERPAGE': 'Items per page',
            'OF': 'of',
            'TIADA': 'none',
            'RESULT': 'Results',
            'BTN': {
                 'DELETE' : 'Delete',
                 'BACK' : 'Back'
            },

            'DELETE':{
                'POST' : 'Delete Post',
                'CONFIRM': 'Are You Sure To Delete This Post?',
                'ERROR': 'Wall Post Was Unsucessfully Deleted',
                'SUCCESS': 'Wall Post Was Sucessfully Deleted',
            }
        }
    }
};
