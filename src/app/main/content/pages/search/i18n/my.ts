export const locale = {
    lang: 'my',
    data: {
        'CARIAN': {
            'SEARCH': 'Carian',
            'KEMENTERIAN': 'Kementerian',
            'AGENSI': 'Agensi',
            'BAHAGIAN': 'Bahagian',
            'ITEMSPERPAGE': 'Item per halaman',
            'OF': 'daripada',
            'TIADA': 'tiada',
            'RESULT': 'Hasil Carian',
            'BTN': {
                'DELETE' : 'Padam',
                'BACK' : 'Kembali'
           },

            'DELETE':{
                'POST' : 'Padam Post',
                'CONFIRM': 'Anda Pasti Untuk Memadam Post Ini?',
                'ERROR': 'Kandungan Tidak Berjaya Dipadam',
                'SUCCESS': 'Kandungan Berjaya Dipadam',
            }
        }
    }
};
