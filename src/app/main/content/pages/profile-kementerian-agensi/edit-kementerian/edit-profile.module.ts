import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule, 
    MatCheckboxModule,
    MatIconModule, 
    MatInputModule,
    MatChipsModule,
    MatFormFieldModule,
    MatMenuModule,
    MatSelectModule,
    MatDialogModule,
    MatStepperModule,
    MatSnackBarModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { EditProfileKementerianComponent } from "./edit-profile.component";

const routes = [
    {
        path     : 'edit/:id',
        component: EditProfileKementerianComponent
    }
];

@NgModule({
    declarations: [
        EditProfileKementerianComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatInputModule,
        
        MatFormFieldModule,
        MatChipsModule,
        MatMenuModule,
        MatDialogModule,
        MatSnackBarModule,
        MatSelectModule,

        FuseSharedModule
    ],
    
})
export class EditProfileKementerianModule
{
}