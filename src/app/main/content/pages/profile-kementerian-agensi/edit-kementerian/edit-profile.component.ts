import { Component, ViewChild } from '@angular/core';
import { User } from '../../../model/user';
import { PenggunaService } from '../../../services/pengguna/pengguna.service';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service'; 
import { locale as  malay } from '../i18n/my'; 
import { locale as  english } from '../i18n/en'; 
import { BandarNegeriService } from '../../../services/references/bandarNegeri.service';
import { Negeri } from '../../../model/ref/negeri';
import { Bandar } from '../../../model/ref/bandar';
import { Agensi } from '../../../model/general/agensi';
import { KementerianService } from '../../../services/agency/kementerian.service';
import { AgensiService } from '../../../services/agency/agensi.service';
import { Kementerian } from '../../../model/general/kementerian';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material';
import { DataService } from '../../../services/wall3/data.service';

declare let toastr: any;
@Component({
    selector   : 'edit-profile',
    templateUrl: './edit-profile.component.html',
    styleUrls  : ['./edit-profile.component.scss'],
    animations : fuseAnimations
})
export class EditProfileKementerianComponent
{
    id: string;
    _id: any;
    profileForm: FormGroup;
    profileFormErrors: any;
    
    user: User;
    negeri: Negeri;
    bandar: Bandar;
    agensi: Agensi;
    kementerian: Kementerian;

    userLs:any[];
    negeriLs :any [];
    kementerianLs:any[];
    agensiLs:any[];
    bandarLs: any[];
    bahagianLs: any[];
    skemaLs: any[];
    gradeLs:any[];


    successTxt: string;
    existTxt: string;

    negeriSemasa: any[];
    kemanterianSemasa: any[];
    agensiSemasa: any[];
    bandarSemasa: any[];
    bahagianSemasa: any[];
    skemaSemasa: any[];
    gradeSemasa:any[];
    currentObj:any[];

    verticalStepperStep1: FormGroup;
    verticalStepperStep2: FormGroup;
    verticalStepperStep3: FormGroup;
    verticalStepperStep1Errors: any;
    verticalStepperStep2Errors: any;
    verticalStepperStep3Errors: any;

    selectedFile: File;
    imageSrc: string = '';
    loaded: boolean = false;
    imageLoaded: boolean = false;
    picForm: FormGroup;

    picImage: any;

    //document upload
    @ViewChild('image') uploadImage;

constructor(    
        private penggunaService: PenggunaService,
        private formBuilder: FormBuilder,
        private authenticationService: AuthenticationService,
        private route: Router,
        private router: ActivatedRoute,
        private translate: TranslateService,
        private translateLoader: FuseTranslationLoaderService, 
        private _kementerian: KementerianService,
        private _agensi: AgensiService,
        private BandarNegeri: BandarNegeriService,
        private _dataservice: DataService,
        public snackBar: MatSnackBar
)
{
    this.translateLoader.loadTranslations(malay, english); 

    this.translate.get('PROFILE_KEMENTERIAN.FAIL').subscribe((res: string) => {
        this.existTxt = res;
    });
    this.translate.get('PROFILE_KEMENTERIAN.SUCCESS').subscribe((res: string) => {
        this.successTxt = res;
    });

    this.verticalStepperStep1Errors = {
        name : {},
        code  : {},
        phoneNo : {},
        address : {},
        postcode : {},
        city : {},
        state : {},
        urlPortal : {}
    };

    
} 
    ngOnInit(){
         // Vertical Stepper form stepper
         this.verticalStepperStep1 = this.formBuilder.group({
            name: ['', Validators.required],
            address: [ '' , Validators.required ],
            postcode: [ '' , [Validators.required, Validators.maxLength(5)]],
            phoneNo:['',Validators.required],
            city:[ '' , Validators.required ],
            state:[ '' , Validators.required ],
            code:[ '' , Validators.required ],
            url:[ '' , Validators.required ],
        });
        

        this.verticalStepperStep1.valueChanges.subscribe(() => {
            this.onSteper1FormValuesChanged();
        });
        this.id = this.router.snapshot.paramMap.get('id');
        this.BandarNegeri.getState().subscribe(
            data =>{
                this.negeriLs = data;
                this._id = this.router.snapshot.paramMap.get('id');
                if(this._id){

                    this._kementerian.getKementerianById(this._id).subscribe(
                        kementerian=>{
                            var state =  kementerian.state;
                            var city =  kementerian.city;
                            var stateid = "";
                            var cityid = "";
                            if(state){
                                stateid = state.id;
                                this.BandarNegeri.getCity(stateid).subscribe(
                                    data => {
                                        this.bandarLs = data;
                                        if(city){
                                            cityid = city.id;
                                            if(stateid){
                                                this.negeriSemasa = this.negeriLs.filter(value => parseInt(value.id) === parseInt(stateid));
                                                this.negeri = this.negeriSemasa[0];
                                                if(cityid){
                                                    this.bandarSemasa = this.bandarLs.filter(value => parseInt(value.id) === parseInt(cityid));
                                                    this.bandar = this.bandarSemasa[0];
                                                    
                                                    this.verticalStepperStep1.setValue({
                                                        name: kementerian.name,
                                                        code: kementerian.code,
                                                        phoneNo: kementerian.phoneNo,
                                                        address: kementerian.address,
                                                        postcode: kementerian.postcode,
                                                        city: kementerian.city.id,
                                                        state: kementerian.state.id,
                                                        url: kementerian.urlPortal
                                                    });
                                                }
                                            }
                                        }
                                    }
                                );
                            }                            
                        }
                    )
                    
                }
            }
        );
        
    }
    onSteper1FormValuesChanged(){
        for ( const field in this.verticalStepperStep1Errors )
        {
            if ( !this.verticalStepperStep1Errors.hasOwnProperty(field) )
            {
                continue;
            }
  
            // Clear previous errors
            this.verticalStepperStep1Errors[field] = {};
  
            // Get the control
            const control = this.verticalStepperStep1.get(field);
  
            if ( control && control.dirty && !control.valid )
            {
                this.verticalStepperStep1Errors[field] = control.errors;
            }
        }
    }   

   
    

    submit()
    {
        this._id = this.router.snapshot.paramMap.get('id');
        if(this._id){
            let kementerian : Kementerian = new Kementerian();
            kementerian.setAddress(this.verticalStepperStep1.controls['address'].value),
            kementerian.setCode(this.verticalStepperStep1.controls['code'].value),
            kementerian.setName(this.verticalStepperStep1.controls['name'].value),
            kementerian.setPhoneNo(this.verticalStepperStep1.controls['phoneNo'].value),
            kementerian.setPostcode(this.verticalStepperStep1.controls['postcode'].value),
            kementerian.setUrlPortal(this.verticalStepperStep1.controls['url'].value),
            kementerian.setCity(this.bandar),
            kementerian.setState(this.negeri),
            kementerian.setId(this._id);
            console.log(kementerian)
            this._kementerian.updateKementerian(kementerian).subscribe(
                success=>{
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                        });
                        this.route.navigate(['/kementerianProfile/profileKementerian']);
                },
                error=>{
                    console.log(error)

                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                      });
                      this.route.navigate(['/kementerianProfile/profileKementerian']);
                }
            )
        }
    }
    
    back(){
        this.route.navigate(['/kementerianProfile/profileKementerian']);
    }

    
    setSBState(id: any): void {
        // Match the selected ID with the ID's in array
        this.negeriSemasa = this.negeriLs.filter(value => parseInt(value.id) === parseInt(id));
        this.negeri = this.negeriSemasa[0];
        
        //load city based on state
        this.BandarNegeri.getCity(id).subscribe(
          data => {
            this.bandarLs = data;
          }
        );
    }

    setSBCity(id: any): void {

        // Match the selected ID with the ID's in array
        this.bandarSemasa = this.bandarLs.filter(value => parseInt(value.id) === parseInt(id));
        this.bandar = this.bandarSemasa[0];
    }

    handleInputChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

        var pattern = /image-*/;
        var reader = new FileReader();

        if (!file.type.match(pattern)) {
            //toastr.danger(message.global.invalidFormatImage);
            return;
        }

        this.loaded = false;

        this.picForm.get('image').setValue(file);
        //console.log(this.picForm.get('image').value);

        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        //save direct to db
    }
    handleImageLoad() {
        this.imageLoaded = true;
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    }

}