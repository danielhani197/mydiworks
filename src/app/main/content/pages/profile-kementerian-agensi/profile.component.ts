import { Component, ViewEncapsulation, ViewChild } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { ScrollEvent } from 'ngx-scroll-event';
import { User } from '../../model/user';
import { PenggunaService } from '../../services/pengguna/pengguna.service';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { KementerianService } from '../../services/agency/kementerian.service';
import { AgensiService } from '../../services/agency/agensi.service';
import { ActivatedRoute } from '@angular/router';
import { FuseAboutKementerianComponent } from './tab/kementerian/about.component';
import { FuseAboutAgensiComponent } from './tab/agensi/about.component';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as  malay } from './i18n/my'; 
import { locale as  english } from './i18n/en'; 
import { Kementerian } from '../../model/general/kementerian';

@Component({
    selector     : 'fuse-profile-kementerian-agensi',
    templateUrl  : './profile.component.html',
    styleUrls    : ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileKementerianAgensiComponent
{
    @ViewChild(FuseAboutKementerianComponent) timeLine: FuseAboutKementerianComponent;
    @ViewChild(FuseAboutAgensiComponent) timeLine2: FuseAboutAgensiComponent;
    user: User;
    imageSrc:any;
    name: any;
    kementerianId:Kementerian;
    _id: any;
    agensiId:any;
    agensiTab:any;
    kementerianTab:any;
    
    constructor( 
        private penggunaService: PenggunaService,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private kementerianService: KementerianService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private agensiService: AgensiService,
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
    }

    ngOnInit(){
        this._id = this.route.snapshot.paramMap.get('id');
        
        this.user = this.authenticationService.getCurrentUser();
        this.kementerianId = this.user.kementerian;
        if(this.user.agensi == null){
                this.kementerianService.getKementerianById(this.kementerianId.id).subscribe(
                kementerian =>{
                    this._id = kementerian.id
                    this.name = kementerian.name;
                    //this.kementerianTab= 'enable';
                    
                }
            )
        }
        if(this.user.agensi != null){
            this.agensiId = this.user.agensi.id;
            this.agensiService.getAgencyById(this.agensiId).subscribe(
                agensi =>{
                    //this.kementerianTab = 'disable';
                    // this.agensiTab = 'enable'
                    this._id = agensi.id;
                    this.name = agensi.name;
                }
            )
        }
    }
}
