import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ScrollEventModule } from 'ngx-scroll-event';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule, MatDividerModule, MatIconModule, MatTabsModule, MatFormFieldModule, MatChipsModule, MatSnackBarModule, MatMenuModule, MatDialogModule, MatCheckboxModule, MatOptionModule, MatSelectModule, MatDatepickerModule, MatInputModule } from '@angular/material';
import { ProfileKementerianAgensiComponent } from './profile.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { DataService } from 'app/main/content/services/wall3/data.service';
import { FuseAboutKementerianComponent } from './tab/kementerian/about.component';
import { FuseAboutAgensiComponent } from './tab/agensi/about.component';

const routes = [
    {
        path     : 'profileKementerian',
        component: ProfileKementerianAgensiComponent
    }
];

@NgModule({
    declarations: [
        ProfileKementerianAgensiComponent,
        FuseAboutKementerianComponent,
        FuseAboutAgensiComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        ScrollEventModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatTabsModule,
        MatFormFieldModule,
        MatChipsModule,
        MatSnackBarModule,
        MatMenuModule,
        MatDialogModule,
        MatCheckboxModule,
        MatOptionModule,
        MatSelectModule,
        MatDatepickerModule,
        MatInputModule,

        FuseSharedModule
    ],
    providers   : [
        DataService
    ]
})
export class ProfileKementerianAgensiModule
{
}
