export const locale = {
    lang: 'en',
    data: {
        'PROFILE_KEMENTERIAN':{
            'TITLE': 'General Information',
            'NAME':'Name',
            'ADDRESS':'Address',
            'CODE':'Short Name',
            'TEL':'No Phone',
            'STATE':'State',
            'CITY':'City',
            'POSTCODE':'Postcode',
            'MINISTRY': 'Ministry',
            'URL': 'Url Portal',
            'SAVE': 'Save',
            'BACK':'Back',
            'EDIT_PROFILE':'Edit Ministry / Agency',
            'EDIT': 'Update',
            'SUCCESS': 'Data Successfully Update',
            'FAIL': 'Data Not Successfully Update', 
            'KEMENTERIAN': 'Ministry'
        }
    }
}