export const locale = {
    lang: 'my',
    data: {
        'PROFILE_KEMENTERIAN':{
            'TITLE':'Maklumat Am',
            'NAME':'Nama',
            'ADDRESS':'Alamat',
            'CODE':'Nama Ringkas',
            'TEL':'Nombor Telefon',
            'STATE':'Negeri',
            'CITY':'Bandar',
            'POSTCODE':'Poskod',
            'MINISTRY': 'Kementerian',
            'URL': 'Url Portal',
            'SAVE': 'Simpan',
            'BACK':'Kembali',
            'EDIT_PROFILE':'Kemaskini Kementerian / Agensi',
            'EDIT': 'Kemaskini',
            'SUCCESS': 'Maklumat Berjaya Disimpan',
            'FAIL': 'Maklumat Tidak Berjaya Disimpan', 
            'KEMENTERIAN': 'Kementerian'
        }
    }
}