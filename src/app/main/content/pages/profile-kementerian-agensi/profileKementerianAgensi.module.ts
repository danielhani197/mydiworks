import { NgModule } from '@angular/core';
import { ProfileKementerianAgensiModule } from './profile.module';
import { EditProfileKementerianModule } from './edit-kementerian/edit-profile.module';
import { EditProfileAgensiModule } from './edit-agensi/edit-profile.module';
@NgModule({
    imports: [
        ProfileKementerianAgensiModule,
        EditProfileKementerianModule,
        EditProfileAgensiModule

    ]
})
export class KementerianAgensiModule
{

}
