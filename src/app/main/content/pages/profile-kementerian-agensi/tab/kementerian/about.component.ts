import { Component } from '@angular/core';
import { PenggunaService } from '../../../../services/pengguna/pengguna.service';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Agensi } from '../../../../model/general/agensi';
import { Kementerian } from '../../../../model/general/kementerian';
import { Bahagian } from '../../../../model/general/bahagian';
import { KementerianService } from '../../../../services/agency/kementerian.service';
import { AgensiService } from '../../../../services/agency/agensi.service';
import { locale as  malay } from '../../i18n/my'; 
import { locale as  english } from '../../i18n/en'; 
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { RoleService } from '../../../../services/role.service';
import { User } from '../../../../model/user';
import { Authorities } from '../../../../model/authorities';
import { UserAuthority } from '../../../../model/userAuthority';

@Component({
    selector   : 'fuse-about-kementerian',
    templateUrl: './about.component.html',
    styleUrls  : ['./about.component.scss'],
    animations : fuseAnimations
})
export class FuseAboutKementerianComponent
{
    _id: string;
    name: any;
    url:any;
    state:any;
    city: any;
    namaSingkatan:any;
    address: any;
    poskod: any;
    phoneNo: any;

    listUser:any[];
    i: number;
    countLength: number;
    rolesAdmin:any;
    role: Authorities[];

    constructor(   
                    private formBuilder: FormBuilder,
                    private kementerianService: KementerianService,
                    private agensiService: AgensiService,
                    private authenticationService: AuthenticationService,
                    private userService:PenggunaService,
                    private route: ActivatedRoute,
                    private fuseTranslationLoader: FuseTranslationLoaderService,
                    private router: Router,
                    private roles: RoleService,
        )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
    } 
    
    ngOnInit(){
        const userObj = this.authenticationService.getCurrentUser();
        this._id = userObj.kementerian.id;

        this.kementerianService.getKementerianById(this._id).subscribe(
            kementerian =>{
                this.name = kementerian.name;
                this.namaSingkatan = kementerian.code;
                this.phoneNo= kementerian.phoneNo;
                this.address = kementerian.address;
                this.poskod = kementerian.postcode;
                this.city = kementerian.city.name;
                this.state = kementerian.state.name;
                this.url = kementerian.urlPortal;
            }
        )
        this.role = userObj.authorities;
        for(this.i=0; this.i< this.role.length; this.i++){
            if(this.role[this.i].authority == 'ROLE_ADMIN'){
                this.rolesAdmin = this.role[this.i].authority;
            }
        }
    }
    editForm(){

        const userObj = this.authenticationService.getCurrentUser();
        this._id = userObj.kementerian.id;

        this.kementerianService.getKementerianById(this._id).subscribe(
            data =>{
                this.router.navigate(['kementerianProfile/edit/'+data.id]);
            }
        )
    }
}
