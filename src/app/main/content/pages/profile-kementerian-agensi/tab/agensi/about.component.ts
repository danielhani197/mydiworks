import { Component } from '@angular/core';
import { PenggunaService } from '../../../../services/pengguna/pengguna.service';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Agensi } from '../../../../model/general/agensi';
import { Kementerian } from '../../../../model/general/kementerian';
import { Bahagian } from '../../../../model/general/bahagian';
import { KementerianService } from '../../../../services/agency/kementerian.service';
import { AgensiService } from '../../../../services/agency/agensi.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as  malay } from '../../i18n/my'; 
import { locale as  english } from '../../i18n/en'; 
import { RoleService } from '../../../../services/role.service';
import { UserAuthority } from '../../../../model/userAuthority';
import { Authorities } from '../../../../model/authorities';
import { User } from '../../../../model/user';


@Component({
    selector   : 'fuse-about-agensi',
    templateUrl: './about.component.html',
    styleUrls  : ['./about.component.scss'],
    animations : fuseAnimations
})
export class FuseAboutAgensiComponent
{
    _id: string;
    name: any;
    url:any;
    state:any;
    city: any;
    namaSingkatan:any;
    address: any;
    poskod: any;
    phoneNo: any;
    kementerian: any;
    
    listUser:any[];
    i: number;
    countLength: number;
    rolesAdmin:any;
    role:Authorities[];

    constructor(   
                    private formBuilder: FormBuilder,
                    private kementerianService: KementerianService,
                    private agensiService: AgensiService,
                    private authenticationService: AuthenticationService,
                    private userService: PenggunaService,
                    private route: ActivatedRoute,
                    private fuseTranslationLoader: FuseTranslationLoaderService,
                    private router: Router,
                    private roles: RoleService,
        )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
    } 
    
    ngOnInit(){
        const userObj = this.authenticationService.getCurrentUser();
        this._id = userObj.agensi.id;
    
        this.agensiService.getAgencyById(this._id).subscribe(
            agensi =>{
                this.name = agensi.name;
                this.namaSingkatan = agensi.code;
                this.phoneNo= agensi.phoneNo;
                this.address = agensi.address;
                this.poskod = agensi.postcode;
                this.city = agensi.city.name;
                this.state = agensi.state.name;
                this.kementerian = agensi.ministry.name;
                this.url = agensi.urlPortal;
            }
        )
                this.role = userObj.authorities;
                for(this.i=0; this.i< this.role.length; this.i++){
                    if(this.role[this.i].authority == 'ROLE_ADMIN'){
                        this.rolesAdmin = this.role[this.i].authority;
                    }
                }
    }
    editForm(){
        const userObj = this.authenticationService.getCurrentUser();
        this._id = userObj.agensi.id;
        console.log(this._id)
        this.agensiService.getAgencyById(this._id).subscribe(
            data =>{
                this.router.navigate(['kementerianProfile/Edit/'+data.id]);
            }
        )

        
    }
}
