import { Component, ViewChild } from '@angular/core';
import { User } from '../../../model/user';
import { PenggunaService } from '../../../services/pengguna/pengguna.service';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service'; 
import { locale as  malay } from '../i18n/my'; 
import { locale as  english } from '../i18n/en'; 
import { BandarNegeriService } from '../../../services/references/bandarNegeri.service';
import { Negeri } from '../../../model/ref/negeri';
import { Bandar } from '../../../model/ref/bandar';
import { Agensi } from '../../../model/general/agensi';
import { KementerianService } from '../../../services/agency/kementerian.service';
import { AgensiService } from '../../../services/agency/agensi.service';
import { Kementerian } from '../../../model/general/kementerian';
import { BahagianService } from '../../../services/agency/bahagian.service';
import { ValueTransformer } from '@angular/compiler/src/util';
import { Bahagian } from '../../../model/general/bahagian';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material';
import { Skema } from '../../../model/general/skema';
import { Grade } from '../../../model/general/grade';
import { SkemaService } from '../../../services/general/skema.service';
import { GradeService } from '../../../services/general/grade.service';
import { DataService } from '../../../services/wall3/data.service';
import { RoleService } from '../../../services/role.service';

declare let toastr: any;
@Component({
    selector   : 'edit-profile',
    templateUrl: './edit-profile.component.html',
    styleUrls  : ['./edit-profile.component.scss'],
    animations : fuseAnimations
})
export class EditProfileAgensiComponent
{
    id: string;
    _id: any;
    profileForm: FormGroup;
    profileFormErrors: any;
    
    user: User;
    negeri: Negeri;
    bandar: Bandar;
    agensi: Agensi;
    kementerian: Kementerian;
    bahagian: Bahagian;
    skema: Skema;
    grade: Grade;

    userLs:any[];
    negeriLs :any [];
    kementerianLs:any[];
    agensiLs:any[];
    bandarLs: any[];
    bahagianLs: any[];
    skemaLs: any[];
    gradeLs:any[];


    successTxt: string;
    existTxt: string;

    negeriSemasa: any[];
    kemanterianSemasa: any[];
    agensiSemasa: any[];
    bandarSemasa: any[];
    bahagianSemasa: any[];
    skemaSemasa: any[];
    gradeSemasa:any[];
    currentObj:any[];

    agensiForm: FormGroup;
    verticalStepperStep2: FormGroup;
    verticalStepperStep3: FormGroup;
    verticalStepperStep1Errors: any;
    verticalStepperStep2Errors: any;
    verticalStepperStep3Errors: any;

    selectedFile: File;
    imageSrc: string = '';
    loaded: boolean = false;
    imageLoaded: boolean = false;
    picForm: FormGroup;
    roles:any;

    picImage: any;

    //document upload
    @ViewChild('image') uploadImage;

constructor(    
        private penggunaService: PenggunaService,
        private formBuilder: FormBuilder,
        private authenticationService: AuthenticationService,
        private route: Router,
        private router: ActivatedRoute,
        private translate: TranslateService,
        private translateLoader: FuseTranslationLoaderService, 
        private _kementerian: KementerianService,
        private bahagianService: BahagianService,
        private _agensi: AgensiService,
        private BandarNegeri: BandarNegeriService,
        private skemaService: SkemaService,
        private _dataservice: DataService,
        private gradeService: GradeService,
        public snackBar: MatSnackBar,
        private _roles: RoleService,
)
{
    this.translateLoader.loadTranslations(malay, english); 

    this.translate.get('PROFILE_KEMENTERIAN.FAIL').subscribe((res: string) => {
        this.existTxt = res;
    });
    this.translate.get('PROFILE_KEMENTERIAN.SUCCESS').subscribe((res: string) => {
        this.successTxt = res;
    });

    this.verticalStepperStep1Errors = {
        name: {}
    };

    this.verticalStepperStep2Errors = {
        phone:{},
        email: {},
        userEmail: {},
        address: {},
        postcode:{},
        bandar:{},
        negeri:{}
    };

    this.verticalStepperStep3Errors = {
        kementerian : {},
        agensi       : {},
        bahagian: {},
        position:{},
        skema: {},
        grade:{},
    };
} 
    ngOnInit(){
         // Vertical Stepper form stepper
         this.agensiForm = this.formBuilder.group({
            name    : ['', [Validators.required]],
            code    : ['', Validators.required],
            phoneNo    : ['', [Validators.required]],
            address    : ['', [Validators.required]],
            postcode    : ['', [Validators.required, Validators.pattern("[0-9]{5}")]],
            city    : ['', [Validators.required]],
            state    : ['', [Validators.required]],
            kementerian    : [{value:'', disabled:true }, [Validators.required]],
            urlPortal    : ['', [Validators.required]]
        });

    
        this.agensiForm.valueChanges.subscribe(() => {
            this.onSteper1FormValuesChanged();
        });
        this.user = this.authenticationService.getCurrentUser();
        console.log(this.user)
        this._roles.getRoleById(this.user.id).subscribe(
            data => {
                
                console.log("hai")
            }
        )

        this.BandarNegeri.getState().subscribe(
            states =>{
                this.negeriLs = states;
                this._kementerian.getKementerians().subscribe(
                    data => {
                        this.kementerianLs = data;
                        this._id = this.router.snapshot.paramMap.get('id');
                        console.log(this._id)
                        if(this._id){
                            this._agensi.getAgencyById(this._id).subscribe(
                                agency=> {
                                    var state =  agency.state;
                                    var city =  agency.city;
                                    var kementerian = agency.ministry;
                                    var stateid = "";
                                    var cityid = "";
                                    var kementerianid = "";

                                    if(state&&city&&kementerian){
                                        stateid = state.id;
                                        cityid = city.id;
                                        kementerianid = kementerian.id;

                                        this.BandarNegeri.getCity(stateid).subscribe(
                                            bandar=>{
                                                this.bandarLs = bandar;
                                                
                                                if(stateid&&cityid&&kementerianid){
                                                    this.bandarSemasa = this.bandarLs.filter(value => parseInt(value.id) === parseInt(cityid));
                                                    this.bandar = this.bandarSemasa[0];

                                                    this.negeriSemasa = this.negeriLs.filter(value => parseInt(value.id) === parseInt(stateid));
                                                    this.negeri = this.negeriSemasa[0];

                                                    this._kementerian.getKementerianById(kementerianid).subscribe(
                                                        ref => {
                                                            console.log(ref)
                                                            this.kementerian = ref;
                                                            this.agensiForm.setValue({
                                                                name: agency.name,
                                                                code: agency.code,
                                                                phoneNo: agency.phoneNo,
                                                                address: agency.address,
                                                                postcode: agency.postcode,
                                                                city: agency.city.id,
                                                                state: agency.state.id,
                                                                kementerian: agency.ministry.name,
                                                                urlPortal: agency.urlPortal
                                                            });
                                                        }
                                                    )
                                                }
                                            }
                                        )
                                    }
                                }
                            )
                        }
                    }
                )
            }
        ); 
        

        

        
    }
    onSteper1FormValuesChanged(){
        for ( const field in this.verticalStepperStep1Errors )
        {
            if ( !this.verticalStepperStep1Errors.hasOwnProperty(field) )
            {
                continue;
            }
  
            // Clear previous errors
            this.verticalStepperStep1Errors[field] = {};
  
            // Get the control
            const control = this.agensiForm.get(field);
  
            if ( control && control.dirty && !control.valid )
            {
                this.verticalStepperStep1Errors[field] = control.errors;
            }
        }
    }   

    submit()
    {
        this._id = this.router.snapshot.paramMap.get('id');
        if(this._id){
            let agensi : Agensi = new Agensi();
            agensi.setAddress(this.agensiForm.controls['address'].value),
            agensi.setName(this.agensiForm.controls['name'].value),
            agensi.setCode(this.agensiForm.controls['code'].value),
            agensi.setPhoneNo(this.agensiForm.controls['phoneNo'].value),
            agensi.setUrlPortal(this.agensiForm.controls['urlPortal'].value),
            agensi.setPostcode(this.agensiForm.controls['postcode'].value),
            agensi.setMinistry(this.kementerian),
            agensi.setCity(this.bandar),
            agensi.setState(this.negeri),
            agensi.setId(this._id);
          
            this._agensi.updateAgensi(agensi).subscribe(
                success=>{
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                        });
                        this.route.navigate(['/kementerianProfile/profileKementerian']);
                },
                error=>{
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                      });
                      this.route.navigate(['/kementerianProfile/profileKementerian']);
                }
            )
        }
    }
    
    back(){
        this.route.navigate(['/kementerianProfile/profileKementerian']);
    }

    
    setSBState(id: any): void {
        // Match the selected ID with the ID's in array
        this.negeriSemasa = this.negeriLs.filter(value => parseInt(value.id) === parseInt(id));
        this.negeri = this.negeriSemasa[0];
        
        //load city based on state
        this.BandarNegeri.getCity(id).subscribe(
          data => {
            this.bandarLs = data;
          }
        );
    }

    setSBCity(id: any): void {

        // Match the selected ID with the ID's in array
        this.bandarSemasa = this.bandarLs.filter(value => parseInt(value.id) === parseInt(id));
        this.bandar = this.bandarSemasa[0];
    }
}