import { EditProfileAgensiComponent } from "./edit-profile.component";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule, 
    MatCheckboxModule,
    MatIconModule, 
    MatInputModule,
    MatChipsModule,
    MatFormFieldModule,
    MatMenuModule,
    MatSelectModule,
    MatDialogModule,
    MatStepperModule,
    MatSnackBarModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';


const routes = [
    {
        path     : 'Edit/:id',
        component: EditProfileAgensiComponent
    }
];

@NgModule({
    declarations: [
        EditProfileAgensiComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatInputModule,
        
        MatStepperModule,
        MatFormFieldModule,
        MatChipsModule,
        MatMenuModule,
        MatDialogModule,
        MatSnackBarModule,
        MatSelectModule,

        FuseSharedModule
    ],
    exports     : [
    ]
})
export class EditProfileAgensiModule
{
}