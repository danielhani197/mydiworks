import { NgModule } from '@angular/core';

import { SenaraiPengumumanComponent } from './senarai-pengumuman/senarai-pengumuman.component';
import { TetapanPengumumanComponent } from './tetapan-pengumuman/tetapan-pengumuman.component';

import { SenaraiPengumumanDeleteDialogComponent } from './senarai-pengumuman/senarai-pengumuman-delete-dialog.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule, MatCheckboxModule, MatIconModule, MatFormFieldModule, MatInputModule, MatChipsModule, MatMenuModule, MatDialogModule, MatSnackBarModule, MatSelectModule, MatStepperModule } from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FuseSharedModule } from '@fuse/shared.module';
import { SenaraiPengumumanModule } from './senarai-pengumuman/senarai-pengumuman.module';
import { TetapanPengumumanModule } from './tetapan-pengumuman/tetapan-pengumuman.module';



@NgModule({
    // declarations:[
    //     SenaraiPengumumanComponent,
    //     SenaraiPengumumanDeleteDialogComponent
    // ],
    imports: [
        // Auth
        SenaraiPengumumanModule,
        TetapanPengumumanModule

    ]
})
export class PengumumanModule
{}