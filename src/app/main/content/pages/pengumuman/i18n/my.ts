export const locale = {
    lang: 'my',
    data: {
        'PENGUMUMAN': {
            'SEARCH': 'Carian',
            'TITLE': 'Pengumuman',
            'LIST': 'Senarai Pengumuman',
            'ADD': 'Tambah Pengumuman',
            'DETAILS': 'Maklumat Pengumuman',
            'NAME': 'Tajuk',
            'STARTDATE': 'Tarikh Mula',
            'ENDDATE': 'Tarikh Akhir',
            'CONTENT': 'Keterangan',
            'BTN' : {
                'DELETE' : 'Padam',
                'EDIT' : 'Kemaskini',
                'SUBMIT' : 'HANTAR',
                'BACK' : 'KEMBALI',
            },
            'DELETEPENGUMUMAN' : {
                'DELETEPENGUMUMAN' : 'Padam Pengumuman',
                'CONFIRM' : 'Adakan anda pasti untuk padam pengumuman ini?',
                'DELETE' : 'Padam',
                'BACK' : 'Kembali',
                'ERROR' : 'Pengumuman ini tidak berjaya dipadamkan',
                'SUCCESS' : 'Pengumuman ini berjaya dipadamkan'
            },
            'ERROR': 'Pengumuman tidak berjaya di simpan',
            'SUCCESS': 'Pengumuman Berjaya di hantar'
        
  
        }
    }
};
