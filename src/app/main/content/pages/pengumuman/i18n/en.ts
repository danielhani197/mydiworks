export const locale = {
    lang: 'en',
    data: {
        'PENGUMUMAN': {
            'SEARCH': 'Search',
            'TITLE': 'Announcement',
            'LIST': 'Announcements List',
            'ADD': 'Add Announcement',
            'DETAILS': 'Announcement Details',
            'NAME': 'Title',
            'STARTDATE': 'Start Date',
            'ENDDATE': 'End Date',
            'CONTENT': 'Content',
            'BTN' : {
                'DELETE' : 'Delete',
                'EDIT' : 'Edit',
                'SUBMIT' : 'SUBMIT',
                'BACK' : 'BACK',
            },
            'DELETEPENGUMUMAN' : {
                'DELETEPENGUMUMAN' : 'Delete Announcement',
                'CONFIRM' : 'Are you sure to delete this Announcement details?',
                'DELETE' : 'Delete',
                'BACK' : 'Back',
                'ERROR' : 'Announcement Details Are Not Successfully Deleted',
                'SUCCESS' : 'Announcement Details Was Successfully Deleted'
            },
            'ERROR': 'Announcement Details Are Not Successfully Submitted',
            'SUCCESS': 'Announcement Details Are Successfully Submitted'
        }
    }
};
