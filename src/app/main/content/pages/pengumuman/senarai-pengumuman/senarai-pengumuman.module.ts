import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { 
    MatButtonModule, 
    MatCheckboxModule, 
    MatIconModule, 
    MatFormFieldModule, 
    MatInputModule,
    MatChipsModule,
    MatMenuModule,
    MatDialogModule,
    MatSnackBarModule
} from '@angular/material';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { SenaraiPengumumanComponent } from './senarai-pengumuman.component';
import { SenaraiPengumumanDeleteDialogComponent } from './senarai-pengumuman-delete-dialog.component';

const routes = [
    {
        path     : 'list',
        component: SenaraiPengumumanComponent
    }
];

@NgModule({
    declarations: [
        SenaraiPengumumanComponent,
        SenaraiPengumumanDeleteDialogComponent
    ],
    entryComponents: [
        SenaraiPengumumanDeleteDialogComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatMenuModule,
        MatDialogModule,
        MatSnackBarModule,

        NgxDatatableModule,

        FuseSharedModule,
    ],
    exports: [

    MatDialogModule,

    ] 
})
export class SenaraiPengumumanModule
{
}
