import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { Page } from 'app/main/content/model/util/page';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

import { Pengumuman } from 'app/main/content/model/pengumuman/pengumuman';
import { PengumumanService } from 'app/main/content/services/pengumuman/pengumuman.service';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { SenaraiPengumumanDeleteDialogComponent } from './senarai-pengumuman-delete-dialog.component';

@Component({
    selector   : 'senarai-pengumuman',
    templateUrl: './senarai-pengumuman.component.html',
    styleUrls  : ['./senarai-pengumuman.component.scss']
})
export class SenaraiPengumumanComponent implements OnInit
{
    rows: any[];
    temp = [];
    page = new Page();

    _idToDelete : string;
    loadingIndicator = true;
    reorderable = true;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private http: HttpClient,
        private _pengumuman: PengumumanService,
        private _router: Router,
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        public _dialog: MatDialog,
        //public _dialogRef: MatDialogRef<SenaraiPengumumanDeleteDialogComponent>,
        // @Inject(MAT_DIALOG_DATA) public data:any
    )
    {
        this.page.pageNumber = 0;
        this.page.size = 10;
        this.page.sortField = "title";
        this.page.sort = "asc";
        this.fuseTranslationLoader.loadTranslations(malay, english);
    }

    ngOnInit()
    {
        this.setPage({ offset: 0 });
    }
    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._pengumuman.getPengumumanList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }
    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._pengumuman.getPengumumanList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;
    
        this._pengumuman.getPengumumanList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }
 
    editPengumuman(id: string){
         this._router.navigate(['pengumuman/tetapan/'+id]);
    }

    deletePengumuman(id: string){
        this._idToDelete = id;
        let dialogRef = this._dialog.open(SenaraiPengumumanDeleteDialogComponent, {
            width: '500px',
            data: { id: this._idToDelete }
        });

        dialogRef.afterClosed().subscribe(result => {
            this.setPage({ offset: 0 });
        });
    }

    tambah(){
        this._router.navigate(['pengumuman/tetapan']);
    }

}
