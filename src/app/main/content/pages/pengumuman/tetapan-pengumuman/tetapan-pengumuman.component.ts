import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

import { Pengumuman } from 'app/main/content/model/pengumuman/pengumuman';
import { PengumumanService } from 'app/main/content/services/pengumuman/pengumuman.service';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { MatSnackBar } from '@angular/material';
import { Page } from 'app/main/content/model/util/page';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { PagedData } from '../../../model/util/paged-data';
import { Subscriber } from 'rxjs';

@Component({
    selector   : 'tetapan-pengumuman',
    templateUrl: './tetapan-pengumuman.component.html',
    styleUrls  : ['./tetapan-pengumuman.component.scss'],
    animations : fuseAnimations
})
export class TetapanPengumumanComponent implements OnInit
{   
  _id: string;
  pengumumanLs:       any[];  
  pengumumanForm: FormGroup;
  pengumumanFormErrors: any;
  successTxt: string;
  existTxt: string;

  loadingIndicator = true;
  reorderable = true;

  editorConfig = 
  {
      "editable": true,
      "spellcheck": true,
      "height": "auto",
      "minHeight": "0",
      "width": "auto",
      "minWidth": "0",
      "translate": "yes",
      "enableToolbar": true,
      "showToolbar": true,
      "placeholder": "Enter text here...",
      "imageEndPoint": "",
      "toolbar": [
          ["bold", "italic", "underline", "strikeThrough", "superscript", "subscript"],
          ["fontName", "fontSize", "color"],
          ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
          ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
          ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
          // ["link", "unlink", "image", "video"]
      ]
  };

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private pengumumanServis: PengumumanService,
        private _router: Router,
        private _toastr: ToastrService,
        private _translate: TranslateService,
        private formBuilder: FormBuilder,
        public snackBar: MatSnackBar,
        private _auth: AuthenticationService,
    )
    {

        this.fuseTranslationLoader.loadTranslations(malay, english);
        
        this._translate.get('PENGUMUMAN.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('PENGUMUMAN.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
    }
    
    ngOnInit() 
    {
        this.pengumumanForm = this.formBuilder.group({

            title    : ['',[Validators.required]],
            startDatetime  :  ['',[Validators.required]],
            endDatetime : ['',[Validators.required]],
            teksPengumuman    : ['',[Validators.required]],
            //   startDatetime    : ['', [Validators.required, Validators.pattern("^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|[12][\/\-]\d{4}$")]],
            //   endDatetime    : ['', [Validators.required, Validators.pattern("^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|[12][\/\-]\d{4}$")]],
      });

      this.pengumumanForm.valueChanges.subscribe(() => {
        this.onPengumumanFormValuesChanged();

      });

        
        this._id = this.route.snapshot.paramMap.get('id');
    
        if(this._id){
            this.pengumumanServis.getPengumumanById(this._id).subscribe(
                pengumuman=>{

                    var pengumuman = pengumuman
                    let startDate = new Date(pengumuman.startDatetime)
                    let endDate = new Date(pengumuman.endDatetime)
                    this.pengumumanForm.setValue({
                        title: pengumuman.title,
                        startDatetime: startDate,
                        endDatetime: endDate,
                        teksPengumuman: pengumuman.content
                    });

                }
            )
        }
            
        
    }
   
    

    backButton(){
        this._router.navigate(['/pengumuman/list']);
    }

    hantar(){

        if(this._id){
        let pengumuman: Pengumuman = new Pengumuman();
        pengumuman.setTitle(this.pengumumanForm.controls['title'].value);
        pengumuman.setStart_date(this.pengumumanForm.controls['startDatetime'].value);
        pengumuman.setEnd_date(this.pengumumanForm.controls['endDatetime'].value);
        pengumuman.setContent(this.pengumumanForm.controls['teksPengumuman'].value);
        pengumuman.setId(this._id);

        this.pengumumanServis.updatePengumuman(pengumuman).subscribe(
            success=>{
                console.log("2")
                this.snackBar.open(this.successTxt, "Berjaya di Hantar", {
                    panelClass: ['blue-snackbar']
                    });
                    
                    this._router.navigate(['/pengumuman/list']);
            },
            error=>{
                console.log("21")
                console.log(error)
                this.snackBar.open(this.existTxt, "Tidak Berjaya di Hantar", {
                    panelClass: ['red-snackbar']
                    });
                    this._router.navigate(['/pengumuman/list']);
            }

       )
    }else{

        let pengumuman: Pengumuman = new Pengumuman();
        pengumuman.setTitle(this.pengumumanForm.controls['title'].value);
        pengumuman.setStart_date(this.pengumumanForm.controls['startDatetime'].value);
        pengumuman.setEnd_date(this.pengumumanForm.controls['endDatetime'].value);
        pengumuman.setContent(this.pengumumanForm.controls['teksPengumuman'].value);

        this.pengumumanServis.createPengumuman(pengumuman).subscribe(
            success=>{
                console.log("2")
                this.snackBar.open(this.successTxt, "Berjaya di Hantar", {
                    panelClass: ['blue-snackbar']
                    });
                    
                    this._router.navigate(['/pengumuman/list']);
            },
            error=>{
                console.log("21")
                console.log(error)
                this.snackBar.open(this.existTxt, "Tidak Berjaya di Hantar", {
                    panelClass: ['red-snackbar']
                    });
                    this._router.navigate(['/pengumuman/list']);
            }

       )

       console.log(pengumuman)


    }
}

    onPengumumanFormValuesChanged(){
        for ( const field in this.pengumumanFormErrors )
        {
            if ( !this.pengumumanFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.pengumumanFormErrors[field] = {};

            // Get the control
            const control = this.pengumumanFormErrors.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.pengumumanFormErrors[field] = control.errors;
            }
        }
    
    } 
}