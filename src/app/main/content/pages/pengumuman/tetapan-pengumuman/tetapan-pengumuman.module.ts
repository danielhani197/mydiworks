import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatIconModule, MatSelectModule, MatStepperModule, MatSnackBarModule,MatDatepickerModule  } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgxEditorModule } from 'ngx-editor';

import { TetapanPengumumanComponent } from './tetapan-pengumuman.component';
import { FormBuilder, FormGroup, Validators, FormControl, FormsModule } from '@angular/forms';



const routes = [
    {
        path     : 'tetapan',
        component: TetapanPengumumanComponent
    },
    {
        path     : 'tetapan/:id',
        component: TetapanPengumumanComponent
    }
];

@NgModule({
    declarations: [
        TetapanPengumumanComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        FormsModule,
        MatIconModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        NgxEditorModule,
        MatSnackBarModule,
        MatDatepickerModule,
        FuseSharedModule
    ],
    exports:[
        // TetapanPengumumanComponent
    ]
})
export class TetapanPengumumanModule
{
}
