import { NgModule } from '@angular/core';

import { SenaraiPenggunaModule } from './senarai-pengguna/senarai-pengguna.module';
import { SenaraiPengesahanPenggunaModule } from './senarai-pengesahan-pengguna/senarai-pengesahan-pengguna.module';
import { SenaraiPerananPenggunaModule } from './senarai-peranan-pengguna/senarai-peranan-pengguna.module';
@NgModule({
    imports: [
        // Auth
        SenaraiPenggunaModule,
        SenaraiPengesahanPenggunaModule,
        SenaraiPerananPenggunaModule
    ]
})
export class PenggunaModule
{

}
