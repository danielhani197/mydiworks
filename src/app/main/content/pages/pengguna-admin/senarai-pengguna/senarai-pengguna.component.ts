import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from "@angular/router";

import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from 'app/main/content/model/util/page';

import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service'

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { locale as englishNav } from 'app/navigation/i18n/en';
import { locale as malayNav } from 'app/navigation/i18n/my';
import { locale as english } from './../i18n/en';
import { locale as malay } from './../i18n/my';

//For Term Popup Only
import { MatDialog, MatSnackBar} from '@angular/material';
import { TermService } from 'app/main/content/services/setting/term.service'
import { TranslateService } from '@ngx-translate/core';
import { AdminService } from '../../../services/pengguna/admin.service';
import { PenggunaService } from '../../../services/pengguna/pengguna.service';
import { User } from '../../../model/user';
import { HistoryRouteService } from '../../../services/util/historyroute.service';
//END of Term Popup

@Component({
    selector   : 'senarai-pengguna-admin',
    templateUrl: './senarai-pengguna.component.html',
    styleUrls  : ['./senarai-pengguna.component.scss']
})
export class SenaraiPenggunaComponent implements OnInit
{
    id: string;

    rows: any[];
    temp = [];
    page = new Page();

    loadingIndicator = true;
    reorderable = true;

    successMsg: string;
    deleteMsg: string;
    conflictMsg: string;
    currentUser: User;
    ministryId: string;
    agencyId: string;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private http: HttpClient,
        private _authentication: AuthenticationService,
        private _router: Router,
        private route: ActivatedRoute,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _dialog: MatDialog,
        private _term: TermService,
        private _user: PenggunaService,
        private _admin: AdminService,
        private snackBar: MatSnackBar,
        private translate: TranslateService,
        private _routeHist: HistoryRouteService

    )
    {
        this.currentUser = _authentication.getCurrentUser();

        var ministry = this.currentUser.kementerian;
        var agency = this.currentUser.agensi;
        if(ministry){
            var ministryId = ministry.id;
            this.ministryId = ministryId;
        }if(agency){
            var agencyId = agency.id;
            this.agencyId = agencyId;
        }

        this.fuseTranslationLoader.loadTranslations(malayNav, englishNav, malay, english);

        this.translate.get('USER.MSG.GENERAL-SUCCESS').subscribe((res: string) => {
            this.successMsg = res;
        });

        this.translate.get('USER.MSG.DELETE').subscribe((res: string) => {
            this.deleteMsg = res;
        });

        this.translate.get('USER.MSG.CONFLICT').subscribe((res: string) => {
            this.conflictMsg = res;
        });

        this.page.pageNumber = 0;
        this.page.size = 10;
        this.page.sortField = "name";
        this.page.sort = "asc";
        this.page.ministryId = this.ministryId;
        this.page.agencyId = this.agencyId;

        this._routeHist.setRole(HistoryRouteService.ISADMIN);
    }

    ngOnInit(){
        this.setPage({ offset: 0 });      
    }

    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._admin.getUserApproved(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    resetPwd(id : string) {
        // this._router.navigate(['/auth/reset-password-2/'+ id]);
        this._router.navigate(['/user/profile/'+ id]);
    }

    onSort(event) {
        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._admin.getUserApproved(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;

        this._admin.getUserApproved(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
        // Whenever the filter changes, always go back to the first page
        //this.table.offset = 0;
    }

    delete(id: string){
        if(id){
            this._user.deleteUserById(id).subscribe(
                sucess => {
                    this.snackBar.open(this.deleteMsg, "OK");
                    this.setPage({ offset: 0 });
                },
                error => {
                    this.snackBar.open(this.conflictMsg, "OK",{
                        panelClass: 'red-snackbar'
                    });
                    this.setPage({ offset: 0 });
                }
            )
        }
    }

    activate(id: string){
        if(id){
            this._user.activateUser(id).subscribe(
                sucess => {
                    this.snackBar.open(this.successMsg, "OK");
                    this.setPage({ offset: 0 });
                }
            )
        }
    }

    deactivate(id: string){
        if(id){
            this._user.deActivateUser(id).subscribe(
                sucess => {
                    this.snackBar.open(this.successMsg, "OK");
                    this.setPage({ offset: 0 });
                }
            )
        }
    }
}
