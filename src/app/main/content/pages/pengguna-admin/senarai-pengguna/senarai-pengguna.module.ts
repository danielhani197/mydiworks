import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { 
    MatButtonModule, 
    MatCheckboxModule, 
    MatIconModule, 
    MatFormFieldModule, 
    MatInputModule,
    MatChipsModule,
    MatMenuModule,
    MatDialogModule
} from '@angular/material';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { FuseSharedModule } from '@fuse/shared.module';

import { SenaraiPenggunaComponent } from './senarai-pengguna.component';
import { TranslateModule } from '@ngx-translate/core';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

const routes = [
    {
        path     : 'senarai',
        component: SenaraiPenggunaComponent
    }
];

@NgModule({
    declarations: [
        SenaraiPenggunaComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatMenuModule,
        SweetAlert2Module,

        NgxDatatableModule,
        FuseSharedModule,
        TranslateModule
    ],
})
export class SenaraiPenggunaModule
{
}
