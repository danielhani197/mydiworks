import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { 
    MatButtonModule, 
    MatCheckboxModule, 
    MatIconModule, 
    MatFormFieldModule, 
    MatInputModule,
    MatChipsModule,
    MatMenuModule,
    MatSnackBarModule
    
} from '@angular/material';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FuseSharedModule } from '@fuse/shared.module';
import { SenaraiPengesahanPenggunaComponent } from './senarai-pengesahan-pengguna.component';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { TranslateModule } from '@ngx-translate/core';

const routes = [
    {
        path     : 'baru',
        component: SenaraiPengesahanPenggunaComponent
    }
];

@NgModule({
    declarations: [
        SenaraiPengesahanPenggunaComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatMenuModule,
        MatSnackBarModule,

        SweetAlert2Module,

        NgxDatatableModule,

        FuseSharedModule,
        TranslateModule
    ],
})
export class SenaraiPengesahanPenggunaModule
{
}
