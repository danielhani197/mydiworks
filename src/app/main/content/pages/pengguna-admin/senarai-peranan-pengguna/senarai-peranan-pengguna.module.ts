import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import {
    MatButtonModule,
          MatCheckboxModule,
          MatFormFieldModule,
          MatInputModule,
          MatIconModule,
          MatSelectModule,
          MatStepperModule,
          MatDialogModule,
          MatChipsModule,
          MatMenuModule,
          MatSortModule,
          MatPaginatorModule,
          MatTableModule,
          MatSidenavModule,
          MatRippleModule,
          MatToolbarModule, 

          MatSnackBarModule
    } from '@angular/material';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { SenaraiPerananPenggunaComponent } from './senarai-peranan-pengguna.component';
// import {MaklumatPerananComponent} from './maklumat-peranan/maklumat-peranan.component';


const routes = [
  {
      path  : 'peranan',
      component : SenaraiPerananPenggunaComponent

  },
//   { 
//       path  :'role_details/:id', 
//       component:  MaklumatPerananComponent 
// } 
];
@NgModule({
    declarations: [
        SenaraiPerananPenggunaComponent,
        // MaklumatPerananComponent
    ],

    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatMenuModule,
        MatSnackBarModule,
        MatSelectModule,
        MatStepperModule,
        MatDialogModule,
        MatSortModule,
        MatPaginatorModule,
        MatTableModule,
        MatSidenavModule,
        MatRippleModule,
        MatToolbarModule, 

        SweetAlert2Module,

        NgxDatatableModule,

        FuseSharedModule,
        TranslateModule
    ],

})
export class SenaraiPerananPenggunaModule
{

}
