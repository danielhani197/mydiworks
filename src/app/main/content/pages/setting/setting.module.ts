import { NgModule } from '@angular/core';

import { SenaraiSoalanModule } from './jawapan-soalan/senarai-soalan/senarai-soalan.module';
import { JawapanKeselamatanModule } from './jawapan-soalan/jawapan-keselamatan.module';
import {PermohonanPerananModule} from './permohonan-peranan/permohonan-peranan.module'

@NgModule({
    imports: [

        SenaraiSoalanModule,
        JawapanKeselamatanModule,
        PermohonanPerananModule
    ]
})
export class SettingModule
{

}
