export const locale = {
    lang: 'my',
    data: {
        'SOALAN': {
            'SEARCH': 'Carian',
            'DETAIL': 'Keterangan',
            'TITLE': 'Soalan Keselamatan',
            'LIST': 'Senarai Soalan Keselamatan',
            'NEW': 'Soalan Keselamatan',
            'DETAILS': 'Keterangan Soalan Keselamatan',
            'EXIST': 'Soalan Keselamatan Telah Wujud',
            'BTN' : {
                'DELETE' : 'Padam',
                'EDIT' : 'Kemaskini',
                'SUBMIT' : 'Hantar',
                'BACK'  : 'Kembali'
            },
            'FORMERROR' : {
                'DETAIL': 'Keterangan wajib diisi',
                },
            'DELETESOALAN' : {
                'DELETESOALAN' : 'Padam Soalan Keselamatan',
                'CONFIRM' : 'Adakah anda pasti untuk memadam soalan keselamatan ini?',
                'DELETE' : 'Padam',
                'BACK' : 'Kembali',
                'ERROR' : 'Soalan Keselamatan Tidak Dapat Dipadam',
                'SUCCESS' : 'Soalan Keselamatan Telah Berjaya Dipadam'
            },
            'ADDSOALAN' : {
                'ADDSOALAN' : 'Tambah Soalan Keselamatan',
                'CONFIRM' : 'Adakah anda pasti untuk menambah soalan keselamatan ini?',
                'DELETE' : 'Padam',
                'BACK' : 'Kembali',
                'ERROR' : 'Soalan Keselamatan Tidak Dapat Ditambah',
                'SUCCESS' : 'Soalan Keselamatan Telah Berjaya Ditambah'
            },
            'ERROR': 'Soalan Keselamatan Tidak Dapat Disimpan',
            'SUCCESS': 'Soalan Keselamatan Telah Berjaya Dihantar'
        },

        'JAWAPAN': {
          'SEARCH': 'Carian',
          'TITLE': 'Jawapan Keselamatan',
          'DETAILS': 'Maklumat Jawapan Keselamatan',
          'JAWAPAN': 'Jawapan',
          'SOALAN': 'Soalan',
          'BTN' : {
              'DELETE' : 'Padam',
              'EDIT' : 'Kemaskini',
              'SUBMIT' : 'Hantar',
              'BACK'  : 'Kembali'
        },
        'FORMERROR' : {
            'DETAIL': 'Keterangan wajib di isi',
            'JAWAPAN': 'Jawapan wajib di isi',
            'SOALAN': 'Soalan wajib di isi',
        },
        'DELETEJAWAPAN' : {
          'DELETESOALAN' : 'Padam Jawapan Keselamatan',
          'CONFIRM' : 'Adakah anda pasti untuk memadam jawapan keselamatan ini?',
          'DELETE' : 'Padam',
          'BACK' : 'Kembali',
          'ERROR' : 'Jawapan Keselamatan Tidak Dapat Dipadam',
          'SUCCESS' : 'Jawapan Keselamatan Telah Berjaya Dipadam'
        },
        'ERROR': 'Jawapan Keselamatan Tidak Dapat Disimpan',
        'SUCCESS': 'Jawapan Keselamatan Telah Berjaya Dihantar'
        },
        'ROLE': {
            'TITLE': 'Permohonan Pentadbir Agensi',
            'DETAILS': 'Maklumat Permohonan Admin',
            'SETTING': 'Tetapan Peranan',
            'NAMA': 'Nama',
            'EMAIL':  'Alamat Email',
            'AGENSI':  'Agensi',
            'ROLES': 'Peranan',
            'REMARK'    :'Catatan',
            'BTN' : {
                'DELETE' : 'Padam',
                'EDIT' : 'Kemaskini',
                'SUBMIT' : 'Hantar',
                'BACK'  : 'Kembali'
          },

          'ERROR': 'Permohonan Admin Tidak Dapat Disimpan',
          'SUCCESS': 'Permohonan Admin Telah Berjaya Dihantar'
          }
      }
    }
