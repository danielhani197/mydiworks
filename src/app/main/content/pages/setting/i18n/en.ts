export const locale = {
    lang: 'en',
    data: {
        'SOALAN': {
            'SEARCH': 'Search',
            'TITLE': 'Security Question',
            'LIST': 'Security Questions List',
            'NEW': 'Add Security Question',
            'DETAILS': 'Security Question Details',
            'EXIST': 'Security Question Already Exist',
            'BTN' : {
                'DELETE' : 'Delete',
                'EDIT' : 'Edit',
                'SUBMIT' : 'Submit',
                'BACK'  : 'Back'
            },
            'FORMERROR' : {
                'DETAIL': 'Detail is required',
            },
            'DELETESOALAN' : {
                'DELETESOALAN' : 'Delete Security Question',
                'CONFIRM' : 'Are you sure to delete this Security Question details?',
                'DELETE' : 'Delete',
                'BACK' : 'Back',
                'ERROR' : 'Security Question Details Was Unsuccessfully Deleted',
                'SUCCESS' : 'Security Question Details Was Successfully Deleted'
            },
            'ADDSOALAN' : {
                'ADDSOALAN' : 'ADD Security Question',
                'CONFIRM' : 'Are you sure to add this Security Question details?',
                'DELETE' : 'Delete',
                'BACK' : 'Back',
                'ERROR' : 'Security Question Details Was Unsuccessfully Added',
                'SUCCESS' : 'Security Question Details Was Successfully Added'
            },
            'ERROR': 'Security Question Details Are Unsuccessfully Submitted',
            'SUCCESS': 'Security Question Details Are Successfully Submitted'
        },

        'JAWAPAN': {
          'SEARCH': 'Search',
          'TITLE': 'Security Answer',
          'DETAILS': 'Security Answer Details',
          'JAWAPAN': 'Answer',
          'SOALAN': 'Question',
          'BTN' : {
              'DELETE' : 'Delete',
              'EDIT' : 'Edit',
              'SUBMIT' : 'Submit',
              'BACK'  : 'Back'
        },
        'FORMERROR' : {
            'DETAIL': 'Detail is required',
            'JAWAPAN': 'Answer is required',
            'SOALAN': 'Question is required',
        },
        'DELETEJAWAPAN' : {
            'DELETEJAWAPAN' : 'Delete Security Answer',
            'CONFIRM' : 'Are you sure to delete this Security Answer details?',
            'DELETE' : 'Delete',
            'BACK' : 'Back',
            'ERROR' : 'Security Answer Details Was Unsuccessfully Deleted',
            'SUCCESS' : 'Security Answer Details Was Successfully Deleted'
        },

        'ERROR': 'Security Answers Details Are Unsuccessfully Submitted',
        'SUCCESS': 'Security Answers Details Are Successfully Submitted'
    },
    'ROLE': {
        'TITLE': 'Admin of Agency Application',
        'DETAILS': 'Details of Admin Application',
        'SETTING': 'Role Setting',
        'NAMA': 'Name',
        'EMAIL':  'Email Address',
        'AGENSI':  'Agency',
        'ROLES': 'Role',
        'REMARK'    :'Remarks',
        'BTN' : {
            'DELETE' : 'Delete',
            'EDIT' : 'Edit',
            'SUBMIT' : 'Submit',
            'BACK'  : 'Back'
      },

      'ERROR': 'Admin Application Are Unsuccessfully Submitted',
      'SUCCESS': 'Admin Application Are Successfully Submitted'
      }
  }
}
