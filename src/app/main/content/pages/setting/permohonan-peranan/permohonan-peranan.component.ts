import { Component, OnInit,ViewChild, Inject, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { HttpClient } from '@angular/common/http';

import { Router,  ActivatedRoute } from "@angular/router";
import {MatDialog} from '@angular/material';

import { Page } from '../../../model/util/page';
import { PagedData } from '../../../model/util/paged-data';

import { MatSnackBar } from '@angular/material';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

import { User } from 'app/main/content/model/user';
import { AppAuthority } from "../../../model/appAuthority";
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service'
import { RoleService } from '../../../services/role.service';
import { PenggunaService } from '../../../services/pengguna/pengguna.service';
import { Kementerian } from 'app/main/content/model/general/kementerian';
import { KementerianService } from '../../../services/agency/kementerian.service';
import { Agensi } from 'app/main/content/model/general/agensi';
import { AgensiService } from '../../../services/agency/agensi.service';
import { Notifikasi } from '../../../model/notifikasi';
import { NotifikasiService } from '../../../services/setting/notifikasi.service';



@Component({
    selector   : 'permohonan-peranan',
    templateUrl: './permohonan-peranan.component.html',
    styleUrls  : ['./permohonan-peranan.component.scss'],
    animations : fuseAnimations
})

export class PermohonanPerananComponent implements OnInit{

    appForm: FormGroup;
    appFormErrors: any;

    kementerian: Kementerian;
    agensi: any;

    successTxt: string;
    existTxt: string;

    isNotValid : boolean;

    roleLs : any[];
    currentRole : any[];

    app: AppAuthority;
    currentUser: any;

    constructor(
        private http: HttpClient,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _router: Router,
        private _translate: TranslateService,
        public snackBar: MatSnackBar,
        private _pengguna: PenggunaService,
        private _role: RoleService,
        private _auth: AuthenticationService,
        private _kementerian: KementerianService,
        private _agensi: AgensiService,
        private _noti: NotifikasiService,

 
    
      ){
        this.fuseTranslationLoader.loadTranslations(malay, english);
   
        this._translate.get('ROLE.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('ROLE.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
      }

    ngOnInit(){
        this.currentUser = this._auth.getCurrentUser();

        this.appForm = this.formBuilder.group({
            name:  [{value :'', disabled:true}],
            agensi:  [{value :'', disabled:true}],
            email:  [{value :'', disabled:true}],
            remarks: ['',[Validators.required]],    
          });

        if(this.currentUser.id){
          this._pengguna.getUserById(this.currentUser.id).subscribe(
            user => {
              this.kementerian = this.currentUser.kementerian;
              if(this.currentUser.agensi == null){
                this._kementerian.getKementerianById(this.kementerian.id).subscribe(
                  kementerian =>{     
                    this.appForm.patchValue({
                      agensi: kementerian.name,
                    })                
                  }

                )
              }

              if(this.currentUser.agensi != null){
                this.agensi = this.currentUser.agensi.id
                this._agensi.getAgencyById(this.agensi).subscribe(
                  agensi =>{
                    this.appForm.patchValue({
                      agensi: agensi.name,
                    })
                  }
                )
              }

              this.appForm.patchValue({
                name: user.name,
                email: user.email,

              })
            }
          )
        }
 
    }

    hantar(){

      let app: AppAuthority = new AppAuthority();         
      app.setRemarks(this.appForm.controls['remarks'].value),
      app.setStatus("NEW"),
      app.setUser(this.currentUser),
      

      this._role.createRole(app).subscribe(
        success => {
            this.snackBar.open(this.successTxt, "OK", {
                panelClass: ['blue-snackbar']});

                this._router.navigate(['/wall']);
        }
      );
    }
}

      


