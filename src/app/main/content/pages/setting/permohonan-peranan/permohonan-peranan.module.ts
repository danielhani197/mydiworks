import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule,
          MatCheckboxModule,
          MatFormFieldModule,
          MatInputModule,
          MatIconModule,
          MatSelectModule,
          MatStepperModule,
          MatDialogModule,
          MatChipsModule,
          MatMenuModule,
          MatSortModule,
          MatPaginatorModule,
          MatTableModule,
          MatSidenavModule,
          MatRippleModule,
          MatToolbarModule, 

          MatSnackBarModule } from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { PermohonanPerananComponent } from './permohonan-peranan.component';

const routes = [
  {
      path  :'role/apply',
      component:  PermohonanPerananComponent
  },
];

@NgModule({
    declarations: [
        PermohonanPerananComponent,

    ],

    imports     : [
        RouterModule.forChild(routes),
        MatButtonModule, 
        MatCheckboxModule, 
        MatFormFieldModule, 
        MatInputModule, 
        MatIconModule, 
        MatSelectModule, 
        MatStepperModule, 
        MatSnackBarModule, 
        MatSidenavModule, 
        MatMenuModule,
        MatRippleModule,
        MatToolbarModule,
        MatChipsModule,
        MatDialogModule,
        MatSortModule,
        MatPaginatorModule,
        MatTableModule,

      SweetAlert2Module,

      NgxDatatableModule,

      FuseSharedModule,
      TranslateModule
    ],
})
export class PermohonanPerananModule
{
}