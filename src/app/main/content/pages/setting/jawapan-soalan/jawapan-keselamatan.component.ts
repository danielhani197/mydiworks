import { Component, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material';

import {JawapanKeselamatan} from 'app/main/content/model/setting/jawapanKeselamatan';
import {JawapanKeselamatanService} from 'app/main/content/services/setting/jawapanKeselamatan.service';
import {SoalanKeselamatan} from 'app/main/content/model/setting/soalanKeselamatan';
import {SoalanKeselamatanService} from 'app/main/content/services/setting/soalanKeselamatan.service';
import {User} from 'app/main/content/model/user';
import {AuthenticationService} from 'app/main/content/services/authentication/authentication.service';


import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';

@Component({
    selector  : 'jawapan-keselamatan',
    templateUrl : './jawapan-keselamatan.component.html',
    styleUrls : ['./jawapan-keselamatan.component.scss'],
    animations : fuseAnimations


})
export class JawapanKeselamatanComponent implements OnInit
{

  _id: string;
  _answId: string;
  verifiedId: string;

  s1: SoalanKeselamatan;
  s2: SoalanKeselamatan;
  s3: SoalanKeselamatan;
  s4: SoalanKeselamatan;
  s5: SoalanKeselamatan;

  soalan1 :string;
  soalan2 : string;
  soalan3 : string;
  soalan4 : string;
  soalan5 : string;

  jawapanForm: FormGroup;
  jawapanFormErrors: any;
  jawapanKeselamatan: JawapanKeselamatan;

  currentUser: any;

  successTxt: string;
  existTxt: string;

  soalanKeselamatan: SoalanKeselamatan;
  soalanLs :  SoalanKeselamatan[] = new Array();
  currentSoalan  : any;

  user: User;

  jawapanLs : JawapanKeselamatan[] = new Array();

  // private jawapanLs = JawapanKeselamatan[];

  loadingIndicator = true;
  reorderable = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(

  private http: HttpClient,
  private fuseTranslationLoader: FuseTranslationLoaderService,
  private formBuilder: FormBuilder,
  private route: ActivatedRoute,
  public snackBar: MatSnackBar,
  private _translate: TranslateService,
  private _jawapanKeselamatan : JawapanKeselamatanService,
  private _soalanKeselamatan : SoalanKeselamatanService,
  private _authentication  : AuthenticationService,
  private _router: Router
)
  {
    this.fuseTranslationLoader.loadTranslations(malay, english);

    this._translate.get('JAWAPAN.ERROR').subscribe((res: string) => {
        this.existTxt = res;
    });
    this._translate.get('JAWAPAN.SUCCESS').subscribe((res: string) => {
        this.successTxt = res;
    });

    this.jawapanFormErrors = {
      jawapan: {},
      soalanKeselamatan: {},
    };


  }

  ngOnInit()
  {
    this.jawapanForm = this.formBuilder.group({
      jawapan1:  ['', [Validators.required]],
      jawapan2:  ['', [Validators.required]],
      jawapan3:  ['', [Validators.required]],
      jawapan4:  ['', [Validators.required]],
      jawapan5:  ['', [Validators.required]],
      soalanKeselamatan1: ['',[Validators.required]],
      soalanKeselamatan2: ['',[Validators.required]],
      soalanKeselamatan3: ['',[Validators.required]],
      soalanKeselamatan4: ['',[Validators.required]],
      soalanKeselamatan5: ['',[Validators.required]],

    });

    this.jawapanForm.valueChanges.subscribe(() => {
      this.onJawapanFormValuesChanged();
    });

    // load Soalan Keselamatan value
      this._soalanKeselamatan.getSoalanKeselamatan().subscribe(
        data => {
          this.soalanLs = data;
      });

      // console.log(this._id)
        this.user = this._authentication.getCurrentUser();

            this._id  = this.user.id;

            this._jawapanKeselamatan.getJawapanKeselamatanById(this._id).subscribe(
              jawapanKeselamatan  =>  {
                let ls : JawapanKeselamatan[] = new Array();
                ls = jawapanKeselamatan;

                this.verifiedId = ls[0].id
            if(this.verifiedId){
              

                for(var i = 0; i<ls.length ; i++){

                  let obj = ls[i];
                  //console.log(i,"<--------->", obj)
                  var id = obj.id;
                  var soalanKeselamatan = obj.soalanKeselamatan;
                  var rank = obj.rank;
                  var jawapan = obj.jawapan;


                  if (rank == "1"){
                    this.soalan1 = id;
                    this.s1 = soalanKeselamatan;
                    this.jawapanForm.patchValue({
                      jawapan1 : jawapan,
                      soalanKeselamatan1 : soalanKeselamatan.id
                    })
                  }

                if (rank == "2"){
                  this.soalan2 = id;
                  this.s2 = soalanKeselamatan;
                  this.jawapanForm.patchValue({
                    jawapan2 : jawapan,
                    soalanKeselamatan2 : soalanKeselamatan.id
                  })
                }

                if (rank == "3"){
                  this.soalan3 = id;
                  this.s3 = soalanKeselamatan;
                  this.jawapanForm.patchValue({
                    jawapan3 : jawapan,
                    soalanKeselamatan3 : soalanKeselamatan.id
                  })
                }

                if (rank == "4"){
                  this.soalan4 = id;
                  this.s4 = soalanKeselamatan;
                  this.jawapanForm.patchValue({
                    jawapan4 : jawapan,
                    soalanKeselamatan4 : soalanKeselamatan.id
                  })
                }

                if (rank == "5"){
                  this.soalan5 = id;
                  this.s5 = soalanKeselamatan;
                  this.jawapanForm.patchValue({
                    jawapan5 : jawapan,
                    soalanKeselamatan5 : soalanKeselamatan.id
                    })
                  }

                }
              }
            }
          )
  };

  hantar(){

    if(this.verifiedId){

      let jawapanKeselamatan1: JawapanKeselamatan = new JawapanKeselamatan ();
      jawapanKeselamatan1.setJawapan(this.jawapanForm.controls['jawapan1'].value),
      jawapanKeselamatan1.setSoalanKeselamatan(this.s1),
      jawapanKeselamatan1.setId(this.soalan1),
      jawapanKeselamatan1.setRank("1"),
      jawapanKeselamatan1.setUser(this.user)


      let jawapanKeselamatan2: JawapanKeselamatan = new JawapanKeselamatan ();
      jawapanKeselamatan2.setJawapan(this.jawapanForm.controls['jawapan2'].value),
      jawapanKeselamatan2.setSoalanKeselamatan(this.s2),
      jawapanKeselamatan2.setId(this.soalan2),
      jawapanKeselamatan2.setRank("2"),

      jawapanKeselamatan2.setUser(this.user)


      let jawapanKeselamatan3: JawapanKeselamatan = new JawapanKeselamatan ();
      jawapanKeselamatan3.setJawapan(this.jawapanForm.controls['jawapan3'].value),
      jawapanKeselamatan3.setSoalanKeselamatan(this.s3),
      jawapanKeselamatan3.setId(this.soalan3),
      jawapanKeselamatan3.setRank("3"),
      jawapanKeselamatan3.setUser(this.user)


      let jawapanKeselamatan4: JawapanKeselamatan = new JawapanKeselamatan ();
      jawapanKeselamatan4.setJawapan(this.jawapanForm.controls['jawapan4'].value),
      jawapanKeselamatan4.setSoalanKeselamatan(this.s4),
      jawapanKeselamatan4.setId(this.soalan4),
      jawapanKeselamatan4.setRank("4"),
      jawapanKeselamatan4.setUser(this.user)


      let jawapanKeselamatan5: JawapanKeselamatan = new JawapanKeselamatan ();
      jawapanKeselamatan5.setJawapan(this.jawapanForm.controls['jawapan5'].value),
      jawapanKeselamatan5.setSoalanKeselamatan(this.s5),
      jawapanKeselamatan5.setId(this.soalan5),
      jawapanKeselamatan5.setRank("5"),
      jawapanKeselamatan5.setUser(this.user)

      this.jawapanLs.push(jawapanKeselamatan1);
      this.jawapanLs.push(jawapanKeselamatan2);
      this.jawapanLs.push(jawapanKeselamatan3);
      this.jawapanLs.push(jawapanKeselamatan4);
      this.jawapanLs.push(jawapanKeselamatan5);
      this._jawapanKeselamatan.updateJawapanKeselamatan(this.jawapanLs).subscribe(
        success=>{
          this.snackBar.open(this.successTxt, "OK", {
              panelClass: ['blue-snackbar']
            });
            this._router.navigate(['/setting/security_ans']);
       },
        error=>{
          
          this.snackBar.open(this.existTxt, "OK", {
              panelClass: ['red-snackbar']
            });
            this._router.navigate(['/setting/security_ans']);
       }
      )

    }else{
      let jawapanKeselamatan1: JawapanKeselamatan = new JawapanKeselamatan ();
      jawapanKeselamatan1.setJawapan(this.jawapanForm.controls['jawapan1'].value),
      jawapanKeselamatan1.setSoalanKeselamatan(this.s1),
      jawapanKeselamatan1.setRank("1"),
      jawapanKeselamatan1.setUser(this.user)


      let jawapanKeselamatan2: JawapanKeselamatan = new JawapanKeselamatan ();
      jawapanKeselamatan2.setJawapan(this.jawapanForm.controls['jawapan2'].value),
      jawapanKeselamatan2.setSoalanKeselamatan(this.s2),
      jawapanKeselamatan2.setRank("2"),
      jawapanKeselamatan2.setUser(this.user)


      let jawapanKeselamatan3: JawapanKeselamatan = new JawapanKeselamatan ();
      jawapanKeselamatan3.setJawapan(this.jawapanForm.controls['jawapan3'].value),
      jawapanKeselamatan3.setSoalanKeselamatan(this.s3),
      jawapanKeselamatan3.setRank("3"),
      jawapanKeselamatan3.setUser(this.user)


      let jawapanKeselamatan4: JawapanKeselamatan = new JawapanKeselamatan ();
      jawapanKeselamatan4.setJawapan(this.jawapanForm.controls['jawapan4'].value),
      jawapanKeselamatan4.setSoalanKeselamatan(this.s4),
      jawapanKeselamatan4.setRank("4"),
      jawapanKeselamatan4.setUser(this.user)


      let jawapanKeselamatan5: JawapanKeselamatan = new JawapanKeselamatan ();
      jawapanKeselamatan5.setJawapan(this.jawapanForm.controls['jawapan5'].value),
      jawapanKeselamatan5.setSoalanKeselamatan(this.s5),
      jawapanKeselamatan5.setRank("5"),
      jawapanKeselamatan5.setUser(this.user)

      this.jawapanLs.push(jawapanKeselamatan1);
      this.jawapanLs.push(jawapanKeselamatan2);
      this.jawapanLs.push(jawapanKeselamatan3);
      this.jawapanLs.push(jawapanKeselamatan4);
      this.jawapanLs.push(jawapanKeselamatan5);

      console.log(this.jawapanLs)
      this._jawapanKeselamatan.createJawapanKeselamatan(this.jawapanLs).subscribe(
        success=>{
          this.snackBar.open(this.successTxt, "OK", {
              panelClass: ['blue-snackbar']
            });
            this._router.navigate(['/setting/security_ans']);
       },
        error=>{
          
          this.snackBar.open(this.existTxt, "OK", {
              panelClass: ['red-snackbar']
            });
            this._router.navigate(['/setting/security_ans']);
       }
      )
    }
  }

  onJawapanFormValuesChanged(){
    for ( const field in this.jawapanFormErrors )
    {
        if ( !this.jawapanFormErrors.hasOwnProperty(field) )
        {
            continue;
        }

        // Clear previous errors
        this.jawapanFormErrors[field] = {};

        // Get the control
        const control = this.jawapanForm.get(field);

        if ( control && control.dirty && !control.valid )
        {
            this.jawapanFormErrors[field] = control.errors;
        }
    }
  }

  setSoalanKeselamatan1(soalanId: any):  void{
    console.log(soalanId)
      this.currentSoalan  = this.soalanLs.filter(value => value.id === soalanId)
      this.soalanKeselamatan = this.currentSoalan[0];
      this.s1 = this.soalanKeselamatan;
      
  }
  setSoalanKeselamatan2(soalanId: any):  void{
    console.log(soalanId)
    this.currentSoalan  = this.soalanLs.filter(value => value.id === soalanId)
    this.soalanKeselamatan = this.currentSoalan[0];
    this.s2 = this.soalanKeselamatan;
  }
  setSoalanKeselamatan3(soalanId: any):  void{
    console.log(soalanId)
    this.currentSoalan  = this.soalanLs.filter(value => value.id === soalanId)
    this.soalanKeselamatan = this.currentSoalan[0];
    this.s3 = this.soalanKeselamatan;
  }
  setSoalanKeselamatan4(soalanId: any):  void{
    console.log(soalanId)
    this.currentSoalan  = this.soalanLs.filter(value => value.id === soalanId)
    this.soalanKeselamatan = this.currentSoalan[0];
    this.s4 = this.soalanKeselamatan;
  }
  setSoalanKeselamatan5(soalanId: any):  void{
    console.log(soalanId)
    this.currentSoalan  = this.soalanLs.filter(value => value.id === soalanId)
    this.soalanKeselamatan = this.currentSoalan[0];
    this.s5 = this.soalanKeselamatan;
  }

  redirectAgencyPage() {
      this._router.navigate(['/setting/security_ans']);
  }

}
