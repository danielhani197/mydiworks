import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';

import {SoalanKeselamatan} from 'app/main/content/model/setting/soalanKeselamatan';
import {SoalanKeselamatanService} from 'app/main/content/services/setting/soalanKeselamatan.service';


@Component({
    selector: 'delete-dialog',
    templateUrl: 'delete-dialog.component.html',
    animations : fuseAnimations
    })

    export class DeleteDialogComponent implements OnInit
    {
      _id: string

      successTxt: string;
      existTxt: string;

      constructor(
      private _translate: TranslateService,
      private fuseTranslationLoader: FuseTranslationLoaderService,
      private _agensi: SoalanKeselamatanService,
      private _toastr: ToastrService,
      private _router: Router,
      public _dialogRef: MatDialogRef<DeleteDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any
    ){
      this._translate.get('SOALAN.DELETESOALAN.ERROR').subscribe((res: string) => {
          this.existTxt = res;
      });
      this._translate.get('SOALAN.DELETESOALAN.SUCCESS').subscribe((res: string) => {
          this.successTxt = res;
      });
      this.fuseTranslationLoader.loadTranslations(malay, english);
    }

    ngOnInit(){
        this._id = this.data.id;
    }

    confirmDelete(): void {

        this._agensi.deleteSoalanKeselamatanById(this._id).subscribe(
            success=>{
                this._toastr.success(this.successTxt, null, {
                    positionClass: 'toast-top-right'
                });
                this._dialogRef.close();
            },
            error=>{

                this._toastr.error(this.existTxt, null, {
                    positionClass: 'toast-top-right'
                });
                this._dialogRef.close();
            }
        )

    }


  }
