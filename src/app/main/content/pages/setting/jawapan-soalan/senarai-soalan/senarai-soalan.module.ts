import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import {
  MatButtonModule,
  MatCheckboxModule,
  MatIconModule,
  MatFormFieldModule,
  MatInputModule,
  MatChipsModule,
  MatMenuModule,
  MatDialogModule,
  MatStepperModule
} from '@angular/material';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { SenaraiSoalanComponent } from './senarai-soalan.component';
import { SenaraiSoalanDialogComponent } from './senarai-soalan-dialog.component';
import { DeleteDialogComponent } from './delete-dialog.component';



const routes = [
  {
      path  : 'question_list',
      component : SenaraiSoalanComponent

  }
];
@NgModule({
    declarations: [
        SenaraiSoalanComponent,
        SenaraiSoalanDialogComponent,
        DeleteDialogComponent
    ],
    entryComponents: [
    SenaraiSoalanDialogComponent,
    DeleteDialogComponent
    ],

    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatMenuModule,
        MatDialogModule,
        MatStepperModule,

        NgxDatatableModule,

        FuseSharedModule,
    ],
    exports: [

    MatDialogModule,

    ]
})
export class SenaraiSoalanModule
{

}
