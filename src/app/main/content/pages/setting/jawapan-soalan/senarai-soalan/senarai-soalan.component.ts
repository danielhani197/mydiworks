import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {MatDialog,  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Page } from 'app/main/content/model/util/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import {SoalanKeselamatan} from 'app/main/content/model/setting/soalanKeselamatan';
import {SoalanKeselamatanService} from 'app/main/content/services/setting/soalanKeselamatan.service';
import {SenaraiSoalanDialogComponent} from './senarai-soalan-dialog.component';
import {DeleteDialogComponent} from './delete-dialog.component';


@Component({
    selector   : 'senarai-soalan',
    templateUrl: './senarai-soalan.component.html',
    styleUrls  : ['./senarai-soalan.component.scss']
})
export class SenaraiSoalanComponent implements OnInit
{


    rows: any[];
    temp = [];
    page = new Page();

    _idToUpdate : string;
    _idToDelete : string;
    loadingIndicator = true;
    reorderable = true;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private http: HttpClient,
        private _router: Router,
        private _soalanKeselamatan : SoalanKeselamatanService,
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        public _dialog: MatDialog

    )
    {
      this.fuseTranslationLoader.loadTranslations(malay, english);

      this.page.pageNumber = 0;
      this.page.size = 10;
      this.page.sortField = "keterangan";
      this.page.sort = "asc";
    }
    ngOnInit(){
        this.setPage({ offset: 0 });


    }

    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._soalanKeselamatan.getSoalanKeselamatanList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
                    console.log(this.rows)
        });
    }

    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._soalanKeselamatan.getSoalanKeselamatanList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;

        this._soalanKeselamatan.getSoalanKeselamatanList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    tambahSoalan(){
    let dialogRef = this._dialog.open(SenaraiSoalanDialogComponent,{
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(
        result => {
            this.setPage({ offset: 0 });
        }
      );
  }

    updateSoalan(id: string){
      this._idToUpdate = id;
      let dialogRef = this._dialog.open(SenaraiSoalanDialogComponent, {
        width: '500px',
        data: {id: this._idToUpdate}

      });
      dialogRef.afterClosed().subscribe(result => {
          this.setPage({ offset: 0 });
      });
    }

    deleteSoalan(id: string){
        this._idToDelete = id;
        let dialogRef = this._dialog.open(DeleteDialogComponent, {
            width: '500px',
            data: { id: this._idToDelete }
        });

        dialogRef.afterClosed().subscribe(result => {
            this.setPage({ offset: 0 });
        });
    }

    reloadSenarai(){
        this._soalanKeselamatan.getSoalanKeselamatanList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
                    console.log(this.rows)
        });
    }
}
