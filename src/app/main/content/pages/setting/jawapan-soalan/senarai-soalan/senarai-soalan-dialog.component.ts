import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { MatSnackBar } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';

import {SoalanKeselamatan} from 'app/main/content/model/setting/soalanKeselamatan';
import {SoalanKeselamatanService} from 'app/main/content/services/setting/soalanKeselamatan.service';


@Component({
  selector: 'senarai-soalan-dialog',
  templateUrl: 'senarai-soalan-dialog.component.html',
  animations : fuseAnimations
})
export class SenaraiSoalanDialogComponent implements OnInit
{
   _id: string;
   // id: string;
   soalanForm: FormGroup;
   soalanKeselamatan: SoalanKeselamatan;
   soalanFormErrors: any;

   errKeteranganExist: boolean = false;

   successTxt: string;
   existTxt: string;
   keteranganTxt: string;

   constructor(
       private _translate: TranslateService,
       private fuseTranslationLoader: FuseTranslationLoaderService,
       private formBuilder: FormBuilder,
       private route: ActivatedRoute,
       private _soalanKeselamatan: SoalanKeselamatanService,
       public snackBar: MatSnackBar,
       private _router: Router,
       public _dialogRef: MatDialogRef<SenaraiSoalanDialogComponent>,
       @Inject(MAT_DIALOG_DATA) public data: any
   )
   {
     this.fuseTranslationLoader.loadTranslations(malay, english);

     this._translate.get('SOALAN.ERROR').subscribe((res: string) => {
         this.existTxt = res;
     });
     this._translate.get('SOALAN.SUCCESS').subscribe((res: string) => {
         this.successTxt = res;
     });

     this._translate.get('SOALAN.EXIST').subscribe((res: string) => {
          this.keteranganTxt = res;
     });

     this.fuseTranslationLoader.loadTranslations(malay, english);
   }

   ngOnInit()
   {
     this.soalanForm = this.formBuilder.group({
       keterangan : ['', [Validators.required]]
     });

     this.soalanForm.valueChanges.subscribe(() => {
         this.onSoalanFormValuesChanged();
     });

     this._id = this.data.id;

     //this._id = this.route.snapshot.paramMap.get('id');

     // load
     if (this._id){
       this._soalanKeselamatan.getSoalanKeselamatanById(this._id).subscribe(
         soalanKeselamatan => {
           this.soalanKeselamatan = soalanKeselamatan;
           this.soalanForm.setValue({
             keterangan: soalanKeselamatan.keterangan,
           });
         }
       )
     }
   }

   hantar(){
    this.resetCustomError();

     if(this._id){
        let soalanKeselamatan: SoalanKeselamatan = new SoalanKeselamatan();
        soalanKeselamatan.setKeterangan(this.soalanForm.controls['keterangan'].value),
        soalanKeselamatan.setId(this._id)

        this._soalanKeselamatan.updateSoalanKeselamatan(soalanKeselamatan).subscribe(
          // console.log(id);
          success=>{
            this.snackBar.open(this.successTxt, "OK", {
                panelClass: ['blue-snackbar']
              });
              this._dialogRef.close();
         },
         error => {

          var message = error.error.errorMessage;

          if(message == "ERR_EXIST_KETERANGAN"){
              this.errKeteranganExist = true;
          }

          this.snackBar.open(this.keteranganTxt, "OK", {
            panelClass: ['red-snackbar']
          });
      }

        )

     }else{
       let soalanKeselamatan: SoalanKeselamatan = new SoalanKeselamatan ();
       soalanKeselamatan.setKeterangan(this.soalanForm.controls['keterangan'].value)

       this._soalanKeselamatan.createSoalanKeselamatan(soalanKeselamatan).subscribe(
        success=>{
          this.snackBar.open(this.successTxt, "OK", {
              panelClass: ['blue-snackbar']
            });
            this._dialogRef.close();

       },
       error=>{         
        var message = error.error.errorMessage;

        if(message == "ERR_EXIST_KETERANGAN"){
            this.errKeteranganExist = true;
        }

        this.snackBar.open(this.keteranganTxt, "OK", {
          panelClass: ['red-snackbar']
        });
        
     }
       )
     }
   }

   resetCustomError(){
     this.errKeteranganExist = false;
   }

   onSoalanFormValuesChanged(){
       for ( const field in this.soalanFormErrors )
       {
           if ( !this.soalanFormErrors.hasOwnProperty(field) )
           {
               continue;
           }

           // Clear previous errors
           this.soalanFormErrors[field] = {};

           // Get the control
           const control = this.soalanForm.get(field);

           if ( control && control.dirty && !control.valid )
           {
               this.soalanFormErrors[field] = control.errors;
           }
       }
   }


   redirectSoalanPage() {
     this._router.navigate(['/setting/question_list']);
   }

}
