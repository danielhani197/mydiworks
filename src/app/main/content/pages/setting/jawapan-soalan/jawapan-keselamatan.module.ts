import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { MatButtonModule,
         MatFormFieldModule,
         MatInputModule,
         MatCheckboxModule,
         MatIconModule,
         MatSelectModule,
         MatStepperModule

       } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { JawapanKeselamatanComponent} from './jawapan-keselamatan.component';

const routes = [

  {
    path : 'security_ans',
    component : JawapanKeselamatanComponent
  },
  {
      path     : 'security_ans/:id',
      component: JawapanKeselamatanComponent
  }
];

@NgModule({
    declarations : [
      JawapanKeselamatanComponent
    ],
    imports :[
    RouterModule.forChild(routes),

    TranslateModule,

    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatStepperModule,

    FuseSharedModule
  ]
})
export class JawapanKeselamatanModule
 {

 }
