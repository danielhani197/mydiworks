import { Component, OnInit,ViewChild, Inject, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { HttpClient } from '@angular/common/http';

import { Router,  ActivatedRoute } from "@angular/router";
import {MatDialog} from '@angular/material';


import { MatSnackBar } from '@angular/material';
import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

import { User } from 'app/main/content/model/user';
import { AppAuthority } from "../../../../model/appAuthority";
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service'
import { RoleService } from '../../../../services/role.service';
import { PenggunaService } from '../../../../services/pengguna/pengguna.service';
import { Agensi } from 'app/main/content/model/general/agensi';
import { Kementerian } from 'app/main/content/model/general/kementerian';
import { AgensiService } from 'app/main/content/services/agency/agensi.service';
import { KementerianService } from 'app/main/content/services/agency/kementerian.service';
import { HistoryRouteService } from '../../../../services/util/historyroute.service';
import { Notifikasi } from 'app/main/content/model/notifikasi';
import { NotifikasiService } from 'app/main/content/services/setting/notifikasi.service';


@Component({
    selector   : 'maklumat-peranan',
    templateUrl: './maklumat-peranan.component.html',
    styleUrls  : ['./maklumat-peranan.component.scss'],

    animations : fuseAnimations
})

export class MaklumatPerananComponent implements OnInit{

    id: string;
    detailForm: FormGroup;
    successTxt: string;

    agensi: Agensi;
    kementerian: Kementerian;
    user : User;
    currentUser : User;
    userId: string;
    app: AppAuthority;
 

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _router: Router,
        private _translate: TranslateService,
        public snackBar: MatSnackBar,
        private _pengguna: PenggunaService,
        private _role: RoleService,
        private _auth: AuthenticationService,
        private _agensi: AgensiService,
        private _kementerian: KementerianService,
        private _routeHist: HistoryRouteService,
        private _noti: NotifikasiService
 
    
      ){

        this.fuseTranslationLoader.loadTranslations(malay, english);

        this._translate.get('ROLE.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
      }

    ngOnInit(){

        this.id = this.route.snapshot.paramMap.get('id');
        this.currentUser = this._auth.getCurrentUser();

        this.detailForm = this.formBuilder.group({
            name:  [{value :'', disabled:true}],
            agensi:  [{value :'', disabled:true}],
            email:  [{value :'', disabled:true}],
            role:  [{value :'', disabled:true}],
            remarks: [{value :'', disabled:true}],
            adminremarks: ['',[Validators.required]],
            
   
          });

    // load
     if (this.id){
        this._role.getRoleById(this.id).subscribe(
          role => {


            if(role.user.agensi != null){
              this.agensi = role.user.agensi;
              this._agensi.getAgencyById(this.agensi.id).subscribe(
                agensi=>{
                  this.detailForm.patchValue({
                    agensi: agensi.name
                  })
                }
              )
            }else{
              this.kementerian = role.user.kementerian
              this._kementerian.getKementerianById(this.kementerian.id).subscribe(
                kementerian=>{
                  this.detailForm.patchValue({
                    agensi: kementerian.name
                  })
                }
              )
            }
            this.user = role.user;            
            this.app = role;
            this.detailForm.patchValue({

              name: role.user.name,
              email: role.user.email,
              role: role.roleid,
              remarks: role.remarks,
              adminremarks: role.adminremarks

            });
          }
        )
      }

    }

    hantar(){  
      if(this.id){
          let app: AppAuthority = new AppAuthority ();
          app.setAdminRemarks(this.detailForm.controls['adminremarks'].value)
          app.setStatus("APPROVE");
          app.setId(this.id);
          app.setUser(this.user);
          app.setModifiedby(this.currentUser)
          console.log(app)
          this._role.updateRole(app).subscribe(
          success=>{
            this._pengguna.getUserById(this.id).subscribe(
              data =>{
                  let noti: Notifikasi = new Notifikasi()
                  noti.setCreatedby(this._auth.getCurrentUser()),
                  noti.setUser(data),
                  noti.setMaklumat("Approve Your Request As Admin Agency"),
                  noti.setStatus("New"),
                  noti.setTindakan(""),
                  noti.setModifiedby(this._auth.getCurrentUser())
                  
                  this._noti.createNoti(noti).subscribe(
                      success=>{ 
                          
                      }
                  )
              }
          )
            this.snackBar.open(this.successTxt, "OK", {
                panelClass: ['blue-snackbar']
              });

              if(HistoryRouteService.ISADMIN === this._routeHist.getRole()){
                this._router.navigate(['agensi/pengguna/peranan']);
              }else{
                this._router.navigate(['user/role_list']);
              }

              
          })
        }
    }

    reject(){
      if(this.id){
        
        let app: AppAuthority = new AppAuthority ();
        app.setAdminRemarks(this.detailForm.controls['adminremarks'].value)
        app.setStatus("REJECT");
        app.setId(this.id);
        app.setUser(this.user)

        this._role.updateRole(app).subscribe(
         success=>{
          console.log(this.user.id)
          this._pengguna.getUserById(this.user.id).subscribe(
            data =>{
                console.log(data)
                let noti: Notifikasi = new Notifikasi()
                noti.setCreatedby(this._auth.getCurrentUser()),
                noti.setUser(data),
                noti.setMaklumat("Reject Your Request As Admin Agency"),
                noti.setStatus("New"),
                noti.setTindakan(""),
                noti.setModifiedby(this._auth.getCurrentUser())
                
                this._noti.createNoti(noti).subscribe(
                    success=>{ 
                        
                    }
                )
            }
        )
           this.snackBar.open(this.successTxt, "OK", {
               panelClass: ['blue-snackbar']
             });
             if(HistoryRouteService.ISADMIN === this._routeHist.getRole()){
              this._router.navigate(['agensi/pengguna/peranan']);
            }else{
              this._router.navigate(['user/role_list']);
            }
        })
      }
    }

    redirectPage(){
      if(HistoryRouteService.ISADMIN === this._routeHist.getRole()){
        this._router.navigate(['agensi/pengguna/peranan']);
      }else{
        this._router.navigate(['user/role_list']);
      }
    } 

      
}

