import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule,
          MatCheckboxModule,
          MatFormFieldModule,
          MatInputModule,
          MatIconModule,
          MatSelectModule,
          MatStepperModule,
          MatDialogModule,
          MatChipsModule,
          MatMenuModule,
          MatSortModule,
          MatPaginatorModule,
          MatTableModule,
          MatSidenavModule,
          MatRippleModule,
          MatToolbarModule, 

          MatSnackBarModule } from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { MaklumatPerananComponent } from './maklumat-peranan.component';

const routes = [
    { 
        path  :'role_details/:id', 
        component:  MaklumatPerananComponent 
  } 
];

@NgModule({
    declarations: [
        MaklumatPerananComponent,

    ],

    imports     : [
        RouterModule.forChild(routes),
        MatButtonModule, 
        MatCheckboxModule, 
        MatFormFieldModule, 
        MatInputModule, 
        MatIconModule, 
        MatSelectModule, 
        MatStepperModule, 
        MatSnackBarModule, 
        MatSidenavModule, 
        MatMenuModule,
        MatRippleModule,
        MatToolbarModule,
        MatChipsModule,
        MatDialogModule,
        MatSortModule,
        MatPaginatorModule,
        MatTableModule,

      SweetAlert2Module,

      NgxDatatableModule,

      FuseSharedModule,
      TranslateModule
    ],
})
export class MaklumatPerananModule
{
}