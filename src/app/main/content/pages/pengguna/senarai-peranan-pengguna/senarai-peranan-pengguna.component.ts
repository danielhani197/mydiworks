import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {MatDialog,  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Page } from 'app/main/content/model/util/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material';

import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import {AppAuthority} from 'app/main/content/model/appAuthority';
import {RoleService} from 'app/main/content/services/role.service';
import { HistoryRouteService } from '../../../services/util/historyroute.service';


@Component({
    selector   : 'senarai-peranan-pengguna',
    templateUrl: './senarai-peranan-pengguna.component.html',
    styleUrls  : ['./senarai-peranan-pengguna.component.scss']
})
export class SenaraiPerananPenggunaComponent implements OnInit
{
    id: string;
    rows: any[];
    temp = [];
    page = new Page();

    _idToUpdate : string;
    _idToDelete : string;
    loadingIndicator = true;
    reorderable = true;

    successMsg : string;
    failMsg : string;

    successDelMsg : string;
    failDelMsg : string;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private http: HttpClient,
        private _router: Router,
        private _role : RoleService,
        private translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        public _dialog: MatDialog,
        public snackBar: MatSnackBar,
        private _routeHist: HistoryRouteService


    )
    {
      this.fuseTranslationLoader.loadTranslations(malay, english);
        
      this.translate.get('ROLE.APPROVE.SUCCESS').subscribe((res: string) => {
        this.successMsg = res;
        });
        this.translate.get('ROLE.APPROVE.ERROR').subscribe((res: string) => {
            this.failMsg = res;
        });
        this.translate.get('ROLE.REJECT.SUCCESS').subscribe((res: string) => {
            this.successDelMsg = res;
        });
        this.translate.get('ROLE.REJECT.ERROR').subscribe((res: string) => {
            this.failDelMsg = res;
        });
     
      this.page.pageNumber = 0;
      this.page.size = 10;
      this.page.sortField = "createdon";
      this.page.sort = "asc";
      this.page.status = "APPROVE";

      this._routeHist.setRole(HistoryRouteService.ISSAM);
    }

    ngOnInit(){
        this.setPage({ offset: 0 });


    }

    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._role.getRoleList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._role.getRoleList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;

        this._role.getRoleList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    //  approveRole(id: string){

    //     let app: AppAuthority = new AppAuthority();         
    //     app.id = id;
    //     app.status = "APPROVE";

    //     this._role.updateRole(app).subscribe(
    //         success => {                    
    //             this.snackBar.open(this.successMsg, "OK");                 
    //             this.setPage({ offset: 0 });

    //     },
    //     error => {
    //             this.snackBar.open(this.failMsg, "OK",{
    //             panelClass: ['red-snackbar']
    //             });
    //         }
    //     );
    //  }
                

    // rejectRole(id: string){

    //     let app: AppAuthority = new AppAuthority();
    //     app.id = id;
    //     app.status = "REJECT";
        
    //     this._role.updateRole(app).subscribe(
    //         success => {
                
    //             this.snackBar.open(this.successDelMsg, "OK",{
    //                 panelClass: ['blue-snackbar']
    //             });                   
    //             this.setPage({ offset: 0 });
    //         },
    //         error => {
    //             this.snackBar.open(this.failDelMsg, "OK",{
    //                 panelClass: ['red-snackbar']
    //             });
    //         }
    //     ); 
    // }

    openDetail(id:string){
        this._router.navigate(['user/role_details/'+id]);

    }
}
