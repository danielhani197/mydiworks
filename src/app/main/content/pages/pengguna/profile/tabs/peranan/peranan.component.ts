import { Component, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PenggunaService } from '../../../../../services/pengguna/pengguna.service';
import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from '../../../i18n/en';
import { locale as malay } from '../../../i18n/my';
import { UserAuthorities } from '../../../../../model/userAuthorities';
import { User } from '../../../../../model/user';
import { AuthenticationService } from '../../../../../services/authentication/authentication.service';
import { Authority } from '../../../../../model/authority';
import { MatSnackBar } from '@angular/material';
import { HistoryRouteService } from '../../../../../services/util/historyroute.service';

@Component({
    selector   : 'profile-peranan',
    templateUrl: './peranan.component.html',
    styleUrls  : ['./peranan.component.scss'],
    animations : fuseAnimations
})
export class PerananComponent
{
    @ViewChild('PENGGUNA') public pengguna;
    @ViewChild('PENTADBIR_AGENSI') public pentadbirAgensi;
    @ViewChild('PENTADBIR_SISTEM') public pentadbirSistem;
    
    currentAdmin: User;
    user: User;
    _id: string;
    listAuthorities: UserAuthorities[] = new Array<UserAuthorities>();
    listToSend: UserAuthorities[] = new Array<UserAuthorities>();
    successTxt: string;
    existTxt: string;
    
    constructor(    
        private formBuilder: FormBuilder,
        private _router: Router,
        private route: ActivatedRoute,
        private _pengguna: PenggunaService,
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _auth: AuthenticationService,
        private snackBar: MatSnackBar,
        private _routeHist: HistoryRouteService
    )
    {
        this._id = this.route.snapshot.paramMap.get('id');
        this.fuseTranslationLoader.loadTranslations(malay, english);

        this._translate.get('USER.EDIT.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('USER.EDIT.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
    } 
    
    ngOnInit(){
        this.loadPengguna();
        this.currentAdmin = this._auth.getCurrentUser();
    }

    loadPengguna(){
        this._pengguna.getUserById(this._id).subscribe(
            user=>{
                this.user = user;
                this._pengguna.getAuthoritiesListByUserId(this._id).subscribe(
                    data=>{
                        
                        this.listAuthorities = data;
                        for(let obj of this.listAuthorities){
                            if (obj.authority){
                                if(obj.authority.name == 'ROLE_USER'){
                                    this.pengguna.checked = true;
                                }
                                if (obj.authority.name == 'ROLE_ADMIN'){
                                    this.pentadbirAgensi.checked = true;
                                }
                                if (obj.authority.name == 'ROLE_SUPERADMIN'){
                                    this.pentadbirSistem.checked = true;
                                }
                            }
                        }
                    }
                )
            }
        )
    }

    backButton(){
        if(HistoryRouteService.ISADMIN === this._routeHist.getRole()){
            this._router.navigate(['/agensi/pengguna/senarai']);
        }else{
            this._router.navigate(['/user/list']);
        }
    }

    hantar(){
        this.listToSend = [];

        if(this._id){
            //userAuthority pentadbir agensi
            let userAuthorityPentadbirAgensi = new UserAuthorities();
                let authorityPentadbirAgensi = new Authority();
                authorityPentadbirAgensi.setId('SECURITY-AUTHORITY-ADMIN')
            if(this.pentadbirAgensi.checked){
                userAuthorityPentadbirAgensi.setId('checked')
            }else(
                userAuthorityPentadbirAgensi.setId('unchecked')
            )
            userAuthorityPentadbirAgensi.setUser(this.user)
            userAuthorityPentadbirAgensi.setCreatedby(this.currentAdmin.id)
            userAuthorityPentadbirAgensi.setCreatedon(new Date())
            userAuthorityPentadbirAgensi.setAuthority(authorityPentadbirAgensi)
            
            this.listToSend.push(userAuthorityPentadbirAgensi)
        
            //userAuthority pentadbir sistem
            let userAuthorityPentadbirSistem = new UserAuthorities();
                let authorityPentadbirSistem = new Authority();
                authorityPentadbirSistem.setId('SECURITY-AUTHORITY-SUPERADMIN')
            if(this.pentadbirSistem.checked){
                userAuthorityPentadbirSistem.setId('checked')
            }else{
                userAuthorityPentadbirSistem.setId('unchecked')
            }
            userAuthorityPentadbirSistem.setUser(this.user)
            userAuthorityPentadbirSistem.setCreatedby(this.currentAdmin.id)
            userAuthorityPentadbirSistem.setCreatedon(new Date())
            userAuthorityPentadbirSistem.setAuthority(authorityPentadbirSistem)
        
            this.listToSend.push(userAuthorityPentadbirSistem)
        
            //submit here
            this._pengguna.adminKemaskiniPeranan(this.listToSend).subscribe(
                success=>{
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                    });
                    this.loadPengguna();
                },
                error =>{
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });
                }
            )
        }
        
    }
    
}
