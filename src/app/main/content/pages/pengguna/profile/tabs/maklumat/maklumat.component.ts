import { Component } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Negeri } from '../../../../../model/ref/negeri';
import { Bandar } from '../../../../../model/ref/bandar';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from '../../../i18n/en';
import { locale as malay } from '../../../i18n/my';
import { TranslateService } from '@ngx-translate/core';
import { BandarNegeriService } from '../../../../../services/references/bandarNegeri.service';
import { User } from '../../../../../model/user';
import { PenggunaService } from '../../../../../services/pengguna/pengguna.service';
import { MatSnackBar } from '@angular/material';
import { HistoryRouteService } from '../../../../../services/util/historyroute.service';

@Component({
    selector   : 'profile-maklumat',
    templateUrl: './maklumat.component.html',
    styleUrls  : ['./maklumat.component.scss'],
    animations : fuseAnimations
})
export class MaklumatComponent
{
    _id: string;
    maklumatForm: FormGroup;
    maklumatFormErrors: any;
    city: Bandar;
    state: Negeri;
    successTxt: string;
    existTxt: string;
    stateLs: Negeri[] = new Array<Negeri>();
    cities: Bandar[] = new Array<Bandar>();
    currentState: any[];
    currentCity: any[];
    user: User;
    notExistTXT: string

    errUsernameExist = false;
   
    constructor(    
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private _translate: TranslateService,
        private _bandarNegeri: BandarNegeriService,
        private _router: Router,
        private route: ActivatedRoute,
        private _pengguna: PenggunaService,
        public snackBar: MatSnackBar,
        private _routeHist: HistoryRouteService
    )
    {
        this._id = this.route.snapshot.paramMap.get('id');
        this.fuseTranslationLoader.loadTranslations(malay, english);

        // Reactive form errors
        this.maklumatFormErrors = {
            name : {},
            ic: {},
            address : {},
            postcode : {},
            city : {},
            state : {},
        };

        this._translate.get('USER.EDIT.EXIST').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('USER.EDIT.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
        this._translate.get('USER.EDIT.NOTEXIST').subscribe((res: string) => {
            this.notExistTXT = res;
        });
     
    } 
    
    ngOnInit(){
        this.maklumatForm = this.formBuilder.group({
            name    : ['', [Validators.required]],
            ic    : ['', [Validators.required, Validators.pattern("[0-9]{12}")]],
            address    : ['', [Validators.required]],
            postcode    : ['', [Validators.required, Validators.pattern("[0-9]{5}")]],
            city    : ['', [Validators.required]],
            state    : ['', [Validators.required]],
        });

        this.maklumatForm.valueChanges.subscribe(() => {
            this.onMaklumatFormaluesChanged();
        });

        this._bandarNegeri.getState().subscribe(
            state=>{
                this.stateLs = state;
                this.loadPengguna();
            }
        )
    }

    onMaklumatFormaluesChanged(){
        this.errUsernameExist = false
        for ( const field in this.maklumatFormErrors )
        {
            if ( !this.maklumatFormErrors.hasOwnProperty(field) )
            {
                continue;
            }
            
            // Clear previous errors
            this.maklumatFormErrors[field] = {};

            // Get the control
            const control = this.maklumatForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.maklumatFormErrors[field] = control.errors;
            }
        }
    }

    setSBState(id: any): void {
        // Match the selected ID with the ID's in array
        this.currentState = this.stateLs.filter(value => parseInt(value.id) === parseInt(id));
        this.state = this.currentState[0];
        
        //load city based on state
        this._bandarNegeri.getCity(id).subscribe(
          data => {
            this.cities = data;
          }
        );
    }
  
    setSBCity(id: any): void {
        
        this.currentCity = this.cities.filter(value => parseInt(value.id) === parseInt(id));
        this.city = this.currentCity[0];

    }
    
    backButton(){
        if(HistoryRouteService.ISADMIN === this._routeHist.getRole()){
            this._router.navigate(['/agensi/pengguna/senarai']);
        }else{
            this._router.navigate(['/user/list']);
        }
    }

    hantar(){
        if(this._id){
            let user : User = new User();
            user.setId(this._id);
            user.setName(this.maklumatForm.controls['name'].value);
            user.setUsername(this.maklumatForm.controls['ic'].value);
            user.setAddress(this.maklumatForm.controls['address'].value);
            user.setNegeri(this.state);
            user.setBandar(this.city);
            user.setPostcode(this.maklumatForm.controls['postcode'].value);
            this._pengguna.adminkemaskiniMaklumat(user).subscribe(
                success=>{
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                    });
                    this.loadPengguna();
                    this.errUsernameExist = false
                },
                error =>{
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });
                    this.errUsernameExist = true
                }
            )
        }
    }
    

    loadPengguna(){
        this._pengguna.getUserById(this._id).subscribe(
            data=>{
                this.user = data;
                let userState = data.negeri;
                let userCity = this.user.bandar;

                if(userState){
                    this.setSBState(userState.id)
                    
                    if(userCity){
                        this.city = userCity;
                        
                        this.maklumatForm.patchValue({
                            name: data.name,
                            ic: data.username,
                            position: data.position,
                            address: data.address,
                            postcode: data.postcode,
                            state: data.negeri.id,
                            city: data.bandar.id
                        })
                        
                    } else {
                        this.maklumatForm.patchValue({
                            name: data.name,
                            ic: data.username,
                            position: data.position,
                            address: data.address,
                            postcode: data.postcode,
                            state: data.negeri.id
                        })
                        
                    }
                    
                }else{
                    
                    this.maklumatForm.patchValue({
                        name: data.name,
                        ic: data.username,
                        position: data.position,
                        address: data.address,
                        postcode: data.postcode,
                    })
                }
            },
            error=>{
                this.snackBar.open(this.notExistTXT, "OK", {
                    panelClass: ['red-snackbar']
                });
                this.backButton();
            }
        )
    }
}
