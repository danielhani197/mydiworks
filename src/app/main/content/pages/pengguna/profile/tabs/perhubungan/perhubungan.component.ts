import { Component } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PenggunaService } from '../../../../../services/pengguna/pengguna.service';
import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from '../../../i18n/en';
import { locale as malay } from '../../../i18n/my';
import { User } from '../../../../../model/user';
import { MatSnackBar } from '@angular/material';
import { HistoryRouteService } from '../../../../../services/util/historyroute.service';

@Component({
    selector   : 'profile-perhubungan',
    templateUrl: './perhubungan.component.html',
    styleUrls  : ['./perhubungan.component.scss'],
    animations : fuseAnimations
})
export class PerhubunganComponent
{
    _id: string;
    perhubunganForm: FormGroup;
    perhubunganFormErrors: any;
    successTxt: string;
    existTxt: string;

    errEmailExist = false;
    errEmailPersonalExist = false;

    constructor(    
        private formBuilder: FormBuilder,
        private _router: Router,
        private route: ActivatedRoute,
        private _pengguna: PenggunaService,
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private snackBar: MatSnackBar,
        private _routeHist: HistoryRouteService
    )
    {

        this._id = this.route.snapshot.paramMap.get('id');
        this.fuseTranslationLoader.loadTranslations(malay, english);

        // Reactive form errors
        this.perhubunganFormErrors = {
            personalEmail : {},
            workEmail : {},
            telephone : {}
        };

        this._translate.get('USER.EDIT.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('USER.EDIT.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
    
        this.loadPengguna();
    } 
    
    ngOnInit(){
        this.perhubunganForm = this.formBuilder.group({
            workEmail    : ['', [Validators.required, Validators.email]],
            personalEmail    : ['', [Validators.email]],
            telephone    : ['', [Validators.required, Validators.maxLength(15)]],
        });

        this.perhubunganForm.valueChanges.subscribe(() => {
            this.onPerhubunganFormaluesChanged();
        });
    }

    onPerhubunganFormaluesChanged(){
        this.errEmailExist = false;
        this.errEmailPersonalExist = false;
        for ( const field in this.perhubunganFormErrors )
        {
            if ( !this.perhubunganFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.perhubunganFormErrors[field] = {};

            // Get the control
            const control = this.perhubunganForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.perhubunganFormErrors[field] = control.errors;
            }
        }
    }

    backButton(){
        if(HistoryRouteService.ISADMIN === this._routeHist.getRole()){
            this._router.navigate(['/agensi/pengguna/senarai']);
        }else{
            this._router.navigate(['/user/list']);
        }
    }

    hantar(){
        if(this._id){
            let user : User = new User();
            user.setId(this._id);
            user.setUserEmail(this.perhubunganForm.controls['personalEmail'].value);
            user.setEmail(this.perhubunganForm.controls['workEmail'].value);
            user.setPhoneNo(this.perhubunganForm.controls['telephone'].value);
        
            this._pengguna.adminKemaskiniPerhubungan(user).subscribe(
                success=>{
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                    });
                    this.loadPengguna();
                },
                error =>{
                    var messages = error.error;

                    for(let errMessage of messages){
                        
                        if(errMessage == "ERR_EXIST_EMAIL"){

                            this.errEmailExist = true;

                        }else if(errMessage == "ERR_EXIST_EMAIL_PERSONAL"){

                            this.errEmailPersonalExist = true;

                        }else if(errMessage == "ERR_EXIST_EMAIL_BOTH"){

                            this.errEmailExist = true;
                            this.errEmailPersonalExist = true;

                        }
                    }
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });
                }
            )
        }
    }
    
    loadPengguna(){
        this._pengguna.getUserById(this._id).subscribe(
            data=>{
                this.perhubunganForm.patchValue({
                    personalEmail: data.userEmail,
                    workEmail: data.email,
                    telephone: data.phoneNo
                })
            }
        )
    }
}
