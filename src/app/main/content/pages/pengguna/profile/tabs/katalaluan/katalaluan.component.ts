import { Component } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from '../../../i18n/en';
import { locale as malay } from '../../../i18n/my';
import { TranslateService } from '@ngx-translate/core';
import { BandarNegeriService } from '../../../../../services/references/bandarNegeri.service';
import { AuthenticationService } from '../../../../../services/authentication/authentication.service';
import { User } from '../../../../../model/user';
import { PenggunaService } from '../../../../../services/pengguna/pengguna.service';
import { MatSnackBar } from '@angular/material';
import { PasswordValidation } from '../../../../../common/utilities/password-validation';
import { HistoryRouteService } from '../../../../../services/util/historyroute.service';

@Component({
    selector: 'profile-katalaluan',
    templateUrl: './katalaluan.component.html',
    styleUrls: ['./katalaluan.component.scss'],
    animations: fuseAnimations
})
export class KatalaluanComponent {
    _id: string;
    successTxt: string;
    existTxt: string;
    user: User;

    resetPasswordForm: FormGroup;
    resetPasswordFormErrors: any;

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private _translate: TranslateService,
        public snackBar: MatSnackBar,
        private _router: Router,
        private route: ActivatedRoute,
        private _authentication: AuthenticationService,
        private _routeHist: HistoryRouteService
    ) {
        this._id = this.route.snapshot.paramMap.get('id');
        this.fuseTranslationLoader.loadTranslations(malay, english);

        this.resetPasswordFormErrors = {
            newpass: {},
            confirmpass: {}
        };

        this._translate.get('USER.EDIT.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('USER.EDIT.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });

    }

    ngOnInit() {
        this.resetPasswordForm = this.formBuilder.group({
            
            newpass: ['', [Validators.required, Validators.pattern(PasswordValidation.pattern)]],
            confirmpass: ['', Validators.required]
        },
        {
            validator: PasswordValidation.MatchPassword
        })

        this.resetPasswordForm.valueChanges.subscribe(() => {
            this.onResetPasswordFormValuesChanged();
        });
    }


    res() {

        if (this._id) {

            let user: User = new User();
            user.setOldPassword(this.resetPasswordForm.controls['newpass'].value);
            user.setId(this._id);

            this._authentication.res(user).subscribe(
                success => {
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                    });
                    if(HistoryRouteService.ISADMIN === this._routeHist.getRole()){
                        this._router.navigate(['/agensi/pengguna/senarai']);
                    }else{
                        this._router.navigate(['/user/list']);
                    }
                },
                error => {
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });
                }
            );
        }
    }

    backButton() {
        if(HistoryRouteService.ISADMIN === this._routeHist.getRole()){
            this._router.navigate(['/agensi/pengguna/senarai']);
        }else{
            this._router.navigate(['/user/list']);
        }
    }

    onResetPasswordFormValuesChanged() {
        for (const field in this.resetPasswordFormErrors) {
            if (!this.resetPasswordFormErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.resetPasswordFormErrors[field] = {};

            // Get the control
            const control = this.resetPasswordForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.resetPasswordFormErrors[field] = control.errors;
            }
        }
    }

}
