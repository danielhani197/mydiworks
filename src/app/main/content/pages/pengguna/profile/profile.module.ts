import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ScrollEventModule } from 'ngx-scroll-event';
import { CdkTableModule } from '@angular/cdk/table';
import { MatButtonModule, MatAutocompleteModule, MatTableModule, MatDividerModule, MatIconModule, MatTabsModule, MatFormFieldModule, MatChipsModule, MatSnackBarModule, MatMenuModule, MatDialogModule, MatCheckboxModule, MatOptionModule, MatSelectModule, MatDatepickerModule, MatInputModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { ProfileComponent } from './profile.component';
import { MaklumatComponent } from './tabs/maklumat/maklumat.component';
import { PerhubunganComponent } from './tabs/perhubungan/perhubungan.component';
import { PerananComponent } from './tabs/peranan/peranan.component';
import { KatalaluanComponent } from './tabs/katalaluan/katalaluan.component';
import { AgensiComponent } from './tabs/agensi/agensi.component';

const routes = [
    {
        path     : 'profile/:id',
        component: ProfileComponent
    }
];

@NgModule({
    declarations: [
        ProfileComponent,
        MaklumatComponent,
        PerananComponent,
        PerhubunganComponent,
        KatalaluanComponent,
        AgensiComponent

    ],
    entryComponents: [

    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        ScrollEventModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatTabsModule,
        MatFormFieldModule,
        MatChipsModule,
        MatSnackBarModule,
        MatMenuModule,
        MatDialogModule,
        MatCheckboxModule,
        MatOptionModule,
        MatSelectModule,
        MatDatepickerModule,
        MatInputModule,
        MatTableModule,
        CdkTableModule,
        MatAutocompleteModule,

        FuseSharedModule
    ],
    providers   : [
        
    ]
})
export class ProfileModule
{
}
