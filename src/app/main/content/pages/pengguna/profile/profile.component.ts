import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { PenggunaService } from '../../../services/pengguna/pengguna.service';
import { AuthenticationService } from '../../../services/authentication/authentication.service';

import { locale as english } from './../i18n/en';
import { locale as malay } from './../i18n/my';
import { locale as englishNav } from 'app/navigation/i18n/en';
import { locale as malayNav } from 'app/navigation/i18n/my';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

@Component({
    selector     : 'kemaskini-profile',
    templateUrl  : './profile.component.html',
    styleUrls    : ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProfileComponent
{
    

    constructor( 
        private penggunaService: PenggunaService,
        private authenticationService: AuthenticationService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english, malayNav, englishNav);

    }

    ngOnInit(){
        

    }
}
