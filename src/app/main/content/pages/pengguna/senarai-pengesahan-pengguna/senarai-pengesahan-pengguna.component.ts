import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from "@angular/router";

import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import { User } from 'app/main/content/model/user';
import { Page } from 'app/main/content/model/util/page';

import { PenggunaService } from 'app/main/content/services/pengguna/pengguna.service';
import { NextcloudService } from 'app/main/content/services/pengguna/nextcloud.service';


import { locale as english } from './../i18n/en';
import { locale as malay } from './../i18n/my';
import { locale as englishNav } from 'app/navigation/i18n/en';
import { locale as malayNav } from 'app/navigation/i18n/my';
import { HistoryRouteService } from '../../../services/util/historyroute.service';


@Component({
    selector   : 'senarai-pengesahan-pengguna',
    templateUrl: './senarai-pengesahan-pengguna.component.html',
    styleUrls  : ['./senarai-pengesahan-pengguna.component.scss']
})
export class SenaraiPengesahanPenggunaComponent implements OnInit
{
    id: string;

    rows: any[];
    temp = [];
    page = new Page();

    loadingIndicator = true;
    reorderable = true;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    successMsg : string;
    failMsg : string;
    successDelMsg : string;
    failDelMsg : string;

    constructor(
        private http: HttpClient,
        private _pengguna: PenggunaService,
        private _router: Router,
        private route: ActivatedRoute,
        private snackBar: MatSnackBar,
        private translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _nc: NextcloudService,
        private _routeHist: HistoryRouteService

    )
    {
        this.page.pageNumber = 0;
        this.page.size = 10;
        this.page.sortField = "name";
        this.page.sort = "asc";

        this.fuseTranslationLoader.loadTranslations(malay, english, malayNav, englishNav);

        this.translate.get('USER.MSG.SUCCESS').subscribe((res: string) => {
            this.successMsg = res;
        });
        this.translate.get('USER.MSG.FAIL').subscribe((res: string) => {
            this.failMsg = res;
        });
        this.translate.get('USER.MSG.SUCCESSDELETE').subscribe((res: string) => {
            this.successDelMsg = res;
        });
        this.translate.get('USER.MSG.FAILDELETE').subscribe((res: string) => {
            this.failDelMsg = res;
        });

        this._routeHist.setRole(HistoryRouteService.ISSAM);
    }

    ngOnInit(){
        this.setPage({ offset: 0 });  
    }

    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._pengguna.getUsersNew(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    resetPwd(id : string) {
        this._router.navigate(['/auth/reset-password-2/'+ id]);
    }

    onSort(event) {
        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._pengguna.getUsersNew(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;
    
        this._pengguna.getUsersNew(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
        // Whenever the filter changes, always go back to the first page
        //this.table.offset = 0;
    }

    approve(id: string){
        if(id){
            this._pengguna.approveNewUser(id).subscribe(
                success => {
                    this.snackBar.open(this.successMsg, "OK");
                    this.setPage({ offset: 0 });
                },
                error => {
                    this.snackBar.open(this.failMsg, "OK",{
                        panelClass: 'red-snackbar'
                    });
                }
            );
        }
    }

    reject(id: string){
        if(id){
            this._pengguna.rejectNewUser(id).subscribe(
                success => {
                    this.snackBar.open(this.successDelMsg, "OK");
                    this.setPage({ offset: 0 });
                },
                error => {
                    this.snackBar.open(this.failDelMsg, "OK",{
                        panelClass: 'red-snackbar'
                    });
                }
            );
        }
    }

    addUser(){
        this._nc.addNewUser().subscribe(
            success => {
                console.log("success");
            },error => {
                console.log(error)
            }
        )
    }
}

