import { NgModule } from '@angular/core';

import { SenaraiPenggunaModule } from './senarai-pengguna/senarai-pengguna.module';
import { SenaraiPengesahanPenggunaModule } from './senarai-pengesahan-pengguna/senarai-pengesahan-pengguna.module';
import { SenaraiPerananPenggunaModule } from './senarai-peranan-pengguna/senarai-peranan-pengguna.module';
import { MaklumatPerananModule } from './senarai-peranan-pengguna/maklumat-peranan/maklumat-peranan.module';
import { ProfileModule } from './profile/profile.module';
@NgModule({
    imports: [
        // Auth
        SenaraiPenggunaModule,
        SenaraiPengesahanPenggunaModule,
        SenaraiPerananPenggunaModule,
        MaklumatPerananModule,
        ProfileModule
    ]
})
export class PenggunaModule
{

}
