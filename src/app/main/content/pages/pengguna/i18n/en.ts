export const locale = {
    lang: 'en',
    data: {
        'USER': {
            'NAME': 'Name',
            'IC': 'Identification Card No.',
            'PASSWORD': 'Password',
            'EMAIL': 'Email',
            'TUJUAN': 'Purposes Of Use',
            'MINISTRY': 'Ministry',
            'AGENCY': 'Agency',
            'DIVISION': 'Division',
            'CHOOSE':'Please Choose',
            'ADDRESS': 'Address',
            'CITY': 'City',
            'STATE': 'State',
            'POSTCODE': 'Postcode',
            'PERSONALEMAIL': 'Personal Email',
            'WORKEMAIL': 'Work Email',
            'TELEPHONE': 'Tel',
            'POSITION': 'Position',
            'SKEMA': 'Scheme',
            'GRADE': 'Grade',
            'ERR': {
                'REQUIRED': {
                    'NAME':'Name is required.',
                    'USERNAME':'Username is required.',
                    'PASSWORD':'Password is required.',
                    'EMAIL':'Email is required.',
                    'MINISTRY':'Ministry is required.',
                    'AGENCY':'Agency is required.'
                },
                'EXIST': {
                    'USERNAME':'Username already exist.',
                    'EMAIL':'Email already exist.',
                },
                'FORMAT' : {
                    'INVALID' : {
                        'IC':'Please enter the valid format. Eg: 950402036543.',
                        'EMAIL' : 'Please enter the valid email format.',
                        'USERNAME' : 'Please enter 12 digits only.',
                        'NUMBER' : "Only digits allowed."
                    },
                    'EXAMPLE': {
                        'USERNAME': 'Example Format: 900402036543',
                        'PHONE': 'Example Format: 019-4444333'
                    }
                }
            },
            'MSG': {
                'SUCCESS':'User has been successfully approved.',
                'FAIL':'User already exist, Please check your user list.',
                'SUCCESSDELETE':'User has been successfully rejected.',
                'FAILDELETE':'There something went wrong, please contact ADMIN.',
                'GENERAL-SUCCESS':'Data has been successfully saved.',
                'DELETE':'User has been successfully removed.',
                'CONFLICT':'Fail to remove user. Please remove user in PET members first before proceed',

            },
            'EDIT': {
                'DETAILS': 'Details',
                'CONTACT': 'Contact',
                'AGENCY': 'Agency',
                'PASSWORD': 'Password',
                'PASSTXT': 'Password must be at least</br><code>12 characters long</code> and contain: </br>At least<code>one uppercase letter</code> , <code>one lowercase letter</code> , </br><code>one number</code> and <code>one symbol</code></br>Allowed symbols are: <code>[@ % + ! # $ ^ ? / ~]</code>',
                'ROLE': {
                    'ROLE': 'Role',
                    'AGEADMIN': 'Agency Administrator',
                    'SYSADMIN': 'System Administrator',
                    'USER': 'User'
                },
                'EXIST': 'Identification Card no. already exist',
                'NOTEXIST': 'User does not exist',
                'SUCCESS': 'User details have been successfully updated',
                'RESETPWD': 'New Password',
                'NEWRESETPWD': 'Confirm New Password',
                'BTN': {
                    'SUBMIT': 'Submit',
                    'BACK': 'Back'
                },
                'FORMERROR':{
                    'NAME': 'Name is required',
                    'IC': {
                        'REQUIRED': 'IC number is required',
                        'PATTERN': 'Please enter the valid format. Eg: 950402036543.',
                        'EXIST': 'Identification Card no. already exist'
                    },
                    'PERSONALEMAIL': {
                        'REQUIRED': 'Personal email is required',
                        'PATTERN': 'Please enter the valid format. Eg: example@example.com',
                        'EXIST': 'Email already exist'
                    },
                    'WORKEMAIL': {
                        'REQUIRED': 'Company email is required',
                        'PATTERN': 'Please enter the valid format. Eg: example@example.com.',
                        'EXIST': 'Email already exist'
                    },
                    'TELEPHONE': {
                        'REQUIRED': 'Telephone number is required',
                        'PATTERN': 'Please enter the valid format. Cth: 0122344567',
                    },
                    'KEMENTERIAN': 'Ministry is required',
                    'BAHAGIAN': 'Division is required',
                    'ADDRESS': 'Address is required',
                    'CITY': 'City is required',
                    'STATE': 'State is required',
                    'POSITION': 'Position is required',
                    'SKEMA': 'Scheme is required',
                    'GRADE': 'Grade is required',
                    'RESETPWD': {
                        'REQUIRED': 'New password is required',
                        'WEAK': 'Wrong password format',
                        'NOTSAME': 'Password does not match'
                    },
                    'POSTCODE': {
                        'REQUIRED': 'Postode is required',
                        'PATTERN': 'Please enter a valid postcode format'
                    }
                },
                'ERROR': 'Informations Are Unsuccessfully Submitted'
            }
        },
        'ROLE': {
            'TITLE': 'Admin Application',
            'DETAILS': 'Details of Admin Application',
            'SETTING': 'Role Setting',
            'NAMA': 'Name',
            'EMAIL':  'Email Address',
            'AGENSI':  'Agency',
            'ROLES': 'Role',
            'REMARK'    :'Remarks',
            'ADMINREMARK'    :'Remarks For Admin',

            'BTN' : {
                'DELETE' : 'Delete',
                'EDIT' : 'Edit',
                'SUBMIT' : 'Submit',
                'BACK'  : 'Back',
                'REJECT' : 'Reject',
                'APPROVE' : 'Approve',
          },
    
          'ERROR': 'Admin Application Are Unsuccessfully Submitted',
          'SUCCESS': 'Admin Application Are Successfully Submitted'
        }
    }
};
