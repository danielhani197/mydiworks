export const locale = {
    lang: 'my',
    data: {
        'USER': {
            'NAME': 'Nama',
            'IC': 'No. Kad Pengenalan',
            'PASSWORD': 'Kata Laluan',
            'EMAIL': 'Emel',
            'TUJUAN': 'Tujuan Penggunaan',
            'MINISTRY': 'Kementerian',
            'AGENCY': 'Agensi',
            'DIVISION': 'Bahagian',
            'CHOOSE':'Sila Pilih',
            'ADDRESS': 'Alamat',
            'CITY': 'Bandar',
            'STATE': 'Negeri',
            'POSTCODE': 'Poskod',
            'PERSONALEMAIL': 'Emel Peribadi',
            'WORKEMAIL': 'Emel Syarikat',
            'TELEPHONE': 'Tel',
            'POSITION': 'Jawatan',
            'SKEMA': 'Skim',
            'GRADE': 'Gred',
            'ERR': {
                'REQUIRED': {
                    'NAME':'Sila isi Nama penuh.',
                    'USERNAME':'Sila isi Kata Nama Pengguna.',
                    'PASSWORD':'Sila isi Kata Laluan.',
                    'EMAIL':'Sila isi Emel.',
                    'MINISTRY':'Sila pilih Kementerian.',
                    'AGENCY':'Sila pilih Agensi.'
                },
                'EXIST': {
                    'USERNAME':'Kata Nama Pengguna telah wujud.',
                    'EMAIL':'Emel telah wujud.',
                },
                'FORMAT' : {
                    'INVALID' : {
                    'IC':'Sila masukkan format yang betul. Cth: 950402036543.',
                    'EMAIL' : 'Sila masukkan format emel yang betul.',
                    'USERNAME' : 'Sila masukkan hanya 12 nombor sahaja.',
                    'NUMBER' : "Hanya digit dibenarkan."
                    },
                    'EXAMPLE': {
                        'USERNAME': 'Contoh Format: 900402036543',
                        'PHONE': 'Contoh Format: 019-4444333'
                    }
                }
            },
            'MSG': {
                'SUCCESS':'Permohonan Pengguna telah berjaya diluluskan.',
                'FAIL':'Pengguna telah wujud, sila semak senarai pengguna.',
                'SUCCESSDELETE':'Permohonan Pengguna telah berjaya ditolak.',
                'FAILDELETE':'Terdapat ralat, sila hubungi Pentadbir.',
                'GENERAL-SUCCESS':'Data telah berjaya disimpan.',
                'DELETE':'Pengguna telah berjaya dipadam',
                'CONFLICT':'Pengguna tidak berjaya dipadam. Sila padam pengguna dalam Ahli PET terlebih dahulu',
            },
            'EDIT': {
                'DETAILS': 'Maklumat',
                'CONTACT': 'Perhubungan',
                'AGENCY': 'Agensi',
                'PASSWORD': 'Kata Laluan',
                'PASSTXT': 'Kata laluan mesti mempunyai sekurang-kurangnya</br><code>12 aksara</code> dan mengandungi seperti berikut: </br>Sekurang-kurangnya<code>satu huruf besar</code> , <code>satu huruf kecil</code> , </br><code>satu nombor</code> dan <code>satu simbol</code></br>Simbol yang dibenarkan adalah<code>[@ % + ! # $ ^ ? / ~]</code>',
                'ROLE': {
                    'ROLE': 'Peranan',
                    'AGEADMIN': 'Pentadbir Agensi',
                    'SYSADMIN': 'Pentadbir Sistem',
                    'USER': 'Pengguna'
                },
                'EXIST': 'No. Kad Pengenalan telah wujud',
                'NOTEXIST': 'Maklumat pengguna tidak wujud',
                'SUCCESS': 'Maklumat pengguna telah berjaya dikemaskini',
                'RESETPWD': 'Kata Laluan Baru',
                'NEWRESETPWD': 'Sahkan Kata Laluan Baru',
                'BTN': {
                    'SUBMIT': 'Hantar',
                    'BACK': 'Kembali'
                },
                'FORMERROR':{
                    'NAME': 'Nama wajib diisi',
                    'IC': {
                        'REQUIRED': 'No. Kad Pengenalan wajib diisi',
                        'PATTERN': 'Sila masukkan format yang betul. Cth: 950402036543.',
                        'EXIST': 'No. Kad Pengenalan telah wujud'
                    },
                    'PERSONALEMAIL': {
                        'REQUIRED': 'Emel peribadi wajib diisi',
                        'PATTERN': 'Sila masukkan format yang betul. Cth: example@example.com.',
                        'EXIST': 'Emel telah wujud'
                    },
                    'WORKEMAIL': {
                        'REQUIRED': 'Emel syarikat wajib diisi',
                        'PATTERN': 'Sila masukkan format yang betul. Cth: example@example.com.',
                        'EXIST': 'Emel telah wujud'
                    },
                    'TELEPHONE': {
                        'REQUIRED': 'Nombor telefon wajib diisi',
                        'PATTERN': 'Sila masukkan format yang betul. Cth: 0122344567',
                    },
                    'KEMENTERIAN': 'Kementerian wajib diisi',
                    'BAHAGIAN': 'Bahagian wajib diisi',
                    'ADDRESS': 'Alamat wajib diisi',
                    'CITY': 'Bandar wajib diisi',
                    'STATE': 'Negeri wajib diisi',
                    'POSITION': 'Jawatan wajib diisi',
                    'SKEMA': 'Skim wajib diisi',
                    'GRADE': 'Gred wajib diisi',
                    'RESETPWD': {
                        'REQUIRED': 'Kata laluan wajib diisi',
                        'WEAK': 'Salah format kata laluan',
                        'NOTSAME': 'Kata laluan tidak sepadan'
                    },
                    'POSTCODE': {
                        'REQUIRED': 'Poskod wajib diisi',
                        'PATTERN': 'Sila masukkan format poskod yang sah'
                    }
                },
                'ERROR': 'Maklumat Tidak Dapat Dihantar'
            }
        },
        'ROLE': {
            'TITLE': 'Permohonan Admin',
            'DETAILS': 'Maklumat Permohonan Admin',
            'SETTING': 'Tetapan Peranan',
            'NAMA': 'Nama',
            'EMAIL':  'Alamat Email',
            'AGENSI':  'Agensi',
            'ROLES': 'Peranan',
            'REMARK'    :'Catatan',
            'ADMINREMARK'    :'Catatan Untuk Admin',
            'BTN' : {
                'DELETE' : 'Padam',
                'EDIT' : 'Kemaskini',
                'SUBMIT' : 'Hantar',
                'BACK'  : 'Kembali',
                'REJECT' : 'Tolak',
                'APPROVE' : 'Lulus',
            },       
            'ERROR': 'Permohonan Admin Tidak Dapat Disimpan',
            'SUCCESS': 'Permohonan Admin Telah Berjaya Dihantar'
            
          }
    }
};
