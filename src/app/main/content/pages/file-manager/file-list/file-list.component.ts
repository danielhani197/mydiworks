import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../../model/user';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Page } from 'app/main/content/model/util/page';

import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { MyBoxService } from '../../../services/mybox/mybox.service';
import { FileManagerService } from '../file-manager.service';
import { fuseAnimations } from '@fuse/animations/index';
import { MyBoxAttachment } from '../../../model/mybox/myboxAttachment';

@Component({
    selector   : 'fuse-file-list',
    templateUrl: './file-list.component.html',
    styleUrls  : ['./file-list.component.scss'],
    animations : fuseAnimations
})
export class FuseFileManagerFileListComponent implements OnInit
{
    files: any;
    displayedColumns = ['icon', 'filename', 'type','createdby', 'fileSize', 'createddate', 'detail-button'];
    selected: any;
    list: any;
    listInFolder: any;
    _id: string;
    parentid: string;
    user: User;
    page = new Page();
    size: any;

    constructor(
        private fileManagerService: FileManagerService,
        private _mybox: MyBoxService,
        private router:Router,
        private _auth: AuthenticationService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
        this.fileManagerService.onFilesChanged.subscribe(mybox => {
            this.files = mybox;
        });
        this.fileManagerService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });

        this.fileManagerService.onFileDelete.subscribe(parentid => {
            this.func2(parentid)
        })

        this.user = this._auth.getCurrentUser();
        this._id = this.user.id;
    }

    ngOnInit()
    {   
        this._mybox.setlocation("home");     
        this.setTable();
        this._mybox.filter("0")
    }

    onSelect(selected)
    {
        this.fileManagerService.onFileSelected.next(selected);
    }

    func2(id: string){
        if(id == "0"){
            this.setTable();
            this._mybox.filter(id)
            this._mybox.setlocation(id)
        }else{
            this._mybox.filter(id)
            this._mybox.setlocation(id);
            this._mybox.getListInFolder(id).subscribe(
                data => {
                    this.list = data;
                    
                }
            )
        }
    }
    
    setTable(){     
        this._mybox.getMyboxList(this._id).subscribe(
            data => {
                this.list = data;
            }
        )
    }

    // setTableShared(){
    //     this._mybox.getMyboxSharedList(this._id).subscribe(
    //         data => {
    //             this.list = data;
    //         }
    //     )
    // }

    updateFilter(event: any) {
        const val = event.target.value.toLowerCase();
        this.page.search = val
    
        this._mybox.getMBList(this.page).subscribe(
            data => {
                this.list = data.data;
            }
        );
    }

    formatNumber2Decimal(fileSize:number){
        this.size = fileSize/1048576;
        return this.size.toFixed(2);
    }
}
