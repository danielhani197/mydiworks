import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { AuthenticationService } from '../../services/authentication/authentication.service';

import { TokenService } from 'app/main/content/services/util/token.service';
import { MyBoxAttachment } from '../../model/mybox/myboxAttachment';
import { User } from '../../model/user';
import { MyBoxShare } from '../../model/mybox/myboxShare';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class FileManagerService implements Resolve<any>
{
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});
    onFileDelete: BehaviorSubject<any> =  new BehaviorSubject({});
    onFilterChanged: Subject<any> = new Subject();

    private globalUrl = `${environment.apiUrl}/api/mybox`;
    private shareUrl = `${environment.apiUrl}/api/share`;

    _id: string;
    _parentid: string;
    user: User;
    myFile: MyBoxAttachment[];
    _myFile: MyBoxAttachment;
    filterBy: string;
    file:any;

    selectedFile: string[] = [];

    constructor(
        private _auth: AuthenticationService,
        private http: HttpClient,
        private _token: TokenService,
    )
    {
        this.user = this._auth.getCurrentUser();
        this._id = this.user.id;
    }

    /**
     * The File Manager App Main Resolver
     * 
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getMybox(this._id)
            ]).then(
                ([MyBoxAttachment]) => {

                    this.onFilterChanged.subscribe(filter => {
                        this.filterBy = filter;
                        this.getMybox(this._id);
                    });
                    resolve();
                },
                reject
            );
        });
    }

    getMybox(id: string): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get<MyBoxAttachment[]>(this.globalUrl+'/getmybox/'+id, this._token.jwtBearer())
                .subscribe((mybox: any) => {
                    this.myFile = mybox;
                    if ( this.filterBy === 'Share' )
                    {
                        this.myFile = this.myFile.filter(_file => {
                            return this._myFile.status.includes(_file.id);
                        });
                        this.onFilesChanged.next(this.myFile);
                        mybox[0].createddate == mybox[0].share.createddate;
                        mybox[0].share == mybox[0].share;
                        this.onFileSelected.next(mybox[0]);
                        resolve(mybox);
                    }
                    else{
                        this.onFilesChanged.next(this.myFile);
                        this.onFileSelected.next(mybox[0]);
                        resolve(mybox);
                    }
                    
                }, reject);
        });
    }

    toggleSelectedFile(id)
    {
        
        // First, check if we already have that contact as selected...
        if ( this.selectedFile.length > 0 )
        {
            const index = this.selectedFile.indexOf(id);
           
            if ( index !== -1 )
            {
                this.selectedFile.splice(index, 1);

                // Trigger the next event
                this.onFilesChanged.next(this.selectedFile);
                // Return
                return;
                
            }
        }

        // If we don't have it, push as selected
        this.selectedFile.push(id);
        // Trigger the next event
        
        this.onFilesChanged.next(this.selectedFile);

    }

    deselectFile()
    {
        this.selectedFile = [];

        if ( this.selectedFile.length > 0 )
        {
            const index = this.selectedFile.length;
           
            if ( index !== -1 )
            {
                this.selectedFile.splice(index, 1);

                // Trigger the next event
                this.onFilesChanged.next(this.selectedFile);
                // Return
                return;
                
            }
        }

        // Trigger the next event
        this.onFilesChanged.next(this.selectedFile);
    }
}
