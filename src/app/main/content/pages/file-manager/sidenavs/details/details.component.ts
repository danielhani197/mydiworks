import { Component, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations/index';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './../../../file-manager/i18n/en';
import { locale as malay } from './../../../file-manager/i18n/my';
import { MatSnackBar, MatDialog } from '@angular/material';
import { FileManagerService } from '../../file-manager.service';
import { User } from '../../../../model/user';
import { MyBoxAttachment } from 'app/main/content/model/mybox/myboxAttachment';
import { MyBoxService } from '../../../../services/mybox/mybox.service';
import { TranslateService } from '@ngx-translate/core';
import { DialogListUser } from './dialog-list.component';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import * as JSZip from 'jszip';
import { FolderMyBox } from '../../../../model/mybox/folderMyBox';

@Component({
    selector   : 'fuse-file-manager-details-sidenav',
    templateUrl: './details.component.html',
    styleUrls  : ['./details.component.scss'],
    animations : fuseAnimations
})
export class FuseFileManagerDetailsSidenavComponent implements OnInit
{
    selected: any;

    _id: string;
    errorTxt: string;
    successTxt: string;
    user: User;
    file: any;
    size:any;
    zip: JSZip = new JSZip();
    constructor(
        private fileManagerService: FileManagerService,
        private _mybox: MyBoxService,
        public snackBar: MatSnackBar,
        private _translate: TranslateService,
        public _dialog: MatDialog,
        private _authentication: AuthenticationService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);

        this._translate.get('ERROR').subscribe((res: string) => {
            this.errorTxt = res;
        });
    
        this._translate.get('SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
    }

    ngOnInit()
    {
        this.fileManagerService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
    }

    delete(id: string){
        
        this._mybox.deleteMyBoxFileById(id).subscribe(
            success=>{               
                this.snackBar.open(this.successTxt, "OK", {
                    panelClass: ['blue-snackbar']
                });
                this.fileManagerService.onFileDelete.next(this._mybox.getParentId())
                this.selected = null;

            },
            error=>{                    
                this.snackBar.open(this.errorTxt, "OK", {
                    panelClass: ['red-snackbar']
                });

                this.fileManagerService.onFileDelete.next(this._mybox.getParentId())
            }
        )
    }

    base64ToArrayBuffer(base64) {
        var binaryString = window.atob(base64);
        var binaryLen = binaryString.length;
        var bytes = new Uint8Array(binaryLen);
        for (var i = 0; i < binaryLen; i++) {
           var ascii = binaryString.charCodeAt(i);
           bytes[i] = ascii;
        }
        return bytes;
    }

    download(selected){
        let instanceId = selected.instanceid
        let id = selected.id

        // console.log("download >>>>>>>>>", selected);
        if (instanceId != null){
            this._mybox.getMyBoxFileById(instanceId).subscribe(

                data => {
                    this.file = data; 
                    let a = document.createElement("a");
                    document.body.appendChild(a);
                    //a.style = "display: none"; 
    
                    var dataFile = this.base64ToArrayBuffer(this.file.content);
                    var blob = new Blob([dataFile], {type:data.formatType});
                    var url= window.URL.createObjectURL(blob);
                    a.href = url;
                    a.download = this.file.name;
                    a.click();
                    window.URL.revokeObjectURL(url);
                }
            );
        }else {
            
            this._mybox.getMyBoxFolderById(id).subscribe(
                data=>{
                    var zipname = this.selected.filename
                    this.createFolder(data, zipname)

                    setTimeout(()=>{ 
                        this.zip.generateAsync({type:"blob"}).then(
                            function(content) {
                            
                                let a = document.createElement("a");
                                document.body.appendChild(a);
                                var url= window.URL.createObjectURL(content);
                                a.href = url;
                                a.download = zipname;
                                a.click();
                                window.URL.revokeObjectURL(url);
                            }
                        );
                    }, 1000)

                    //start download zip
                    
                    
                }
            )
        }
    }

    createFolder(listFolders: FolderMyBox, currentpath: string){
        this.zip.folder(currentpath)
        for(let obj of listFolders.instances){
            this._mybox.getMyBoxFileById(obj).subscribe(
                data => {
                    this.zip.file(currentpath+"/"+data.name, this.base64ToArrayBuffer(data.content))
                }
            )
        }
        for(let obj of listFolders.folders){
            this.createFolder(obj, currentpath+'/'+obj.name)
        }
    }

    share(id: string){
        console.log(id)
        
        let dialogRef = this._dialog.open(DialogListUser,
          {
            width: '700px',
            data: {id : id}
            
          }
        );
        dialogRef.afterClosed().subscribe(
            result => {
                  
            }
          );
    
    }

    formatNumber2Decimal(fileSize:number){
        this.size = fileSize/1048576;
        return this.size.toFixed(2);
    }
}
