import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MyBoxService } from '../../../../services/mybox/mybox.service';
import { Pet } from '../../../../model/pet/pet';
import { AhliPet } from '../../../../model/pet/ahliPet';
import { Page } from 'app/main/content/model/util/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { PetService } from '../../../../services/pet/pet.service';
import { PenggunaService } from '../../../../services/pengguna/pengguna.service';
import { User } from '../../../../model/user';
import { PagedData } from '../../../../model/util/paged-data';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MyBoxShare } from '../../../../model/mybox/myboxShare';
import { NotifikasiService } from '../../../../services/setting/notifikasi.service';
import { Notifikasi } from '../../../../model/notifikasi';
import { FuseQuickPanelComponent } from '../../../../../quick-panel/quick-panel.component';
import { ShareComponent } from '../../share/share.component';
//import { FuseQuickPanelComponent } from '../../../../../quick-panel/quick-panel.component';


@Component({
    selector   : 'dialog-list',
    templateUrl: './dialog-list.component.html',
})
export class DialogListUser implements OnInit
{
    loadingIndicator = true;
    selected  = [];
    rows: any[];
    page = new Page();

    successTxt: string;
    existTxt: string;

    users:User[];
    owner:any;
    id: any;
    
    
    notifikasi: Notifikasi[];

    pagedData: PagedData<User> = new PagedData<User>();
    @ViewChild(FuseQuickPanelComponent) notiLoad: FuseQuickPanelComponent;

    @ViewChild(ShareComponent) sharefile: ShareComponent;
    constructor( 
       private _mybox: MyBoxService,
       private _pet: PetService,
       private _user: AuthenticationService,
       private _translate: TranslateService,
       public snackBar: MatSnackBar,
       private _notifi: NotifikasiService,
       public _dialogRef: MatDialogRef<DialogListUser>,
        //private _notiComponent: FuseQuickPanelComponent,
        @Inject(MAT_DIALOG_DATA) public data: any
    )
    {
        this._translate.get('MYBOX.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('MYBOX.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });

        this.page.pageNumber = 0;
        this.page.size = 10;
        this.page.sortField = "name";
        this.page.sort = "asc";
    }
    ngOnInit(){
        this.setPage({ offset: 0 });
        this._mybox.getMyBoxById(this.data.id).subscribe(

            dataFile => {
                console.log(dataFile)
            }
        )
    }

    setPage(pageInfo){
        this.page.userId = this._pet.getUsers();
        this.page.pageNumber = pageInfo.offset;
        this._user.getUserApproved(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }


    onSort(event) {
        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._user.getUserApproved(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;

        this._user.getUserApproved(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

   share(){
    if(this.data.id){
            let myBoxShare: MyBoxShare = new MyBoxShare();
            this.owner = this._user.getCurrentUser();
            
            for(const user of this.selected){
                this.id =this.data.id;
                this._mybox.getMyBoxById(this.id).subscribe(

                    dataFile => {
                        myBoxShare.setFile(dataFile),
                        myBoxShare.setOwner(this.owner),
                        myBoxShare.setShared(user)

                        
                        this._mybox.shareMyBoxFile(myBoxShare).subscribe(
                            success=>{ 
                            
                    this._mybox.getLatersShare().subscribe(
                        dataShare =>{
                            let noti: Notifikasi = new Notifikasi()
                            noti.setCreatedby(this.owner),
                            noti.setUser(user);
                            if(dataFile.type == "folder"){
                               noti.setMaklumat("Share A Folder")
                            }
                            else{
                                noti.setMaklumat("Share A Document")
                            }
                            
                            noti.setStatus("New"),
                            noti.setTindakan("/form/"+this.id),
                            noti.setModifiedby(this.owner),
                            noti.setShare(dataShare)
                            
                            this._notifi.createNoti(noti).subscribe(
                                success=>{ 
                            },
                                
                                error=>{
                                }
                            )
                        }
                    )
                            this.snackBar.open(this.successTxt, "OK", {
                                panelClass: ['blue-snackbar']
                                });
                            this.setPage({ offset: 0 });
                            },
                                
                                error=>{
                                
                                    this.snackBar.open(this.existTxt, "OK", {
                                        panelClass: ['red-snackbar']
                                        });
                                    this.setPage({ offset: 0 });
                                }
                            )     
                        }
                    )
                }     
            }
        this._dialogRef.close();
        this._pet.resetSelectedUser();
   }    

    selectUser(id: string){       
        this._pet.selectUser(id);

    }
    
    onSelect({ selected }) {
    
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
        this.selectUser(selected.id);
        
    }


    displayCheck(row) {
        return row.name !== null;
    }

    onActivate(event) {
        
    }
    cekid(id){
        for(let obj of this.selected){
            if(obj.id==id){
                return true;
            }
        }
        // console.log(id)
        return false;
    }

    reset(){
        this.notifikasi = []
    }
    
}