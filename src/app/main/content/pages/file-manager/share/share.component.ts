import { Component, OnInit, ViewChild } from '@angular/core';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { Observable, Subscription } from 'rxjs';

import { MyBoxService } from '../../../services/mybox/mybox.service';
import { FileManagerService } from '../file-manager.service';
import { fuseAnimations } from '@fuse/animations/index';
import { PetService } from '../../../services/pet/pet.service';
import { PermohonanPetService } from '../../../services/pet/permohonanPet.service';
import { User } from '../../../model/user';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { Page } from '../../../model/util/page';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { MatTableDataSource, MatSort } from '@angular/material';
import { MyBoxShare } from '../../../model/mybox/myboxShare';
import { MyBoxAttachment } from '../../../model/mybox/myboxAttachment';

@Component({
    selector   : 'fuse-share',
    templateUrl: './share.component.html',
    styleUrls  : ['./share.component.scss'],
    animations : fuseAnimations
})
export class ShareComponent implements OnInit
{
    files: any;
    // displayedColumns = ['icon', 'name', 'type', 'owner', 'size', 'modified', 'detail-button'];
    displayedColumns = ['icon', 'filename', 'type','createdby', 'fileSize', 'createddate', 'detail-button'];
    selected: any;
    list: any;
    selected2:any;

    rows: any[];
    selectedUser : string[]= [];

    selectedRequest: string[] = [];
    checkboxes: {};
    onContactsChangedSubscription: Subscription;

    _id: string;
    user:User;
    page = new Page();

    dataShare: MyBoxShare[];
    file: any[] = [];

    mybox: MyBoxAttachment;
    size:any;
   
    dataSource = new MatTableDataSource<any>(this.list);
    selection = new SelectionModel<any>(true, []);

    constructor(
        private fileManagerService: FileManagerService,
        private _mybox: MyBoxService,
        private _pet : PetService,
        private _auth: AuthenticationService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
    )
    {

        
        //this.fuseTranslationLoader.loadTranslations(malay, english);
        this.fileManagerService.onFilesChanged.subscribe(mybox => {
            this.files = mybox;
        });
        this.fileManagerService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });

        this.user = this._auth.getCurrentUser();
        this._id = this.user.id;

    }
    @ViewChild(MatSort) sort: MatSort;

    ngOnInit()
    {   
        this._mybox.setlocation("home");     
        this.setTable();
        this._mybox.filter("0")
    }

    onSelect(selected)
    {
        this.fileManagerService.onFileSelected.next(selected);
    }

    func2(id: string){
        if(id == "0"){
            this.setTable();
            this._mybox.filter(id)
            this._mybox.setlocation(id)
        }else{
            this._mybox.filter(id)
            this._mybox.setlocation(id);
            this._mybox.getListInFolder(id).subscribe(
                data => {
                    this.list = data;
                    
                }
            )
        }
    }
    
    setTable(){     
       
        this._mybox.getShareFolder(this._id).subscribe(
            data => {
               
                this.dataShare = data
                for( const data of this.dataShare){
                    
                    // this._mybox.getMyBoxId(data.file.id).subscribe(
                    //         shareData => {
                                //console.log(data)
                                // if(data.file.id == shareData.id){
                                    let myBox: MyBoxAttachment = new MyBoxAttachment();
                                    myBox.setFilename(data.file.filename),
                                    myBox.setCreatedby(data.owner),
                                    myBox.setFoldername(data.file.folderName),
                                    myBox.setInstanceid(data.file.instanceid),
                                    myBox.setStatus(data.file.status),
                                    myBox.setCreateddate(data.createdate),
                                    myBox.setParentId(data.file.parentid),
                                    myBox.setShare(data.owner)
                                    myBox.setType(data.file.type),
                                    myBox.setFileSize(data.file.fileSize)
                                    this.file.push(myBox);
                                // }
                                console.log(myBox)
                                // this.file.push(data);
                        //     }
                        // )
                }
                this.list = this.file;
            }
        )
    }  
    updateFilter(event: any) {
        const val = event.target.value.toLowerCase();
        this.page.search = val
    
        this._mybox.getMBList(this.page).subscribe(
            data => {
                this.list = data.data;
            }
        );
    } 
    
    selectUser(id: string){       
        this._pet.selectUser(id);
        console.log(id)
    }

    onCheck({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
        this.selectUser(selected.id);
    }

    cekid(id){
        this.fileManagerService.toggleSelectedFile(id);
    }
    setToFilter(){
        for( let obj of this.rows){
          this.selectedUser.push(obj.id)
        }
        this._pet.setCurrentList(this.selectedUser)
    }

    formatNumber2Decimal(fileSize:number){
        this.size = fileSize/1048576;
        return this.size.toFixed(2);
    }
    
}