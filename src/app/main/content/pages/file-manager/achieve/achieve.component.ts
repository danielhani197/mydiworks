import { Component, OnInit } from '@angular/core';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { Observable, Subscription } from 'rxjs';

import { MyBoxService } from '../../../services/mybox/mybox.service';
import { FileManagerService } from '../file-manager.service';
import { fuseAnimations } from '@fuse/animations/index';
import { PetService } from '../../../services/pet/pet.service';
import { PermohonanPetService } from '../../../services/pet/permohonanPet.service';
import { User } from '../../../model/user';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { Page } from '../../../model/util/page';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { MatTableDataSource } from '@angular/material';

@Component({
    selector   : 'fuse-achieve',
    templateUrl: './achieve.component.html',
    styleUrls  : ['./achieve.component.scss'],
    animations : fuseAnimations
})
export class AchieveComponent implements OnInit
{
    files: any;
    // displayedColumns = ['icon', 'name', 'type', 'owner', 'size', 'modified', 'detail-button'];
    displayedColumns = ['select', 'icon', 'filename', 'type','createdby', 'fileSize', 'createddate', 'detail-button'];
    selected: any;
    list: any;
    selected2:any;

    rows: any[];
    selectedUser : string[]= [];

    selectedRequest: string[] = [];
    checkboxes: {};
    onContactsChangedSubscription: Subscription;

    _id: string;
    user:User;
    page = new Page();

   
    dataSource = new MatTableDataSource<any>(this.list);
    selection = new SelectionModel<any>(true, []);

    constructor(
        private fileManagerService: FileManagerService,
        private _mybox: MyBoxService,
        private _pet : PetService,
        private _auth: AuthenticationService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
    )
    {

        
        //this.fuseTranslationLoader.loadTranslations(malay, english);
        this.fileManagerService.onFilesChanged.subscribe(mybox => {
            this.files = mybox;
        });
        this.fileManagerService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });

        this.user = this._auth.getCurrentUser();
        this._id = this.user.id;

        this.onContactsChangedSubscription =
            this.fileManagerService.onFileSelected.subscribe(selectedRequest => {
                for ( const id in this.checkboxes )
                {
                    if ( !this.checkboxes.hasOwnProperty(id) )
                    {
                        continue;
                    }

                    this.checkboxes[id] = selectedRequest.includes(id);
                }
                this.selectedRequest = selectedRequest;
            });

            this.user = this._auth.getCurrentUser();
        this._id = this.user.id;
    }


    ngOnInit()
    {   
        this._mybox.setlocation("home");     
        this.setTable();
    }

    onSelect(selected)
    {
        this.fileManagerService.onFileSelected.next(selected);
    }

    func2(id: string){
        this._mybox.setlocation(id);

        this._mybox.getListInFolder(id).subscribe(
            data => {
                this.list = data;
                
            }
        )
    }
    
    setTable(){     
        this._mybox.getMyboxList(this._id).subscribe(
            data => {
                this.list = data;
            }
        )
    }  
    updateFilter(event: any) {
        const val = event.target.value.toLowerCase();
        this.page.search = val
    
        this._mybox.getMBList(this.page).subscribe(
            data => {
                this.list = data.data;
            }
        );
    } 
    
    selectUser(id: string){       
        this._pet.selectUser(id);
        console.log(id)
    }

    onCheck({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
        this.selectUser(selected.id);
    }

    cekid(id){
        this.fileManagerService.toggleSelectedFile(id);
    }
    setToFilter(){
        for( let obj of this.rows){
          this.selectedUser.push(obj.id)
        }
        this._pet.setCurrentList(this.selectedUser)
    }

    isAllSelected() {
        const numSelected = this.list.id;
        const numRows = this.list.id;
        console.log(numSelected)
        return numSelected === numRows;
        
      }
    
      /** Selects all rows if they are not all selected; otherwise clear selection. */
      masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
      }
    
    
}