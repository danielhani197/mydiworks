import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';

import { MatButtonModule, 
        MatMenuModule,
        MatIconModule, 
        MatRippleModule, 
        MatSidenavModule, 
        MatSlideToggleModule, 
        MatTableModule, 
        MatCheckbox, 
        MatFormFieldModule,
        MatCheckboxModule, 
        MatDialogModule, 
        MatInputModule, 
        MatSelectModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { FuseFileManagerComponent } from './file-manager.component';
import { FileManagerService } from './file-manager.service';
import { FuseFileManagerFileListComponent } from './file-list/file-list.component';
import { FuseFileManagerDetailsSidenavComponent } from './sidenavs/details/details.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DialogListUser } from './sidenavs/details/dialog-list.component';
import { AchieveComponent } from './achieve/achieve.component';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { FormsModule } from '@angular/forms';
import { ShareComponent } from './share/share.component';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
    {
        path     : 'mybox',
        component: FuseFileManagerComponent,
        children : [],
        resolve  : {
            files: FileManagerService
        }
    }
];

@NgModule({
    declarations: [
        FuseFileManagerComponent,
        FuseFileManagerFileListComponent,
        FuseFileManagerDetailsSidenavComponent,
        AchieveComponent,
        ShareComponent,
        DialogListUser,
    ],
    entryComponents: [
        DialogListUser
    ],
    imports     : [
        RouterModule.forChild(routes),

        CdkTableModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatDialogModule,
        MatCheckboxModule,
        MatIconModule,
        MatMenuModule,
        NgxDatatableModule,
        MatInputModule,
        MatSelectModule, 
        MatRippleModule,
        MatSidenavModule, 
        MatFormFieldModule,
        SweetAlert2Module, 
        MatCheckboxModule,
        TranslateModule,
        FormsModule,
        FuseSharedModule,

    ],
    providers   : [
        FileManagerService
    ]
})
export class FuseFileManagerModule
{
}
