export const locale = { 
    lang: 'en', 
    data: {         
        'MYBOX': 
            { 
                'FILE':'File has been upload', 
                'FILEERROR':'File exceed the limit',
                'FOLDER':'Folder has been create',
                'FOLDERERROR':'Folder cannot be create',
                'TYPE':'Type',
                'SIZE':'Size',
                'OWNER':'Owner',
                'CREATEDDATE':'Created',
                'SEARCH':'Search',
                'DELETE':'File has been delete',
                'ACHIEVE':'Achieve',            
                'SHARE':'Sharred with me',
                'FILED':'My Files',
                'UPLOAD':'Upload',
                'NEWFOLDER':'New Folder',
                'NAME':'Name',
                'SHARE_DATE':'Share Date',
                'SUCCESS': 'Share Success'
            
            }
    } 
}