export const locale = { 
    lang: 'my', 
    data: { 
        'MYBOX': 
            { 
                'FILE':'Fail telah dimuat naik', 
                'FILEERROR':'Fail melebihi saiz maksima',
                'FOLDER':'Folder telah dicipta',
                'FOLDERERROR':'Folder tidak boleh dicipta',
                'TYPE':'Jenis',
                'SIZE':'Saiz',
                'OWNER':'Pemilik',
                'CREATEDDATE':'Tarikh Dicipta',
                'SEARCH':'Carian',
                'DELETE':'Fail telah dipadam',
                'ACHIEVE':'Capaian',                
                'SHARE':'Berkongsi dengan saya',
                'FILED':'Fail saya',
                'UPLOAD':'Muat Naik',
                'NEWFOLDER':'Cipta Folder',
                'NAME':'Nama',
                'SHARE_DATE':'Tarikh Kongsi',
                'SUCCESS': 'Berjaya Kongsi'

            }
        
    } 
}