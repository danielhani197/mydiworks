import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar, MatDialogRef } from '@angular/material';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { TranslateService } from '@ngx-translate/core';
import { Page } from 'app/main/content/model/util/page';

import { locale as english } from './i18n/en';
import { locale as malay } from './i18n/my';
import { ActivatedRoute } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { User } from '../../model/user';
import { MyBoxAttachment } from 'app/main/content/model/mybox/myboxAttachment';
import { MyBoxService } from '../../services/mybox/mybox.service';

import { FileManagerService } from './file-manager.service';
import { FuseFileManagerFileListComponent } from './file-list/file-list.component';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { Subscription } from 'rxjs';
import { PermohonanPetService } from '../../services/pet/permohonanPet.service';
import { AchieveComponent } from './achieve/achieve.component';
import { ShareComponent } from './share/share.component';

@Component({
    selector     : 'fuse-file-manager',
    templateUrl  : './file-manager.component.html',
    styleUrls    : ['./file-manager.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class FuseFileManagerComponent implements OnInit
{
    selected: any;
    pathArr: BreadCrumblings[] = new Array<BreadCrumblings>();

    _id: string;
    errorFolderTxt: string;
    successFolderTxt: string;
    errorFileTxt: string;
    successFileTxt: string;
    user: User;
    event:any;
    currentLocation: any;

    loadingIndicator = true;
    rows: any[];
    page = new Page();

    filter:string = '0';

    fileManagerForm: FormGroup;

    hasSelectedFile: boolean;
    selectedFile:string[];
    isIndeterminate: boolean;
    file:any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>; 
    onUserDataChangedSubscription: Subscription;
    onFilesChanged: Subscription; 

    @ViewChild(FuseFileManagerFileListComponent) fileList:  FuseFileManagerFileListComponent;
    @ViewChild(AchieveComponent) achieveComponent: AchieveComponent;
    @ViewChild(ShareComponent) share: ShareComponent;

    constructor(
        private fileManagerService: FileManagerService,
        private _auth: AuthenticationService,
        private route: ActivatedRoute,
        private http: HttpClient,
        private _router: Router,
        private formBuilder: FormBuilder,
        private _translate: TranslateService,
        private _mybox: MyBoxService,
        public snackBar: MatSnackBar,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private permohonanService: PermohonanPetService,
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
        this._translate.get('MYBOX.FOLDERERROR').subscribe((res: string) => {
            this.errorFolderTxt = res;
        });
    
        this._translate.get('MYBOX.FOLDER').subscribe((res: string) => {
            this.successFolderTxt = res;
        });

        this._translate.get('MYBOX.FILEERROR').subscribe((res: string) => {
            this.errorFileTxt = res;
        });
    
        this._translate.get('MYBOX.FILE').subscribe((res: string) => {
            this.successFileTxt = res;
        });
    
        this.user = this._auth.getCurrentUser();
        this._id = this.user.id;    

        this._mybox.listen().subscribe(
            (e:any)=>{
                this.getBreadCrumbs(e)
            }
        )
        this.onUserDataChangedSubscription =
            this.fileManagerService.onFilesChanged.subscribe(file => {
                this.file = file;
            });

            this.fileManagerService.onFilesChanged.subscribe(
                selectedFile => {
                        this.selectedFile = selectedFile;
                        setTimeout(() => {
                            this.hasSelectedFile = selectedFile.length > 0;
                            this.isIndeterminate = (selectedFile.length !== this.fileManagerService.selectedFile.length && selectedFile.length > 0);
                        }, 0);
                    }
                );

    }

    ngOnInit()
    {
        this.fileManagerService.deselectFile();
        this.filter = '0';
        this.fileManagerForm = this.formBuilder.group({
            fileUpload          : [''],
            newFolder           : ['']
        });

        this.fileManagerService.onFileSelected.subscribe(selected => {
            this.selected = selected;
        });
       

        this.onFilesChanged =
        this.fileManagerService.onFilesChanged
            .subscribe(selectedFile => {
                this.hasSelectedFile = selectedFile.length > 0;
            });
    }

    getBreadCrumbs(id: string){
        
        this._mybox.getBreadCrumbsById(id).subscribe(
            data=>{
                this.pathArr = data
            }
        )
    }

    clickBreadCrumbs(id: string){
        if(this.filter == "0"){
            this.fileList.func2(id);
        }else if(this.filter == "1"){
            this.share.func2(id);
        }
    }

    eventChange(filter){
        this.filter = filter;        
    }

    fileUploadChange(e) {

        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

        var pattern = "application/x-msi";
        var pattern2 = "application/x-ms-dos-executable";
        var pattern3 = "application/vnd.debian.binary-package";
        
        var reader = new FileReader();

        if (file.type == pattern || file.type == pattern2 || file.type == pattern3 || file.size >= "10485760") {
            this.snackBar.open(this.errorFileTxt, "OK", {
            panelClass: ['red-snackbar']
            });
            return;       

        }else{
            reader.readAsDataURL(file);
            this.fileManagerForm.get('fileUpload').setValue(file);

            //save direct to db
            let myBoxAttachment: MyBoxAttachment = new MyBoxAttachment();
            myBoxAttachment.setCreatedby(this.user);
            myBoxAttachment.setUserid(this.user.id);

            let input = new FormData();
            input.append('fileUpload', this.fileManagerForm.get('fileUpload').value);       
            input.append('info', new Blob([JSON.stringify(myBoxAttachment)],
            {
                type: "application/json"
            }));
            
            const formModel = input;
            this._mybox.saveFile(formModel).subscribe(

                success=>{                   
                    this.snackBar.open(this.successFileTxt, "OK", {
                        panelClass: ['blue-snackbar']
                    });
                    this.fileList.func2(this._mybox.getParentId());
                },
                error=>{
                    this.snackBar.open(this.errorFileTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });
                }
            )
        }
    } 

    createFolder() {

        if(this.fileManagerForm.controls['newFolder'].value){
            let myBoxAttachment: MyBoxAttachment = new MyBoxAttachment();
            myBoxAttachment.setCreatedby(this.user);
            myBoxAttachment.setUserid(this.user.id);
            myBoxAttachment.setFoldername(this.fileManagerForm.controls['newFolder'].value);
        
            this._mybox.saveFolder(myBoxAttachment).subscribe(
                success => {
                    
                    this.snackBar.open(this.successFolderTxt, "OK", { 
                        panelClass: ['blue-snackbar'] 
                    }); 
                    this.fileManagerForm.controls['newFolder'].setValue(null)
                    this.fileList.func2(this._mybox.getParentId());
        
                },
                error => {
                    this.snackBar.open(this.errorFolderTxt, "OK", { 
                        panelClass: ['red-snackbar'] 
                    }); 
                }
            );
        }
    }

    stopPropagation(event){
        event.stopPropagation();       
    }

    updateFilter(event: any){
        this.fileList.updateFilter(event);
    }
    updateFilter1(event: any){
        this.share.updateFilter(event);
        this.share.setTable();
    }
    updateFilter2(event: any){
        this.achieveComponent.updateFilter(event);
    }

    ngOnDestroy()
    {
        this.onUserDataChangedSubscription.unsubscribe();
    }
    base64ToArrayBuffer(base64) {
                    var binaryString = window.atob(base64);
                    var binaryLen = binaryString.length;
                    var bytes = new Uint8Array(binaryLen);
                    for (var i = 0; i < binaryLen; i++) {
                    var ascii = binaryString.charCodeAt(i);
                    bytes[i] = ascii;
                    }
                    return bytes;
                }
    download(){
        for(const id of this.fileManagerService.selectedFile){
            
            this._mybox.getMyBoxFileById(id).subscribe(
                data => {
                    this.file = data; 
                    let a = document.createElement("a");
                    document.body.appendChild(a);
                    //a.style = "display: none"; 
    
                    var dataFile = this.base64ToArrayBuffer(this.file.content);
                    var blob = new Blob([dataFile], {type:data.formatType});
                    var url= window.URL.createObjectURL(blob);
                    a.href = url;
                    a.download = this.file.name;
                    a.click();
                    window.URL.revokeObjectURL(url);        
                }
            );
        }
    }

}



export class BreadCrumblings {
    path: string;
    link: string;
}