import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { MatTableModule, MatFormFieldModule, MatChipsModule, MatMenuModule, MatDialogModule, MatButtonModule, MatIconModule, MatRippleModule, MatSlideToggleModule, MatSidenavModule, MatCheckboxModule, MatInputModule, MatSelectModule } from "@angular/material";
import { FuseSharedModule } from "@fuse/shared.module";
import { FormShareComponent } from "./form.component";
import { CdkTableModule } from "@angular/cdk/table";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { TranslateModule } from "@ngx-translate/core";
import { FormsModule } from "@angular/forms";
import { SweetAlert2Module } from "@toverux/ngx-sweetalert2";


const routes: Routes = [
    {
        path     : ':id',
        component: FormShareComponent
    }
];
@NgModule({
    declarations: [
        FormShareComponent
    ],
    entryComponents: [
    ],
    imports     : [
        RouterModule.forChild(routes),
        CdkTableModule,
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatSidenavModule,
        MatTableModule,
        MatDialogModule,
        MatCheckboxModule,
        MatIconModule,
        MatMenuModule,
        NgxDatatableModule,
        MatInputModule,
        MatSelectModule, 
        MatRippleModule,
        MatSidenavModule, 
        MatFormFieldModule,
        SweetAlert2Module, 
        MatCheckboxModule,
        TranslateModule,
        FormsModule,
        FuseSharedModule,
    ],
    providers   : [
    
    ]
})
export class FormShareModule
{
}
