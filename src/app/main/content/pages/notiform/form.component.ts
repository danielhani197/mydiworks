import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';

import { locale as english } from '../file-manager/i18n/en';
import { locale as malay } from '../file-manager/i18n/my';
import { fuseAnimations } from '@fuse/animations';
import { MyBoxService } from '../../services/mybox/mybox.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

@Component({
    selector: 'form',
    templateUrl: 'form.component.html',
    styleUrls  : ['./form.component.scss'],
    animations : fuseAnimations
    })
    export class FormShareComponent implements OnInit
    {
        displayedColumns = ['icon', 'filename', 'type','createdby', 'fileSize', 'createddate', 'detail-button'];
        id:string;
        list:any[] = [];
        files:any[];
        selected: any;

        file: any;

        dataSource = new MatTableDataSource<any>(this.list);
        constructor(
            private route: ActivatedRoute,
            private _mybox: MyBoxService,
            private fuseTranslationLoader: FuseTranslationLoaderService,
        ) { 
            this.fuseTranslationLoader.loadTranslations(malay, english);
        }
        ngOnInit(){
            this.share();
        }

        share(){
            this.id = this.route.snapshot.paramMap.get('id');
            this._mybox.getMyBoxId(this.id).subscribe(
                shareData => {
                    this.file = shareData;
                    this.list.push(this.file);
                    console.log(this.list)
                }
            )
            
        }
        func2(id: string){
            this._mybox.setlocation(id);
    
            this._mybox.getListInFolder(id).subscribe(
                data => {
                    this.list = data;
                }
                
            )
        }

        base64ToArrayBuffer(base64) {
            var binaryString = window.atob(base64);
            var binaryLen = binaryString.length;
            var bytes = new Uint8Array(binaryLen);
            for (var i = 0; i < binaryLen; i++) {
               var ascii = binaryString.charCodeAt(i);
               bytes[i] = ascii;
            }
            return bytes;
        }
    
        download(){
            const id = this.route.snapshot.paramMap.get('id');
            console.log("download >>>>>>>>>", id);
            this._mybox.getMyBoxFileById(id).subscribe(
    
                data => {
                    this.file = data; 
                    let a = document.createElement("a");
                    document.body.appendChild(a);
                    //a.style = "display: none"; 
    
                    var dataFile = this.base64ToArrayBuffer(this.file.content);
                    var blob = new Blob([dataFile], {type:data.formatType});
                    var url= window.URL.createObjectURL(blob);
                    a.href = url;
                    a.download = this.file.name;
                    a.click();
                    window.URL.revokeObjectURL(url);
                }
            );
        }
     }