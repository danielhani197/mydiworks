export const locale = { 
    lang: 'en', 
    data: {         
        'PERMOHONAN': 
            { 
                'TITLE':'Request Join PET', 
                'SEARCH':'Search',
                'PET_NAME': 'Pet Name',
                'JENIS': 'Type',
                'BUT_REQUEST': 'Request Join PET',
                'BUT_BACK': 'Back',  
                'INFOR':'PET Information',
                'ERROR': 'Request Error',
                'SUCCESS':'Request Success',
                'JOIN': 'Joined',
                'REQUEST': 'Requested',
                'FORMAT': 'File Format is incorrect or File size limit is exceed'
                
            }, 
        'MELULUS': 
            { 
                'TITLE':'Approve Member PET ', 
                'SEARCH':'Search',
                'NOTES':'Notes',
                'USER': 'Applicant',
                'AGENCY': 'Agency',
                'APPROVE':{
                    'APPROVE':'Approve',
                    'SUCCESS': 'Request Approval',
                    'ERROR': 'Request Approval Cancel'
                },
                'REJECT':{
                    'REJECT':'Reject',
                    'SUCCESS': 'Request Rejected',
                    'ERROR': 'Request Rejected Cancel'
                }
            },
        'PET': { 
            'ALLPET': 'All PET',
            'SEARCH': 'Search',
            'TITLE': 'PET',
            'LIST': 'PET List',
            'LISTAPP': 'Request Create PET List',
            'NEW': 'Add PET',
            'DETAILS': 'PET Details',
            'NAMA': 'Name Of PET',
            'PEMOHON': 'Name Of Applicant   :  ',
            'NAMASINGKATAN': 'Short Name Of PET',
            'JENIS': 'Type Of PET',
            'AGENSI': 'Agency',
            'KEMENTERIAN': 'Ministry',
            'BAHAGIAN': 'Department',
            'CATATAN': 'Note',
            'KETERANGAN': 'Details',
            'TUJUAN': 'Purpose',
            'SURAT': 'Appointment Confirmation Letter :',
            'MAX': 'File limit is',
            'FORMAT': '.pdf only',
            'PILIH': 'Please Choose File to Upload',
            'ERROR': 'PET Details Are Unsuccessfully Submitted',
            'SUCCESS': 'PET Details Are Successfully Submitted',
                'BTN' : {
                    'DELETE' : 'Delete',
                    'DIACTIVED' : 'Diactived',
                    'ACTIVED' : 'Actived',
                    'EDIT' : 'Edit',
                    'SUBMIT' : 'Submit',
                    'BACK'  : 'Back',
                    'TAMBAHAHLI': 'Add Members',
                    'UPDATE': 'Update Roles',
                 },
                 'REJECT':{
                    'TITLE': 'Reject For Creating PET',
                    'DETAILS': 'Reject Justification',
                    'ERROR': 'Rejection Was Unsuccessfully',
                    'SUCCESS': 'Rejection Was Successfully',
                },
                'POPUP' : {
                    'TITLE' : 'Delete PET',
                    'CONFIRM' : 'Are you sure to delete this PET?',
                    'DELETE' : 'Delete',
                    'BACK' : 'Back',
                    'ERROR' : 'PET Was Unsuccessfully Deleted',
                    'SUCCESS' : 'PET Was Successfully Deleted'
                },
                'DIACTIVE':{
                    'ERROR': 'Maklumat PET Tidak Berjaya Di NyahAktif',
                    'SUCCESS': 'Maklumat PET Telah Berjaya Di NyahAktif'
                },
                'ACTIVE':{
                    'ERROR': 'Maklumat PET Tidak Berjaya Di Aktifkan',
                    'SUCCESS': 'Maklumat PET Telah Berjaya Di Aktifkan'
                },
                'DEACTIVE':{
                    'APPROVAL': 'List Application Of PET Deactivation',
                    'ERROR': 'PET Details Was Unsuccessfully Deactivated',
                    'SUCCESS': 'PET Details Was Successfully Deactivated'
                }

             
        },
         'AHLIPET' :{
            'ALL': 'All PET Member',
            'SEARCH': 'Search',
            'TITLE': 'Pet Member',
            'LIST': 'PET Member List',           
            'DETAILS': 'PET Member Details',
            'NEW': 'Request Create PET',
            'ERROR': 'PET Details Are Unsuccessfully Submitted',
            'SUCCESS': 'PET Details Are Successfully Submitted'

         }
    } 
}