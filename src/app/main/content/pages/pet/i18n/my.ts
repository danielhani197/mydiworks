export const locale = { 
    lang: 'my', 
    data: { 
        'PERMOHONAN': 
            { 
                'TITLE':'Permohonan Sertai PET', 
                'SEARCH':'Carian',
                'PET_NAME': 'Nama Pet',
                'JENIS': 'Jenis',
                'BUT_REQUEST': 'Pemohonan Sertai PET',
                'BUT_BACK': 'Kembali',
                'INFOR': 'Maklumat PET',
                'ERROR': 'Permoonan Tidak Berjaya',
                'SUCCESS':'Permohonan Berjaya',
                'JOIN': 'Telah Sertai',
                'REQUEST': 'Masih Dalam Permohonan',
                'FORMAT': 'Format fail salah atau Saiz fail melebihi had'
            }, 
        'MELULUS': 
            { 
                'TITLE':'Melulus Ahli PET', 
                'SEARCH':'Carian' ,
                'NOTES':'Catatan',
                'USER': 'Pemohon',
                'AGENCY': 'Agensi',
                'APPROVE':{
                    'APPROVE':'Lulus',
                    'SUCCESS': 'Permohonan Diluluskan',
                    'ERROR': 'Pengesahan Pemohon Batal'
                },
                'REJECT':{
                    'REJECT': 'Gagal',
                    'SUCCESS': 'Permohonan Ditolak',
                    'ERROR': 'Pemohonan Ditolak Batal'
                }
            },
        'PET': { 
            'ALLPET': 'Semua PET',
            'SEARCH': 'Carian',
            'TITLE': 'PET',
            'LIST': 'Senarai PET ',
            'LISTAPP': 'Senarai Permohonan Bina Pet',
            'NEW': 'Tambah PET',
            'DETAILS': 'Maklumat PET',
            'NAMA': 'Nama PET',
            'PEMOHON': 'Nama Pemohon    :  ',
            'NAMASINGKATAN': 'Nama Singkatan PET',
            'JENIS': 'Jenis Bagi PET',
            'AGENSI': 'Agensi',
            'KEMENTERIAN': 'Kementerian',
            'BAHAGIAN': 'Bahagian',
            'CATATAN': 'Catatan',
            'KETERANGAN': 'Keterangan',
            'TUJUAN': 'Tujuan',
            'SURAT': 'Surat Pengesahan Perlantikan :',
            'MAX': 'Maksima saiz setiap fail adalah',
            'FORMAT': '.pdf Sahaja',
            'PILIH': 'Sila Pilih Fail Untuk Dimuatnaik',
            'BTN' : {
                'DELETE' : 'Padam',
                'EDIT' : 'Kemaskini',
                'DIACTIVED' : 'Nyah Aktif',
                'ACTIVED' : 'Aktif',
                'SUBMIT' : 'Hantar',
                'BACK'  : 'Kembali',
                'TAMBAHAHLI': 'Tambah Ahli',
                'UPDATE': 'Kemaskini Peranan',
            },
            'REJECT':{
                'TITLE': 'Penolakan Permohonan Bina PET',
                'DETAILS': 'Catatan Justifikasi Penolakan',
                'ERROR': 'Penolakan Bina PET Tidak Berjaya',
                'SUCCESS': 'Penolakan Bina PET Berjaya',
            },
            'POPUP' : {
                'TITLE' : 'Padam PET',
                'CONFIRM' : 'Adakah anda pasti untuk memadam PET ini?',
                'DELETE' : 'Padam',
                'BACK' : 'Kembali',
                'ERROR' : 'PET Tidak Dapat Dipadam',
                'SUCCESS' : 'PET Telah Berjaya Dipadam'
                 }, 
            'DIACTIVE':{
                'ERROR': 'Maklumat PET Tidak Berjaya Di NyahAktif',
                'SUCCESS': 'Maklumat PET Telah Berjaya Di NyahAktif'
            },
            'ACTIVE':{
                'ERROR': 'Maklumat PET Tidak Berjaya Di Aktifkan',
                'SUCCESS': 'Maklumat PET Telah Berjaya Di Aktifkan'
            },
            'ERROR': 'Maklumat PET Tidak Dapat Disimpan',
            'SUCCESS': 'Maklumat PET Telah Berjaya Dihantar',

            'DEACTIVE':{
                'APPROVAL': 'Senarai Permohonan NyahAktif PET',
                'ERROR': 'Maklumat PET Tidak Berjaya Di NyahAktifkan',
                'SUCCESS': 'Maklumat PET Telah Berjaya Di NyahAktifkan'
            }
            
        },
        'AHLIPET' :{
            'ALL': 'Semua Ahli PET',
            'SEARCH': 'Carian',
            'TITLE': 'Ahli PET',
            'LIST': 'Senarai Ahli PET',           
            'DETAILS': 'Maklumat Ahli PET',
            'NEW': 'Mohon Bina PET',
            'ERROR': 'Maklumat PET Tidak Dapat Disimpan',
            'SUCCESS': 'Maklumat PET Telah Berjaya Dihantar'

         }

    
 
    } 
}