import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from "@angular/router";

import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { Page } from 'app/main/content/model/util/page';



import { locale as english } from './../i18n/en';
import { locale as malay } from './../i18n/my';
import { PetService } from 'app/main/content/services/pet/pet.service';
import { PagedData } from 'app/main/content/model/util/paged-data';


@Component({
    selector   : 'senarai-nyahaktif',
    templateUrl: './senarai-nyahaktif.component.html',
    styleUrls  : ['./senarai-nyahaktif.component.scss']
})
export class SenaraiNyahaktifComponent implements OnInit
{
    id: string;

    rows: any[];
    temp = [];
    page = new Page();

    loadingIndicator = true;
    reorderable = true;


    successMsg : string;
    failMsg : string;
    successDelMsg : string;
    failDelMsg : string;

    constructor(
        private http: HttpClient,
        private _router: Router,
        private route: ActivatedRoute,
        private snackBar: MatSnackBar,
        private translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _pet: PetService,

    )
    {
        this.page.pageNumber = 0;
        this.page.size = 10;
        this.page.sortField = "pemohon";
        this.page.sort = "asc";

        this.fuseTranslationLoader.loadTranslations(malay, english);

        this.translate.get('PET.DEACTIVE.SUCCESS').subscribe((res: string) => {
            this.successMsg = res;
        });
        this.translate.get('PET.DEACTIVE.FAIL').subscribe((res: string) => {
            this.failMsg = res;
        });

    }

    ngOnInit(){
        this.setPage({ offset: 0 });  
    }

    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._pet.getDeactiveList(this.page).subscribe(
            pagedData =>{
                this.page = pagedData.page;
                this.rows = pagedData.data;
                this.loadingIndicator = false;
            }
        )
    }


    onSort(event) {
        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._pet.getDeactiveList(this.page).subscribe(
            pagedData => {
                this.page = pagedData.page;
                this.rows = pagedData.data;
                this.loadingIndicator = false;
            }
        )

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;
    
        this._pet.getDeactiveList(this.page).subscribe(
            pagedData=> {
                this.page = pagedData.page;
                this.rows = pagedData.data;
                this.loadingIndicator = false;
            }
        )
        
    }

    approve(id: string){
        if(id){
            this._pet.approveDeactive(id).subscribe(
                success => {
                    this.snackBar.open(this.successMsg, "OK",{
                        panelClass: ['blue-snackbar']
                    });
                    this.setPage({ offset: 0 });
                },
                error => {
                    this.snackBar.open(this.failMsg, "OK",{
                        panelClass: ['red-snackbar']
                    })
                }
            )
        }
    }

    reject(id: string){
        if(id){
            this._pet.rejectDeactive(id).subscribe(
                success => {
                    this.snackBar.open(this.successMsg, "OK",{
                        panelClass: ['blue-snackbar']
                    });
                    this.setPage({ offset: 0 });

                },
                error => {
                    this.snackBar.open(this.failMsg, "OK",{
                        panelClass: ['red-snackbar']
                    })
                }
            )
        }
    }

}

