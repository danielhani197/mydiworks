import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { 
    MatButtonModule, 
    MatCheckboxModule, 
    MatIconModule, 
    MatFormFieldModule, 
    MatInputModule,
    MatChipsModule,
    MatMenuModule,
    MatSnackBarModule
    
} from '@angular/material';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FuseSharedModule } from '@fuse/shared.module';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { TranslateModule } from '@ngx-translate/core';

import { SenaraiNyahaktifComponent} from './senarai-nyahaktif.component';

const routes = [
    {
        path     : 'deactive',
        component: SenaraiNyahaktifComponent
    }
];

@NgModule({
    declarations: [
        SenaraiNyahaktifComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatMenuModule,
        MatSnackBarModule,

        SweetAlert2Module,

        NgxDatatableModule,

        FuseSharedModule,
        TranslateModule
    ],
})
export class SenaraiNyahaktifModule
{
}
