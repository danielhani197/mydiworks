import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router,  ActivatedRoute } from "@angular/router";
import {MatDialog,  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Page } from 'app/main/content/model/util/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material';


import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import {AhliPetService} from 'app/main/content/services/pet/ahliPet.service';
import { AhliPet } from '../../../../../model/pet/ahliPet';
import { AuthenticationService } from '../../../../../services/authentication/authentication.service';
import { PetAuthorities } from '../../../../../model/pet/petAuthorities';


@Component({
    selector   : 'fuse-senarai-ahliPet',
    templateUrl: './senarai-ahliPet.component.html',
    styleUrls  : ['./senarai-ahliPet.component.scss']
})
export class SenaraiAhliPetComponent implements OnInit
{ 
    editing = {};
    id: string;
    user2 : any;
    rows: any[];
    temp = [];
    page = new Page();

    successTxt: string;
    existTxt: string;

    _idToUpdate : string;
    _idToDelete : string;
    loadingIndicator = true;
    reorderable = true;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private http: HttpClient,
        private _router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private _ahliPet : AhliPetService,
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        public _dialog: MatDialog,
        public snackBar: MatSnackBar,


    )
    {
        this._translate.get('PET.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('PET.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
      this.fuseTranslationLoader.loadTranslations(malay, english);

      this.page.pageNumber = 0;
      this.page.size = 10;
      this.page.sortField = "user.name";
      this.page.sort = "asc";
    }
    ngOnInit(){
        this.id = this.route.snapshot.paramMap.get('id');
        this.setPage({ offset: 0 });
        
        

    }

    setPage(pageInfo){
        this.page.status = 'Active';
        this.page.pageNumber = pageInfo.offset;
        this._ahliPet.getAhliPetList(this.page, this.id).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._ahliPet.getAhliPetList(this.page, this.id).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;

        this._ahliPet.getAhliPetList(this.page, this.id).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }
    updateValue(event, id, rowIndex, cell) {
        
        this.editing[rowIndex + '-' + cell] = false;       
        let role : string = event.target.value;
        let ahli : AhliPet = new AhliPet();
        
        let petAuth : PetAuthorities = new PetAuthorities()

        petAuth.setId(role)

        ahli.setId(id);
        ahli.setPetAuthorities(petAuth);

        this._ahliPet.updatePet(ahli).subscribe(
            success=>{
                this.snackBar.open(this.successTxt, "OK", {
                    panelClass: ['blue-snackbar']
                  });

            },
            error=>{
                
                this.snackBar.open(this.existTxt, "OK", {
                    panelClass: ['red-snackbar']
                  });
                  
            }
        )

    }

}
