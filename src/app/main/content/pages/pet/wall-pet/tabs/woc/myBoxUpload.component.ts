import { Component, OnInit, Inject} from '@angular/core';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../../../../profile/i18n/en';
import { locale as malay } from '../../../../profile/i18n/my';
import { AuthenticationService } from '../../../../../services/authentication/authentication.service';
import { User } from '../../../../../model/user';
import { PetAttachment } from '../../../../../model/wall3/petAttachment';
import { Taksonomi } from '../../../../../model/wall3/taksonomi';
import { TaksonomiService } from '../../../../../../../../app/main/content/services/taksonomi/taksonomi.service';
import { MyBoxService } from '../../../../../../../../app/main/content/services/mybox/mybox.service';
import { FormControl } from '@angular/forms';
import { DataService } from 'app/main/content/services/wall3/data.service';

declare var $: any;

@Component({
  selector: 'myBoxUploadComponent',
  templateUrl: 'myBoxUpload.component.html',
  styleUrls  : ['myBoxUpload.component.scss'],
  animations : fuseAnimations
})
export class MyBoxUploadComponent implements OnInit
{
    attachId: string;
    filename: string;
    uploadmForm: FormGroup;
    uploadmFormErrors: any;
    user: User;
    taksonomiLs: any[];
    myboxLs: any[];
    taks: any[];
    pa: PetAttachment;
    taksonomi: Taksonomi;
    searchTerm : FormControl = new FormControl();
    myTaks = <any>[];
    size: any;
    displayedColumns = ['icon', 'filename', 'fileSize', 'customColumn1'];
    errTaxNull = false;
   constructor(
       private _translate: TranslateService,
       private fuseTranslationLoader: FuseTranslationLoaderService,
       private formBuilder: FormBuilder,
       private route: ActivatedRoute,
       private _auth: AuthenticationService,
       private _myboxservice: MyBoxService,
       private _taksonomiservice: TaksonomiService,
       public snackBar: MatSnackBar,
       private _router: Router,
       private _dataservice: DataService,
       public _dialog: MatDialog,
       public _dialogRef: MatDialogRef<MyBoxUploadComponent>,
       @Inject(MAT_DIALOG_DATA) public data: any
   )
    {
    
    this.fuseTranslationLoader.loadTranslations(malay, english);
    this.user = this._auth.getCurrentUser();

    }

   ngOnInit()
   {
    this.uploadmForm = this.formBuilder.group({
        tajuk             : [ ],
        keterangan        : [ ],
        taksonomi         : [ ],
        jenis             : [ ],
        peristiwa         : [ ],
        tarikhPeristiwa   : [ ],
        lokasi            : [ ],
      });

      this._taksonomiservice.getTaks().subscribe(
        taks => {
          this.taksonomiLs = taks;
        }
      )

      this.searchTerm.valueChanges.subscribe(
        term => {
          if (term !=''){
            this._taksonomiservice.keyUpSearch(term).subscribe(
              data=> {
                this.myTaks = data;
              }
            )
          }else {
            this.myTaks = this.taksonomiLs
          }
      })

      this._myboxservice.setlocation("home");     
      this.setTable();   
   }

  setTable() {
    this._myboxservice.getMyboxList(this.user.id).subscribe(
      myboxList => {
        this.myboxLs = myboxList;
      })
  }

  submitTambahFile(instanceid: string, filename: string){
    this.attachId = instanceid;
    this.filename = filename;
    document.getElementById('mybox').style.display = 'block';
    document.getElementById('content-atas').style.display = 'none';
  }

  func2(id: string){
    this._myboxservice.setlocation(id);
    this._myboxservice.getListInFolder(id).subscribe(
      data => {
        this.myboxLs = data;
      }
    )
  }

  upload(){
    if(!this.taksonomi){
      this.errTaxNull = true
    }else{
      this._dataservice.filter();
      if (this.uploadmForm.valid) {

        let pa: PetAttachment = new PetAttachment ();

          pa.setTajuk(this.uploadmForm.controls['tajuk'].value);
          pa.setPeristiwa(this.uploadmForm.controls['peristiwa'].value);
          pa.setPeristiwadate(this.uploadmForm.controls['tarikhPeristiwa'].value);
          pa.setKeterangan(this.uploadmForm.controls['keterangan'].value);
          pa.setLokasi(this.uploadmForm.controls['lokasi'].value);
          pa.setCreatedby(this.user);
          pa.setTaksonomi(this.taksonomi);
          pa.setInstanceid(this.attachId);
          pa.setFilename(this.filename)

          this._dataservice.setInfo(pa);
          this._dialogRef.close(); 
      }
    }
    
    }   

    setTaksonomi(id: any) { 
      this.taks = this.myTaks.filter(value => value.id === id);
      this.taksonomi = this.taks[0];
      this.errTaxNull = false;
    }

    formatNumber2Decimal(fileSize:number){
      this.size = fileSize/1048576;
      return this.size.toFixed(2);
    }
  }

