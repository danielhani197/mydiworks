export const locale = {
    lang: 'my',
    data: {
        'ANNOUNCEMENT': {
            'TITLE':'Tajuk',
            'ANNOUNCEMENT':'Pengumuman',
            'CONTENT':'Kandungan',
            'UPDATE_BY':'Kemaskini Oleh',
            'DATE':'Tarikh',
            'UPDATE_BUT':'Kemaskini',
            'CANCEL' : 'Batal',
            'ERROR': 'Gagal Kemaskini Data',
            'SUCCESS': 'Data Berjaya Di Kemaskini',
            'SEARCH' : 'Cari',
            'VIEW': 'Lihat',
            'AGREE':'Saya Setuju',
            'ADD': 'Tambah Pengumuman',
            'START_DATE': 'Tarikh Mula',
            'END_DATE': 'Tarikh Tamat',
            'ACTION': 'Tindakan',
            'BTN' : {
                'DELETE' : 'Padam',
                'EDIT' : 'Kemaskini',
                'SUBMIT' : 'Hantar',
                'BACK'  : 'Kembali',
            },
            'DELETEPENGUMUMAN' : {
                'DELETEPENGUMUMAN' : 'Padam Pengumuman',
                'CONFIRM' : 'Adakah anda pasti untuk memadam maklumat pengumuman ini?',
                'DELETE' : 'Padam',
                'BACK' : 'Kembali',
                'ERROR' : 'Sila Padam Maklumat Agensi Untuk Pengumuman Ini Terlebih Dahulu',
                'SUCCESS' : 'Maklumat Pengumuman Telah Berjaya Dipadam'
            },
        }
    }
};
