import { Calendar } from './../../../../../model/calendar/calendar';
import { Component, OnInit, ViewChild} from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Router, ActivatedRoute } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { MyWallService } from 'app/main/content/services/wall3/myWall.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { WallKandungan } from '../../../../../model/wall3/wallKandungan';
import { Hashtag } from '../../../../../model/wall3/hashtag';
import { AuthenticationService } from '../../../../../services/authentication/authentication.service';
import { User } from '../../../../../model/user';
import { WallKomen } from '../../../../../model/wall3/wallKomen';
import { FileStorageService } from 'app/main/content/services/wall3/filestorage.service'
import { PetAttachment } from '../../../../../model/wall3/petAttachment';
import { DataService } from 'app/main/content/services/wall3/data.service';
import { DocumenDialogComponent } from './documenDialog.component';
import { MyBoxUploadComponent } from './myBoxUpload.component';

import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';
import { Pet } from '../../../../../model/pet/pet';
import { PetService } from '../../../../../services/pet/pet.service';
import { WallPetService } from '../../../../../services/pet/wall-pet.service';
import { EditDialogComponent } from './editDialog.component';
import { DeleteDialogComponent } from './deleteDialog.component';
import { PostCollection } from '../../../../../model/wall3/postCollection';
import { AdvancedPage } from '../../../../../model/util/advanced-page';
import { Notifikasi } from '../../../../../model/notifikasi';
import { NotifikasiService } from '../../../../../services/setting/notifikasi.service';
import { AhliPetService } from '../../../../../services/pet/ahliPet.service';
import { AhliPet } from '../../../../../model/pet/ahliPet';
import { Event } from 'app/main/content/model/calendar/event';

//PengumumanPet
import { PengumumanPetService } from 'app/main/content/services/pet/pengumumanPet.service';
import { Pengumuman } from 'app/main/content/model/pengumuman/pengumuman';
import { CalendarService } from '../../../../../services/calendar/calendar.service';

@Component({
    selector   : 'wall-pet-woc',
    templateUrl: './woc.component.html',
    styleUrls  : ['./woc.component.scss'],
    animations : fuseAnimations
})
export class WocComponent
{

    page: AdvancedPage = new AdvancedPage;

    timeline: PostCollection[]= new Array<PostCollection>();

    _id: string;
    petObj: Pet;
    
    length: any;
    user: User;
    hashtagList: Hashtag[] = new Array<Hashtag>();
    tagList: string[] = new Array<string>();

    successTxt: string;
    existTxt: string;

    postForm: FormGroup;
    postFormErrors: any;

    showLoadingBar: boolean;

    //kalendar
    kalendarEvents: Event[] = new Array<Event>();
    //pengumuman
    pengumumanLs : Pengumuman[] = new Array();
    myKalendar: Calendar = new Calendar();
    
    ahlipet: AhliPet[];
    ahli:AhliPet;
    ahliUser:User;
    lenght:number;
    pa : boolean = false;
    namaFail: string;

    constructor(
        private _auth: AuthenticationService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _pet: PetService,
        private _petWall: WallPetService,
        private _wall: MyWallService,
        private _translate: TranslateService,
        private _ahliPet: AhliPetService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        public snackBar: MatSnackBar,
        public _dialog: MatDialog,
        private _filestorage: FileStorageService,
        private _dataservice: DataService,
        private _notifi:NotifikasiService,
        public pengumumanPetService: PengumumanPetService,
        private calendarService: CalendarService
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
        this._translate.get('WALL.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('WALL.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });

        this.postFormErrors = {
            post : {}
        };

        this.user = this._auth.getCurrentUser();
        
        this.calendarService.petListen().subscribe(
            (_)=>{
                this.updateCalendar();
            }
        )
        
        this.page.pageNumber = 0;
        this.page.size = 10;    
        
        this._dataservice.listen().subscribe(
            (e:any)=>{                
                setTimeout(()=>{ 
                    let uploadedFile: PetAttachment = this._dataservice.getInfo();
                    if(uploadedFile!=null){
                        this.pa = true;
                        this.namaFail = uploadedFile.filename;
                    }    
                }, 1000)                           
            }
        )       
    }
    ngOnInit(): void {
        this.route.params.subscribe(
            params => {
                const id = +params['id'];
                this.getData(id);
            }
        );

        this.updateCalendar();

        //load pengumuman
        this._id = this.route.snapshot.paramMap.get('id');
        this.pengumumanPetService.getListPengumumanPetWall(this._id).subscribe(
            pengumumanPet=>{
                this.pengumumanLs = pengumumanPet;
                console.log(pengumumanPet)

            }

        )
    }

    getData(id){
        this.page.pageNumber = 0;
        this.page.size = 10;    
        
        this._id = this.route.snapshot.paramMap.get('id');
        this.postForm = this.formBuilder.group({
            post: ['', [Validators.required]],
            tagInput: ['']
        })
        this._pet.getPetById(this._id).subscribe(
            data=>{
                this.petObj = data;
                this.page.search = data.id;
                this.getLatestTimeline();
            }
        )

        

    }

    updateCalendar(){
        //calendar tukar ni
        this.calendarService.getPetCurrentWeekEvents(this._id).subscribe(
            list => {
                this.kalendarEvents = list;
            }
        )
    }

    toggleComment(id: string){
        var documentid = 'komen-'+id
        var icon1id = 'icon-arrow1-'+id
        var icon2id = 'icon-arrow2-'+id
        var previewid = 'preview-'+id

        var x = document.getElementById(documentid);
        var y = document.getElementById(icon1id);
        var z = document.getElementById(icon2id);
        var w = document.getElementById(previewid);

        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }

        if (y.style.display === "none") {
            y.style.display = "block";
        } else {
            y.style.display = "none";
        }

        if (z.style.display === "none") {
            z.style.display = "block";
        } else {
            z.style.display = "none";
        }

        w.style.display = 'none'

    }

    addTag(event: any){
        const input = event.input;
        const value = event.value;

        // Add tag
        if ( value )
        {
            this.tagList.push(value);
        }

        // Reset the input value
        if ( input )
        {
            input.value = '';
        }
    }

    removeTag(tag)
    {
        const index = this.tagList.indexOf(tag);
        if ( index >= 0 )
        {
            this.tagList.splice(index, 1);
        }
    }

    onCommentEvent(post: any){
        
        if(this.user){
            let documentId = post.wallKandungan.id;
            var comment = (<HTMLInputElement>document.getElementById(documentId)).value;
            if(comment){
                let wallKomen: WallKomen = new WallKomen();
                wallKomen.setCreateddate(new Date);
                wallKomen.setKeterangan(comment);
                wallKomen.setCreatedby(this.user);
                wallKomen.setId(documentId)
                this._wall.createKomen(wallKomen).subscribe(
                    success=>{
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                          });
                          (<HTMLInputElement>document.getElementById(documentId)).value = null;
                          this.getLatestTimeline();
                    },
                    error=>{
                        
                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['red-snackbar']
                          });
                    }
                )
                this._pet.getPetById(this._id).subscribe(
                    data=>{
                        this.user = this._auth.getCurrentUser();
                        this._ahliPet.getByPetId(this._id).subscribe(
                            ahliPet=>{
                                this.ahlipet = ahliPet;
                                this.lenght = ahliPet.length;
                                for(let i=0;i<this.length;i++){
                                    this.ahli = this.ahlipet[i];
                                    if(this.ahli){
                                        this.ahliUser = this.ahli.user;
                                            if(this.ahliUser.id != this.user.id){
                                            let noti: Notifikasi = new Notifikasi()
                                            
                                                noti.setCreatedby(this.user),
                                                noti.setUser(this.ahliUser),
                                                noti.setMaklumat("Comment in a post "+ data.namaSingkatan),
                                                noti.setStatus("New"),
                                                noti.setTindakan("/pet-post/post/"+documentId),
                                                noti.setModifiedby(this.user)
                                                
                                                this._notifi.createNoti(noti).subscribe(
                                                    uccess=>{

                                                    },
                                                    error=>{
                                                        
                                                    }
                                                )
                                            }
                                    }
                                    
                                            
                                        
                                }
                            }
                        )
                    }
                )
            }
            
        }
    }

    onPostEvent(){
        if(this.user){

            for(let obj of this.tagList){
                let hashtag: Hashtag = new Hashtag();
                hashtag.setName(obj);
                this.hashtagList.push(hashtag);
            }
            let wallKandungan: WallKandungan = new WallKandungan();
            wallKandungan.setKeterangan(this.postForm.controls['post'].value);
            wallKandungan.setCreateddate(new Date);
            wallKandungan.setCreatedby(this.user);
            wallKandungan.setCategory("PET");
            wallKandungan.setHashtagLs(this.hashtagList);
            wallKandungan.setPetid(this._id);
            wallKandungan.setPetname(this.petObj.nama)

            this._wall.createPost(wallKandungan).subscribe(
                wallKandungan=>{

                    let input = new FormData();               
                    var mode = localStorage.getItem("MODE-UPLOAD");

                    if(mode == "PC"){
                        let wa: PetAttachment = this._dataservice.getInfo();
                    
                        if( wa != null){
                            wa.wallKandungan = wallKandungan;
                        
                            input.append('fileUpload', this._dataservice.getData());
                            input.append('info', new Blob([JSON.stringify(wa)],
                                {
                                    type: "application/json"
                                }));   
    
                            const formModel = input;
                            this._filestorage.savePetFile(formModel).subscribe(
                                success=>{
                                    localStorage.removeItem('info');
                                    this.getLatestTimeline();
                                    this._dataservice.clearInfo()
                                }
                            );
                        }
                    }else if (mode == "MYBOX"){
                        let wa: PetAttachment = this._dataservice.getInfo();
                        
                        if( wa != null){
                            wa.wallKandungan = wallKandungan;
                        
                            input.append('info', new Blob([JSON.stringify(wa)],
                                {
                                    type: "application/json"
                                }));      
                            const formModel = input;
                            this._filestorage.savePetMyboxFile(formModel).subscribe(
                                success=>{
                                    localStorage.removeItem('info');
                                    this.getLatestTimeline();
                                    this._dataservice.clearInfo()
                                }
                            );
                        }
                    }
                        
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                    });
                    this.getLatestTimeline();
                    this.postForm.reset();
                    this.tagList = [];
                    this.hashtagList = [];
                },
                error=>{
                    
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });
                }
            )
        }
    }

    formUpload(){
        localStorage.setItem("MODE-UPLOAD","PC")
        let dialogRef = this._dialog.open(DocumenDialogComponent,{
          width: '670px',
          height: '500px'
        });
    }

    myBoxUpload(){
        localStorage.setItem("MODE-UPLOAD","MYBOX")
        let dialogRef = this._dialog.open(MyBoxUploadComponent,{
          width: '900px',
          height: '600px'
        });
    }

    deletePost(id: string){
        let dialogRef = this._dialog.open(DeleteDialogComponent, {
            width: '500px',
            data: { 
                id: id,
                type: "post"
            }
        });
        dialogRef.afterClosed().subscribe(
            data=>{
                this.getLatestTimeline();
            }
        )
    }

    editPost(id: string){
        let dialogRef = this._dialog.open(EditDialogComponent, {
            width: '750px',
            height: '400px',
            data: { 
                id: id,
                type: "post"
            }
        });
        dialogRef.afterClosed().subscribe(
            data=>{
                this.getLatestTimeline();
            }
        )
    }

    deleteComment(id: string){
        let dialogRef = this._dialog.open(DeleteDialogComponent, {
            width: '500px',
            data: { 
                id: id,
                type: "komen"
            }
        });
        dialogRef.afterClosed().subscribe(
            data=>{
                this.getLatestTimeline();
            }
        )
    }

    editComment(id: string){
        let dialogRef = this._dialog.open(EditDialogComponent, {
            width: '750px',
            height: '350px',
            data: { 
                id: id,
                type: "komen"
            }
        });
        dialogRef.afterClosed().subscribe(
            data=>{
                this.getLatestTimeline();
            }
        )
    }

    getLatestTimeline(){
        
        this._petWall.getPostAll(this.page).subscribe(
            data=>{
                this.timeline = data.data;
                this.length = data.advancedPage.totalElements;
            }
        );
    }

    getNext(){
        if( this.length > this.timeline.length ){
            this.page.size += 10
            this._petWall.getPostAll(this.page).subscribe(
                data=>{
                    this.timeline = data.data
                }
            );
        }
        
    }

    streamFile(attachment){

        let a = document.createElement("a");
        document.body.appendChild(a);
        //a.style = "display: none"; 
        var dataFile = this.base64ToArrayBuffer(attachment.content);
        var blob = new Blob([dataFile], {type: attachment.formatType});
        var url= window.URL.createObjectURL(blob);
        a.href = url;
        a.download = attachment.filename;
        a.click();
        window.URL.revokeObjectURL(url);
    }

    base64ToArrayBuffer(base64) {
        var binaryString = window.atob(base64);
        var binaryLen = binaryString.length;
        var bytes = new Uint8Array(binaryLen);
        for (var i = 0; i < binaryLen; i++) {
           var ascii = binaryString.charCodeAt(i);
           bytes[i] = ascii;
        }
        return bytes;
    }

}
