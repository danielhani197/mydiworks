import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { MatSnackBar } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';

import { MohonNyahAktif } from '../../../../../model/pet/mohonNyahAktif';
import { PetService } from '../../../../../services/pet/pet.service';
import { Pet } from 'app/main/content/model/pet/pet';
import { User } from 'app/main/content/model/user';
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service';



@Component({
    selector: 'deactive-dialog',
    templateUrl: 'deactive-dialog.component.html',
    animations: fuseAnimations
})
export class DeactiveDialogComponent implements OnInit {

    petId: string;
    deactiveForm: FormGroup;
    deactiveFormErrors: any;
    adminPet: User;

    successTxt: string;
    existTxt: string;

    constructor(
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        public snackBar: MatSnackBar,
        private _router: Router,
        private _pet: PetService,
        private _auth: AuthenticationService,
        public _dialogRef: MatDialogRef<DeactiveDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {

        this.fuseTranslationLoader.loadTranslations(malay, english);

        this._translate.get('WALL.DEACTIVE.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('WALL.DEACTIVE.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });

        this.fuseTranslationLoader.loadTranslations(malay, english);

    }

    ngOnInit() {
        this.deactiveForm = this.formBuilder.group({
            catatan: ['', [Validators.required]]
        });


        this.deactiveForm.valueChanges.subscribe(() => {
            this.onDeactiveFormValuesChanged();
        });

        this.petId = this.data.id;
        this.adminPet = this._auth.getCurrentUser();

    }

    hantar() {
        if(this.petId){
            let mohonNyahAktif: MohonNyahAktif = new MohonNyahAktif();
            mohonNyahAktif.setCatatan(this.deactiveForm.controls['catatan'].value),
            mohonNyahAktif.setPemohon(this.adminPet)
    
            let pet: Pet = new Pet();
            pet.setId(this.petId);
    
            mohonNyahAktif.setPet(pet);
            this._pet.deactiveApply(mohonNyahAktif).subscribe(
                
                success => {
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                    });
                    this._dialogRef.close();
                },
                error => {
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });
                }
            )
        }
    }

    onDeactiveFormValuesChanged() {
        for (const field in this.deactiveFormErrors) {
            if (!this.deactiveFormErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.deactiveFormErrors[field] = {};

            // Get the control
            const control = this.deactiveForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.deactiveFormErrors[field] = control.errors;
            }
        }
    }


}