import { Component } from '@angular/core';
import { Pet } from '../../../../../model/pet/pet';
import { PenggunaService } from '../../../../../services/pengguna/pengguna.service';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthenticationService } from '../../../../../services/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PetService } from '../../../../../services/pet/pet.service';
import { AhliPetService } from '../../../../../services/pet/ahliPet.service';
import { Agensi } from '../../../../../model/general/agensi';
import { Kementerian } from '../../../../../model/general/kementerian';
import { Bahagian } from '../../../../../model/general/bahagian';
import { KementerianService } from '../../../../../services/agency/kementerian.service';
import { BahagianService } from '../../../../../services/agency/bahagian.service';
import { AgensiService } from '../../../../../services/agency/agensi.service';
import { MatIconRegistry, MatDialog } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { User } from '../../../../../model/user';
import {DeactiveDialogComponent} from './deactive-dialog.component';



@Component({
    selector   : 'fuse-about-pet',
    templateUrl: './about.component.html',
    styleUrls  : ['./about.component.scss'],
    animations : fuseAnimations
})
export class FuseAboutPetComponent
{
    _id: string;
    pet: Pet;
    nama: any;
    namaSingkatan:any;
    keterangnan: any;
    tujuan: any;
    tarikhWujud: any;
    tarikhDeactive: any;
    status:any;
    agensi: any;
    catatan: any;
    kementerian: any;
    bahagian: any;

    ahliPetLs: any;
    kementerianNama:any;

    userId:any;
    peranan: any;
    currentUser: any;

    user: User;
    petObj: Pet;
    //ahliPet: AhliPet;
    i: number;
    countLength: number;
    rolesAdmin:any;
    ahliPet: any[];
    createddate:Date;
    ahliPetUser:User;

    _idToDeactive : string;

    constructor(    private petService: PetService,
                    private _ahliPet: AhliPetService,
                    private formBuilder: FormBuilder,
                    private kementerianService: KementerianService,
                    private bahagianService: BahagianService,
                    private agensiService: AgensiService,
                    private authenticationService: AuthenticationService,
                    private route: ActivatedRoute,
                    private router: Router,
                    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,
                    public _dialog: MatDialog,

        )
    {
        iconRegistry.addSvgIcon(
            'thumbs-up',
            sanitizer.bypassSecurityTrustResourceUrl('assets/img/examples/thumbup-icon.svg'));
    } 
    
    ngOnInit(): void{
        

        this._id = this.route.snapshot.paramMap.get('id');
        this.petService.getPetById(this._id).subscribe(
            data=>{
                this.pet = data;
                this.nama = data.nama;
                this.namaSingkatan = data.namaSingkatan;
                this.keterangnan = data.keterangan;
                this.tujuan = data.tujuan;
                this.createddate = data.createddate;
                this._ahliPet.getByPetId(data.id).subscribe(
                    ahliPet =>{
                            this.ahliPet = ahliPet;
                            this.user = this.authenticationService.getCurrentUser();
                            this.userId = this.user.id;
                            this.countLength = this.ahliPet.length;
                            
                            for( this.i=0 ; this.i< this.countLength;this.i++){
                                this.ahliPetUser = this.ahliPet[this.i].user;
                                if(this.ahliPetUser){
                                     if(this.ahliPetUser.id == this.userId && this.ahliPet[this.i].tempPet.id == this._id){
                                        this.rolesAdmin = this.ahliPet[this.i].peranan;
                                    }
                                }
                               
                            }
                        }
                    )
                if(data.agensi){
                    this.agensi = data.agensi.name;
                }
                if(data.kementerian){
                    this.kementerian = data.kementerian.name;  
                }
                if(data.bahagian){
                    this.bahagian = data.bahagian.name; 
                }
                
                this.catatan = data.catatan;
            }            
        )
    }
    editForm(){
        this._id = this.route.snapshot.paramMap.get('id');
        this.petService.getPetById(this._id).subscribe(
            data=>{
                
                this.router.navigate(['profilePet/edit/'+data.id]);
                console.log(data.id)
            })

        
    }
    getData(id){
        
        this.petService.getPetById(this._id).subscribe(
            data=>{
                
            }
        )
    }

    deactiveDialog(id: string){
        this._idToDeactive = this.route.snapshot.paramMap.get('id');
        // this._idToDeactive = id;
        let dialogRef = this._dialog.open(DeactiveDialogComponent,{
          width: '800px',
          data: {id: this._idToDeactive}
        });
        dialogRef.afterClosed().subscribe(
            result => {
            }
          );
      }
}
