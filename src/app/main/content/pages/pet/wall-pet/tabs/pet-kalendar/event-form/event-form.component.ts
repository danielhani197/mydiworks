import { Component, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { locale as english } from '../../../../../profile/tabs/calendar/i18n/en';
import { locale as malay } from '../../../../../profile/tabs/calendar/i18n/my';

import { MatColors } from '@fuse/mat-colors';

import { CalendarEvent } from 'app/main/content/model/calendar/calendar-utils';
import { CalendarEventModel } from 'app/main/content/model/calendar/event-form.model';
import { User } from 'app/main/content/model/user';
import { Calendar } from 'app/main/content/model/calendar/calendar';

import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service'
import { CalendarService } from '../../../../../../services/calendar/calendar.service';
import { Event } from '../../../../../../model/calendar/event';
import { AhliPetService } from '../../../../../../services/pet/ahliPet.service';
import { DatePipe } from '@angular/common';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
    selector     : 'fuse-calendar-event-form-dialog',
    templateUrl  : './event-form.component.html',
    styleUrls    : ['./event-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FuseCalendarEventFormDialogComponent implements OnInit
{
    event: Event;
    dialogTitle: string;
    eventForm: FormGroup;
    action: string;
    presetColors = MatColors.presets;
    petId: string;
    eventFormErrors: any;
    namaKalendar: string;
    jenisKalendar: string;
    successTxt: string;
    existTxt: string;
    user: User;
    myKalendar: Calendar[];
    thisKalendar: Calendar;
    thisKalendarName: string;
    cantsave = false;
    allDay = false;
    isAdmin = false;
    constructor(
        public snackBar: MatSnackBar,
        public dialogRef: MatDialogRef<FuseCalendarEventFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private _authentication: AuthenticationService,
        private route: ActivatedRoute,
        private _calendar: CalendarService,
        private _ahliPet: AhliPetService,
        private datePipe: DatePipe,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _translate: TranslateService,
    )

    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
        this._translate.get('CALENDAR.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('CALENDAR.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
        this.user = this._authentication.getCurrentUser();
        this.event = data.event;
        this.action = data.action;
        this.petId = data.petId;

        this.eventFormErrors = {
            title : {},
            start_datetime: {},
            end_datetime: {},
            starttime: {},
            endtime: {},
            location: {},
            notes: {}
        };
    }

    onEventFormValuesChange(){

        for ( const field in this.eventFormErrors )
        {
            if ( !this.eventFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.eventFormErrors[field] = {};

            // Get the control
            const control = this.eventForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.eventFormErrors[field] = control.errors;
            }
        }
    }

    allDayfn(){
        this.allDay = this.eventForm.controls['allDay'].value
        if(this.allDay){
            this.eventForm.get('starttime').clearValidators();
            this.eventForm.get('starttime').updateValueAndValidity();
            this.eventForm.get('endtime').clearValidators();
            this.eventForm.get('endtime').updateValueAndValidity();
        }else{
            this.eventForm.get('starttime').setValidators([Validators.required]);
            this.eventForm.get('endtime').setValidators([Validators.required]);
        }
    }

    getCombinedDateTime(date, time){
        var datePart = this.datePipe.transform(date,"yyyy-MM-dd")
        var timePart = " "+time+":00"
        console.log(datePart + timePart)
        return new Date(datePart + timePart);
    }

    ngOnInit(){

        this.eventForm = this.formBuilder.group({
            title: ["", Validators.required],
            start_datetime: ["", Validators.required],
            end_datetime: ["", Validators.required],
            location: [],
            allDay: [false],
            starttime: ["", Validators.required],
            endtime: ["", Validators.required],
            notes: [],
            pic: [{value: this.user.name, disabled: true}]
        });

        this.eventForm.valueChanges.subscribe(() => {
            this.onEventFormValuesChange();
        });

        this._ahliPet.isUserAdmin(this.petId, this.user.id).subscribe(
            data=>{
                this.isAdmin = data;
                if(this.action == 'edit'){
                    this.patchFormValues();
                }
            }
        )

    }
    
    delete(){
        this._calendar.deleteEventById(this.event.id).subscribe(
            success=>{

                this._translate.get('CALENDAR.FORM.DELETE.SUCCESS').subscribe((res: string) => {
                    this.successTxt = res;
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                    });
                    this.dialogRef.close();
                });
                
                
            },
            error=>{
                this._translate.get('CALENDAR.FORM.DELETE.FAILED').subscribe((res: string) => {
                    this.existTxt = res;
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });
                });
                
            }
        )
    }

    patchFormValues(){

        this._translate.get('CALENDAR.PETEVENT').subscribe((res: string) => {
            this.jenisKalendar = res;
        });
        this.namaKalendar = this.event.pet.nama;

        this.eventForm.patchValue({
            title: this.event.title,
            start_datetime: new Date(this.event.start),
            end_datetime: new Date(this.event.end),
            kind: this.event.kind,
            allDay: this.event.allDay,
            location: this.event.location,
            notes: this.event.notes,
            pic: this.event.pic.name
        })

        if(!this.event.allDay){
            var start = this.datePipe.transform(this.event.start,"HH:mm")
            var end = this.datePipe.transform(this.event.end,"HH:mm")
            console.log(end)
            this.eventForm.patchValue({
                starttime: start,
                endtime: end
            })
        }

        if(!this.isAdmin){
            this.eventForm.disable();
        }
        this.allDayfn();
    }

    send(){
        

        if(this.action == 'edit'){

            let event : Event = new Event();
            event.setTitle(this.eventForm.controls['title'].value);
            if(this.allDay){
                event.setStart(this.getCombinedDateTime(this.eventForm.controls['start_datetime'].value, "00:00"))
                event.setEnd(this.getCombinedDateTime(this.eventForm.controls['end_datetime'].value, "00:00"))
            }else{
                event.setStart(this.getCombinedDateTime(this.eventForm.controls['start_datetime'].value, this.eventForm.controls['starttime'].value))
                event.setEnd(this.getCombinedDateTime(this.eventForm.controls['end_datetime'].value, this.eventForm.controls['endtime'].value))
            }
            event.setAllDay(this.allDay)
            event.setLocation(this.eventForm.controls['location'].value);
            event.setNote(this.eventForm.controls['notes'].value);
            event.setUser(this.user);
            event.setId(this.event.id)

            this._calendar.editEvent(event).subscribe(
                data=>{
                        this._translate.get('CALENDAR.EDIT.SUCCESS').subscribe((res: string) => {
                            this.successTxt = res;
                            this.snackBar.open(this.successTxt, "OK", {
                                panelClass: ['blue-snackbar']
                            });
                            this.dialogRef.close();
                        });
                        
                        
                    },
                    error=>{
                        this._translate.get('CALENDAR.EDIT.FAILED').subscribe((res: string) => {
                            this.existTxt = res;
                            this.snackBar.open(this.existTxt, "OK", {
                                panelClass: ['red-snackbar']
                            });
                        });
                        
                    }
            )

        }else{

            let event : Event = new Event();
            event.setTitle(this.eventForm.controls['title'].value);
            if(this.allDay){
                event.setStart(this.getCombinedDateTime(this.eventForm.controls['start_datetime'].value, "00:00"))
                event.setEnd(this.getCombinedDateTime(this.eventForm.controls['end_datetime'].value, "00:00"))
            }else{
                event.setStart(this.getCombinedDateTime(this.eventForm.controls['start_datetime'].value, this.eventForm.controls['starttime'].value))
                event.setEnd(this.getCombinedDateTime(this.eventForm.controls['end_datetime'].value, this.eventForm.controls['endtime'].value))
            }
            event.setAllDay(this.allDay)
            event.setLocation(this.eventForm.controls['location'].value);
            event.setNote(this.eventForm.controls['notes'].value);
            event.setUser(this.user);
            event.setKind("PET");
            event.setId(this.petId);
            event.setPic(this.user);
            
            this._calendar.createEvent(event).subscribe(
                data=>{
                    this._translate.get('CALENDAR.NEW.SUCCESS').subscribe((res: string) => {
                        this.successTxt = res;
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                        });
                        this.dialogRef.close();
                    });
                    
                    
                },
                error=>{
                    this._translate.get('CALENDAR.NEW.FAILED').subscribe((res: string) => {
                        this.existTxt = res;
                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['red-snackbar']
                        });
                    });
                    
                }
            )
        }
        
    }
}
