import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthenticationService } from '../../../../../services/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';

//calendar requirement
import { MatDialog, MatDialogRef } from '@angular/material';
import { Subject } from 'rxjs';
import { startOfDay, isSameDay, isSameMonth, isThursday } from 'date-fns';
import { CalendarEventAction, CalendarEventTimesChangedEvent, CalendarMonthViewDay } from 'angular-calendar';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { FuseCalendarEventFormDialogComponent } from './event-form/event-form.component';
import { CalendarEventModel } from 'app/main/content/model/calendar/event-form.model';
import { ToastrService } from 'ngx-toastr';
import { Calendar } from 'app/main/content/model/calendar/calendar';
import { CalendarEvent } from 'app/main/content/model/calendar/calendar-utils'
import { Event } from 'app/main/content/model/calendar/event';
import { User } from 'app/main/content/model/user';
import { CalendarService } from '../../../../../services/calendar/calendar.service';
import { AhliPetService } from '../../../../../services/pet/ahliPet.service';

@Component({
  selector: 'wall-pet-kalendar',
  templateUrl: './pet-kalendar.component.html',
  styleUrls: ['./pet-kalendar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations : fuseAnimations
})
export class PetKalendarComponent implements OnInit
{
    view: string;
    viewDate: Date;
    events: CalendarEvent[];
    event: CalendarEvent;

    user: User;
    petId: string;
    myKalendar: Calendar;
    myKalendarId: string;
    // rolesAdmin:any;

    public actions: CalendarEventAction[];
    activeDayIsOpen: boolean;
    refresh: Subject<any> = new Subject();
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    selectedDay: any;

    isAdmin = false;

    constructor(
        public dialog: MatDialog,
        private _authentication: AuthenticationService,
        private _toastr: ToastrService,
        private calendarService: CalendarService,
        private route: ActivatedRoute,
        private _ahliPet: AhliPetService
    )
    {
        this.user = this._authentication.getCurrentUser();
        this.view = 'month';
        this.viewDate = new Date();
        this.activeDayIsOpen = true;
        this.selectedDay = {date: startOfDay(new Date())};

    }

    ngOnInit()
    {
        this.petId = this.route.snapshot.paramMap.get('id');
        this._ahliPet.isUserAdmin(this.petId, this.user.id).subscribe(
            data=>{
                this.isAdmin = data;
            }
        )
        this.setEvents();
    }

    setEvents()
    {
        this.calendarService.getPetEvents(this.petId).subscribe(
            data=>{
                this.calendarService.update()
                this.events = data
            }
        )
    }

    /**
     * Before View Renderer
     * @param {any} header
     * @param {any} body
     */
    beforeMonthViewRender({header, body})
    {
        // console.info('beforeMonthViewRender');
        /**
         * Get the selected day
         */
        const _selectedDay = body.find((_day) => {
            return _day.date.getTime() === this.selectedDay.date.getTime();
        });

        if ( _selectedDay )
        {
            /**
             * Set selectedday style
             * @type {string}
             */
            _selectedDay.cssClass = 'mat-elevation-z3';
        }

    }

    /**
     * Day clicked
     * @param {MonthViewDay} day
     */
    dayClicked(day: CalendarMonthViewDay): void
    {
        const date: Date = day.date;
        const events: CalendarEvent[] = day.events;

        if ( isSameMonth(date, this.viewDate) )
        {
            if ( (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0 )
            {
                this.activeDayIsOpen = false;
            }
            else
            {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
        this.selectedDay = day;
        this.refresh.next();
    }

    /**
     * Event times changed
     * Event dropped or resized
     * @param {CalendarEvent} event
     * @param {Date} newStart
     * @param {Date} newEnd
     */
    eventTimesChanged({event, newStart, newEnd}: CalendarEventTimesChangedEvent): void
    {
        event.start = newStart;
        event.end = newEnd;
        // console.warn('Dropped or resized', event);
        this.refresh.next(true);
    }

    /**
     * Delete Event
     * @param event
     */
    editEvent(event: Event)
    {
        
        this.dialogRef = this.dialog.open(FuseCalendarEventFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data      : {
                event : event,
                action: 'edit',
                petId: this.petId
            }
        });

        this.dialogRef.afterClosed().subscribe(
            _=>{
                this.setEvents();
            }
        )
    }


    addEvent(): void
    {
        this.dialogRef = this.dialog.open(FuseCalendarEventFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data      : {
                action: 'new',
                petId: this.petId
            }
        });
        this.dialogRef.afterClosed().subscribe(
            _=>{
                this.setEvents();
            }
        )
    }
}

