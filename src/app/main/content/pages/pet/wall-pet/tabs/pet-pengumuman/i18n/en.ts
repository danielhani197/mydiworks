export const locale = {
    lang: 'en',
    data: {
        'ANNOUNCEMENT': {
            'TITLE':'Title',
            'ANNOUNCEMENT':'Announcement',
            'CONTENT':'Content',
            'UPDATE_BY':'Update By',
            'DATE':'Date',
            'ERROR': 'Data Fail to Update',
            'UPDATE_BUT': 'Update',
            'CANCEL':'Cancel',
            'SEARCH':'Search',
            'VIEW':'View',
            'AGREE':'I am Agree',
            'ADD': 'Tambah Pengumuman',
            'START_DATE': 'Start Date',
            'END_DATE': 'End Date',
            'ACTION': 'Action',
            'BTN' : {
                'DELETE' : 'Delete',
                'EDIT' : 'Edit',
                'SUBMIT' : 'Submit',
                'BACK'  : 'Back',
            },
            'DELETEPENGUMUMAN' : {
                'DELETEPENGUMUMAN' : 'Delete Announcement',
                'CONFIRM' : 'Are you sure to delete this ministry details?',
                'DELETE' : 'Delete',
                'BACK' : 'Back',
                'ERROR' : 'Please Delete Agency Under This Announcement First',
                'SUCCESS' : 'Announcement Details Was Successfully Deleted'
            },
            'SUCCESS': 'Announcement Details Are Successfully Submitted'
        }
    }
};
