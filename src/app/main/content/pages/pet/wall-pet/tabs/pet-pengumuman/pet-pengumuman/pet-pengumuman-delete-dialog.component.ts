import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';

import { Pengumuman } from 'app/main/content/model/pengumuman/pengumuman';
import { PengumumanPetService } from 'app/main/content/services/pet/pengumumanPet.service';

@Component({
    selector: 'pet-pengumuman-delete-dialog',
    templateUrl: 'pet-pengumuman-delete-dialog.component.html',
    animations : fuseAnimations
    })
    export class PetPengumumanDeleteDialogComponent implements OnInit
    {
        id: string;

        successTxt: string;
        existTxt: string;

        constructor(
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private pengumumanPetService: PengumumanPetService,
        private _toastr: ToastrService,
        private _router: Router,
        public _dialogRef: MatDialogRef<PetPengumumanDeleteDialogComponent>,
        public snackBar: MatSnackBar,
        @Inject(MAT_DIALOG_DATA) public data: any
        ) { 
            this._translate.get('ANNOUNCEMENT.DELETEPENGUMUMAN.ERROR').subscribe((res: string) => {
                this.existTxt = res;
            });
            this._translate.get('ANNOUNCEMENT.DELETEPENGUMUMAN.SUCCESS').subscribe((res: string) => {
                this.successTxt = res;
            });
            this.fuseTranslationLoader.loadTranslations(malay, english);
        }
        

        ngOnInit(){
            this.id = this.data.id;
            console.log(this.data.id)
        }
        confirmDelete(): void {
            
            this.pengumumanPetService.deletePengumuman(this.id).subscribe(
                success=>{
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                      });
                      this._dialogRef.close();
                },
                error=>{
                    
            //         this.snackBar.open(this.existTxt, "OK", {
            //             panelClass: ['red-snackbar']
            //           });
            //           this._dialogRef.close();
                }
            )
            
        }


        // redirectPengumumanPage() {
        // this._router.navigate(['/pengumuman/list']);
        // }

    }
