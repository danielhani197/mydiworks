import { Component, OnInit, ViewChild} from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { MyWallService } from 'app/main/content/services/wall3/myWall.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { AuthenticationService } from '../../../../../services/authentication/authentication.service';
import { User } from '../../../../../model/user';

import { FileStorageService } from 'app/main/content/services/wall3/filestorage.service'

import { DataService } from 'app/main/content/services/wall3/data.service';

import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';
import { ActivatedRoute } from '@angular/router';
import { Pet } from '../../../../../model/pet/pet';
import { PetService } from '../../../../../services/pet/pet.service';
import { WallPetService } from '../../../../../services/pet/wall-pet.service';
import { Page } from '../../../../../model/util/page';
import { KementerianService } from '../../../../../services/agency/kementerian.service';
import { PetAttachment } from '../../../../../model/wall3/petAttachment';
import { Notifikasi } from '../../../../../model/notifikasi';
import { NotifikasiService } from '../../../../../services/setting/notifikasi.service';

@Component({
    selector   : 'wall-pet-gallery',
    templateUrl: './gallery.component.html',
    styleUrls  : ['./gallery.component.scss'],
    animations : fuseAnimations
})
export class GalleryComponent
{

    @ViewChild('myTable') table: any;
    
    _id: string;
    user: User;

    versionForm: FormGroup;
    successTxt: string;
    existTxt: string;
    updateTxt: string;
    updateErrorTxt: string;
    errorTxt: string;
    finalizeTxt: string;
    finalizeErrorTxt: string;

    rows: any[];
    rownew: any[]
    
    temp = [];
    page = new Page();
    pagenew = new Page();

    _idToDelete : string;
    loadingIndicator = true;
    reorderable = true;

    file2: any;
    status: any;
    data: PetAttachment;
    lock: any;
    
    expanded: any = {};
    timeout: any;

    limitTxt: string;

    constructor(
        private _auth: AuthenticationService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _pet: PetService,
        private _petWall: WallPetService,
        private _wall: MyWallService,
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        public snackBar: MatSnackBar,
        public _dialog: MatDialog,
        private _filestorage: FileStorageService,
        private _dataservice: DataService,
        private _notifi:NotifikasiService,
    )
    {

        this.page.pageNumber = 0;
        this.page.size = 10;
        this.page.sortField = "filename";
        this.page.sort = "asc";

        this.pagenew.pageNumber = 0;
        this.pagenew.size = 1000;
        this.pagenew.sortField = "version";
        this.pagenew.sort = "desc";

        this.fuseTranslationLoader.loadTranslations(malay, english);
        this._translate.get('WALL.UNLOCKED').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('WALL.LOCKED').subscribe((res: string) => {
            this.successTxt = res;
        });
        this._translate.get('WALL.FILEUPDATE').subscribe((res: string) => {
            this.updateTxt = res;
        });
        this._translate.get('WALL.FILEUPDATEERROR').subscribe((res: string) => {
            this.updateErrorTxt = res;
        });
        this._translate.get('WALL.ERROR').subscribe((res: string) => {
            this.errorTxt = res;
        });
        this._translate.get('WALL.FINALIZE').subscribe((res: string) => {
            this.finalizeTxt = res;
        });
        this._translate.get('WALL.FINALIZEERROR').subscribe((res: string) => {
            this.finalizeErrorTxt = res;
        });

        this.user = this._auth.getCurrentUser();
        this._id = this.route.snapshot.paramMap.get('id');
        
        
    }

    ngOnInit(): void {
        this.route.params.subscribe(
            params => {
                const id = +params['id'];
                this.getData(id);
            }
        );
    }

    getData(id)
    {
        this.page.pageNumber = 0;
        this.page.size = 10;
        this.page.sortField = "filename";
        this.page.sort = "asc";

        this.pagenew.pageNumber = 0;
        this.pagenew.size = 1000;
        this.pagenew.sortField = "version";
        this.pagenew.sort = "desc";
        this._id = this.route.snapshot.paramMap.get('id');
        this.versionForm = this.formBuilder.group({
            fileUpload          : [''],
        });
        this.setPage({ offset: 0 });
        
    }

    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._petWall.getGallery(this.page, this._id).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._petWall.getGallery(this.page, this._id).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;
    
        this._petWall.getGallery(this.page, this._id).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    base64ToArrayBuffer(base64) {
        var binaryString = window.atob(base64);
        var binaryLen = binaryString.length;
        var bytes = new Uint8Array(binaryLen);
        for (var i = 0; i < binaryLen; i++) {
           var ascii = binaryString.charCodeAt(i);
           bytes[i] = ascii;
        }
        return bytes;
    }

    download(id: string){
        this._filestorage.getFilesById(id).subscribe(

            data => {
                this.file2 = data; 
                let a = document.createElement("a");
                document.body.appendChild(a);
                //a.style = "display: none"; 

                var dataFile = this.base64ToArrayBuffer(this.file2.content);
                var blob = new Blob([dataFile], {type:data.formatType});
                var url= window.URL.createObjectURL(blob);
                a.href = url;
                a.download = this.file2.name;
                a.click();
                window.URL.revokeObjectURL(url);
            }
        );
    }


    unlockFile(id){
        this._filestorage.getPetFilesById(id).subscribe(
            data => {
                this.data = data;
                this.status = data.status;
                this.lock = data.lockedby.id;

             if (this.status == 2 && (this.user.id === this.lock || this.user.id === "SECURITY-USER-SAM001")) {
                
                let petAttachment: PetAttachment = new PetAttachment();
                petAttachment.setId(this.data.id);
                petAttachment.setStatus("1");
                petAttachment.setLockedby(this.user);
    
                this._filestorage.updateStatus(petAttachment).subscribe(
                    success=>{
                        this._petWall.getGallery(this.page, this._id).subscribe(
                            pagedData => {
                                    this.page = pagedData.page;
                                    this.rows = pagedData.data;
                                    this.loadingIndicator = false;
                        });
                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['blue-snackbar']
                        });
                    },
                    error=>{
                        this._petWall.getGallery(this.page, this._id).subscribe(
                            pagedData => {
                                    this.page = pagedData.page;
                                    this.rows = pagedData.data;
                                    this.loadingIndicator = false;
                        });
                        this.snackBar.open(this.errorTxt, "OK", {
                            panelClass: ['red-snackbar']
                        });
                    }
                );
            }       
        })   
    }


    edit(instanceid: string, id: string){
        this._filestorage.getPetFilesById(id).subscribe(
            data => {
                this.data = data;
                this.status = data.status;

            if (this.status == 1){
                let petAttachment: PetAttachment = new PetAttachment();
                petAttachment.setId(this.data.id);
                petAttachment.setStatus("2");
                petAttachment.setLockedby(this.user);

                this._filestorage.getFilesById(instanceid).subscribe(
    
                    data => {
                        this.file2 = data;  
        
                        let a = document.createElement("a");
                        document.body.appendChild(a);
                        //a.style = "display: none"; 
        
                        var dataFile = this.base64ToArrayBuffer(this.file2.content);
                        var blob = new Blob([dataFile], {type:data.formatType});
                        var url= window.URL.createObjectURL(blob);
                        a.href = url;
                        a.download = this.file2.name;
                        a.click();
                        window.URL.revokeObjectURL(url);

                this._filestorage.updateStatus(petAttachment).subscribe(

                    success=>{
                        this._petWall.getGallery(this.page, this._id).subscribe(
                            pagedData => {
                                    this.page = pagedData.page;
                                    this.rows = pagedData.data;
                                    this.loadingIndicator = false;
                        });
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                        });
                    },
                    error=>{
                        this._petWall.getGallery(this.page, this._id).subscribe(
                            pagedData => {
                                    this.page = pagedData.page;
                                    this.rows = pagedData.data;
                                    this.loadingIndicator = false;
                        });
                        this.snackBar.open(this.errorTxt, "OK", {
                            panelClass: ['red-snackbar']
                        });
                    }
                )  
            })
        }
    });        
}

    delete(id: string){
        console.log("delete >>>>>>>>>", id);
    }

    finalized(id: string){
        this._filestorage.getPetFilesById(id).subscribe(
            data => {
                this.data = data;

             if (this.user.id === "SECURITY-USER-SAM001") {
                
                let petAttachment: PetAttachment = new PetAttachment();
                petAttachment.setId(this.data.id);
                petAttachment.setStatus("3");
    
                this._filestorage.updateStatus(petAttachment).subscribe(
                    success=>{
                        this._petWall.getGallery(this.page, this._id).subscribe(
                            pagedData => {
                                    this.page = pagedData.page;
                                    this.rows = pagedData.data;
                                    this.loadingIndicator = false;
                        });
                        this.snackBar.open(this.finalizeTxt, "OK", {
                            panelClass: ['blue-snackbar']
                        });
                    },
                    error=>{
                        this._petWall.getGallery(this.page, this._id).subscribe(
                            pagedData => {
                                    this.page = pagedData.page;
                                    this.rows = pagedData.data;
                                    this.loadingIndicator = false;
                        });
                        this.snackBar.open(this.finalizeErrorTxt, "OK", {
                            panelClass: ['red-snackbar']
                        });
                    }
                );
            }       
        })   
    }

    fileUploadChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

        var pattern = "application/x-msi";
        var pattern2 = "application/x-ms-dos-executable";
        var pattern3 = "application/vnd.debian.binary-package";
        
        var reader = new FileReader();

        if (file.type == pattern || file.type == pattern2 || file.type == pattern3) {
          this.snackBar.open(this.existTxt, "OK", {
            panelClass: ['red-snackbar']
          });
            return;       

        }else{
            //this.fileName = file.name;
            reader.readAsDataURL(file);
            this.versionForm.get('fileUpload').setValue(file);

            //save direct to db
            let petAttachment: PetAttachment = new PetAttachment();
            petAttachment.setModifiedby(this.user);
            petAttachment.setUserid(this.user.id);

            let input = new FormData();
            input.append('fileUpload', this.versionForm.get('fileUpload').value);       
            input.append('info', new Blob([JSON.stringify(petAttachment)],
            {
                type: "application/json"
            }));
            
            const formModel = input;
            this._filestorage.saveNewVersion(formModel, this._id).subscribe(

                success=>{
                    this._petWall.getGallery(this.page, this._id).subscribe(
                        pagedData => {
                                this.page = pagedData.page;
                                this.rows = pagedData.data;
                                this.loadingIndicator = false;
                    });
                    this.snackBar.open(this.updateTxt, "OK", {
                        panelClass: ['blue-snackbar']
                    });
                },
                error=>{
                    this._petWall.getGallery(this.page, this._id).subscribe(
                        pagedData => {
                                this.page = pagedData.page;
                                this.rows = pagedData.data;
                                this.loadingIndicator = false;
                    });
                    this.snackBar.open(this.updateErrorTxt, "OK", {
                        panelClass: ['red-snackbar']
                    });
                }
            )
            this._pet.getPetById(this._id).subscribe(
                data=>{
                            let noti: Notifikasi = new Notifikasi()
                            noti.setCreatedby(this.user),
                            noti.setUser(this.user),
                            noti.setMaklumat("Upload file in PET Galleri"),
                            noti.setStatus("New"),
                            noti.setTindakan("/pet/woc/"+data.id),
                            noti.setModifiedby(this.user)
                            console.log(noti)
                            
                            this._notifi.createNoti(noti).subscribe(
                                success=>{ 
                                    console.log(success)
                            },
                                error=>{
                                    console.log(error)    
                                }
                            )
                }
            )
        }
    }    

    toggleExpandRow(row) {
        
        this.table.rowDetail.collapseAllRows();
        this.table.rowDetail.toggleExpandRow(row);

        this.pagenew.search = row.filename;
        this._petWall.getGalleryDrop(this.pagenew, this._id).subscribe(
            pagedData => {
                    this.pagenew = pagedData.page;
                    this.rownew = pagedData.data;
        });
    } 
 

    onCollapseRow() {
        
        this.table.rowDetail.collapseAllRows();
    } 
}
