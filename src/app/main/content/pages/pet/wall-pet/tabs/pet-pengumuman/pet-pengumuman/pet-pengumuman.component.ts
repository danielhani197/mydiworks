import { Component, OnInit, ViewChild } from '@angular/core';
import { formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute, Router } from "@angular/router";
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { ToastrService } from 'ngx-toastr';
import { Page } from 'app/main/content/model/util/page';
import { PengumumanPetService } from 'app/main/content/services/pet/pengumumanPet.service';
import { Pengumuman } from 'app/main/content/model/pengumuman/pengumuman';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { PetPengumumanDeleteDialogComponent } from './pet-pengumuman-delete-dialog.component';
import { PetService } from 'app/main/content/services/pet/pet.service';
import { Pet } from 'app/main/content/model/pet/pet';


@Component({
  selector: 'app-pet-pengumuman',
  templateUrl: './pet-pengumuman.component.html',
  styleUrls: ['./pet-pengumuman.component.scss'],
  animations: fuseAnimations
})
export class PetPengumumanComponent implements OnInit {

  rows: any[];
  pengumumanPetForm: FormGroup;
  successTxt: string;
  existTxt: string;

  page = new Page();

  loadingIndicator = true;
  reorderable = true;

  mode: string = "list";
  id: string;
  _idToDelete: string;
  petId: string;
  currentPet: Pet;

  editorConfig = 
    {
        "editable": true,
        "spellcheck": true,
        "height": "auto",
        "minHeight": "0",
        "width": "auto",
        "minWidth": "0",
        "translate": "yes",
        "enableToolbar": true,
        "showToolbar": true,
        "placeholder": "Enter text here...",
        "imageEndPoint": "",
        "toolbar": [
            ["bold", "italic", "underline", "strikeThrough", "superscript", "subscript"],
            ["fontName", "fontSize", "color"],
            ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
            ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
            ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
            // ["link", "unlink", "image", "video"]
        ]
    };

  @ViewChild(PetPengumumanComponent) table: PetPengumumanComponent;

  constructor(
    // private http: HttpClient, 
    private fuseTranslationLoader: FuseTranslationLoaderService,
    private route: ActivatedRoute,
    private _toastr: ToastrService,
    private _router: Router,
    private pengumumanPetService: PengumumanPetService,
    private formBuilder: FormBuilder,
    public snackBar: MatSnackBar,
    public _dialog: MatDialog,
    private petService: PetService,
  ) {
    var pet = this.currentPet;
    if(pet){
      var petId = pet.id;
      this.petId = petId;
    }

    this.page.pageNumber = 0;
    this.page.size = 10;
    this.page.sortField = "title";
    this.page.sort = "asc";
    this.page.petId = this.petId;
    this.fuseTranslationLoader.loadTranslations(malay, english);
    this.petId = this.route.snapshot.paramMap.get('id');
  }



  ngOnInit() {

    this.setPage({ offset: 0 });
    this.pengumumanPetForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      startdatetime: ['', [Validators.required]],
      enddatetime: ['', [Validators.required]],
      teksPengumuman: ['', [Validators.required]],
      idPengumuman: [''],
      petId: ['']
    })

  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    this.page.search = val;

    this.pengumumanPetService.getPengumumanList(this.page, this.petId).subscribe(
      pagedData => {
        this.page = pagedData.page;
        this.rows = pagedData.data;
        this.loadingIndicator = false;
        console.log("------------->", this.rows);

      });
  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.pengumumanPetService.getPengumumanList(this.page, this.petId).subscribe(
      pagedData => {
        this.page = pagedData.page;
        this.rows = pagedData.data;
        this.loadingIndicator = false;
        console.log("------------->", this.rows);

      });


    //load
    // this.pengumumanPetService.getPengumumanByID(this.id).subscribe(
    //   pengumuman => {
    //     this.pet= pengumuman.petId
    //     this.petService.getPetById(this.petId).subscribe(

    //     )
    //   }
    // )
  }

  updatePage(id: string) {
    console.log(id);

    // let pengumuman: Pengumuman = new Pengumuman ();
    // pengumuman.setTitle(this.pengumumanPetForm.controls['name'].value),
    // pengumuman.setContent(this.pengumumanPetForm.controls['code'].value),
    // pengumuman.setStart_date(this.start_datetime),
    // pengumuman.setEnd_date(this.end_datetime),
    // pengumuman.setId(this._id)

    // private pengumumanPetService: PengumumanPetService,
    //         this.pengumumanPetService.updateBahagian(pengumuman).subscribe(
    //             success=>{
    //                 this.snackBar.open(this.successTxt, "OK", {
    //                     panelClass: ['blue-snackbar']
    //                   });
    //                   this._router.navigate(['/pengumumanPet/list']);
    //             },
    //             error=>{

    //                 this.snackBar.open(this.existTxt, "OK", {
    //                     panelClass: ['red-snackbar']
    //                   });
    //                   this._router.navigate(['/pengumumanPet/list']);
    //             }
    //         )
    this.mode = "edit";



  }

  back() {
    this.mode = "list";
  }

  submit() {
    
    var idPengumuman = this.pengumumanPetForm.controls['idPengumuman'].value;

    if(idPengumuman !=""){
      let pengumuman: Pengumuman = new Pengumuman();
      pengumuman.setTitle(this.pengumumanPetForm.controls['title'].value);
      pengumuman.setStart_date(this.pengumumanPetForm.controls['startdatetime'].value);
      pengumuman.setEnd_date(this.pengumumanPetForm.controls['enddatetime'].value);
      pengumuman.setContent(this.pengumumanPetForm.controls['teksPengumuman'].value);
      pengumuman.setId(idPengumuman);
      console.log(pengumuman);
      console.log(idPengumuman);
      console.log("testingedit");

      this.pengumumanPetService.editPengumuman(pengumuman).subscribe(
        success => {
          this.snackBar.open(this.successTxt, "Pengumuman telah dikemaskini", {
            panelClass: ['blue-snackbar']
          });
          this.setPage({ offset: 0 });
        },
        error => {

          this.snackBar.open(this.existTxt, "error", {
            panelClass: ['red-snackbar']
          });
        }
      )
      this.pengumumanPetForm.reset();
      this.mode = "list";   

    }else{
      
      let pengumuman: Pengumuman = new Pengumuman();
      pengumuman.setTitle(this.pengumumanPetForm.controls['title'].value);
      pengumuman.setStart_date(this.pengumumanPetForm.controls['startdatetime'].value);
      pengumuman.setEnd_date(this.pengumumanPetForm.controls['enddatetime'].value);
      pengumuman.setContent(this.pengumumanPetForm.controls['teksPengumuman'].value);

      let pet : Pet = new Pet();
      pet.setId(this.petId)

      pengumuman.setPet(pet);
      console.log(pengumuman);
      console.log("testing");

      this.pengumumanPetService.createPengumuman(pengumuman).subscribe(
        success => {
          this.snackBar.open(this.successTxt, "Pengumuman telah ditambah", {
            panelClass: ['blue-snackbar']
          });
          this.setPage({ offset: 0 });
        },
        error => {

          this.snackBar.open(this.existTxt, "error", {
            panelClass: ['red-snackbar']
          });
        }
      )
      this.pengumumanPetForm.reset();

      this.mode = "list";
    }
  }

  updatePengumuman(id: string) {

    

    if (id) {
      // this.petId = this.route.snapshot.paramMap.get('id');
      // var pet = this.petService.getPetById;
      // if(pet){
      //   var petpet = .id;
      //   // this.petId = petId;
      // }
      // console.log(petpet);
      console.log("testingView");
      this.pengumumanPetService.getPengumumanByID(id).subscribe(
        objPengumuman => {


          var pengumuman = pengumuman
          let startDate = new Date(objPengumuman.startDatetime)
          let endDate = new Date(objPengumuman.endDatetime)
          this.pengumumanPetForm.setValue({
            title: objPengumuman.title,
            startdatetime: startDate,
            enddatetime: endDate,
            teksPengumuman: objPengumuman.content,
            idPengumuman: objPengumuman.id, //just added
            petId: this.petId, //just added
          });
          console.log(objPengumuman);
        }
      )
      this.mode = "view";
    }
  }

  deletePengumuman(id: string) {


    this._idToDelete = id;
    let dialogRef = this._dialog.open(PetPengumumanDeleteDialogComponent, {
      width: '500px',
      data: { id: this._idToDelete }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.setPage({ offset: 0 });
    });
  }

}

