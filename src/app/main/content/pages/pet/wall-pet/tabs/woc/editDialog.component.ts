import { Component, OnInit, Inject } from '@angular/core';
import { Router } from "@angular/router";
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';
import { User } from '../../../../../model/user';
import { Hashtag } from '../../../../../model/wall3/hashtag';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WallKomen } from '../../../../../model/wall3/wallKomen';
import { WallKandungan } from '../../../../../model/wall3/wallKandungan';
import { WallPetService } from '../../../../../services/pet/wall-pet.service';
import { AuthenticationService } from '../../../../../services/authentication/authentication.service';
import { MyWallService } from '../../../../../services/wall3/myWall.service';

@Component({
    selector: 'edit-dialog',
    templateUrl: 'editDialog.component.html',
    animations : fuseAnimations
    })
    export class EditDialogComponent implements OnInit
    {
        _id: string;
        _type: string;
        _user: User;

        successTxt: string;
        existTxt: string;

        hashtagList: Hashtag[] = new Array<Hashtag>();
        tagList: string[] = new Array<string>();

        editForm: FormGroup;
        editFormErrors: any;

        komen: WallKomen;
        post: WallKandungan;

        constructor(
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _wall: MyWallService,
        private _wallPet: WallPetService,
        private _auth: AuthenticationService,
        private formBuilder: FormBuilder,
        public snackBar: MatSnackBar,
        public _dialogRef: MatDialogRef<EditDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
        ) { 

            this.fuseTranslationLoader.loadTranslations(malay, english);

            if(data.type=='komen'){
                this._translate.get('WALL.EDIT.KOMEN.ERROR').subscribe((res: string) => {
                    this.existTxt = res;
                });
                this._translate.get('WALL.EDIT.KOMEN.SUCCESS').subscribe((res: string) => {
                    this.successTxt = res;
                });
            }else{
                this._translate.get('WALL.EDIT.POST.ERROR').subscribe((res: string) => {
                    this.existTxt = res;
                });
                this._translate.get('WALL.EDIT.POST.SUCCESS').subscribe((res: string) => {
                    this.successTxt = res;
                });
            }

            this.editFormErrors = {
                editArea : {}
            };
            this._id = this.data.id;
            this._type = this.data.type;
            this._user = this._auth.getCurrentUser();
            
        }
        

        ngOnInit(){
            this.editForm = this.formBuilder.group({
                editArea: ['', [Validators.required]]
            })

            if( this._type == "komen"){
                this._wall.getKomenById(this._id).subscribe(
                    data=>{
                        this.komen = data;
                        this.editForm.setValue({
                            editArea: this.komen.keterangan
                        })
                    }
                )
            }else{
                this._wall.getPostById(this._id).subscribe(
                    data=>{
                        this.post = data;
                        let hashLs: Hashtag[] = this.post.hashtagLs;
                        for(let obj of hashLs){
                            this.tagList.push(obj.nama)
                        }

                        this.editForm.setValue({
                            editArea: this.post.keterangan
                        })
                    }
                )
            }
            
        }

        addTag(event: any){
            const input = event.input;
            const value = event.value;
    
            // Add tag
            if ( value )
            {
                this.tagList.push(value);
            }
    
            // Reset the input value
            if ( input )
            {
                input.value = '';
            }
        }
    
        removeTag(tag){

            const index = this.tagList.indexOf(tag);
            if ( index >= 0 )
            {
                this.tagList.splice(index, 1);
            }
        }

        confirmEdit(): void {
            if(this._id){
                if(this._type == "komen"){
                    let wallKomen: WallKomen = new WallKomen();
                    wallKomen.setId(this.komen.id);
                    wallKomen.setKeterangan(this.editForm.controls['editArea'].value);
                    wallKomen.setModifiedby(this._user);
                    wallKomen.setModifieddate(new Date);
                    
                    this._wall.updateKomen(wallKomen).subscribe(
                        success=>{
                            this.snackBar.open(this.successTxt, "OK", {
                                panelClass: ['blue-snackbar']
                            });
                            
                            this._dialogRef.close();
                            
                        },
                        error=>{
                            
                            this.snackBar.open(this.existTxt, "OK", {
                                panelClass: ['red-snackbar']
                            });
                            this._dialogRef.close();
                        }
                    )
                }else{
                    
                    for(let obj of this.tagList){
                        let hashtag: Hashtag = new Hashtag();
                        hashtag.setName(obj);
                        this.hashtagList.push(hashtag);
                    }
                    let wallKandungan: WallKandungan = new WallKandungan();
                    wallKandungan.setId(this.post.id)
                    wallKandungan.setKeterangan(this.editForm.controls['editArea'].value);
                    wallKandungan.setHashtagLs(this.hashtagList);
                    wallKandungan.setModifiedby(this._user);
                    wallKandungan.setModifieddate(new Date);
                    this._wall.updatePost(wallKandungan).subscribe(
                        success=>{
                            this.snackBar.open(this.successTxt, "OK", {
                                panelClass: ['blue-snackbar']
                            });
                            
                            this._dialogRef.close();
                            
                        },
                        error=>{
                            
                            this.snackBar.open(this.existTxt, "OK", {
                                panelClass: ['red-snackbar']
                            });
                            this._dialogRef.close();
                        }
                    )
                }
            }
            
        }

    }