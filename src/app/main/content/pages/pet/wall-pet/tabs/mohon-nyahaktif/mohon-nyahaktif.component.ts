import { Component } from '@angular/core';
import { Pet } from '../../../../../model/pet/pet';
import { PenggunaService } from '../../../../../services/pengguna/pengguna.service';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthenticationService } from '../../../../../services/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PetService } from '../../../../../services/pet/pet.service';
import { AhliPetService } from '../../../../../services/pet/ahliPet.service';
import { Agensi } from '../../../../../model/general/agensi';
import { Kementerian } from '../../../../../model/general/kementerian';
import { Bahagian } from '../../../../../model/general/bahagian';
import { KementerianService } from '../../../../../services/agency/kementerian.service';
import { BahagianService } from '../../../../../services/agency/bahagian.service';
import { AgensiService } from '../../../../../services/agency/agensi.service';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { User } from '../../../../../model/user';


@Component({
    selector   : 'fuse-mohon-nyahaktif',
    templateUrl: './mohon-nyahaktif.component.html',
    styleUrls  : ['./mohon-nyahaktif.component.scss'],
    animations : fuseAnimations
})
export class FuseMohonNyahAktifComponent
{
    _id: string;
    pet: Pet;
    nama: any;
    namaSingkatan:any;
    keterangnan: any;
    tujuan: any;
    tarikhWujud: any;
    tarikhDeactive: any;
    status:any;
    agensi: any;
    catatan: any;
    kementerian: any;
    bahagian: any;

    ahliPetLs: any;
    kementerianNama:any;

    userId:any;
    peranan: any;
    currentUser: any;

    user: User;
    petObj: Pet;
    i: number;
    countLength: number;
    rolesAdmin:any;
    ahliPet: any[];

    constructor(    private petService: PetService,
                    private _ahliPet: AhliPetService,
                    private formBuilder: FormBuilder,
                    private kementerianService: KementerianService,
                    private bahagianService: BahagianService,
                    private agensiService: AgensiService,
                    private authenticationService: AuthenticationService,
                    private route: ActivatedRoute,
                    private router: Router,
                    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer
        )
    {
        iconRegistry.addSvgIcon(
            'thumbs-up',
            sanitizer.bypassSecurityTrustResourceUrl('assets/img/examples/thumbup-icon.svg'));
    } 
    
    ngOnInit(): void{
        

        this._id = this.route.snapshot.paramMap.get('id');
        this.petService.getPetById(this._id).subscribe(
            data=>{
                this.pet = data;
                this.nama = data.nama;
                this.namaSingkatan = data.namaSingkatan;
                this.keterangnan = data.keterangan;
                this.tujuan = data.tujuan;
                this._ahliPet.getByPetId(data.id).subscribe(
                    ahliPet =>{
                            this.ahliPet = ahliPet;
                            this.user = this.authenticationService.getCurrentUser();
                            this.userId = this.user.id;
                            this.countLength = this.ahliPet.length;
            
                            for( this.i=0 ; this.i< this.countLength;this.i++){
                                if(this.ahliPet[this.i].user.id == this.userId && this.ahliPet[this.i].tempPet.id == this._id){
                                    this.rolesAdmin = this.ahliPet[this.i].peranan;
                                }
                            }
                        }
                    )
                if(data.agensi){
                    this.agensi = data.agensi.name;
                }
                if(data.kementerian){
                    this.kementerian = data.kementerian.name;  
                }
                if(data.bahagian){
                    this.bahagian = data.bahagian.name; 
                }
                
                this.catatan = data.catatan;
            }            
        )
    }
    editForm(){
        this._id = this.route.snapshot.paramMap.get('id');
        this.petService.getPetById(this._id).subscribe(
            data=>{
                
                this.router.navigate(['profilePet/edit/'+data.id]);
                console.log(data.id)
            })

        
    }
    getData(id){
        
        this.petService.getPetById(this._id).subscribe(
            data=>{
                
            }
        )
    }
}
