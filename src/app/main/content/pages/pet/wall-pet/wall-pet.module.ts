import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CdkTableModule } from '@angular/cdk/table';

import { MatButtonModule, MatTableModule, MatDividerModule, MatIconModule, MatTabsModule, MatFormFieldModule, MatSlideToggleModule, MatToolbarModule, MatChipsModule, MatSnackBarModule, MatMenuModule, MatDialogModule, MatCheckboxModule, MatOptionModule, MatSelectModule, MatDatepickerModule, MatInputModule} from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { WallPetComponent } from './wall-pet.component';
import { WocComponent } from './tabs/woc/woc.component';
import { EditDialogComponent } from './tabs/woc/editDialog.component';
import { DeleteDialogComponent } from './tabs/woc/deleteDialog.component';
import { ScrollEventModule } from 'ngx-scroll-event';
import { DocumenDialogComponent } from './tabs/woc/documenDialog.component';
import { DataService } from 'app/main/content/services/wall3/data.service';
import { GalleryComponent } from './tabs/gallery/gallery.component';
import { FuseAboutPetComponent } from './tabs/about/about.component';
import {FuseMohonNyahAktifComponent} from './tabs/mohon-nyahaktif/mohon-nyahaktif.component';
import { SenaraiAhliPetComponent } from './tabs/senarai-ahliPet/senarai-ahliPet.component';
import { SenaraiAhliPetAdminComponent } from './tabs/senarai-ahliPet-admin/senarai-ahliPet.component';
import { PetPengumumanComponent } from './tabs/pet-pengumuman/pet-pengumuman/pet-pengumuman.component';
import { NgxEditorModule } from 'ngx-editor';
import { MyBoxUploadComponent } from './tabs/woc/myBoxUpload.component';

//calendar requirement
import { CalendarModule } from 'angular-calendar';
import { ColorPickerModule } from 'ngx-color-picker';
import { FuseConfirmDialogModule } from '@fuse/components';
import { PetKalendarComponent } from './tabs/pet-kalendar/pet-kalendar.component';
import { FuseCalendarEventFormDialogComponent } from './tabs/pet-kalendar/event-form/event-form.component';
import { PetPengumumanDeleteDialogComponent } from './tabs/pet-pengumuman/pet-pengumuman/pet-pengumuman-delete-dialog.component';
import { DeactiveDialogComponent} from './tabs/about/deactive-dialog.component';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
const routes = [
    {
        path     : 'woc/:id', 
        component: WallPetComponent,
    },
    {
        path     : 'pengumuman/pet/list/:id', 
        component: PetPengumumanComponent,
    }
];

@NgModule({
    declarations: [
        WallPetComponent,
        WocComponent,
        GalleryComponent,
        EditDialogComponent,
        DeleteDialogComponent,
        DocumenDialogComponent,
        SenaraiAhliPetComponent,
        SenaraiAhliPetAdminComponent,
        FuseAboutPetComponent,
        PetKalendarComponent,
        FuseCalendarEventFormDialogComponent,
        PetPengumumanComponent,
        FuseMohonNyahAktifComponent,
        MyBoxUploadComponent,
        PetPengumumanDeleteDialogComponent,
        DeactiveDialogComponent
        ],
    entryComponents: [
        EditDialogComponent,
        DeleteDialogComponent,
        DocumenDialogComponent,
        FuseCalendarEventFormDialogComponent,
        PetPengumumanComponent,
        MyBoxUploadComponent,
        PetPengumumanDeleteDialogComponent,
        DeactiveDialogComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        ScrollEventModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatTabsModule,
        MatFormFieldModule,
        MatChipsModule,
        MatSnackBarModule,
        MatMenuModule,
        MatDialogModule,
        MatCheckboxModule,
        MatOptionModule,
        MatSelectModule,
        MatDatepickerModule,
        MatInputModule,
        NgxDatatableModule,
        NgxEditorModule,
        MatAutocompleteModule,
        MatTableModule,
        CdkTableModule,
        SweetAlert2Module,
        MatSlideToggleModule,
        MatToolbarModule,
        
        CalendarModule.forRoot(),
        ColorPickerModule,
        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers   : [
        DataService
    ]
})
export class WallPetModule
{
}