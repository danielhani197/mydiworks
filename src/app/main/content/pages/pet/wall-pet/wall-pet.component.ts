import { Component, ViewEncapsulation, OnInit, ViewChild, OnDestroy, EventEmitter, Output } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { PetService } from '../../../services/pet/pet.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Pet } from '../../../model/pet/pet';
import { ScrollEvent } from 'ngx-scroll-event';
import { WocComponent } from './tabs/woc/woc.component';
import { User } from '../../../model/user';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { AhliPetService } from '../../../services/pet/ahliPet.service';
import { AhliPet } from '../../../model/pet/ahliPet';
import { ChatPanelService } from '../../../../chat-panel/chat-panel.service';
import { ChatPanelComponent } from '../../../../chat-panel/chat-panel.component';
import { PetAuthorities } from '../../../model/pet/petAuthorities';


@Component({
    selector     : 'wall-pet',
    templateUrl  : './wall-pet.component.html',
    styleUrls    : ['./wall-pet.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class WallPetComponent implements OnInit
{
    //@Output() onFilter: EventEmitter = new EventEmitter();
    @ViewChild(WocComponent) timeLine: WocComponent;
    _id: string;
    
    name:any;
    imageSrc:any;
    userId:any;
    currentUser: any;

    user: User;
    user2: User;
    petObj: Pet;
    //ahliPet: AhliPet;
    i: number;
    countLength: number;
    rolesAdmin:any;
    ahliPet: AhliPet[];
    petAuth: PetAuthorities;
    ahliPetUser:User;

    handleScroll(event: ScrollEvent) {

        if (event.isReachingBottom) {
            this.timeLine.getNext();
        }
    
    }

    constructor(
        private _pet: PetService,
        private _router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private _ahliPet: AhliPetService,
        private _chat: ChatPanelService,
    )
    {
        
    }

    ngOnDestroy(){
        this._chat.setGroups("bahagian");
        this._chat.filter("bahagian");
    }

    ngOnInit(): void {
        this.route.params.subscribe(
            params => {
                const id = +params['id'];
                this.getData(id);
            }
        );
    }
    
    getData(id){
        this._id = this.route.snapshot.paramMap.get('id');
        
        this._pet.getPetById(this._id).subscribe(
            data=>{
                this._chat.setGroups(this._id);
                this._chat.filter(this._id)
                this._ahliPet.getByPetId(data.id).subscribe(
                    ahliPet =>{
                        this.ahliPet = ahliPet;
                        this.user = this.authenticationService.getCurrentUser();
                        this.userId = this.user.id;
                        this.countLength = this.ahliPet.length;
                        // this._ahliPet.getByUserId(this.userId).subscribe(
                        //     dataUser =>{
                        //         for(let a = 0; a< dataUser.length; a++){
                        //             if(dataUser[a].pet){
                        //                 if(dataUser[a].pet.id == this._id && dataUser[a].user != null){
                        //                     this.rolesAdmin = dataUser[a].petAuthorities.id;
                        //                 }
                        //             }
                                    
                        //         }
                        //     }
                        // )
                        for( this.i=0 ; this.i< this.countLength;this.i++){
                            //this.ahliPetUser = this.ahliPet[this.i].user;
                            if(this.ahliPet[this.i].user){
                                if(this.ahliPet[this.i].user.id == this.userId && this.ahliPet[this.i].tempPet.id == this._id){
                                    this.rolesAdmin = this.ahliPet[this.i].petAuthorities.id;
                                }
                            }
                            
                        }
                    }
                )
                this.petObj = data;
                this.name = this.petObj.nama;
                this.imageSrc =  data.image;
            }
        )
    }
}
