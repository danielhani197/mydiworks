import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule, 
    MatCheckboxModule,
    MatIconModule, 
    MatInputModule,
    MatChipsModule,
    MatFormFieldModule,
    MatMenuModule,
    MatSelectModule,
    MatDialogModule,
    MatStepperModule,
    MatSnackBarModule, 
    MatTabsModule,
    MatDividerModule} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { UpdateAhliPetComponent } from './update-ahliPet.component';
import { UpdateAhliPetDeleteDialogComponent } from './delete-ahliPet-dialog.component';
import { ListUpdateComponent } from './tab/edit-list-pet/list-update.component';
import { ScrollEventModule } from 'ngx-scroll-event';
import { DataService } from '../../../../services/wall3/data.service';
import { TambahAhliPetDialogComponent } from './senarai-pengguna-dialog.component';
import { UpdateAhliPetActiveDialogComponent } from './tab/edit-list-pet/delete-ahliPet-dialog.component';

const routes = [
    {
        path     : 'updateRoles/:id',
        component: UpdateAhliPetComponent
    }
];

@NgModule({
    declarations: [
        UpdateAhliPetComponent,
        UpdateAhliPetDeleteDialogComponent,
        ListUpdateComponent,
        TambahAhliPetDialogComponent,
        UpdateAhliPetActiveDialogComponent
    ],
    entryComponents: [
        UpdateAhliPetDeleteDialogComponent,
        TambahAhliPetDialogComponent,
        UpdateAhliPetActiveDialogComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        ScrollEventModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatTabsModule,
        MatFormFieldModule,
        MatChipsModule,
        MatSnackBarModule,
        MatMenuModule,
        MatDialogModule,
        MatCheckboxModule,
        MatSelectModule,
        MatInputModule,
        NgxDatatableModule,
        FuseSharedModule
    ],
    exports     : [
        UpdateAhliPetComponent,
    ],
    providers   : [
        DataService
    ]
})
export class UpdateAhliPetModule
{

}
