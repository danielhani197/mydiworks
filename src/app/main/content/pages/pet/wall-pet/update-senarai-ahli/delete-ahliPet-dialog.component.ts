import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';
import { AhliPetService } from '../../../../services/pet/ahliPet.service';
import { AhliPet } from '../../../../model/pet/ahliPet';
import { ListUpdateComponent } from './tab/edit-list-pet/list-update.component';

@Component({
    selector: 'delete-ahliPet-dialog',
    templateUrl: 'delete-ahliPet-dialog.component.html',
    animations : fuseAnimations
    })
    export class UpdateAhliPetDeleteDialogComponent implements OnInit
    {
        _id: string;

        successTxt: string;
        existTxt: string;
        ahliPet:AhliPet;

        @ViewChild(ListUpdateComponent) filter: ListUpdateComponent;
        constructor(
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _toastr: ToastrService,
        private _router: Router,
        public snackBar: MatSnackBar,
        private _ahliPet: AhliPetService,
        public _dialogRef: MatDialogRef<UpdateAhliPetDeleteDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
        ) { 
            this._translate.get('WALL.ENABLE').subscribe((res: string) => {
                this.existTxt = res;
            });
            this._translate.get('WALL.DISABLE').subscribe((res: string) => {
                this.successTxt = res;
            });
            this.fuseTranslationLoader.loadTranslations(malay, english);
        }
        

        ngOnInit(){
            this._id = this.data.id;
        }
        confirmDelete(): void {
            
            this._ahliPet.updatePetStatus(this._id).subscribe(
                success=>{
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                      });
                    this._dialogRef.close();
                    
                },
                error=>{
                    console.log(error)
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                      });
                    this._dialogRef.close();
                }
            )
        }

    }