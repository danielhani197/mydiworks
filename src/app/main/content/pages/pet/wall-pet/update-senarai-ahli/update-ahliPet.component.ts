import { Component, OnInit, ViewChild, Inject, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router,  ActivatedRoute } from "@angular/router";
import {MatDialog,  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PagedData } from '../../../../model/util/paged-data';
import { Page } from 'app/main/content/model/util/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material';

import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import {AhliPetService} from 'app/main/content/services/pet/ahliPet.service';
import { AhliPet } from '../../../../model/pet/ahliPet';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { ListUpdateComponent} from './tab/edit-list-pet/list-update.component';
import {TambahAhliPetDialogComponent} from './senarai-pengguna-dialog.component';
import { Pet } from '../../../../model/pet/pet';
import { User } from '../../../../model/user';
import { PetService } from '../../../../services/pet/pet.service';
import { fuseAnimations } from '@fuse/animations';
import { UpdateAhliPetDeleteDialogComponent } from './delete-ahliPet-dialog.component';
import { PetAuthorities } from '../../../../model/pet/petAuthorities';


@Component({
    selector   : 'fuse-update-ahliPet',
    templateUrl: './update-ahliPet.component.html',
    styleUrls  : ['./update-ahliPet.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class UpdateAhliPetComponent implements OnInit
{ 

    data = [];
    editing = {};
    id: string;
    user2 : any;
    rows: any[];
    temp = [];
    page = new Page();

    successTxt: string;
    existTxt: string;

    _idToUpdate : string;
    _idToDelete : string;
    loadingIndicator = true;
    reorderable = true;

    name:any;
    imageSrc:any;
    userId:any;
    peranan: any;
    currentUser: any;
    filterBy:any;

    user: User;
    ahliPet:AhliPet[];
    petObj: Pet;
    pet: Pet

    selectedUser : string[]= [];
    @ViewChild(ListUpdateComponent) filter: ListUpdateComponent;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private http: HttpClient,
        private _router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private _ahliPet : AhliPetService,
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        public _dialog: MatDialog,
        private _pet: PetService,
        public snackBar: MatSnackBar,
        

    )
    {
        this._translate.get('PET.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('PET.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
      this.fuseTranslationLoader.loadTranslations(malay, english);

      this.page.pageNumber = 0;
      this.page.size = 5;
      this.page.sortField = "petAuthorities.name";
      this.page.sort = "asc";
      this.page.status = "Active";
    }
    ngOnInit(){
        this.filterBy = 'Inactive';
        this.id = this.route.snapshot.paramMap.get('id');
        this.setPage({ offset: 0 });
        this.filter.setPage({ offset: 0});
        this._pet.getPetById(this.id).subscribe(
            data=>{
                
                this.petObj = data;
                this.name = this.petObj.nama;
                if (data.image) {
                    this.imageSrc = "data:image/JPEG;base64," + data.image;
                }
                if (!data.image) {
                    this.imageSrc = "assets/images/avatars/profile.jpg";
                }
                this._ahliPet.getByPetId(data.id).subscribe(
                    ahliPet =>{
                        this.ahliPet = ahliPet;

                        this.user = this.authenticationService.getCurrentUser();
                        this.userId = this.user.id;
                        let senaraiAhli: AhliPet[] = data.ahliPet;
                        for(const ahli of senaraiAhli){
                            let user : User = new User;
                            user = ahli.user;
                            this.rows.push(user);
                        }
                    }
                )                                                                                                                                                                
            }
        )
    }
   
    openPopAhli(){
        this.setToFilter();
        let dialogRef = this._dialog.open(TambahAhliPetDialogComponent,
          {
            width: '700px',
            data: {id : this.id}
            
          }
        );
        dialogRef.afterClosed().subscribe(
            result => {
                this.loadAhli();
                this.setPage({ offset: 0 });
            }
          );
    
    }
    refresh(){
        this.setPage({ offset: 0 });
    }
    setToFilter(){
        for( let obj of this.rows){
          this.selectedUser.push(obj.id)
        }
        this._pet.setCurrentList(this.selectedUser)
    }
    
    loadAhli(){
        let pagedData: PagedData<User> = new PagedData<User>();
        pagedData = this._pet.getPaged()
        this.rows.push(...pagedData.data);
        this.page.size = 10;
        this.page.totalElements = this.rows.length;
        this.page.pageNumber = 0;
        this.loadingIndicator = false;
    
    }

    setPage(pageInfo){
        this.page.status = 'Active';

        this.page.pageNumber = pageInfo.offset;
        this._ahliPet.getAhliPetList(this.page, this.id).subscribe(
            pagedData => {
                    console.log(pagedData)
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;
        

        this._ahliPet.getAhliPetList(this.page, this.id).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;

        this._ahliPet.getAhliPetList(this.page, this.id).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }
    updateValue(event, id, rowIndex, cell) {
        
        this.editing[rowIndex + '-' + cell] = false;       
        let role : string = event.target.value;
        let ahli : AhliPet = new AhliPet();

        let petAuth : PetAuthorities = new PetAuthorities()

        petAuth.setId(role)

        ahli.setId(id);
        ahli.setPetAuthorities(petAuth);

        this._ahliPet.updatePet(ahli).subscribe(
            success=>{
                this.snackBar.open(this.successTxt, "OK", {
                    panelClass: ['blue-snackbar']
                  });

            },
            error=>{
                
                this.snackBar.open(this.existTxt, "OK", {
                    panelClass: ['red-snackbar']
                  });
                  
            }
        )
    }
    disable(id: string){
        let dialogRef = this._dialog.open(UpdateAhliPetDeleteDialogComponent,
          {
            width: '700px',
            data: {id : id}
            
          }
        );
        dialogRef.afterClosed().subscribe(
            result => {
                  this.setPage({ offset: 0 });
                  this.filter.run();
            }
          );
    
    }
}
