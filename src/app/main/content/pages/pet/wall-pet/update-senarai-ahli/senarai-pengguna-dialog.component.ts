import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from "@angular/router";
import { Page } from 'app/main/content/model/util/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatSnackBar, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as englishNav } from '../i18n/en';
import { locale as malayNav } from '../i18n/my';
import { PagedData } from '../../../../model/util/paged-data';

import { User } from 'app/main/content/model/user';
import { AhliPet } from '../../../../model/pet/ahliPet';
import {Pet} from '../../../../model/pet/pet';
import {PenggunaService} from 'app/main/content/services/pengguna/pengguna.service';
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service';
import { PetService } from '../../../../services/pet/pet.service';
import { AhliPetService } from '../../../../services/pet/ahliPet.service';
import { Notifikasi } from '../../../../model/notifikasi';
import { NotifikasiService } from '../../../../services/setting/notifikasi.service';

@Component({
    selector   : 'senarai-pengguna-dialog',
    templateUrl: './senarai-pengguna-dialog.component.html',
})
export class TambahAhliPetDialogComponent implements OnInit
{
    id: string;

    selected  = [];
    rows: any[];
    temp = [];
    page = new Page();

    successTxt: string;
    existTxt: string;
    ahli : AhliPet[];
    ahliPet:AhliPet;
    loadingIndicator = true;
    reorderable = true;
    userList : User[] = [];
    selectedUser:any;
    user  : User;
    arrayId :any[];
    pagedData: PagedData<User> = new PagedData<User>();
    
    i:number;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private http: HttpClient,
        private _router: Router,
        private _authentication: AuthenticationService,
        private route: ActivatedRoute,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _pengguna: PenggunaService,
        private _pet: PetService,
        private _ahliPet: AhliPetService,
        private _translate: TranslateService,
        public snackBar: MatSnackBar,
        private _noti : NotifikasiService,
        public _dialogRef: MatDialogRef<TambahAhliPetDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    )
    {
        this.fuseTranslationLoader.loadTranslations(malayNav, englishNav);
        this._translate.get('PET.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('PET.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });

        this.page.pageNumber = 0;
        this.page.size = 10;
        this.page.sortField = "name";
        this.page.sort = "asc";
    }

    ngOnInit(){
        this.setPage({ offset: 0 });
    }

    setPage(pageInfo){
        this.page.userId = this._pet.getUsers();
        this.page.pageNumber = pageInfo.offset;
        this._pet.getAhliPet(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }


    onSort(event) {
        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._pet.getAhliPet(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;

        this._pet.getAhliPet(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

   submit(){
    if(this.data.id){
            let pet: Pet = new Pet();
            pet.setId(this.data.id)
            
            let senaraiAhli: AhliPet[] = [];
            // this._pet.loadUser(this.selected); 
            for(const user of this.selected){
                let ahliPet : AhliPet = new AhliPet;
                ahliPet.setUser(user)
                senaraiAhli.push(ahliPet);
                console.log(ahliPet)
            }
            console.log(senaraiAhli)
            pet.setAhliPet(senaraiAhli)

            this._pet.updateAhliPet(pet).subscribe(
            success=>{
                this.snackBar.open(this.successTxt, "OK", {
                    panelClass: ['blue-snackbar']
                    });
                this.setPage({ offset: 0 });
                
                
                    
            },
            error=>{
                
                this.snackBar.open(this.existTxt, "OK", {
                    panelClass: ['red-snackbar']
                    });
                this.setPage({ offset: 0 });
                    
            }) 
            this.ahli = senaraiAhli;
            for(this.i = 0; this.i < this.ahli.length; this.i++){
                this._pet.getPetById(this.data.id).subscribe(
                    data =>{
                            this.ahliPet = this.ahli[this.i];
                            let noti: Notifikasi = new Notifikasi()
                            noti.setCreatedby(this._authentication.getCurrentUser()),
                            noti.setUser(this.ahliPet.user),
                            noti.setMaklumat(this._authentication.getCurrentUser().name+ " add you in PET " + data.nama),
                            noti.setStatus("New"),
                            noti.setTindakan("/pet/woc/"+pet.id)
                            noti.setModifiedby(this._authentication.getCurrentUser())
                            
                            this._noti.createNoti(noti).subscribe(
                                success=>{ 
                                    console.log(success)
                            },
                                error=>{
                                    console.log(error)    
                                }
                            )
                    }
                )
            } 
        }
        this._dialogRef.close();
        this._pet.resetSelectedUser();
   }    

    selectUser(id: string){       
        this._pet.selectUser(id);

    }
    
    onSelect({ selected }) {
    
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
        this.selectUser(selected.id);
        
    }


    displayCheck(row) {
        return row.name !== null;
    }

    onActivate(event) {
        
    }
    cekid(id){
        for(let obj of this.selected){
            if(obj.id==id){
                return true;
            }
        }
        return false;
    }
    
}
