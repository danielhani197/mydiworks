import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Router,  ActivatedRoute } from "@angular/router";
import {MatDialog,  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { MatSnackBar } from '@angular/material';
import {AhliPetService} from 'app/main/content/services/pet/ahliPet.service';
import { AhliPet } from '../../../../../../model/pet/ahliPet';
import { AuthenticationService } from '../../../../../../services/authentication/authentication.service';
import { PetService } from '../../../../../../services/pet/pet.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { PagedData } from '../../../../../../model/util/paged-data';
import { Page } from 'app/main/content/model/util/page';
import { User } from '../../../../../../model/user';
import { Pet } from '../../../../../../model/pet/pet';
import { UpdateAhliPetActiveDialogComponent} from './delete-ahliPet-dialog.component';
import { UpdateAhliPetComponent } from '../../update-ahliPet.component';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../../../i18n/en';
import { locale as malay } from '../../../i18n/my';
import { Observable } from 'rxjs';
import { AnonymousSubscription } from 'rxjs/Subscription';

@Component({
    selector   : 'fuse-list-ahliPet',
    templateUrl: './list-update.component.html',
    styleUrls  : ['./list-update.component.scss']
})
export class ListUpdateComponent implements OnInit
{
    editing = {};
    id: string;
    user2 : any;
    rows: any[];
    temp = [];
    page = new Page();

    successTxt: string;
    existTxt: string;

    _idToUpdate : string;
    _idToDelete : string;
    loadingIndicator = true;
    reorderable = true;

    name:any;
    imageSrc:any;
    userId:any;
    peranan: any;
    currentUser: any;

    user: User;
    ahliPet:AhliPet[];
    petObj: Pet;
    filterBy:any;
    pet:any;
    
    selectedUser : string[]= [];

    timerSubscription: AnonymousSubscription;

    postsSubscription: AnonymousSubscription;

    @ViewChild(DatatableComponent) table: DatatableComponent;
    constructor(
        private _router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private _ahliPet : AhliPetService,
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        public _dialog: MatDialog,
        private _pet: PetService,
        public snackBar: MatSnackBar,
        //private app: UpdateAhliPetComponent


    )
    {
        this._translate.get('PET.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('PET.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });
      this.fuseTranslationLoader.loadTranslations(malay, english);

      this.page.pageNumber = 0;
      this.page.size = 10;
      this.page.sortField = "petAuthorities.id";
      this.page.sort = "asc";
      this.page.status = "Inactive";
    }
    ngOnInit(){
        this.id = this.route.snapshot.paramMap.get('id');
        
        this.filterBy = "Inactive"
        this._pet.getPetById(this.id).subscribe(
            data=>{
                this.petObj = data;
                this.name = this.petObj.nama;
                this.imageSrc =  data.image;                                                                                                                                                                                                    
            }
    
        )
        this.run();
        

    }
    run(){
       this.setPage({ offset: 0 });
       
        
    }

    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._ahliPet.getAhliPetList(this.page, this.id).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
                     //this.subscribeToData();
        });
    }

    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;
        

        this._ahliPet.getAhliPetList(this.page, this.id).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;

        this._ahliPet.getAhliPetList(this.page, this.id).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }
    changeFilter(filterBy) {
        const val = filterBy;
        console.log(val)
        if(val == 'Inactive'){
            this.page.status = 'Inactive';
        }
        else{
            this.page.status = val; 
        }
        this.filterBy = val;
        this._ahliPet.getAhliPetList(this.page, this.id).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }
    enable(id: string){
        let dialogRef = this._dialog.open(UpdateAhliPetActiveDialogComponent,
          {
            width: '700px',
            data: {id : id}
            
          }
        );
        dialogRef.afterClosed().subscribe(
            result => {
                  this.run();
                  //this.app.setPage({ offset:0});
            }
          );
    
    }
    subscribeToData(): void {

        this.timerSubscription = Observable.timer(5000)
          .subscribe(() => this.run());
      }
}
