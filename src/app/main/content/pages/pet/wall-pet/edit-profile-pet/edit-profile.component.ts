import { Component, ViewChild } from '@angular/core';
import { User } from '../../../../model/user';
import { PenggunaService } from '../../../../services/pengguna/pengguna.service';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service'; 
import { locale as  malay } from '../i18n/my'; 
import { locale as  english } from '../i18n/en'; 
import { BandarNegeriService } from '../../../../services/references/bandarNegeri.service';
import { Negeri } from '../../../../model/ref/negeri';
import { Bandar } from '../../../../model/ref/bandar';
import { Agensi } from '../../../../model/general/agensi';
import { KementerianService } from '../../../../services/agency/kementerian.service';
import { AgensiService } from '../../../../services/agency/agensi.service';
import { Kementerian } from '../../../../model/general/kementerian';
import { BahagianService } from '../../../../services/agency/bahagian.service';
import { Bahagian } from '../../../../model/general/bahagian';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material';
import { DataService } from '../../../../services/wall3/data.service';
import { PetService } from '../../../../services/pet/pet.service';
import { Pet } from '../../../../model/pet/pet';

//declare let toastr: any;
@Component({
    selector   : 'edit-profile-pet',
    templateUrl: './edit-profile.component.html',
    styleUrls  : ['./edit-profile.component.scss'],
    animations : fuseAnimations
})
export class EditProfilePetComponent
{
    id: string;
    _id: any;
    profileForm: FormGroup;
    profileFormErrors: any;
    
    user: User;
    negeri: Negeri;
    bandar: Bandar;
    agensi: Agensi;
    kementerian: Kementerian;
    bahagian: Bahagian;
    petObj: Pet;

    userLs:any[];
    negeriLs :any [];
    kementerianLs:any[];
    agensiLs:any[];
    bandarLs: any[];
    bahagianLs: any[];
    skemaLs: any[];
    gradeLs:any[];


    successTxt: string;
    existTxt: string;

    negeriSemasa: any[];
    kemanterianSemasa: any[];
    agensiSemasa: any[];
    bandarSemasa: any[];
    bahagianSemasa: any[];
    skemaSemasa: any[];
    gradeSemasa:any[];
    currentObj:any[];

    verticalStepperStep1: FormGroup;
    verticalStepperStep2: FormGroup;
    verticalStepperStep3: FormGroup;
    verticalStepperStep1Errors: any;
    verticalStepperStep2Errors: any;
    verticalStepperStep3Errors: any;

    selectedFile: File;
    imageSrc: string = '';
    loaded: boolean = false;
    imageLoaded: boolean = false;
    picForm: FormGroup;

    picImage: any;

    //document upload
    @ViewChild('image') uploadImage;

constructor(    
        private penggunaService: PenggunaService,
        private formBuilder: FormBuilder,
        private authenticationService: AuthenticationService,
        private route: Router,
        private translate: TranslateService,
        private translateLoader: FuseTranslationLoaderService, 
        private _kementerian: KementerianService,
        private _bahagian: BahagianService,
        private _agensi: AgensiService,
        private BandarNegeri: BandarNegeriService,
        private _dataservice: DataService,
        private router: ActivatedRoute,
        private petService: PetService,
        public snackBar: MatSnackBar
)
{

    this.user = this.authenticationService.getCurrentUser();
    this._id = this.router.snapshot.paramMap.get('id');
        
    this.translateLoader.loadTranslations(malay, english); 

    this.translate.get('WALL.ABOUT.FAIL').subscribe((res: string) => {
        this.existTxt = res;
    });
    this.translate.get('WALL.ABOUT.SUCCESS').subscribe((res: string) => {
        this.successTxt = res;
    });

    this.verticalStepperStep1Errors = {
        namaSingkatan:{},
        name: {}
    };

    this.verticalStepperStep2Errors = {
        
        keterangan: {},
        tujuan: {},
       // tarikhwujud: {},
        catatan:{}
    };

    this.verticalStepperStep3Errors = {
        kementerian : {},
        agensi       : {},
        bahagian: {}
    };
} 
    ngOnInit(){
        this._id = this.router.snapshot.paramMap.get('id');
         // Vertical Stepper form stepper
         this.verticalStepperStep1 = this.formBuilder.group({
            name: [{ value:'', disabled:true } , Validators.required],
            namaSingkatan: [ { value:'', disabled:true }  , Validators.required],
        });

        this.verticalStepperStep2 = this.formBuilder.group({
            
            keterangan:['',Validators.required],
            tujuan: [ '' , Validators.required ],
           // tarikhwujud:[ '' , Validators.required],
            catatan: [ '' , Validators.required]
        });

        this.verticalStepperStep3 = this.formBuilder.group({
            agensi: [{ value:'', disabled:true } ,  Validators.required ],
            kementerian:[{ value:'', disabled:true } ,  Validators.required   ],
            bahagian:[{ value:'', disabled:true } ,  Validators.required   ]
        });
        
        this.picForm = new FormGroup({
            image: new FormControl()
        });
    
        this.verticalStepperStep1.valueChanges.subscribe(() => {
            this.onSteper1FormValuesChanged();
        });

        this.verticalStepperStep2.valueChanges.subscribe(() => {
            this.onSteper2FormValuesChanged();
        });

        this.verticalStepperStep3.valueChanges.subscribe(() => {
            this.onSteper3FormValuesChanged();
        });

        this._agensi.getAllAgencies().subscribe(
            data => {
              this.agensiLs = data;
          });
      
          // load kementerian
          this._kementerian.getKementerians().subscribe(
            data => {
              this.kementerianLs = data;
            }
          );
      
          // load bahagian
          this._bahagian.getAllBahagian().subscribe(
            data => {
              this.bahagianLs = data;
            }
          );
                this.petService.getPetById(this._id).subscribe(
                data => {
                    this.petObj = data;
                    this.kementerian = data.kementerian,
                    this.agensi = data.agensi
                    this.bahagian = data.bahagian

                    if (data.image) {
                        this.imageSrc = "data:image/JPEG;base64," + data.image;
                    }
                    if (!data.image) {
                        this.imageSrc = "assets/images/avatars/profile.jpg";
                    }
                    this.verticalStepperStep1.setValue({
                        name: data.nama,
                        namaSingkatan: data.namaSingkatan,
                    }) 
                    console.log(data)
                    this.verticalStepperStep2.setValue({
                        keterangan: data.keterangan,
                        tujuan: data.tujuan,
                        catatan: data.catatan,
                        //tarikhwujud: data.tarikhwujud
                    })
                    if(this.kementerian != null){
                        this.verticalStepperStep3.patchValue({
                            kementerian: this.kementerian.name
                        })
                    }
                    if(this.agensi != null){
                     this.verticalStepperStep3.patchValue({
                            agensi: this.agensi.name
                        })
                    }
                    if(this.bahagian != null){
                        this.verticalStepperStep3.patchValue({
                            bahagian: this.bahagian.name
                        })
                    }
                    
                    
                   
                }
            )
        
        
    }
    onSteper1FormValuesChanged(){
        for ( const field in this.verticalStepperStep1Errors )
        {
            if ( !this.verticalStepperStep1Errors.hasOwnProperty(field) )
            {
                continue;
            }
  
            // Clear previous errors
            this.verticalStepperStep1Errors[field] = {};
  
            // Get the control
            const control = this.verticalStepperStep1.get(field);
  
            if ( control && control.dirty && !control.valid )
            {
                this.verticalStepperStep1Errors[field] = control.errors;
            }
        }
    }   

    onSteper2FormValuesChanged(){
        for ( const field in this.verticalStepperStep2Errors )
        {
            if ( !this.verticalStepperStep2Errors.hasOwnProperty(field) )
            {
                continue;
            }
  
            // Clear previous errors
            this.verticalStepperStep2Errors[field] = {};
  
            // Get the control
            const control = this.verticalStepperStep2.get(field);
  
            if ( control && control.dirty && !control.valid )
            {
                this.verticalStepperStep2Errors[field] = control.errors;
            }
        }
    }   
    onSteper3FormValuesChanged(){
        for ( const field in this.verticalStepperStep3Errors )
        {
            if ( !this.verticalStepperStep3Errors.hasOwnProperty(field) )
            {
                continue;
            }
  
            // Clear previous errors
            this.verticalStepperStep3Errors[field] = {};
  
            // Get the control
            const control = this.verticalStepperStep3.get(field);
  
            if ( control && control.dirty && !control.valid )
            {
                this.verticalStepperStep3Errors[field] = control.errors;
            }
        }
    } 

    submit()
    {
        
        this._id = this.router.snapshot.paramMap.get('id');
        if(this._id){
        let pet : Pet = new Pet();
        pet.setId(this._id),
        pet.setNama(this.verticalStepperStep1.controls['name'].value),
        pet.setNamaSingkatan(this.verticalStepperStep1.controls['namaSingkatan'].value),
        pet.setKeterangan(this.verticalStepperStep2.controls['keterangan'].value),
        pet.setTujuan(this.verticalStepperStep2.controls['tujuan'].value),
        pet.setCatatan(this.verticalStepperStep2.controls['catatan'].value)       
        pet.setAgensi(this.agensi),
        pet.setBahagian(this.bahagian),
        pet.setKementerian(this.kementerian)

        this.petService.updatePetProfil(pet).subscribe(
            success=>{
                    
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                      });
                      //this._id = this.router.snapshot.paramMap.get('id');
                      this.route.navigate(['/pet/woc/'+ this._id]);
                },
                error=>{
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                      });
                      this.route.navigate(['/pet/woc/'+ this._id]);
                }
            )
        }
    }
    savePic(){
        this._id = this.router.snapshot.paramMap.get('id');
        if(this._id){
            let pet : Pet = new Pet();
            let input = new FormData();
            pet.setId(this._id);
            input.append('image', this.picForm.get('image').value);
            input.append('info', new Blob([JSON.stringify(pet)],
            {
                type: "application/json"
            }));

            const formModel = input;
            this.petService.updatePetProfilPhoto(formModel).subscribe(
                success=>{
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                          });
                },
                error=>{
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                        });
                }
            )
        }
    }
    back(){
        this.route.navigate(['/pet/woc/'+this._id]);
    }

    
    setSBState(id: any): void {
        // Match the selected ID with the ID's in array
        this.negeriSemasa = this.negeriLs.filter(value => parseInt(value.id) === parseInt(id));
        this.negeri = this.negeriSemasa[0];
        
        //load city based on state
        this.BandarNegeri.getCity(id).subscribe(
          data => {
            this.bandarLs = data;
          }
        );
    }

    finishVerticalStepper()
    {
        alert('You have finished the vertical stepper!');
    }

    onFileChanged(event) {
        // const file = event.target.files[0]

        // if (event.target.files && event.target.files[0]) {
        //     var reader = new FileReader();
        //     reader.readAsDataURL(event.target.files[0]); // read file as data url
      
        //     reader.onload = (event) => { // called once readAsDataURL is completed
        //         this.url = event.target.result;
        //     }     
        // }
    }

    

    handleInputChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

        var pattern = /image-*/;
        var reader = new FileReader();

        if (!file.type.match(pattern)) {
            //toastr.danger(message.global.invalidFormatImage);
            return;
        }

        this.loaded = false;

        this.picForm.get('image').setValue(file);
        //console.log(this.picForm.get('image').value);

        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        //save direct to db
    }
    handleImageLoad() {
        this.imageLoaded = true;
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    }
}