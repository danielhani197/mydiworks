export const locale = {
    lang: 'my',
    data: {
        'WALL': {
            'ERROR': 'Maklumat Tidak Berjaya Dihantar.',
            'SUCCESS': 'Maklumat Telah Berjaya Dihantar.',
            'CREATEPOST': 'Tulis Sesuatu',
            'PRIVATE': 'Peribadi',
            'POST': 'Hantar',
            'EDIT': {
                'BACK': 'Kembali',
                'EDIT': 'Kemaskini',
                'KOMEN': {
                    'EDIT': 'Kemaskini Komen',
                    'ERROR': 'Komen Tidak Berjaya Dikemaskini.',
                    'SUCCESS': 'Komen Telah Berjaya Dikemaskini.'
                },
                'POST': {
                    'EDIT': 'Kemaskini Post',
                    'ERROR': 'Post Tidak Berjaya Dikemaskini.',
                    'SUCCESS': 'Post Telah Berjaya Dikemaskini.'
                }
            },
            'DELETE': {
                'BACK': 'Kembali',
                'DELETE': 'Padam',
                'KOMEN':{
                    'CONFIRM': 'Adakah anda pasti untuk memadam komen ini ?',
                    'DELETE': 'Padam Komen',
                    'ERROR': 'Komen Tidak Berjaya Dipadam.',
                    'SUCCESS': 'Komen Telah Berjaya Dipadam.'
                },
                'POST':{
                    'CONFIRM': 'Adakah anda pasti untuk memadam post ini ?',
                    'DELETE': 'Padam Post',
                    'ERROR': 'Post Tidak Berjaya Dipadam.',
                    'SUCCESS': 'Post Telah Berjaya Dipadam.',
                }
            },
            'KOMEN': 'Komen',
            'CREATEKOMEN': 'Tambah komen',
            'ABOUT':{
                'NAME': 'Nama',
                'NAMASINGKATAN': 'Nama Singkatan',
                'KETERANGAN': 'Keterangan',
                'CATATAN': 'Catatan',
                'TUJUAN': 'Tujuan',
                'AGENSI': 'Agensi',
                'BAHAGIAN': 'Bahagian',
                'KEMENTERIAN': 'Kementerian',
                'PREVIOUS':'Kembali',
                'NEXT': 'Seterusnya',
                'SAVE': 'Simpan',
                'FILL_NAME':'Isikan nama anda',
                'FILL_ADDRESS': 'Isikan maklumat anda',
                'FILL_AGENSI': 'Isikan maklumat agensi anda',
                'UPLOAD_PHOTO': 'Muat Naik Gambar Profile',
                'FILL_PET_NAME':'Isi nama PET',
                'FILL_PET_ADDRESS': 'Isi maklumat PET',
                'FILL_PET_AGENSI': 'Maklumat Agensi ',
                'UPLOAD_PET_PHOTO': 'Muat Naik Gambar Profile PET',
                'DONE': 'Selesai',
                'FAIL':'Gagal Simpan Data',
                'SUCCESS': 'Maklumat Berjaya Simpan',
                'IMAGE': 'Gambar',
                'EDIT_PROFILE': 'Kemaskini Profile Pet',
                'SUBMIT': 'Hantar',
                'SAVE_PICTURE':'Simpan Gambar',
                'BACK':'Kembali Ke Wall PET',
                'EDIT':'Kemaskini PET',
                'CREATEDDATE': 'Tarikh Wujud'
            },
            'UNLOCKED': 'Fail telah dibuka kunci.',
            'LOCKED': 'Fail telah dikunci.',
            'FILEUPDATE': 'Fail telah dikemaskini ke versi yang baru.',
            'FILEUPDATEERROR': 'Fail tidak wujud atau fail berada dalam mod kunci.',
            'FINALIZE': 'Fail telah diakhirkan.',
            'FINALIZEERROR': 'Fail hanya boleh diakhirkan oleh Admin.',
            'TAMBAHAHLI':'Kemaskini Ahli & Perenan',
            'DISABLE': 'Nyahaktif',
            'ENABLE': 'Aktif',
            'INACTIVE_STATEMENT': 'Tukar Status Pengguna Kepada Nyahaktif ?',
            'ACTIVE_STATEMENT': 'Tukar Status Pengguna Kepada Aktif ?',
            'LIMIT': 'Jenis fail tidak betul atau fail melebihi saiz maksima.',
            'STATUS_DISABLE': 'Status Nyahaktif',

            'PET':{
                'TITLE': 'Permohonan Nyah Aktif PET',
                'REMARK': 'Catatan',
                'DEACTIVE': 'NyahAktif',
                
            },
            'DEACTIVE':{
                'ERROR' : 'Permohonan NyahAktif PET Tidak Berjaya',
                'SUCCESS' : 'Permohonan Nyahaktif Berjaya'
            },

            'BTN':{
                'BACK': 'Kembali',
                'SUBMIT': 'Hantar'
            }
        }
    }
};
