export const locale = {
    lang: 'en',
    data: {
        'WALL': {
            'ERROR': 'Information Was Unsuccessfully Delivered.',
            'SUCCESS': 'Information Was Successfully Delivered.',
            'CREATEPOST': 'Write Something..',
            'PRIVATE': 'Private',
            'POST': 'Post',
            'EDIT': {
                'BACK': 'Back',
                'EDIT': 'Edit',
                'KOMEN': {
                    'EDIT': 'Edit Comment',
                    'ERROR': 'Comment Was Unsuccessfully Edited.',
                    'SUCCESS': 'Comment Was Successfully Edited.'
                },
                'POST': {
                    'EDIT': 'Edit Post',
                    'ERROR': 'Post Was Unsuccessfully Edited.',
                    'SUCCESS': 'Post Was Successfully Edited.'
                }
            },
            'DELETE': {
                'BACK': 'Back',
                'DELETE': 'Delete',
                'KOMEN':{
                    'CONFIRM': 'Are you sure you want to delete this comment ?',
                    'DELETE': 'Delete Comment',
                    'ERROR': 'Comment Was Unsuccessfully Deleted.',
                    'SUCCESS': 'Comment Was Successfully Deleted.'
                },
                'POST':{
                    'CONFIRM': 'Are you sure you want to delete this post ?',
                    'DELETE': 'Delete Post',
                    'ERROR': 'Post Was Unsuccessfully Deleted.',
                    'SUCCESS': 'Post Was Successfully Deleted.',
                }
            },
            'KOMEN': 'Comment(s)',
            'CREATEKOMEN': 'Add a comment',
            'ABOUT':{
                'NAME': 'Name',
                'NAMASINGKATAN': 'Short Name',
                'KETERANGAN': 'Description',
                'CATATAN': 'Notes',
                'TUJUAN': 'AIM',
                'AGENSI': 'Agensi',
                'BAHAGIAN': 'Section',
                'KEMENTERIAN': 'Ministry',
                'PREVIOUS':'Previous',
                'NEXT': 'Next',
                'SAVE': 'Save',
                'FILL_NAME':'Fill out your name',
                'FILL_ADDRESS': 'Fill out your information',
                'FILL_AGENSI': 'Fill out your agency information',
                'UPLOAD_PHOTO': 'Upload Profile Picture',
                'FILL_PET_NAME':'Fill out PET name',
                'FILL_PET_ADDRESS': 'Fill out PET information',
                'FILL_PET_AGENSI': 'Agency information',
                'UPLOAD_PET_PHOTO': 'Upload PET Image',
                'DONE': 'Done',
                'FAIL':'Fail Save Profile',
                'SUCCESS': 'Save Success',
                'IMAGE': 'Picture',
                'EDIT_PROFILE': 'Update Profile Pet',
                'SUBMIT': 'Submit',
                'SAVE_PICTURE':'Save Picture',
                'BACK':'Back To PET WALL',
                'EDIT':'Update PET',
                'CREATEDDATE': 'Created Date'
            },
            'UNLOCKED': 'File has been unlock.',
            'LOCKED': 'File has been lock.',
            'FILEUPDATE': 'File has been updated to new version.',
            'FILEUPDATEERROR': 'File is not exists or has been in locked session.',
            'FINALIZE': 'File has been finalize.',
            'FINALIZEERROR': 'File can only be finalized by Admin.',
            'TAMBAHAHLI':'Edit Members & Roles',
            'DISABLE': 'Disable',
            'ENABLE': 'Enable',
            'INACTIVE_STATEMENT': 'Change Status To Inactive ?',
            'ACTIVE_STATEMENT': 'Change Status To Active ?',
            'LIMIT': 'File type is incorrect or file size too large.',
            'STATUS_DISABLE': 'Status Inactive',

            'PET':{
                'TITLE': 'Application Of PET Deactivation',
                'REMARK': 'Remark',
                'DEACTIVE': 'Deactivation',
  
            },

            'DEACTIVE':{
                'ERROR' : 'Application Of PET Deactivation Was Unsuccesfully',
                'SUCCESS' : 'Application Of PET Deactivation Was Succesfully'
            },

            'BTN':{
                'BACK': 'Back',
                'SUBMIT': 'Submit'
            }
        }
    }
};
