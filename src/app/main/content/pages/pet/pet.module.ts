import { NgModule } from '@angular/core';

import { TambahPetModule } from './tambah-pet/tambah-pet.module';
import  {SenaraiPetModule}  from './senarai-pet/senarai-pet.module';
import { PermohonanPetModule } from './permohonan-pet/permohonan-pet.module';
import { MelulusPetModule } from './melulus-pet/melulus-pet.module';
import { PermohonanPetFormModule } from './permohonan-pet-form/permohonan-pet-form.module';
import { WallPetModule } from './wall-pet/wall-pet.module';
import { SenaraiAttachmentModule } from './senarai-pet-attachment/senaraiAttachment.module';
import {SenaraiAhliPetModule} from './senarai-ahliPet/senarai-ahliPet.module';
import {MohonBinaPetModule} from './mohon-bina-pet/mohon-bina-pet.module';
import { WpetPostModule } from './wall-pet-post/woc.module';
import { MelulusAhliModule } from './melulus-ahli/melulus-ahli.module';
import {SenaraiNyahaktifModule} from './senarai-nyahaktif/senarai-nyahaktif.module';
// import {SenaraiMohonBinaPetModule} from './mohon-bina-pet/senarai-mohon-bina-pet/senarai-mohon-bina-pet.module';
//import { MelulusModule } from './melulus-pet/contact-list/melulus-list.module'
@NgModule({
    imports: [

        TambahPetModule,
        SenaraiPetModule,
        PermohonanPetModule,
        MelulusPetModule,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        PermohonanPetFormModule,
        WallPetModule,
        PermohonanPetFormModule,
        SenaraiAttachmentModule,
        SenaraiAhliPetModule,
        MohonBinaPetModule,  
        MelulusAhliModule,
        SenaraiNyahaktifModule
    ]
})
export class PetModule
{

}
