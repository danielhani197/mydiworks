import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core'; 
import { FormGroup } from '@angular/forms'; 
import { MatDialog, MatDialogRef } from '@angular/material'; 
import { DataSource } from '@angular/cdk/collections'; 
import { Observable, Subscription, BehaviorSubject } from 'rxjs'; 
import { DatatableComponent } from '@swimlane/ngx-datatable'; 
import { fuseAnimations } from '@fuse/animations'; 
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component'; 
import { PermohonanPetService } from 'app/main/content/services/pet/permohonanPet.service'; 
import { Page } from '../../../../model/util/page'; 
import { MatSnackBar } from '@angular/material';
import { PermohonanPet } from '../../../../model/pet/permohonanPet';
import { AhliPet } from '../../../../model/pet/ahliPet';
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service';

import { TranslateService } from '@ngx-translate/core'; 

 
 
 import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service'; 
 import { locale as  malay } from './../../i18n/my'; 
 import { locale as  english } from './../../i18n/en'; 
import { AhliPetService } from '../../../../services/pet/ahliPet.service';
import { Pet } from '../../../../model/pet/pet';
import { Notifikasi } from '../../../../model/notifikasi';
import { NotifikasiService } from '../../../../services/setting/notifikasi.service';
import { PetAuthorities } from '../../../../model/pet/petAuthorities';

 
@Component({ 
    selector     : 'fuse-pemohon-pemohon-list', 
    templateUrl  : './pemohon-list.component.html', 
    styleUrls    : ['./pemohon-list.component.scss'],   
    animations   : fuseAnimations 
}) 
export class PemohonListComponent implements OnInit, OnDestroy 
{ 
    //@ViewChild(FuseContactsSelectedBarComponent) selected: FuseContactsSelectedBarComponent;
    //@ViewChild(DatatableComponent) table: DatatableComponent;
    _id: any;
    selectedContacts: any[]; 
    checkboxes: {}; 

    successMsg : string;
    failMsg : string;
    
    successDelMsg : string;
    failDelMsg : string;
    
    pet: Pet;
    permohonanPet :any;
    
    rows: any[]; 
    page = new Page(); 
    dialogRef: any; 
    
    loadingIndicator = true; 
    reorderable = true; 
    filterBy : string;
    isSelected: any;
 
    onContactsChangedSubscription: Subscription;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>; 
    onSelectedpermohonanPetChanged: BehaviorSubject<any> = new BehaviorSubject([]);

    selectedRequest: string[] = [];
 
    constructor( 
        public dialog: MatDialog, 
        private permohonanService: PermohonanPetService,
        private snackBar: MatSnackBar, 
        private translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private ahliPetService: AhliPetService,
        private autheticatonService : AuthenticationService,
        private _notifi:NotifikasiService,
    ) 
    { 
 
        this.page.pageNumber = 0; 
        this.page.size = 5; 
        this.page.sortField = "id"; 
        this.page.sort = "asc"; 
        this.page.status = "0";

        this.fuseTranslationLoader.loadTranslations(malay, english); 
        this.translate.get('MELULUS.APPROVE.SUCCESS').subscribe((res: string) => {
            this.successMsg = res;
        });
        this.translate.get('MELULUS.APPROVE.ERROR').subscribe((res: string) => {
            this.failMsg = res;
        });
        this.translate.get('MELULUS.REJECT.SUCCESS').subscribe((res: string) => {
            this.successDelMsg = res;
        });
        this.translate.get('MELULUS.REJECT.ERROR').subscribe((res: string) => {
            this.failDelMsg = res;
        });

        
        this.onContactsChangedSubscription =
            this.permohonanService.onSelectedpermohonanPetChanged.subscribe(selectedRequest => {
                for ( const id in this.checkboxes )
                {
                    if ( !this.checkboxes.hasOwnProperty(id) )
                    {
                        continue;
                    }

                    this.checkboxes[id] = selectedRequest.includes(id);
                }
                this.selectedRequest = selectedRequest;
            });
    } 
 
 
    ngOnInit() 
    { 
        this.setPage({ offset: 0 }); 
        //this.setPage({ offset: this.permohonanService.dataSelectedBar});
        
    } 
 
    setPage(pageInfo){ 
        this.page.pageNumber = pageInfo.offset; 
        this.permohonanService.getPermohonanPetList(this.page).subscribe( 
            pagedData => { 
                    this.page = pagedData.page; 
                    this.rows = pagedData.data; 
                    this.loadingIndicator = false; 
                    
        });
    } 
 
    onSort(event) { 
 
        var field = event.sorts[0].prop; 
        var sort =  event.sorts[0].dir; 
 
        this.page.sortField = field; 
        this.page.sort = sort; 
 
        this.permohonanService.getPermohonanPetList(this.page).subscribe( 
            pagedData => { 
                    this.page = pagedData.page; 
                    this.rows = pagedData.data; 
                    this.loadingIndicator = false; 
        }); 
 
    } 
 
    updateFilter(event) { 
        const val = event.target.value.toLowerCase(); 
        this.page.search = val; 
     
        this.permohonanService.getPermohonanPetList(this.page).subscribe( 
            pagedData => { 
                    this.page = pagedData.page; 
                    this.rows = pagedData.data; 
                    this.loadingIndicator = false; 
            }
        ); 
    } 

    updateFilter2(filterBy) { 
        
        const val = filterBy;
        if(val == '0'){
            this.page.status = '0';
        }
        else{
            this.page.status = val; 
        }
     this.filterBy = val;
        this.permohonanService.getPermohonanPetList(this.page).subscribe( 
            pagedData => {  
                    this.page = pagedData.page; 
                    this.rows = pagedData.data; 
                    this.checkboxes= false;
                    this.loadingIndicator = false; 
            }
        ); 
    } 
    ngOnDestroy() 
    { 
       
        this.onContactsChangedSubscription.unsubscribe();
        this.onSelectedpermohonanPetChanged.unsubscribe();
     } 

    approve(id: string){
        if(id){
            let permohonanPet: PermohonanPet = new PermohonanPet()
            permohonanPet.setPelulus(this.autheticatonService.getCurrentUser()),
            permohonanPet.setStatusPermohonan(1),
            permohonanPet.setId(id)

            this.permohonanService.getPermohonanPetById(id).subscribe(
                sentToAhli => {
                    let ahliPet: AhliPet = new AhliPet();
                    this.pet = sentToAhli.pet
                    
                    let pet: Pet = new Pet();
                    let petAuth: PetAuthorities = new PetAuthorities();
                    petAuth.setId("SECURITY-PET-USER")

                    pet.id = sentToAhli.pet.id;
                    ahliPet.setUser(sentToAhli.pemohon),
                    ahliPet.setPetAuthorities(petAuth),
                    ahliPet.setTempPet(sentToAhli.pet);
                    this.ahliPetService.createAhliPet(ahliPet).subscribe(
                        success => {
                            this.snackBar.open(this.successMsg, "OK");
                            this.setPage({ offset: 0 });
                            this.permohonanService.getPermohonanPetById(id).subscribe(
                                sentToAhli => {
                            
                                    let noti: Notifikasi = new Notifikasi()
                                    noti.setCreatedby(this.autheticatonService.getCurrentUser()),
                                    noti.setUser(sentToAhli.pemohon),
                                    noti.setMaklumat(this.successMsg+ " " + sentToAhli.pet.nama),
                                    noti.setStatus("New"),
                                    noti.setTindakan("/pet/woc/"+pet.id)
                                    noti.setModifiedby(this.autheticatonService.getCurrentUser())
                                    
                                    this._notifi.createNoti(noti).subscribe(
                                        success=>{ 
                                            console.log(success)
                                    },
                                        error=>{
                                            console.log(error)    
                                        }
                                    )
                                }
                            )
                        },
                        error => {
                            console.log(error)
                            this.snackBar.open(this.failMsg, "OK",{
                                panelClass: 'red-snackbar'
                            });
                        }
                    );
                }
            )
            
            this.permohonanService.editPermohonan(permohonanPet).subscribe(
                success => {
                    this.snackBar.open(this.successMsg, "OK");
                    this.setPage({ offset: 0 });
                },
                error => {
                    console.log(error)
                    this.snackBar.open(this.failMsg, "OK",{
                        panelClass: 'red-snackbar'
                    });
                }
            )
            
        }
    }
    reject(id: string){
        if(id){
            let permohonanPet: PermohonanPet = new PermohonanPet();
            permohonanPet.setPelulus(this.autheticatonService.getCurrentUser()),
            permohonanPet.setStatusPermohonan(2),
            permohonanPet.setId(id),
            this.permohonanService.editPermohonan(permohonanPet).subscribe(
                success => {
                    this.snackBar.open(this.successDelMsg, "OK");
                    this.setPage({ offset: 0 });
                },
                error => {
                    console.log(permohonanPet)
                    this.snackBar.open(this.failDelMsg, "OK",{
                        panelClass: 'red-snackbar'
                    });
                }
            );
        }
    }


    
    onSelectedChange(contactId)
    {   
        this.permohonanService.toggleSelectedContact(contactId);
    }
    
    
} 
