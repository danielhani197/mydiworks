import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core'; 
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'; 
import { Subscription } from 'rxjs'; 
import { Router,  ActivatedRoute } from "@angular/router"; 
import { ToastrService } from 'ngx-toastr'; 
import { MatSnackBar,  MatDialog, MatDialogRef } from '@angular/material'; 
import { fuseAnimations } from '@fuse/animations'; 
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service'; 
import { locale as  malay } from '../i18n/my'; 
import { locale as  english } from '../i18n/en'; 
import { TranslateService } from '@ngx-translate/core'; 
 
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component'; 
import { PermohonanPetService } from 'app/main/content/services/pet/permohonanPet.service'; 
import { Page } from './../../../model/util/page'; 
import { PemohonListComponent } from './pemohon-list/pemohon-list.component';
import { PermohonanPet } from '../../../model/pet/permohonanPet'; 
import { AhliPet } from '../../../model/pet/ahliPet'; 
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service';
import { AhliPetService } from '../../../services/pet/ahliPet.service';
import { Pet } from '../../../model/pet/pet';
import { NotifikasiService } from '../../../services/setting/notifikasi.service';
import { Notifikasi } from '../../../model/notifikasi';
import { PetAuthorities } from '../../../model/pet/petAuthorities';

@Component({ 
    selector   : 'melulus-pet', 
    templateUrl: './melulus-pet.component.html', 
    styleUrls  : ['./melulus-pet.component.scss'], 
    animations   : fuseAnimations  
}) 
 
export class MelulusPetComponent implements OnInit 
{ 
  @ViewChild(PemohonListComponent) table: PemohonListComponent;
  //@ViewChild(FuseContactsSelectedBarComponent) selectedBar: FuseContactsSelectedBarComponent;
    rows: any[]; 
    page = new Page(); 
    dialogRef: any; 
    permohonanPet: any;
    selectedContacts: string[]; 
    loadingIndicator = true; 
    reorderable = true; 
    filterBy: string;
    hasSelectedContacts: boolean; 
    searchInput: FormControl; 
    selectedRequest: string[]; 

    isIndeterminate: boolean; 
    onSelectedpermohonanPetChanged: Subscription; 
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>; 
    onUserDataChangedSubscription: Subscription;

    successMsg : string;
    failMsg : string;
    
    successDelMsg : string;
    failDelMsg : string;

    pet: Pet;



  constructor( 
    private fuseTranslationLoader: FuseTranslationLoaderService, 
    private formBuilder: FormBuilder, 
    private route: ActivatedRoute, 
    private _router: Router, 
    private _toastr: ToastrService, 
    private translate: TranslateService, 
    public snackBar: MatSnackBar,  
    private permohonanService: PermohonanPetService,
    public dialog: MatDialog, 
    private ahliPetService: AhliPetService,
    private autheticatonService : AuthenticationService,
    private _notifi:NotifikasiService,
 
 
  ) 
  { 
    //this.filterBy =this.permohonanService.filterBy || '0';
    this.fuseTranslationLoader.loadTranslations(malay, english); 
    this.translate.get('MELULUS.APPROVE.SUCCESS').subscribe((res: string) => {
        this.successMsg = res;
    });
    this.translate.get('MELULUS.APPROVE.ERROR').subscribe((res: string) => {
        this.failMsg = res;
    });
    this.translate.get('MELULUS.REJECT.SUCCESS').subscribe((res: string) => {
        this.successDelMsg = res;
    });
    this.translate.get('MELULUS.REJECT.ERROR').subscribe((res: string) => {
        this.failDelMsg = res;
    });
    this.onUserDataChangedSubscription =
            this.permohonanService.onSelectedpermohonanPetChanged.subscribe(permohonanPet => {
                this.permohonanPet = permohonanPet;
            });

            this.permohonanService.onSelectedpermohonanPetChanged.subscribe(
                selectedContacts => {
                        this.selectedContacts = selectedContacts;
                        setTimeout(() => {
                            this.hasSelectedContacts = selectedContacts.length > 0;
                            this.isIndeterminate = (selectedContacts.length !== this.permohonanService.selectedRequest.length && selectedContacts.length > 0);
                        }, 0);
                    }
                );

  } 
 
  ngOnInit(){ 
   this.filterBy = '0';
   //this.selectedBar.selectAll();
   this.onSelectedpermohonanPetChanged =
   this.permohonanService.onSelectedpermohonanPetChanged
       .subscribe(selectedRequest => {
           this.hasSelectedContacts = selectedRequest.length > 0;
       });
  } 

  updateFilter(event: any) { 
    this.table.updateFilter(event);


} 
  updateFilter2(filterBy) {
    this.filterBy = filterBy
      this.table.updateFilter2(filterBy);
      this.hasSelectedContacts = false;
      this.permohonanService.selectedRequest = [];
  
}

// setPage(pageInfo)
// { 
//     this.page.pageNumber = pageInfo.offset; 
//     this.permohonanService.getPermohonanPetList(this.page).subscribe( 
//         pagedData => { 
//             this.page = pagedData.page; 
//             this.rows = pagedData.data; 
//             this.loadingIndicator = false; 
                    
//         });
//     } 


    // deselect()
    // {
    //     this.permohonanService.deselectPermohonanPet();
    // }

    deselect()
    {
        this.permohonanService.deselectPermohonanPet();
    }
    approveSelectedUser()
    {
        for ( const userId of this.permohonanService.selectedRequest )
        {
                    let permohonanPet: PermohonanPet = new PermohonanPet()
                    permohonanPet.setPelulus(this.autheticatonService.getCurrentUser()),
                    permohonanPet.setStatusPermohonan(1),
                    permohonanPet.setId(userId)

                    this.permohonanService.getPermohonanPetById(userId).subscribe(
                        sentToAhli => {
                            let petAuth: PetAuthorities = new PetAuthorities();
                            petAuth.setId("SECURITY-PET-USER"),
                            petAuth.setName("Ahli")


                            let ahliPet: AhliPet = new AhliPet()
                            this.pet = sentToAhli.pet
                            ahliPet.setUser(sentToAhli.pemohon),
                            ahliPet.setPetAuthorities(petAuth),
                            ahliPet.setTempPet(sentToAhli.pet);
                            this.ahliPetService.createAhliPet(ahliPet).subscribe(
                                success => {
                                    this.snackBar.open(this.successMsg, "OK");
                                    this.table.setPage({ offset: 0 });
                                    this.permohonanService.getPermohonanPetById(userId).subscribe(
                                        sentToAhli => {
                                    
                                            let noti: Notifikasi = new Notifikasi()
                                            noti.setCreatedby(this.autheticatonService.getCurrentUser()),
                                            noti.setUser(sentToAhli.pemohon),
                                            noti.setMaklumat(this.successMsg+ " " + sentToAhli.pet.nama),
                                            noti.setStatus("New"),
                                            noti.setTindakan("/pet/woc/"+sentToAhli.pet.id)
                                            noti.setModifiedby(this.autheticatonService.getCurrentUser())
                                            console.log(noti)
                                            
                                            this._notifi.createNoti(noti).subscribe(
                                                success=>{ 
                                                    console.log(success)
                                            },
                                                error=>{
                                                    console.log(error)    
                                                }
                                            )
                                        }
                                    )
                                },
                                error => {
                                    console.log(error)
                                    this.snackBar.open(this.failMsg, "OK",{
                                        panelClass: 'red-snackbar'
                                    });
                                }
                            );
                        }
                    )

                    this.permohonanService.editPermohonan(permohonanPet).subscribe(
                        success => {
                            this.snackBar.open(this.successMsg, "OK");
                            this.permohonanService.dataSelectedBar = 0;
                             this.table.setPage({ offset: 0 });
                        },
                        error => {
                            this.snackBar.open(this.failMsg, "OK",{
                                panelClass: 'red-snackbar'
                            });
                        }
                    );  
                   const lengthSelect = this.permohonanService.selectedRequest.length;
                    this.permohonanService.selectedRequest.splice(lengthSelect,1); 
                    
        } 

        this.permohonanService.onSelectedpermohonanPetChanged.next(this.permohonanService.selectedRequest);
        this.deselect();
    }


    rejectSelectedUser()
    {  
        for ( const userId of this.permohonanService.selectedRequest )
        {
                    let permohonanPet: PermohonanPet = new PermohonanPet()
                    permohonanPet.setPelulus(this.autheticatonService.getCurrentUser()),
                    permohonanPet.setStatusPermohonan(2),
                    permohonanPet.setId(userId)
                    this.permohonanService.editPermohonan(permohonanPet).subscribe(
                        success => {
                            this.snackBar.open(this.successDelMsg, "OK");
                            this.table.setPage({ offset: 0 });
                        },
                        error => {
                        
                            this.snackBar.open(this.failDelMsg, "OK",{
                                panelClass: 'red-snackbar'
                            });
                        }
                    );  
                    
                   const lengthSelect = this.permohonanService.selectedRequest.length;
                    this.permohonanService.selectedRequest.splice(lengthSelect,1); 
                    
        } 

        this.permohonanService.onSelectedpermohonanPetChanged.next(this.permohonanService.selectedRequest);
        this.deselect();
    }


    ngOnDestroy()
    {
        this.onUserDataChangedSubscription.unsubscribe();
    }
    
} 