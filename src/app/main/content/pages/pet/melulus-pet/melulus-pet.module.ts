import { NgModule } from '@angular/core'; 
import { RouterModule } from '@angular/router'; 
import { MatButtonModule, 
          MatCheckboxModule, 
          MatFormFieldModule, 
          MatInputModule, 
          MatIconModule, 
          MatSelectModule, 
          MatStepperModule, 
          MatSnackBarModule, 
          MatSidenavModule, 
          MatMenuModule,
        MatRippleModule,
          MatToolbarModule, } from '@angular/material'; 
 
import {MatChipsModule} from '@angular/material/chips';
import { NgxDatatableModule } from '@swimlane/ngx-datatable'; 
import { FuseSharedModule } from '@fuse/shared.module'; 
import { TranslateModule } from '@ngx-translate/core'; 
import { PemohonListComponent } from './pemohon-list/pemohon-list.component'; 
import { MelulusPetComponent } from './melulus-pet.component'; 
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2'; 
const routes = [ 
  { 
      path  :'approval', 
      component:  MelulusPetComponent 
  } 
]; 
 
@NgModule({ 
    declarations: [ 
        MelulusPetComponent, 
        PemohonListComponent, 
       // FuseContactsSelectedBarComponent,
        
    ], 
    imports     : [ 
        RouterModule.forChild(routes), 
        TranslateModule, 
        SweetAlert2Module, 
        MatIconModule, 
        MatButtonModule, 
        MatCheckboxModule, 
        MatFormFieldModule, 
        MatInputModule, 
        MatMenuModule, 
        MatSelectModule, 
        MatRippleModule,
        MatToolbarModule,

        MatMenuModule,
        MatChipsModule,
        MatStepperModule, 
        MatSnackBarModule, 
        MatSidenavModule, 
        NgxDatatableModule, 
        FuseSharedModule 
    ], 
}) 
export class MelulusPetModule 
{ 
}