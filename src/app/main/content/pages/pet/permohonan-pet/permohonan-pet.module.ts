import { NgModule } from '@angular/core'; 
import { RouterModule } from '@angular/router'; 
import { MatButtonModule, 
          MatCheckboxModule, 
          MatFormFieldModule, 
          MatInputModule, 
          MatIconModule, 
          MatSelectModule,
          MatSidenavModule, 
          MatStepperModule, 
          MatSnackBarModule, 
          MatChipsModule} from '@angular/material'; 
import { NgxDatatableModule } from '@swimlane/ngx-datatable'; 
import { FuseSharedModule } from '@fuse/shared.module'; 
import { TranslateModule } from '@ngx-translate/core'; 
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { PermohonanPetComponent } from './permohonan-pet.component'; 
 
const routes = [ 
  { 
      path  :'request', 
      component:  PermohonanPetComponent 
  } 
]; 
 
@NgModule({ 
    declarations: [ 
        PermohonanPetComponent 
    ], 
    imports     : [ 
        RouterModule.forChild(routes), 
        TranslateModule, 
 
        MatIconModule, 
        MatButtonModule, 
        MatCheckboxModule, 
        MatFormFieldModule, 
        MatInputModule, 
        MatSelectModule, 
        MatStepperModule,
        MatChipsModule,
        MatSnackBarModule, 
        SweetAlert2Module,
        NgxDatatableModule,
        MatSidenavModule,
        FuseSharedModule 
    ], 
}) 
export class PermohonanPetModule 
{ 
}