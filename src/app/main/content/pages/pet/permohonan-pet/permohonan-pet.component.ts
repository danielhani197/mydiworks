import { Component, OnInit, ViewEncapsulation } from '@angular/core'; 
import { FormBuilder, FormGroup, Validators } from '@angular/forms'; 
import { Router,  ActivatedRoute } from "@angular/router"; 
import { ToastrService } from 'ngx-toastr'; 
import { MatSnackBar } from '@angular/material'; 
import { fuseAnimations } from '@fuse/animations'; 
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service'; 
import { locale as  malay } from '../i18n/my'; 
import { locale as  english } from '../i18n/en'; 
import { TranslateService } from '@ngx-translate/core'; 
import { Page } from './../../../model/util/page';  
import { PermohonanPetService } from 'app/main/content/services/pet/permohonanPet.service'; 
import { PetService } from 'app/main/content/services/pet/pet.service';
import { PermohonanPet } from '../../../model/pet/permohonanPet';
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service';
import { Agensi } from '../../../model/general/agensi';
import { User } from '../../../model/user';
import { Pet } from '../../../model/pet/pet';
import { AhliPetService } from '../../../services/pet/ahliPet.service';
import { AhliPet } from '../../../model/pet/ahliPet';
@Component({ 
    selector   : 'permohonan-pet', 
    templateUrl: './permohonan-pet.component.html', 
    styleUrls  : ['./permohonan-pet.component.scss'], 
    animations : fuseAnimations,
    // encapsulation: ViewEncapsulation.Emulated
}) 
 
export class PermohonanPetComponent implements OnInit 
{ 
    rows: any[]; 
    page = new Page(); 
    dialogRef: any; 
    permohonanPet :any;
    loadingIndicator = true; 
    reorderable = true; 
    errorMsg:any;
    errorMsgRequest:string;
    _id:any;
    i:number = 0;
    
    agensi: Agensi;
    user: User;
    pet: Pet;
    status:any;
    petId:any;
    data:string =null;

    successMsg : string;
    failMsg : string;
    successDelMsg : string;
    failDelMsg : string;
    filter: string;

    petArray: PermohonanPet;
    pets:AhliPet;
    AhliPet: AhliPet[];
    ahli:AhliPet;

  constructor( 
    private fuseTranslationLoader: FuseTranslationLoaderService, 
    private formBuilder: FormBuilder, 
    private route: ActivatedRoute, 
    private _router: Router, 
    private _toastr: ToastrService, 
    private _translate: TranslateService, 
    public snackBar: MatSnackBar,
    private permohonanService: PermohonanPetService,
    private petService: PetService,
    private ahliPetService: AhliPetService,
    private autheticatonService : AuthenticationService
 
 
  ) 
  { 
    this.page.pageNumber = 0; 
        this.page.size = 5; 
        this.page.sortField = "nama"; 
        this.page.sort = "asc"; 
        this.fuseTranslationLoader.loadTranslations(malay, english); 
        this.fuseTranslationLoader.loadTranslations(malay, english); 

        this._translate.get('PERMOHONAN.JOIN').subscribe((res: string) => {
            this.errorMsg = res;
        });

        this._translate.get('PERMOHONAN.REQUEST').subscribe((res: string) => {
            this.errorMsgRequest = res;
        });
  } 
  ngOnInit() 
    { 
        this.filter = 'semua';
        this.setPage({ offset: 0 }); 

        const user = this.autheticatonService.getCurrentUser();
        this.ahliPetService.getByUserId(user.id).subscribe(
            ahli => {
                this.AhliPet = ahli;
        });

    } 
 
      setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this.petService.getPetListAll(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
                    
        });
    }

    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this.petService.getPetListAll(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;
        this.petService.getPetListAll(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }
    changeFilter(filter){
      const val = filter;
      if(val == 'semua'){
          this.page.jenis = '';
      }else{
          this.page.jenis = val;
      }
      this.filter = val;
      this.petService.getPetListAll(this.page).subscribe(
        pagedData => {
          this.page = pagedData.page;
          this.rows = pagedData.data;
          this.loadingIndicator = false;

      });

    }
 
  request(id: string){ 
    const user = this.autheticatonService.getCurrentUser();
    this.ahliPetService.getByUserId(user.id).subscribe(
        data => {
            this.permohonanService.getByUserId(user.id).subscribe(
                pemohonan => {
                    for(let i=0; i < data.length; i++){
                        this._id = data[i].tempPet.id;
                        if(id ==  data[i].tempPet.id){
                            this.data = "join";
                        }
                        // else if(id != data[i].tempPet.id && pemohonan.pelulus == null){
                            
                        //         this.data = "requesting";
                        
                        // }
                    
                    }
                    if(this.data == "join"){
                        this.snackBar.open(this.errorMsg, "OK", {
                                panelClass: ['red-snackbar']
                                });
                            
                        }
                    else if(this.data == null){
                        this._router.navigate(['/pet/request-form/'+id]);
                    }
                    // else if(this.data == "requesting"){
                    //     this.snackBar.open(this.errorMsgRequest, "OK", {
                    //         panelClass: ['red-snackbar']
                    //         });
                    // }
                    this.data = null;
                    }
                )
            }
        )
    
}

}
