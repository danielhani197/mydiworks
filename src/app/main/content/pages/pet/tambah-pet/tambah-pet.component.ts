import { Component, OnInit,ViewChild, Inject, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { HttpClient } from '@angular/common/http';

import { Router,  ActivatedRoute } from "@angular/router";
import {MatDialog} from '@angular/material';

import { Page } from '../../../model/util/page';
import { PagedData } from '../../../model/util/paged-data';

import { MatSnackBar } from '@angular/material';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

import {User} from '../../../model/user';
import {PenggunaService} from '../../../services/pengguna/pengguna.service';
import {Pet} from '../../../model/pet/pet';
import {PetService} from '../../../services/pet/pet.service';
import {SenaraiPenggunaDialogComponent} from './senarai-pengguna-dialog.component';
import { AhliPet } from '../../../model/pet/ahliPet';
import {AhliPetService} from '../../../services/pet/ahliPet.service';
import { Agensi } from '../../../model/general/agensi';
import {AgensiService} from '../../../services/agency/agensi.service';
import { Kementerian } from '../../../model/general/kementerian';
import {KementerianService} from '../../../services/agency/kementerian.service';
import { Bahagian } from '../../../model/general/bahagian';
import {BahagianService} from '../../../services/agency/bahagian.service';
import { PetAuthorities } from '../../../model/pet/petAuthorities';
import { Notifikasi } from '../../../model/notifikasi';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { NotifikasiService } from '../../../services/setting/notifikasi.service';



@Component({
    selector   : 'tambah-pet',
    templateUrl: './tambah-pet.component.html',
    styleUrls  : ['./tambah-pet.component.scss'],
    animations : fuseAnimations
})


export class TambahPetComponent implements OnInit
{
  
  selectedUser : string[] = [];

  pet: Pet;
  petForm: FormGroup;
  petFormErrors: any;

  data = [];
  id: string;
  ahliId: any;
  typePet: string;

  rows: any[] = [];
  temp = [];
  page = new Page();

  successTxt: string;
  existTxt: string;

  loadingIndicator = true;
  reorderable = true;
  
  userList : User[];

  ahliPet: AhliPet;

  agensi: Agensi;
  agensiLs :  Agensi[] = new Array();
  currentAgensi  : any;

  kementerian:Kementerian;
  kementerianLs : Kementerian[] = new Array();
  currentKementerian  : any;

  bahagian: Bahagian;
  bahagianLs :  Bahagian[] = new Array();
  currentBahagian  : any;

  constructor(
    private http: HttpClient,
    private fuseTranslationLoader: FuseTranslationLoaderService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _router: Router,
    private _translate: TranslateService,
    public snackBar: MatSnackBar,
    public _dialog: MatDialog,
    private _pet: PetService,
    private _ahliPet: AhliPetService,
    private _pengguna: PenggunaService,
    private _agensi: AgensiService,
    private _kementerian: KementerianService,
    private _bahagian: BahagianService,
    private _auth: AuthenticationService,
    private _noti: NotifikasiService,

  )
  {
    this.fuseTranslationLoader.loadTranslations(malay, english);

    // Reactive form errors
    this.petFormErrors = {
      nama : {},
      namaSingkatan  : {},
      tujuan : {},
      keterangan : {},
      catatan : {},
      jenis : {},
      ahliPet : {},
      agensi : {},
      kementerian : {},
      bahagian: {},

      
  };

  this._translate.get('PET.ERROR').subscribe((res: string) => {
      this.existTxt = res;
  });
  this._translate.get('PET.SUCCESS').subscribe((res: string) => {
      this.successTxt = res;
  });
    this.page.pageNumber = 0;
    this.page.size = 10;
    this.page.sortField = "name";
    this.page.sort = "asc";
  }

  ngOnInit(){

    this.id = this.route.snapshot.paramMap.get('id');

    this.petForm = this.formBuilder.group({
      nama:  ['', [Validators.required]],
      namaSingkatan:  ['', [Validators.required]],
      tujuan:  ['', [Validators.required]],
      keterangan:  ['', [Validators.required]],
      // catatan:  ['', [Validators.required]],
      jenis:  ['', [Validators.required]],
      agensi:  [''],
      kementerian:  ['', [Validators.required]],
      bahagian:  [''],
    

    });

    this.petForm.valueChanges.subscribe(() => {
      this.onPetFormValuesChanged();
    });

    // load agensi
    this._agensi.getAllAgencies().subscribe(
      data => {
        this.agensiLs = data;
    });

    // load kementerian
    this._kementerian.getKementerians().subscribe(
      data => {
        this.kementerianLs = data;
      }
    );

    // load bahagian
    this._bahagian.getAllBahagian().subscribe(
      data => {
        this.bahagianLs = data;
      }
    );

    this.ahliId = this.ahliPet;

    //load
    if(this.id){
      this._pet.getPetById(this.id).subscribe(
        pet => {
          let senaraiAhli: AhliPet[] = pet.ahliPet;
          for(const ahli of senaraiAhli){
            let user : User = new User;
            user = ahli.user;
            this.rows.push(user);
          }
          this.page.size = 10;
          this.page.totalElements = this.rows.length
          this.page.pageNumber = 0;
          this.loadingIndicator = false;
          this.pet = pet;

          if (pet.kementerian != null){
            this.petForm.patchValue({
              kementerian: pet.kementerian.id,

            })
          }
          if(pet.agensi != null){
            this.petForm.patchValue({
              agensi: pet.agensi.id,
            })
            
          }
          if (pet.bahagian != null){
            this.petForm.patchValue({
              bahagian: pet.bahagian.id,

            })
          }
          this.petForm.patchValue({
            nama: pet.nama,
            namaSingkatan: pet.namaSingkatan,
            tujuan: pet.tujuan,
            keterangan: pet.keterangan,
            catatan: pet.catatan,
            jenis: pet.jenis,

          });
        }
      )
    }


  }

  onPetFormValuesChanged(){
    for ( const field in this.petFormErrors )
    {
        if ( !this.petFormErrors.hasOwnProperty(field) )
        {
            continue;
        }

        // Clear previous errors
        this.petFormErrors[field] = {};

        // Get the control
        const control = this.petForm.get(field);

        if ( control && control.dirty && !control.valid )
        {
            this.petFormErrors[field] = control.errors;
        }
    }
  }

  hantar(){
    if(this.id){
     let pet: Pet = new Pet();
     pet.setNama(this.petForm.controls['nama'].value),
     pet.setNamaSingkatan(this.petForm.controls['namaSingkatan'].value),
     pet.setTujuan(this.petForm.controls['tujuan'].value),
     pet.setKeterangan(this.petForm.controls['keterangan'].value),
     pet.setJenis(this.petForm.controls['jenis'].value),
     pet.setAgensi(this.agensi),
     pet.setKementerian(this.kementerian),
     pet.setBahagian(this.bahagian),
     pet.setId(this.id)

     let senaraiAhli: AhliPet[] = [];
     for(const user of this.rows){
       let ahliPet : AhliPet = new AhliPet;
       
       ahliPet.setUser(user)
       senaraiAhli.push(ahliPet);
     }
     
     pet.setAhliPet(senaraiAhli)

     this._pet.updatePet(pet).subscribe(
      success=>{      
          this.snackBar.open(this.successTxt, "OK", {
              panelClass: ['blue-snackbar']
            });
            for(let i = 0; i < senaraiAhli.length; i++){
              this._pet.getPetById(this.id).subscribe(
                  data =>{
                          this.ahliPet = senaraiAhli[i];
                          let noti: Notifikasi = new Notifikasi()
                          noti.setCreatedby(this._auth.getCurrentUser()),
                          noti.setUser(this.ahliPet.user),
                          noti.setMaklumat(this._auth.getCurrentUser().name+ " add you in PET " + data.nama),
                          noti.setStatus("New"),
                          noti.setTindakan("/pet/woc/"+pet.id)
                          noti.setModifiedby(this._auth.getCurrentUser())
                          
                          this._noti.createNoti(noti).subscribe(
                              success=>{ 
                                  console.log(success)
                          },
                              error=>{
                                  console.log(error)    
                              }
                          )
                  }
              )
          } 
            this._router.navigate(['/pet/senarai']);
      },
      error=>{
          
          this.snackBar.open(this.existTxt, "OK", {
              panelClass: ['red-snackbar']
            });
            this._router.navigate(['/pet/senarai']);
      })  

    }else{
     let pet: Pet = new Pet();
     
     pet.setNama(this.petForm.controls['nama'].value),
     pet.setNamaSingkatan(this.petForm.controls['namaSingkatan'].value),
     pet.setTujuan(this.petForm.controls['tujuan'].value),
     pet.setKeterangan(this.petForm.controls['keterangan'].value),
     pet.setJenis(this.petForm.controls['jenis'].value),
     pet.setAgensi(this.agensi),
     pet.setKementerian(this.kementerian),
     pet.setBahagian(this.bahagian)

     let senaraiAhli: AhliPet[] = [];
     for(const user of this.rows){
       let ahliPet : AhliPet = new AhliPet;
       ahliPet.setUser(user)
       senaraiAhli.push(ahliPet);
     }
     pet.setAhliPet(senaraiAhli)
     this._pet.createPet(pet).subscribe(
         success =>{
            
             this.snackBar.open(this.successTxt, "OK", {
                 panelClass: ['blue-snackbar']
               });
               for(let i = 0; i < senaraiAhli.length; i++){
              
                      this.ahliPet = senaraiAhli[i];
                      let noti: Notifikasi = new Notifikasi()
                      noti.setCreatedby(this._auth.getCurrentUser()),
                      noti.setUser(this.ahliPet.user),
                      noti.setMaklumat(this._auth.getCurrentUser().name+ " add you in PET " + this.petForm.controls['nama'].value),
                      noti.setStatus("New"),
                      // noti.setTindakan("/pet/woc/"+pet.id)
                      noti.setModifiedby(this._auth.getCurrentUser())
                      
                      this._noti.createNoti(noti).subscribe(
                          success=>{ 
                              console.log(success)
                      },
                          error=>{
                              console.log(error)    
                          }
                      )
              }
               this._router.navigate(['/pet/senarai']);
         },
         error=>{
             
             this.snackBar.open(this.existTxt, "OK", {
                 panelClass: ['red-snackbar']
               });
               this._router.navigate(['/pet/senarai']);
         })
         
          
    }
  }

  openPopAhli(){
    
    this.setToFilter();
    
    let dialogRef = this._dialog.open(SenaraiPenggunaDialogComponent,
      {
        width: '700px'
      }
    );

    dialogRef.afterClosed().subscribe(
      result => {
        this.loadAhli();
      }
    );
  }

  setToFilter(){
    for( let obj of this.rows){
      this.selectedUser.push(obj.id)
    }
    this._pet.setCurrentList(this.selectedUser)
  }

  deleteAP(index){
    this.rows.splice(index, 1)
  }

  loadAhli(){
    let pagedData: PagedData<User> = new PagedData<User>();
    pagedData = this._pet.getPaged()
    this.rows.push(...pagedData.data);
    this.page.size = 10;
    this.page.totalElements = this.rows.length;
    this.page.pageNumber = 0;
    this.loadingIndicator = false;  
  }

  setAgensi(agensiId: any): void{
    this.currentAgensi  = this.agensiLs.filter(value => value.id === agensiId)
    this.agensi = this.currentAgensi[0];

    this._bahagian.getBahagiansByAgensiId(agensiId).subscribe(
      data =>{
        this.bahagianLs = data;
      }
    )
  }

  setKementerian(kementerianId: any): void{
    this.currentKementerian = this.kementerianLs.filter(value => value.id === kementerianId)
    this.kementerian = this.currentKementerian[0];

    this._agensi.getAllAgenciesByMinistryId(kementerianId).subscribe(
      data=>{
          this.agensiLs = data;
      }
    )

    this._bahagian.getBahagiansByKementerianId(kementerianId).subscribe(
      data =>{
          this.bahagianLs = data;
      }
    )

  }

  setBahagian(bahagianId: any): void{
    this.currentBahagian = this.bahagianLs.filter(value => value.id === bahagianId)
    this.bahagian = this.currentBahagian[0];

    this._bahagian.getBahagiansByKementerianId(this.kementerian.id).subscribe(
      data =>{
          this.bahagianLs = data;
      }
  )
  }

  redirectPetPage(){
    
    this._router.navigate(['pet/senarai']);

  }

 }
