import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule,
          MatCheckboxModule,
          MatFormFieldModule,
          MatInputModule,
          MatIconModule,
          MatSelectModule,
          MatStepperModule,
          MatDialogModule,
          MatChipsModule,
          MatMenuModule,
          MatSortModule,
          MatPaginatorModule,
          MatTableModule,
          MatSnackBarModule } from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { TambahPetComponent } from './tambah-pet.component';
import { SenaraiPenggunaDialogComponent } from './senarai-pengguna-dialog.component';


const routes = [
  {
      path  :'tambah',
      component:  TambahPetComponent
  },
  {
    path  :'tambah/:id',
    component:  TambahPetComponent
  }
];

@NgModule({
    declarations: [
        TambahPetComponent,
        SenaraiPenggunaDialogComponent
    ],
    entryComponents: [
    SenaraiPenggunaDialogComponent,

    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,

        MatIconModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatSnackBarModule,
        MatDialogModule,
        MatChipsModule,
        MatMenuModule,
        MatSortModule,
        MatTableModule,
        NgxDatatableModule,
        MatPaginatorModule,

        FuseSharedModule
    ],
})
export class TambahPetModule
{
}
