import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from "@angular/router";

import { DatatableComponent } from '@swimlane/ngx-datatable';
import { User } from 'app/main/content/model/user';
import {PenggunaService} from 'app/main/content/services/pengguna/pengguna.service';
import { TranslateService } from '@ngx-translate/core';

import { Page } from 'app/main/content/model/util/page';
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service'

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { PetService } from '../../../services/pet/pet.service';
import { locale as englishNav } from '../i18n/en';
import { locale as malayNav } from '../i18n/my';
import { MatSnackBar, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PagedData } from '../../../model/util/paged-data';

@Component({
    selector   : 'senarai-pengguna-dialog',
    templateUrl: './senarai-pengguna-dialog.component.html',
})
export class SenaraiPenggunaDialogComponent implements OnInit
{
    id: string;

    selected  = [];
    rows: any[];
    temp = [];
    page = new Page();
    userList : User[] = [];

    successTxt: string;
    existTxt: string;

    loadingIndicator = true;
    reorderable = true;

    user  : User;
    private userArray = [];
    pagedData: PagedData<User> = new PagedData<User>();

    

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private http: HttpClient,
        private _router: Router,
        private _authentication: AuthenticationService,
        private route: ActivatedRoute,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _pengguna: PenggunaService,
        private _pet: PetService,
        private _translate: TranslateService,
        public snackBar: MatSnackBar,
        public _dialogRef: MatDialogRef<SenaraiPenggunaDialogComponent>

    )
    {
        this.fuseTranslationLoader.loadTranslations(malayNav, englishNav);
        this._translate.get('PET.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('PET.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });

        this.page.pageNumber = 0;
        this.page.size = 10;
        this.page.sortField = "name";
        this.page.sort = "asc";
    }

    ngOnInit(){
        this.setPage({ offset: 0 });
    }

    setPage(pageInfo){
        
        this.page.userId = this._pet.getUsers();
        
        this.page.pageNumber = pageInfo.offset;
        this._pet.getAhliPet(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }


    onSort(event) {
        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._pet.getAhliPet(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;

        this._pet.getAhliPet(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

   submitTambahAhli(){
        this._pet.loadUser(this.selected);       
        this._dialogRef.close()
        // this._pet.resetSelectedUser();
        // console.log(this.selected)
   }   

    selectUser(id: string){       
        this._pet.selectUser(id);

    }

    onSelect({ selected }) {
    
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
        this.selectUser(selected.id);
    }


    displayCheck(row) {
        return row.name !== null;
    }

    onActivate(event) {
        
    }
    // loadUserPet(){
    //     let pagedData : PagedData<User> =new PagedData<User>();
    //     pagedData = this._pet.getPagedData()
    // }
    
    cekid(id){
        for(let obj of this.selected){
            if(obj.id==id){
                return true;
            }
        }
        // console.log(id)
        return false;
    }
}
