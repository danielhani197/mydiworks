import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSidenavModule,
        MatTableModule,
        MatDialogModule,
        MatToolbarModule, 
        MatOptionModule,
        MatSelectModule} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TranslateModule } from '@ngx-translate/core';
import {SenaraiAhliPetComponent} from './senarai-ahliPet.component'

const routes: Routes = [
    {
        path  :'senarai/ahliPet/:id',
        component:  SenaraiAhliPetComponent
    }
    
];

@NgModule({
    declarations   : [
        SenaraiAhliPetComponent,
        
    ],

    imports        : [
        RouterModule.forChild(routes),

        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSidenavModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        FuseSharedModule,
        NgxDatatableModule,
        MatOptionModule,
        MatSelectModule

    ],

})
export class SenaraiAhliPetModule
{
}
