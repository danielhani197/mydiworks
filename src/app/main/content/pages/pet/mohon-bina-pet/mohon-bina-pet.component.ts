import { Component, OnInit, ViewChild, Directive, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NG_VALIDATORS, FormControl, Validator } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { Router, ActivatedRoute } from "@angular/router";
import { MatDialog } from '@angular/material';

import { Page } from '../../../model/util/page';
import { PagedData } from '../../../model/util/paged-data';

import { MatSnackBar } from '@angular/material';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { FileValidator } from '../../../common/utilities/file-validation';

import { User } from '../../../model/user';
import { PenggunaService } from '../../../services/pengguna/pengguna.service';
import { MohonBinaPet } from '../../../model/pet/mohonBinaPet';
import { MohonBinaPetService } from '../../../services/pet/mohonBinaPet.service';
import { SenaraiPenggunaDialogComponent } from './senarai-pengguna-dialog.component';
import { AhliPet } from '../../../model/pet/ahliPet';
import { AhliPetService } from '../../../services/pet/ahliPet.service';
import { Agensi } from '../../../model/general/agensi';
import { AgensiService } from '../../../services/agency/agensi.service';
import { Kementerian } from '../../../model/general/kementerian';
import { KementerianService } from '../../../services/agency/kementerian.service';
import { Bahagian } from '../../../model/general/bahagian';
import { BahagianService } from '../../../services/agency/bahagian.service';
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service'
import { Notifikasi } from '../../../model/notifikasi';
import { NotifikasiService } from '../../../services/setting/notifikasi.service';

@Component({
  selector: 'mohon-bina-pet',
  templateUrl: './mohon-bina-pet.component.html',
  styleUrls: ['./mohon-bina-pet.component.scss'],
  animations: fuseAnimations,
})


export class MohonBinaPetComponent implements OnInit {


  selectedUser: string[] = [];

  mohonBinaPet: MohonBinaPet;
  mohonBinaPetForm: FormGroup;
  mohonBinaPetFormErrors: any;

  fileForm: FormGroup;

  id: string;
  ahliId: any;
  typePet: string;

  rows: any[] = [];
  temp = [];
  page = new Page();

  successTxt: string;
  existTxt: string;
  formatTxt: string;
  errFileExist: boolean;

  loadingIndicator = true;
  reorderable = true;

  user: User;
  userId: string;
  userList: User[];

  ahliPet: AhliPet;

  agensi: Agensi;
  agensiLs: Agensi[] = new Array();
  currentAgensi: any;
  agensiId: string;

  kementerian: Kementerian;
  kementerianLs: Kementerian[] = new Array();
  currentKementerian: any;
  kementerianId: string;

  bahagian: Bahagian;
  bahagianLs: Bahagian[] = new Array();
  currentBahagian: any;

  //document upload
  @ViewChild('fileUpload') fileUpload;
  fileName: String;

  // formGroup = this.fb.group({
  //   file: [null, Validators.required]
  // });

  constructor(
    private http: HttpClient,
    private fuseTranslationLoader: FuseTranslationLoaderService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _router: Router,
    private _translate: TranslateService,
    public snackBar: MatSnackBar,
    public _dialog: MatDialog,
    private _mohonBinaPet: MohonBinaPetService,
    private _ahliPet: AhliPetService,
    private _pengguna: PenggunaService,
    private _agensi: AgensiService,
    private _kementerian: KementerianService,
    private _bahagian: BahagianService,
    private _auth: AuthenticationService,
    private cd: ChangeDetectorRef,
    private _notifi: NotifikasiService,
    // private fb: FormBuilder


  ) {
    this.fuseTranslationLoader.loadTranslations(malay, english);

    // Reactive form errors
    this.mohonBinaPetFormErrors = {
      nama: {},
      namaSingkatan: {},
      tujuan: {},
      keterangan: {},
      catatan: {},
      jenis: {},
      ahliPet: {},
      agensi: {},
      kementerian: {},
      bahagian: {},
      fileUpload: {}


    };

    this._translate.get('PERMOHONAN.FORMAT').subscribe((res: string) => {
      this.formatTxt = res;
    });

    this._translate.get('AHLIPET.ERROR').subscribe((res: string) => {
      this.existTxt = res;
    });
    this._translate.get('AHLIPET.SUCCESS').subscribe((res: string) => {
      this.successTxt = res;
    });
    this.page.pageNumber = 0;
    this.page.size = 10;
    this.page.sortField = "name";
    this.page.sort = "asc";
  }

  ngOnInit() {


    this.id = this.route.snapshot.paramMap.get('id');
    this.user = this._auth.getCurrentUser();



    this.mohonBinaPetForm = this.formBuilder.group({
      nama: ['', [Validators.required]],
      namaSingkatan: [''],
      tujuan: ['', [Validators.required]],
      keterangan: ['', [Validators.required]],
      jenis: ['', [Validators.required]],
      agensi: [{value :'', disabled:true}],
      kementerian: [{value :'', disabled:true}],
      bahagian: [''],
      fileUpload: ['', [Validators.required]],

    });

    this.mohonBinaPetForm.valueChanges.subscribe(() => {
      this.onPetFormValuesChanged();
    });


    // load bahagian
    this._bahagian.getAllBahagian().subscribe(
      data => {
        this.bahagianLs = data;
      }
    );

    //load current user
    if(this.user.id){
      this._pengguna.getUserById(this.user.id).subscribe(
        user=>{
          this.kementerian = user.kementerian
          this.kementerianId = user.kementerian.id;
          this._kementerian.getKementerianById(this.kementerianId).subscribe(
            kementerian=>{
              this.mohonBinaPetForm.patchValue({
                kementerian: kementerian.name
              })
            }
          )
          this.agensi = user.agensi
          this.agensiId = user.agensi.id;
          this._agensi.getAgencyById(this.agensiId).subscribe(
            agensi=>{
              this.mohonBinaPetForm.patchValue({
                agensi: agensi.name
              })
            }
          )
        }

      )
    }

    this.ahliId = this.ahliPet;

    this.setPage({ offset: 0 });
  }

  setPage(pageInfo) {

    this.page.pageNumber = pageInfo.offset;
    this.rows.push(this.user);
    this.page.size = 10;
    this.page.totalElements = this.rows.length
    this.page.pageNumber = 0;
    this.loadingIndicator = false;
    this.selectedUser.push(this.user.id)
    this._mohonBinaPet.setCurrentList(this.selectedUser)

  }

  onPetFormValuesChanged() {
    for (const field in this.mohonBinaPetFormErrors) {
      if (!this.mohonBinaPetFormErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.mohonBinaPetFormErrors[field] = {};

      // Get the control
      const control = this.mohonBinaPetForm.get(field);

      if (control && control.dirty && !control.valid) {
        this.mohonBinaPetFormErrors[field] = control.errors;
      }
    }
  }


  hantar() {
    let mohonBinaPet: MohonBinaPet = new MohonBinaPet();
    let input = new FormData();
    if (this.mohonBinaPetForm.get('fileUpload').value != null) {

        mohonBinaPet.setNama(this.mohonBinaPetForm.controls['nama'].value),
        mohonBinaPet.setNamaSingkatan(this.mohonBinaPetForm.controls['namaSingkatan'].value),
        mohonBinaPet.setTujuan(this.mohonBinaPetForm.controls['tujuan'].value),
        mohonBinaPet.setKeterangan(this.mohonBinaPetForm.controls['keterangan'].value),
        mohonBinaPet.setJenis(this.mohonBinaPetForm.controls['jenis'].value),
        mohonBinaPet.setAgensi(this.agensi),
        mohonBinaPet.setKementerian(this.kementerian),
        mohonBinaPet.setBahagian(this.bahagian),
        mohonBinaPet.setPemohon(this.user)

      let senaraiAhli: AhliPet[] = [];
      for (const user of this._mohonBinaPet.userList) {
        let ahliPet: AhliPet = new AhliPet;
        ahliPet.setUser(user)
        senaraiAhli.push(ahliPet);
      }

      mohonBinaPet.setAhliPet(senaraiAhli)
      input.append('fileUpload', this.mohonBinaPetForm.get('fileUpload').value);
      input.append('info', new Blob([JSON.stringify(mohonBinaPet)],
        {
          type: "application/json"
        }));

      const formModel = input;

      this._mohonBinaPet.createPetReq(formModel).subscribe(
        success => {
          this.snackBar.open(this.successTxt, "OK", {
            panelClass: ['blue-snackbar']
          });
          this._auth.getUsers().subscribe(
            data => {
              this.userList = data;
              for(let i=0; i< this.userList.length;i++){
                  for(let p=0; p<this.userList[i].authorities.length; p++)
                      if(this.userList[i].authorities[p].rolesname == "Pentadbir Sistem" || this.userList[i].authorities[p].rolesname == "Pentadbir"){
                          let noti: Notifikasi = new Notifikasi()
                          noti.setCreatedby(this._auth.getCurrentUser()),
                          noti.setUser(this.userList[i]),
                          noti.setMaklumat("Request Create PET " + mohonBinaPet.nama),
                          noti.setStatus("New"),
                          noti.setTindakan("")
                          noti.setModifiedby(this._auth.getCurrentUser())
                                   
                          this._notifi.createNoti(noti).subscribe(
                              success=>{ 
                                  console.log(success)
                              },
                              error=>{
                                  console.log(error)    
                              }
                          )
                      }
                    }
              
                }
            )
          
          this._router.navigate(['/wall']);
        },
        error => {
          console.log(error)
          this.snackBar.open(this.existTxt, "OK", {
            panelClass: ['red-snackbar']
          });
          this._router.navigate(['/wall']);
        })
    }else{
      this.errFileExist = true;
      this.snackBar.open(this.existTxt, "OK", {
        panelClass: ['red-snackbar']
      });
    }


  }

  fileUploadChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

    var pattern = "application/pdf";
    var reader = new FileReader();

    if (file.type != pattern && file.size <= "10485760") {

      this.snackBar.open(this.formatTxt, "OK", {
        panelClass: ['red-snackbar']
      });
      return;
    } else {
      this.fileName = file.name;
      reader.readAsDataURL(file);
      this.mohonBinaPetForm.get('fileUpload').setValue(file);
    }
  }

  openPopAhli() {

    this.setToFilter();

    let dialogRef = this._dialog.open(SenaraiPenggunaDialogComponent,
      {
        width: '700px'
      }
    );

    dialogRef.afterClosed().subscribe(
      result => {
        this.loadAhli();
      }
    );
  }

  setToFilter() {

    this.selectedUser = [];

    for (let obj of this.rows) {
      this.selectedUser.push(obj.id)

    }
    this._mohonBinaPet.setCurrentList(this.selectedUser)

  }

  deleteAP(index) {
    this.rows.splice(index, 1)
  }

  loadAhli() {
    let pagedData: PagedData<User> = new PagedData<User>();
    pagedData = this._mohonBinaPet.getPagedData()
    this.rows = pagedData.data;
    this.page.size = 10;
    this.page.totalElements = pagedData.data.length
    this.page.pageNumber = 0;
    this.loadingIndicator = false;
  }

  setAgensi(agensiId: any): void {
    this.currentAgensi = this.agensiLs.filter(value => value.id === agensiId)
    this.agensi = this.currentAgensi[0];

    this._bahagian.getBahagiansByAgensiId(agensiId).subscribe(
      data => {
        this.bahagianLs = data;
      }
    )
  }

  setKementerian(kementerianId: any): void {
    this.currentKementerian = this.kementerianLs.filter(value => value.id === kementerianId)
    this.kementerian = this.currentKementerian[0];

    this._agensi.getAllAgenciesByMinistryId(kementerianId).subscribe(
      data => {
        this.agensiLs = data;
      }
    )
  }

  setBahagian(bahagianId: any): void {
    this.currentBahagian = this.bahagianLs.filter(value => value.id === bahagianId)
    this.bahagian = this.currentBahagian[0];
  }

}
