import { Component,OnInit,ViewChild,ViewEncapsulation} from '@angular/core';
import { Subscription, iif } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Page } from 'app/main/content/model/util/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar,  MatDialog, MatDialogRef  } from '@angular/material';
import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import {MohonBinaPetService} from 'app/main/content/services/pet/mohonBinaPet.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component'; 

import { MohonBinaPet } from '../../../../model/pet/mohonBinaPet';
import { Notifikasi } from '../../../../model/notifikasi';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { NotifikasiService } from '../../../../services/setting/notifikasi.service';
import {RejectDialogComponent} from '../senarai-mohon-bina-pet/reject-dialog.component';
import { User } from 'app/main/content/model/user';
import { PenggunaService } from 'app/main/content/services/pengguna/pengguna.service';
import { UserAuthorities } from 'app/main/content/model/userAuthorities';
import { Authority } from 'app/main/content/model/authority';

@Component({
    selector     : 'senarai-mohon-bina-pet-agensi',
    templateUrl  : './senarai-mohon-bina-pet-agensi.component.html',
    styleUrls    : ['./senarai-mohon-bina-pet-agensi.component.scss'],
    // encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class SenaraiMohonBinaPetAgensiComponent implements OnInit
{
  rows: any[];
  temp = [];
  page = new Page();

  mohonBinaPet: any;
  selectedContacts: string[];
  checkboxes: {}; 
  hasSelectedContacts: boolean; 


  _idToUpdate : string;
  _idToDelete : string;
  _idToReject : string;
  loadingIndicator = true;
  reorderable = true;
  
  project : string;
  event : string;
  team  : string;
  currentAdmin : User;
  kementerianId : string;
  agensiId: string;
  // pagedData: any;

  total: any;
  elemnt: any;
  size: any;
  filter: string;
  filterBy : string;

  isIndeterminate: boolean; 
  onContactsChangedSubscription: Subscription;
  onSelectedpermohonanPetChanged: Subscription; 
  confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>; 
  onUserDataChangedSubscription: Subscription;

  selectedRequest: string[] = [];

  successMsg : string;
  failMsg : string;
  
  successDelMsg : string;
  failDelMsg : string;

  @ViewChild(SenaraiMohonBinaPetAgensiComponent) table: SenaraiMohonBinaPetAgensiComponent;

    constructor(
        public dialog: MatDialog,
        private http: HttpClient,
        private _router: Router,
        private _mohonBinaPet: MohonBinaPetService,
        private translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        public _dialog: MatDialog,
        public snackBar: MatSnackBar, 
        private _auth: AuthenticationService,
        private _notifi : NotifikasiService,
        private _pengguna: PenggunaService,    
        )
    {
    this.fuseTranslationLoader.loadTranslations(malay, english);
    
    this.translate.get('MELULUS.APPROVE.SUCCESS').subscribe((res: string) => {
        this.successMsg = res;
    });
    this.translate.get('MELULUS.APPROVE.ERROR').subscribe((res: string) => {
        this.failMsg = res;
    });
    this.translate.get('MELULUS.REJECT.SUCCESS').subscribe((res: string) => {
        this.successDelMsg = res;
    });
    this.translate.get('MELULUS.REJECT.ERROR').subscribe((res: string) => {
        this.failDelMsg = res;
    });

    
    //load current admin
    this.currentAdmin = this._auth.getCurrentUser();
      
    var kementerian = this.currentAdmin.kementerian;
    var agensi = this.currentAdmin.agensi;

    if(kementerian){
        var kementerianId = kementerian.id;
        this.kementerianId = kementerianId;
    }if(agensi){
        var agensiId = agensi.id;
        this.agensiId = agensiId;
    }
      this.page.pageNumber = 0;
      this.page.size = 10;
      this.page.sortField = "nama";
      this.page.sort = "asc";
      this.page.search = "";
      this.page.jenis = "";
      this.page.agencyId = this.agensiId;
      this.page.ministryId= this.kementerianId;
   
    }

    ngOnInit()
    {
      this.setPage({ offset: 0 });
      this.filter = 'semua';
   
   
    }
    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._mohonBinaPet.getMohonAgensiList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._mohonBinaPet.getMohonAgensiList(this.page).subscribe(
            pagedData => {

                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;
        this._mohonBinaPet.getMohonAgensiList(this.page).subscribe(
            pagedData => {

                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    changeFilter(filter){
      const val = filter;
      if(val == 'semua'){
          this.page.jenis = '';
      }else{
          this.page.jenis = val;
      }
      this.filter = val;
      this._mohonBinaPet.getMohonAgensiList(this.page).subscribe(
        pagedData => {
          this.page = pagedData.page;
          this.rows = pagedData.data;
          this.loadingIndicator = false;

      });

    }

    rejectDialog(id: string){
        this._idToReject = id;
        let dialogRef = this._dialog.open(RejectDialogComponent,{
          width: '800px',
          data: {id: this._idToReject}
        });
        dialogRef.afterClosed().subscribe(
            result => {
                this.setPage({ offset: 0 });
            }
          );
      }

    approvalId(id: string){
        if(id){
            this._mohonBinaPet.approveMohon(id).subscribe(
                success => {                    
                    this.snackBar.open(this.successMsg, "OK");
                    this._mohonBinaPet.deleteById(id).subscribe();
                    this.setPage({ offset: 0 });
                    this._mohonBinaPet.approveMohon(id).subscribe(
                        data =>{
                            let noti: Notifikasi = new Notifikasi()
                            noti.setCreatedby(this.currentAdmin),
                            noti.setUser(data.pemohon),
                            noti.setMaklumat(this.successMsg+ " " + data.nama),
                            noti.setStatus("New"),
                            noti.setTindakan("")
                            noti.setModifiedby(this._auth.getCurrentUser())

                            this._notifi.createNoti(noti).subscribe(
                                success=>{ 
                                    console.log(success)
                            },
                                error=>{
                                    console.log(error)    
                                }
                            )             
                        }
                    )

                    this.setPage({ offset: 0 });

                },
                error => {
                    this.snackBar.open(this.failMsg, "OK",{
                        panelClass: ['red-snackbar']
                    });
                }

            )
        }
;  
    }
}
