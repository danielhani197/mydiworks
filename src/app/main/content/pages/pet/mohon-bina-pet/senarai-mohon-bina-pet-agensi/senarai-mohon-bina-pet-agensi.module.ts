import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { 
    MatButtonModule, 
          MatCheckboxModule, 
          MatFormFieldModule, 
          MatInputModule, 
          MatIconModule, 
          MatSelectModule, 
          MatStepperModule, 
          MatSnackBarModule, 
          MatSidenavModule, 
          MatMenuModule,
          MatRippleModule,
          MatToolbarModule,
          MatDialogModule
    
} from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FuseSharedModule } from '@fuse/shared.module';
import { SenaraiMohonBinaPetAgensiComponent } from './senarai-mohon-bina-pet-agensi.component';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { TranslateModule } from '@ngx-translate/core';
// import { RejectDialogComponent } from './reject-dialog.component';

const routes = [
    {
        path     : 'list/approve/agensi/createPet',
        component: SenaraiMohonBinaPetAgensiComponent
    }
];

@NgModule({
    declarations: [
        SenaraiMohonBinaPetAgensiComponent,
        //RejectDialogComponent

    ],
    entryComponents: [
        //RejectDialogComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule, 
          MatCheckboxModule, 
          MatFormFieldModule, 
          MatInputModule, 
          MatIconModule, 
          MatSelectModule, 
          MatStepperModule, 
          MatSnackBarModule, 
          MatSidenavModule, 
          MatMenuModule,
          MatRippleModule,
          MatToolbarModule,
          MatChipsModule,
          MatDialogModule,

        SweetAlert2Module,

        NgxDatatableModule,

        FuseSharedModule,
        TranslateModule
    ],
})
export class SenaraiMohonBinaPetModule
{
}
