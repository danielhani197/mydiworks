import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";
import { MatSnackBar } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';

import { MohonBinaPet } from '../../../../model/pet/mohonBinaPet';
import {MohonBinaPetService} from 'app/main/content/services/pet/mohonBinaPet.service';



@Component({
    selector: 'reject-dialog',
    templateUrl: 'reject-dialog.component.html',
    animations : fuseAnimations
  })
  export class RejectDialogComponent implements OnInit{

    _id: string;
    rejectForm: FormGroup;
    mohonBinaPet: MohonBinaPet;
    rejectFormErrors: any;
  
    successTxt: string;
    existTxt: string;

    constructor(
        private _translate: TranslateService,
       private fuseTranslationLoader: FuseTranslationLoaderService,
       private formBuilder: FormBuilder,
       private route: ActivatedRoute,
       private _mohonBinaPet: MohonBinaPetService,
       public snackBar: MatSnackBar,
       private _router: Router,
       public _dialogRef: MatDialogRef<RejectDialogComponent>,
       @Inject(MAT_DIALOG_DATA) public data: any
    ){

    this.fuseTranslationLoader.loadTranslations(malay, english);

     this._translate.get('PET.REJECT.ERROR').subscribe((res: string) => {
         this.existTxt = res;
     });
     this._translate.get('PET.REJECT.SUCCESS').subscribe((res: string) => {
         this.successTxt = res;
     });

     this.fuseTranslationLoader.loadTranslations(malay, english);

    }

    ngOnInit(){
        this.rejectForm = this.formBuilder.group({
            catatan : ['', [Validators.required]]
          });


        this.rejectForm.valueChanges.subscribe(() => {
            this.onRejectFormValuesChanged();
        });
   
        this._id = this.data.id;

    }

    hantar(){
        if(this._id){
            let mohonBinaPet: MohonBinaPet = new MohonBinaPet();
            mohonBinaPet.setCatatan(this.rejectForm.controls['catatan'].value),
            mohonBinaPet.setId(this._id),
            this._mohonBinaPet.rejectMohon(mohonBinaPet).subscribe(
                success => {
                        
                    this.snackBar.open(this.successTxt, "OK",{
                        panelClass: ['blue-snackbar']
                    });                   
                    this._dialogRef.close();
                },
                error => {
                    this.snackBar.open(this.existTxt, "OK",{
                        panelClass: ['red-snackbar']
                    });
                }
            )
        }

    }

    onRejectFormValuesChanged(){
        for ( const field in this.rejectFormErrors )
        {
            if ( !this.rejectFormErrors.hasOwnProperty(field) )
            {
                continue;
            }
 
            // Clear previous errors
            this.rejectFormErrors[field] = {};
 
            // Get the control
            const control = this.rejectForm.get(field);
 
            if ( control && control.dirty && !control.valid )
            {
                this.rejectFormErrors[field] = control.errors;
            }
        }
    }

    redirectSoalanPage() {
      }
  }