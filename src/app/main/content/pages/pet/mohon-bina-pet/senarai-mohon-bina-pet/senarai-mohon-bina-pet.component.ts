import { Component,OnInit,ViewChild,ViewEncapsulation} from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Page } from 'app/main/content/model/util/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar,  MatDialog, MatDialogRef  } from '@angular/material';
import { locale as english } from '../../i18n/en';
import { locale as malay } from '../../i18n/my';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import {MohonBinaPetService} from 'app/main/content/services/pet/mohonBinaPet.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component'; 

import { MohonBinaPet } from '../../../../model/pet/mohonBinaPet';
import { Notifikasi } from '../../../../model/notifikasi';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { NotifikasiService } from '../../../../services/setting/notifikasi.service';
import {RejectDialogComponent} from '../senarai-mohon-bina-pet/reject-dialog.component';

@Component({
    selector     : 'senarai-mohon-bina-pet',
    templateUrl  : './senarai-mohon-bina-pet.component.html',
    styleUrls    : ['./senarai-mohon-bina-pet.component.scss'],
    // encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class SenaraiMohonBinaPetComponent implements OnInit
{
  rows: any[];
  temp = [];
  page = new Page();

  mohonBinaPet: any;
  selectedContacts: string[];
  checkboxes: {}; 
  hasSelectedContacts: boolean; 


  _idToUpdate : string;
  _idToDelete : string;
  _idToReject : string;
  loadingIndicator = true;
  reorderable = true;
  
  project : string;
  event : string;
  team  : string;
  // pagedData: any;

  total: any;
  elemnt: any;
  size: any;
  filter: string;
  filterBy : string;

  isIndeterminate: boolean; 
  onContactsChangedSubscription: Subscription;
  onSelectedpermohonanPetChanged: Subscription; 
  confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>; 
  onUserDataChangedSubscription: Subscription;

  selectedRequest: string[] = [];

  successMsg : string;
  failMsg : string;
  
  successDelMsg : string;
  failDelMsg : string;

  @ViewChild(SenaraiMohonBinaPetComponent) table: SenaraiMohonBinaPetComponent;

    constructor(
        public dialog: MatDialog,
        private http: HttpClient,
        private _router: Router,
        private _mohonBinaPet: MohonBinaPetService,
        private translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        public _dialog: MatDialog,
        public snackBar: MatSnackBar, 
        private autheticatonService: AuthenticationService,
        private _notifi : NotifikasiService,
    )
    {
    this.fuseTranslationLoader.loadTranslations(malay, english);
    
    this.translate.get('MELULUS.APPROVE.SUCCESS').subscribe((res: string) => {
        this.successMsg = res;
    });
    this.translate.get('MELULUS.APPROVE.ERROR').subscribe((res: string) => {
        this.failMsg = res;
    });
    this.translate.get('MELULUS.REJECT.SUCCESS').subscribe((res: string) => {
        this.successDelMsg = res;
    });
    this.translate.get('MELULUS.REJECT.ERROR').subscribe((res: string) => {
        this.failDelMsg = res;
    });
  
      this.page.pageNumber = 0;
      this.page.size = 10;
      this.page.sortField = "nama";
      this.page.sort = "asc";
      this.page.search = "";
      this.page.jenis = "";
      this.page.totalElements= 0;
    }

    ngOnInit()
    {
      this.setPage({ offset: 0 });
      this.filter = 'semua';

    }
    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._mohonBinaPet.getMohonList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._mohonBinaPet.getMohonList(this.page).subscribe(
            pagedData => {

                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;
        // this.page.jenis = jenis;
        this._mohonBinaPet.getMohonList(this.page).subscribe(
            pagedData => {

                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    changeFilter(filter){
      const val = filter;
      if(val == 'semua'){
          this.page.jenis = '';
      }else{
          this.page.jenis = val;
      }
      this.filter = val;
      this._mohonBinaPet.getMohonList(this.page).subscribe(
        pagedData => {
          this.page = pagedData.page;
          this.rows = pagedData.data;
          this.loadingIndicator = false;

      });

    }

    rejectDialog(id: string){
        this._idToReject = id;
        let dialogRef = this._dialog.open(RejectDialogComponent,{
          width: '800px',
          data: {id: this._idToReject}
        });
        dialogRef.afterClosed().subscribe(
            result => {
                this.setPage({ offset: 0 });
            }
          );
      }

    approvalId(id: string){
        if(id){
            //event.stopPropagation();       
            this._mohonBinaPet.approveMohon(id).subscribe(
                success => {                    
                    this.snackBar.open(this.successMsg, "OK");
                    this._mohonBinaPet.deleteById(id).subscribe();
                    this.setPage({ offset: 0 });
                    this._mohonBinaPet.approveMohon(id).subscribe(
                        data =>{
                            console.log(data)
                            let noti: Notifikasi = new Notifikasi()
                            noti.setCreatedby(this.autheticatonService.getCurrentUser()),
                            noti.setUser(data.pemohon),
                            noti.setMaklumat(this.successMsg+ " " + data.nama),
                            noti.setStatus("New"),
                            noti.setTindakan("")
                            noti.setModifiedby(this.autheticatonService.getCurrentUser())
                            console.log(noti)
                            
                            this._notifi.createNoti(noti).subscribe(
                                success=>{ 
                                    console.log(success)
                            },
                                error=>{
                                    console.log(error)    
                                }
                            )             
                        }
                    )

                    this.setPage({ offset: 0 });

                },
                error => {
                    this.snackBar.open(this.failMsg, "OK",{
                        panelClass: ['red-snackbar']
                    });
                }

            )
        }
;  
    }
}
