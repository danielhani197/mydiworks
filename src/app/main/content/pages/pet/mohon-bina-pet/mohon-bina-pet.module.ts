import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule,
          MatCheckboxModule,
          MatFormFieldModule,
          MatInputModule,
          MatIconModule,
          MatSelectModule,
          MatStepperModule,
          MatDialogModule,
          MatChipsModule,
          MatMenuModule,
          MatSortModule,
          MatPaginatorModule,
          MatTableModule,
          MatSidenavModule,
          MatRippleModule,
          MatToolbarModule, 
          MatSnackBarModule } from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { MohonBinaPetComponent } from './mohon-bina-pet.component';
import { SenaraiPenggunaDialogComponent } from './senarai-pengguna-dialog.component';
import {SenaraiMohonBinaPetComponent} from './senarai-mohon-bina-pet/senarai-mohon-bina-pet.component';
import { RejectDialogComponent } from './senarai-mohon-bina-pet/reject-dialog.component';
import {SenaraiMohonBinaPetAgensiComponent} from './senarai-mohon-bina-pet-agensi/senarai-mohon-bina-pet-agensi.component';

const routes = [
  {
      path  :'form/create',
      component:  MohonBinaPetComponent
  },
  {
    path     : 'list/approve/createPet',
    component: SenaraiMohonBinaPetComponent
},
{
    path     : 'list/approve/agensi/createPet',
    component: SenaraiMohonBinaPetAgensiComponent
}
  

];

@NgModule({
    declarations: [
        MohonBinaPetComponent,
        SenaraiPenggunaDialogComponent,
        SenaraiMohonBinaPetComponent,
        SenaraiMohonBinaPetAgensiComponent,
        RejectDialogComponent
    ],
    entryComponents: [
    SenaraiPenggunaDialogComponent,
    RejectDialogComponent

    ],
    imports     : [
        RouterModule.forChild(routes),
        MatButtonModule, 
        MatCheckboxModule, 
        MatFormFieldModule, 
        MatInputModule, 
        MatIconModule, 
        MatSelectModule, 
        MatStepperModule, 
        MatSnackBarModule, 
        MatSidenavModule, 
        MatMenuModule,
        MatRippleModule,
        MatToolbarModule,
        MatChipsModule,
        MatDialogModule,
        MatSortModule,
        MatPaginatorModule,
        MatTableModule,

      SweetAlert2Module,

      NgxDatatableModule,

      FuseSharedModule,
      TranslateModule
    ],
})
export class MohonBinaPetModule
{
}