import { Component, OnInit } from "@angular/core";
import { fuseAnimations } from "@fuse/animations";
import { PermohonanPetService } from "../../../services/pet/permohonanPet.service";
import { Page } from "../../../model/util/page";
import { ActivatedRoute } from "@angular/router";
import { FuseTranslationLoaderService } from "@fuse/services/translation-loader.service";
import { locale as  malay } from '../i18n/my'; 
import { locale as  english } from '../i18n/en'; 
import { NotifikasiService } from "../../../services/setting/notifikasi.service";
import { AuthenticationService } from "../../../services/authentication/authentication.service";
import { PetAuthorities } from "../../../model/pet/petAuthorities";
import { AhliPet } from "../../../model/pet/ahliPet";
import { Pet } from "../../../model/pet/pet";
import { AhliPetService } from "../../../services/pet/ahliPet.service";
import { Notifikasi } from "../../../model/notifikasi";
import { TranslateService } from "@ngx-translate/core";
import { PermohonanPet } from "../../../model/pet/permohonanPet";
import { MatSnackBar, MatDialog, MatDialogRef } from "@angular/material";
import { FuseConfirmDialogComponent } from "@fuse/components/confirm-dialog/confirm-dialog.component";
import { Subscription, BehaviorSubject } from "rxjs";

@Component({ 
    selector   : 'melulus-ahli', 
    templateUrl: './melulus-ahli.component.html', 
    styleUrls  : ['./melulus-ahli.component.scss'], 
    animations   : fuseAnimations  
}) 
 
export class MelulusAhliComponent implements OnInit
{

    filterBy:any;
    page = new Page();
    rows: any[]; 
    loadingIndicator = true; 

    checkboxes: {}; 
    id:string;
    pet: any;

    successMsg : string;
    failMsg : string;
    
    successDelMsg : string;
    failDelMsg : string;

    selectedContacts:string
    hasSelectedContacts: boolean; 
    
    onContactsChangedSubscription: Subscription;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>; 
    onSelectedpermohonanPetChanged: BehaviorSubject<any> = new BehaviorSubject([]);



    onSelectedpermohonanPetChangeds: Subscription; 
    //confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>; 
    onUserDataChangedSubscription: Subscription;

    selectedRequest: string[]; 

    isIndeterminate: boolean; 

    constructor(
        private permohonanService:PermohonanPetService,
        private route: ActivatedRoute,     
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _notifi: NotifikasiService,
        private autheticatonService: AuthenticationService,
        private ahliPetService: AhliPetService,
        private translate: TranslateService,
        private snackBar: MatSnackBar, 
        public dialog: MatDialog, 
    )
    {

        this.page.pageNumber = 0; 
        this.page.size = 10; 
        this.page.sortField = "id"; 
        this.page.sort = "asc"; 
        this.page.status = "0";
        this.fuseTranslationLoader.loadTranslations(malay, english); 

        this.translate.get('MELULUS.APPROVE.SUCCESS').subscribe((res: string) => {
            this.successMsg = res;
        });
        this.translate.get('MELULUS.APPROVE.ERROR').subscribe((res: string) => {
            this.failMsg = res;
        });
        this.translate.get('MELULUS.REJECT.SUCCESS').subscribe((res: string) => {
            this.successDelMsg = res;
        });
        this.translate.get('MELULUS.REJECT.ERROR').subscribe((res: string) => {
            this.failDelMsg = res;
        });


        this.onContactsChangedSubscription =
            this.permohonanService.onSelectedpermohonanPetChanged.subscribe(selectedRequest => {
                for ( const id in this.checkboxes )
                {
                    if ( !this.checkboxes.hasOwnProperty(id) )
                    {
                        continue;
                    }

                    this.checkboxes[id] = selectedRequest.includes(id);
                }
                this.selectedRequest = selectedRequest;
            });

            this.permohonanService.onSelectedpermohonanPetChanged.subscribe(
                selectedContacts => {
                        this.selectedContacts = selectedContacts;
                        setTimeout(() => {
                            this.hasSelectedContacts = selectedContacts.length > 0;
                            this.isIndeterminate = (selectedContacts.length !== this.permohonanService.selectedRequest.length && selectedContacts.length > 0);
                        }, 0);
                    }
                );
    
    } 
    ngOnInit(){
        
        this.setPage({ offset: 0 });
        this.filterBy = '0';
        this.onSelectedpermohonanPetChangeds =
        this.permohonanService.onSelectedpermohonanPetChanged
       .subscribe(selectedRequest => {
           this.hasSelectedContacts = selectedRequest.length > 0;
       });
    }
    setPage(pageInfo){ 
        this.id = this.route.snapshot.paramMap.get('id');

        this.page.pageNumber = pageInfo.offset; 
        this.permohonanService.getPermohonanPetListByPetId(this.page, this.id).subscribe( 
            pagedData => { 
                    this.page = pagedData.page; 
                    this.rows = pagedData.data; 
                    this.loadingIndicator = false; 
                    
        });
    } 
 
    onSort(event) { 
        this.id = this.route.snapshot.paramMap.get('id');
        var field = event.sorts[0].prop; 
        var sort =  event.sorts[0].dir; 
 
        this.page.sortField = field; 
        this.page.sort = sort; 
 
        this.permohonanService.getPermohonanPetListByPetId(this.page, this.id).subscribe( 
            pagedData => { 
                    this.page = pagedData.page; 
                    this.rows = pagedData.data; 
                    this.loadingIndicator = false; 
        }); 
 
    } 
 
    updateFilter(event){ 
        this.id = this.route.snapshot.paramMap.get('id');
        const val = event.target.value; 
        this.page.search = val; 
     
        this.permohonanService.getPermohonanPetListByPetId(this.page, this.id).subscribe( 
            pagedData => { 
                    this.page = pagedData.page; 
                    this.rows = pagedData.data; 
                    this.loadingIndicator = false; 
            }
        ); 
    }

    updateFilter2(filterBy) { 
        this.id = this.route.snapshot.paramMap.get('id');
    
        if(filterBy != this.filterBy){
            this.deselect();
        }

        this.page.status = filterBy;
        this.permohonanService.getPermohonanPetListByPetId(this.page, this.id).subscribe( 
            pagedData => {  
                    this.page = pagedData.page; 
                    this.rows = pagedData.data; 
                    this.checkboxes= false;
                    this.loadingIndicator = false; 
            }
        ); 
    } 
    
    approve(id: string){
        if(id){
            let permohonanPet: PermohonanPet = new PermohonanPet()
            permohonanPet.setPelulus(this.autheticatonService.getCurrentUser()),
            permohonanPet.setStatusPermohonan(1),
            permohonanPet.setId(id)

            this.permohonanService.getPermohonanPetById(id).subscribe(
                sentToAhli => {
                    let ahliPet: AhliPet = new AhliPet();
                    this.pet = sentToAhli.pet
                    
                    let pet: Pet = new Pet();
                    let petAuth: PetAuthorities = new PetAuthorities();
                    petAuth.setId("SECURITY-PET-USER")

                    pet.id = sentToAhli.pet.id;
                    ahliPet.setUser(sentToAhli.pemohon),
                    ahliPet.setPetAuthorities(petAuth),
                    ahliPet.setTempPet(sentToAhli.pet);
                    this.ahliPetService.createAhliPet(ahliPet).subscribe(
                        success => {
                            this.snackBar.open(this.successMsg, "OK");
                            this.setPage({ offset: 0 });
                            this.permohonanService.getPermohonanPetById(id).subscribe(
                                sentToAhli => {
                            
                                    let noti: Notifikasi = new Notifikasi()
                                    noti.setCreatedby(this.autheticatonService.getCurrentUser()),
                                    noti.setUser(sentToAhli.pemohon),
                                    noti.setMaklumat(this.successMsg+ " " + sentToAhli.pet.nama),
                                    noti.setStatus("New"),
                                    noti.setTindakan("/pet/woc/"+pet.id)
                                    noti.setModifiedby(this.autheticatonService.getCurrentUser())
                                    
                                    this._notifi.createNoti(noti).subscribe(
                                        success=>{ 
                                            console.log(success)
                                    },
                                        error=>{
                                            console.log(error)    
                                        }
                                    )
                                }
                            )
                        },
                        error => {
                            console.log(error)
                            this.snackBar.open(this.failMsg, "OK",{
                                panelClass: 'red-snackbar'
                            });
                        }
                    );
                }
            )
            
            this.permohonanService.editPermohonan(permohonanPet).subscribe(
                success => {
                    this.snackBar.open(this.successMsg, "OK");
                    this.setPage({ offset: 0 });
                },
                error => {
                    console.log(error)
                    this.snackBar.open(this.failMsg, "OK",{
                        panelClass: 'red-snackbar'
                    });
                }
            )
            
        }
    }
    reject(id: string){
        if(id){
            let permohonanPet: PermohonanPet = new PermohonanPet();
            permohonanPet.setPelulus(this.autheticatonService.getCurrentUser()),
            permohonanPet.setStatusPermohonan(2),
            permohonanPet.setId(id),
            this.permohonanService.editPermohonan(permohonanPet).subscribe(
                success => {
                    this.snackBar.open(this.successDelMsg, "OK");
                    this.setPage({ offset: 0 });
                },
                error => {
                    console.log(permohonanPet)
                    this.snackBar.open(this.failDelMsg, "OK",{
                        panelClass: 'red-snackbar'
                    });
                }
            );
        }
    }

    approveSelectedUser()
    {
        for ( const userId of this.permohonanService.selectedRequest )
        {
                    let permohonanPet: PermohonanPet = new PermohonanPet()
                    permohonanPet.setPelulus(this.autheticatonService.getCurrentUser()),
                    permohonanPet.setStatusPermohonan(1),
                    permohonanPet.setId(userId)

                    this.permohonanService.getPermohonanPetById(userId).subscribe(
                        sentToAhli => {
                            let petAuth: PetAuthorities = new PetAuthorities();
                            petAuth.setId("SECURITY-PET-USER"),
                            petAuth.setName("Ahli")


                            let ahliPet: AhliPet = new AhliPet()
                            this.pet = sentToAhli.pet
                            ahliPet.setUser(sentToAhli.pemohon),
                            ahliPet.setPetAuthorities(petAuth),
                            ahliPet.setTempPet(sentToAhli.pet);
                            this.ahliPetService.createAhliPet(ahliPet).subscribe(
                                success => {
                                    this.snackBar.open(this.successMsg, "OK");
                                    this.setPage({ offset: 0 });
                                    this.permohonanService.getPermohonanPetById(userId).subscribe(
                                        sentToAhli => {
                                    
                                            let noti: Notifikasi = new Notifikasi()
                                            noti.setCreatedby(this.autheticatonService.getCurrentUser()),
                                            noti.setUser(sentToAhli.pemohon),
                                            noti.setMaklumat(this.successMsg+ " " + sentToAhli.pet.nama),
                                            noti.setStatus("New"),
                                            noti.setTindakan("/pet/woc/"+sentToAhli.pet.id)
                                            noti.setModifiedby(this.autheticatonService.getCurrentUser())
                                            console.log(noti)
                                            
                                            this._notifi.createNoti(noti).subscribe(
                                                success=>{ 
                                                    console.log(success)
                                            },
                                                error=>{
                                                    console.log(error)    
                                                }
                                            )
                                        }
                                    )
                                },
                                error => {
                                    console.log(error)
                                    this.snackBar.open(this.failMsg, "OK",{
                                        panelClass: 'red-snackbar'
                                    });
                                }
                            );
                        }
                    )

                    this.permohonanService.editPermohonan(permohonanPet).subscribe(
                        success => {
                            this.snackBar.open(this.successMsg, "OK");
                            this.permohonanService.dataSelectedBar = 0;
                             this.setPage({ offset: 0 });
                        },
                        error => {
                            this.snackBar.open(this.failMsg, "OK",{
                                panelClass: 'red-snackbar'
                            });
                        }
                    );  
                   const lengthSelect = this.permohonanService.selectedRequest.length;
                    this.permohonanService.selectedRequest.splice(lengthSelect,1); 
                    
        } 

        this.permohonanService.onSelectedpermohonanPetChanged.next(this.permohonanService.selectedRequest);
        this.deselect();
    }


    rejectSelectedUser()
    {  
        for ( const userId of this.permohonanService.selectedRequest )
        {
                    let permohonanPet: PermohonanPet = new PermohonanPet()
                    permohonanPet.setPelulus(this.autheticatonService.getCurrentUser()),
                    permohonanPet.setStatusPermohonan(2),
                    permohonanPet.setId(userId)
                    this.permohonanService.editPermohonan(permohonanPet).subscribe(
                        success => {
                            this.snackBar.open(this.successDelMsg, "OK");
                            this.setPage({ offset: 0 });
                        },
                        error => {
                        
                            this.snackBar.open(this.failDelMsg, "OK",{
                                panelClass: 'red-snackbar'
                            });
                        }
                    );  
                    
                   const lengthSelect = this.permohonanService.selectedRequest.length;
                    this.permohonanService.selectedRequest.splice(lengthSelect,1); 
                    
        } 

        this.permohonanService.onSelectedpermohonanPetChanged.next(this.permohonanService.selectedRequest);
        this.deselect();
    }

    deselect()
    {
        this.permohonanService.deselectPermohonanPet();
    }

    onSelectedChange(id)
    {   
        this.permohonanService.toggleSelectedContact(id);
    }

}