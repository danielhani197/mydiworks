import { NgModule } from "@angular/core";
import { MelulusAhliComponent } from "./melulus-ahli.component";
import { FuseSharedModule } from "@fuse/shared.module";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { MatSidenavModule, MatInputModule, MatFormFieldModule, MatCheckboxModule, MatButtonModule, MatIconModule, MatChipsModule, MatRippleModule, MatSelectModule, MatTableModule, MatMenuModule, MatDialogModule, MatToolbarModule, MatDatepickerModule } from "@angular/material";
import { SweetAlert2Module } from "@toverux/ngx-sweetalert2";
import { TranslateModule } from "@ngx-translate/core";
import { RouterModule } from "@angular/router";

const routes = [ 
    { 
        path  :'approvals/:id', 
        component:  MelulusAhliComponent 
    } 
  ]; 
   
  @NgModule({ 
      declarations: [
        MelulusAhliComponent
       ],
      imports : [

        RouterModule.forChild(routes),
        MatSelectModule,
        SweetAlert2Module,
        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSidenavModule,
        MatTableModule,
        MatToolbarModule,
        MatDialogModule,
        MatChipsModule,
        FuseSharedModule,
        NgxDatatableModule 
      ]
  })

  export class MelulusAhliModule{

  }