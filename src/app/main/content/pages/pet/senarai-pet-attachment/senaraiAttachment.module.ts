import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';

import {
  MatButtonModule,
  MatCheckboxModule,
  MatIconModule,
  MatFormFieldModule,
  MatInputModule,
  MatChipsModule,
  MatMenuModule,
  
} from '@angular/material';

import { SenaraiAttachmentComponent } from './senaraiAttachment.component';

const routes = [
  {
      path  : 'file/list',
      component : SenaraiAttachmentComponent

  }
];
@NgModule({
    declarations: [
        SenaraiAttachmentComponent,
    ],

    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatChipsModule,
        MatMenuModule,

        FuseSharedModule,
    ],
})
export class SenaraiAttachmentModule
{

}