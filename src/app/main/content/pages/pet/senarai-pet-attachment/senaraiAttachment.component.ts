import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { User } from '../../../model/user';
import { FileStorageService } from 'app/main/content/services/wall3/filestorage.service'
import { PetAttachment } from '../../../model/wall3/petAttachment';

@Component({
    selector   : 'senarai-pet-attachment',
    templateUrl: './senaraiAttachment.component.html'
})
export class SenaraiAttachmentComponent implements OnInit
{
    loadingIndicator = true;
    reorderable = true;

    versionForm: FormGroup;
    file : PetAttachment[] = new Array();
    instanceid: string;
    file2: any;
    user: User;
    status: any;
    data: PetAttachment;
    lock: any;

     //document upload
     @ViewChild('fileUpload') fileUpload;
     limitTxt: string;

    constructor(
        private http: HttpClient,
        private _translate: TranslateService,
        public snackBar: MatSnackBar,
        private _router: Router,
        private formBuilder: FormBuilder,
        private _auth: AuthenticationService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _filestorage: FileStorageService,

    )
    {
        this._translate.get('FILELIMIT.ERROR').subscribe((res: string) => {
            this.limitTxt = res;
        });

        this.user = this._auth.getCurrentUser();
    }

    ngOnInit(){

        this.versionForm = this.formBuilder.group({
            fileUpload          : [''],
        });

        this._filestorage.getPetFiles().subscribe(
            data => {
                this.file = data;
            }
        );
    }

    downloadLock(instanceid, id){

        this._filestorage.getFilesById(instanceid).subscribe(
            
            data => {
                this.file2 = data;  
                let a = document.createElement("a");
                document.body.appendChild(a);
                //a.style = "display: none"; 

                var dataFile = this.base64ToArrayBuffer(this.file2.content);
                var blob = new Blob([dataFile], {type:data.formatType});
                var url= window.URL.createObjectURL(blob);
                a.href = url;
                a.download = this.file2.name;
                a.click();
                window.URL.revokeObjectURL(url);           

            this._filestorage.getPetFilesById(id).subscribe(
                data => {
                    this.data = data;

                let petAttachment: PetAttachment = new PetAttachment();
                petAttachment.setId(this.data.id);
                petAttachment.setStatus("2");
                petAttachment.setLockedby(this.user);

                this._filestorage.updateStatus(petAttachment).subscribe(
                    success=>{
                        alert("File has been locked")
                    },
                    error=>{
                        alert("error")
                    }
                );
            })                                       
        });
    }

    unlockFile(id){
        this._filestorage.getPetFilesById(id).subscribe(
            data => {
                this.data = data;
                this.status = data.status;
                this.lock = data.lockedby.id;

             if (this.status == 2 && this.user.id === this.lock) {
                
                let petAttachment: PetAttachment = new PetAttachment();
                petAttachment.setId(this.data.id);
                petAttachment.setStatus("1");
                petAttachment.setLockedby(this.user);
    
                this._filestorage.updateStatus(petAttachment).subscribe(
                    success=>{
                        alert("File has been unlocked")
                    },
                    error=>{
                        alert("error")
                    }
                );
            }       
        })   
    }

    downloadFile(id){

        this._filestorage.getFilesById(id).subscribe(

            data => {
                this.file2 = data; 
                let a = document.createElement("a");
                document.body.appendChild(a);
                //a.style = "display: none"; 

                var dataFile = this.base64ToArrayBuffer(this.file2.content);
                var blob = new Blob([dataFile], {type:data.formatType});
                var url= window.URL.createObjectURL(blob);
                a.href = url;
                a.download = this.file2.name;
                a.click();
                window.URL.revokeObjectURL(url);
            }
        );
    }

    base64ToArrayBuffer(base64) {
        var binaryString = window.atob(base64);
        var binaryLen = binaryString.length;
        var bytes = new Uint8Array(binaryLen);
        for (var i = 0; i < binaryLen; i++) {
           var ascii = binaryString.charCodeAt(i);
           bytes[i] = ascii;
        }
        return bytes;
    }

    newUpload(){

        let petAttachment: PetAttachment = new PetAttachment();
        petAttachment.setModifiedby(this.user);
        petAttachment.setUserid(this.user.id);

        let input = new FormData();
        input.append('fileUpload', this.versionForm.get('fileUpload').value);       
        input.append('info', new Blob([JSON.stringify(petAttachment)],
        {
            type: "application/json"
        }));
        
        const formModel = input;
        // this._filestorage.saveNewVersion(formModel).subscribe(

        //     success=>{
        //         alert("File has been updated.")
        //     },
        //     error=>{
        //         alert("File is not exists or has been in locked session.")
        //     }
        // )
    }

    fileUploadChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

        var pattern = "application/x-msi";
        var pattern2 = "application/x-ms-dos-executable";
        var pattern3 = "application/vnd.debian.binary-package";
        
        var reader = new FileReader();

        if (file.type == pattern) {
          this.snackBar.open(this.limitTxt, "OK", {
            panelClass: ['red-snackbar']
          });
            return;
        }
        
        else if (file.type == pattern2) {
          this.snackBar.open(this.limitTxt, "OK", {
            panelClass: ['red-snackbar']
          });
          return;
        }
        
        else if (file.type == pattern3) {
          this.snackBar.open(this.limitTxt, "OK", {
            panelClass: ['red-snackbar']
          });
          return;

        }else{
            //this.fileName = file.name;
            reader.readAsDataURL(file);
            this.versionForm.get('fileUpload').setValue(file);
          }
      }    
}