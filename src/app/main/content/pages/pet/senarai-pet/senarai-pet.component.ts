import { Component,OnInit,ViewChild,ViewEncapsulation} from '@angular/core';
import { MatDialog} from '@angular/material';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Page } from 'app/main/content/model/util/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import {PetService} from 'app/main/content/services/pet/pet.service';
import {DeleteDialogComponent} from './delete-dialog.component';
import { fuseAnimations } from '@fuse/animations';


@Component({
    selector     : 'senarai-pet',
    templateUrl  : './senarai-pet.component.html',
    styleUrls    : ['./senarai-pet.component.scss'],
    animations   : fuseAnimations
})
export class SenaraiPetComponent implements OnInit
{
  rows: any[];
  temp = [];
  page = new Page();

  _idToUpdate : string;
  _idToDelete : string;
  loadingIndicator = true;
  reorderable = true;

  successTxt: string;
  existTxt: string;
  diactiveSucc: string;
  diactiveErr: string;
  activeSucc: string;
  activeErr: string
  
  project : string;
  event : string;
  team  : string;
  // pagedData: any;

  total: any;
  elemnt: any;
  size: any;
  filter: string;

  @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        public dialog: MatDialog,
        private http: HttpClient,
        private _router: Router,
        private _pet: PetService,
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        public _dialog: MatDialog,
        public snackBar: MatSnackBar, 

    )
    {
    this.fuseTranslationLoader.loadTranslations(malay, english);

    this._translate.get('PET.ERROR').subscribe((res: string) => {
        this.existTxt = res;
    });
    this._translate.get('PET.SUCCESS').subscribe((res: string) => {
        this.successTxt = res;
    });
    this._translate.get('PET.DIACTIVE.SUCCESS').subscribe((res: string) => {
        this.diactiveSucc = res;
    });
    this._translate.get('PET.DIACTIVE.ERROR').subscribe((res: string) => {
        this.diactiveErr = res;
    });

    this._translate.get('PET.ACTIVE.SUCCESS').subscribe((res: string) => {
        this.activeSucc = res;
    });
    this._translate.get('PET.ACTIVE.ERROR').subscribe((res: string) => {
        this.activeErr = res;
    });

      this.page.pageNumber = 0;
      this.page.size = 10;
      this.page.sortField = "nama";
      this.page.sort = "asc";
      this.page.search = "";
      this.page.jenis = "";
      this.page.totalElements= 0;
    }

    ngOnInit()
    {
      this.setPage({ offset: 0 });
      this.filter = 'semua';

    }
    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._pet.getPetListAll(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._pet.getPetListAll(this.page).subscribe(
            pagedData => {

                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;
        // this.page.jenis = jenis;
        this._pet.getPetListAll(this.page).subscribe(
            pagedData => {

                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }
    changeFilter(filter){
      const val = filter;
      if(val == 'semua'){
          this.page.jenis = '';
      }else{
          this.page.jenis = val;
      }
      this.filter = val;
      this._pet.getPetListAll(this.page).subscribe(
        pagedData => {
          this.page = pagedData.page;
          this.rows = pagedData.data;
          this.loadingIndicator = false;

      });

    }

    deletePet(id: string){
        this._idToDelete = id;
        let dialogRef = this._dialog.open(DeleteDialogComponent, {
            width: '500px',
            data: { id: this._idToDelete }
        });

        dialogRef.afterClosed().subscribe(result => {
            this.setPage({ offset: 0 });
        });
    }

    diactivePet(id: string){
        if(id){
            console.log(id)
            this._pet.diactive(id).subscribe(
                success=>{
                    this.snackBar.open(this.diactiveSucc, "OK",{
                        panelClass: ['blue-snackbar']
                    });
                    this.setPage({ offset: 0 });
                },
                error=>{
                    this.snackBar.open(this.diactiveErr, "OK",{
                        panelClass: ['red-snackbar']
                    });
                }
            )
        }
    }

    activePet(id: string){
        if(id){
            console.log(id)
            this._pet.active(id).subscribe(
                success=>{
                    this.snackBar.open(this.activeSucc, "OK",{
                        panelClass: ['blue-snackbar']
                    });
                    this.setPage({ offset: 0 });
                },
                error=>{
                    this.snackBar.open(this.activeErr, "OK",{
                        panelClass: ['red-snackbar']
                    });
                }
            )
        }
    }

    updatePet(id:string){
        this._router.navigate(['pet/tambah/'+id]);

    }

    updateAhliPet(id: string){
        this._router.navigate(['pet/senarai/ahliPet/'+id]);
    }
    
    ngOnDestroy()
    {

    }

    tambah(){
        this._router.navigate(['pet/tambah']);
    }
}
