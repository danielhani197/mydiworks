import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { 
    MatButtonModule, 
          MatCheckboxModule, 
          MatFormFieldModule, 
          MatInputModule, 
          MatIconModule, 
          MatSelectModule, 
          MatStepperModule, 
          MatSnackBarModule, 
          MatSidenavModule, 
          MatMenuModule,
          MatRippleModule,
          MatToolbarModule,
          MatDialogModule,
          MatChipsModule
    
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TranslateModule } from '@ngx-translate/core';

import { DeleteDialogComponent } from './delete-dialog.component';
import {SenaraiPetComponent} from './senarai-pet.component';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
const routes: Routes = [
    {
        path     : 'senarai',
        component: SenaraiPetComponent

    }
    
];

@NgModule({
    declarations   : [
        SenaraiPetComponent,
        DeleteDialogComponent,
        
    ],
    entryComponents: [
        DeleteDialogComponent
        ],
    imports        : [
        RouterModule.forChild(routes),

        MatButtonModule, 
          MatCheckboxModule, 
          MatFormFieldModule, 
          MatInputModule, 
          MatIconModule, 
          MatSelectModule, 
          MatStepperModule, 
          MatSnackBarModule, 
          MatSidenavModule, 
          MatMenuModule,
          MatRippleModule,
          MatToolbarModule,
          MatChipsModule,
          MatDialogModule,

        SweetAlert2Module,

        NgxDatatableModule,

        FuseSharedModule,
        TranslateModule

    ],

})
export class SenaraiPetModule
{
}
