import { Component, OnInit } from '@angular/core'; 
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'; 
import { Router,  ActivatedRoute } from "@angular/router"; 
import { ToastrService } from 'ngx-toastr'; 
import { MatSnackBar } from '@angular/material'; 
import { fuseAnimations } from '@fuse/animations'; 
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service'; 
import { locale as  malay } from '../i18n/my'; 
import { locale as  english } from '../i18n/en'; 
import { TranslateService } from '@ngx-translate/core'; 
import { Page } from './../../../model/util/page';  
import { PermohonanPetService } from 'app/main/content/services/pet/permohonanPet.service'; 
import { PetService } from 'app/main/content/services/pet/pet.service';
import { PermohonanPet } from '../../../model/pet/permohonanPet';
import { Pet } from '../../../model/pet/pet';
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service';
import { Agensi } from '../../../model/general/agensi';
import { AgensiService } from 'app/main/content/services/agency/agensi.service';
import { User } from '../../../model/user';
import { NotifikasiService } from '../../../services/setting/notifikasi.service';
import { Notifikasi } from '../../../model/notifikasi';
import { AhliPetService } from '../../../services/pet/ahliPet.service';
import { AhliPet } from '../../../model/pet/ahliPet';
import { Authorities } from '../../../model/authorities';


@Component({ 
    selector   : 'permohonan-pet-form', 
    templateUrl: './permohonan-pet-form.component.html', 
    styleUrls  : ['./permohonan-pet-form.component.scss'], 
    animations : fuseAnimations 
}) 
 
export class PermohonanPetFormComponent implements OnInit 
{ 
  _id: string;
  permohonanForm: FormGroup;
  permohonanFormErrors: any;

  pet: Pet;
  ahliPet: AhliPet[];
  auth: Authorities[];
  date: any;

  currentUser: User;
  agensi : Agensi;
  permohonanPet :PermohonanPet;

  successMsg : string;
  errorMsg : string;

  i:number;
  constructor( 
    private fuseTranslationLoader: FuseTranslationLoaderService, 
    private formBuilder: FormBuilder, 
    private route: ActivatedRoute, 
    private _router: Router, 
    private _toastr: ToastrService, 
    private translate: TranslateService, 
    public snackBar: MatSnackBar,
    private agensiService: AgensiService,
    private permohonanService: PermohonanPetService,
    private petService: PetService,
    private autheticatonService : AuthenticationService,
    private _noti : NotifikasiService,
    private _ahli : AhliPetService,
  
 
  ) 
  { 
        this.fuseTranslationLoader.loadTranslations(malay, english);
        this.translate.get('PERMOHONAN.SUCCESS').subscribe((res: string) => {
            this.successMsg = res;
        });
        this.translate.get('PERMOHONAN.ERROR').subscribe((res: string) => {
            this.errorMsg = res;
        });
       

  } 
  ngOnInit() 
  { 


 this.permohonanForm = this.formBuilder.group({
            nama: [{ value:'', disabled:true } , Validators.required ], 
            namaSingkatan: [{ value:'', disabled:true } , Validators.required ],
            jenis: [{ value:'', disabled:true } , Validators.required ],
            tujuan:[{ value:'', disabled:true } , Validators.required ],
            catatan:[ '' , Validators.required ],
            keterangan: [{ value:'', disabled:true } , Validators.required ],
            id: [{ value:'', disabled:true } , Validators.required ]
         });



    
    this._id = this.route.snapshot.paramMap.get('id');
    
    if (this._id){
      this.petService.getPetById(this._id).subscribe(

        
        pet => {
           this.pet = pet;
           this.permohonanForm.setValue({
                id: pet.id,
                nama: pet.nama,
                namaSingkatan : pet.namaSingkatan,
                jenis : pet.jenis,
                keterangan: pet.keterangan,
                tujuan: pet.tujuan,
                catatan: ''
              })
          })
        
        }

     
       
    this.permohonanForm.valueChanges.subscribe(() => {
        this.onPermohonanFormValuesChanged();
    });

  } 
    
    onPermohonanFormValuesChanged(){
      for ( const field in this.permohonanFormErrors )
      {
          if ( !this.permohonanFormErrors.hasOwnProperty(field) )
          {
              continue;
          }

          // Clear previous errors
          this.permohonanFormErrors[field] = {};

          // Get the control
          const control = this.permohonanForm.get(field);

          if ( control && control.dirty && !control.valid )
          {
              this.permohonanFormErrors[field] = control.errors;
          }
      }
    }
    request(){
    if(this._id){
          this.currentUser = this.autheticatonService.getCurrentUser();

          let permohonanPet : PermohonanPet = new PermohonanPet();
          permohonanPet.setPemohon(this.currentUser),
          permohonanPet.setPet(this.pet),
          permohonanPet.setCatatan(this.permohonanForm.controls['catatan'].value)

          this.permohonanService.createPermohonanPet(permohonanPet).subscribe(
           
              success => {
                  
                  this.snackBar.open(this.successMsg, "OK", {
                      panelClass: ['blue-snackbar']
                    });
                  this._router.navigate(['/pet/request']);
                  let noti: Notifikasi = new Notifikasi()
                  this.petService.getPetById(this.pet.id).subscribe(
                      data =>{
                          
                          this.ahliPet = data.ahliPet;
                          console.log(this.ahliPet)
                          for( this.i= 0; this.i < this.ahliPet.length; this.i++){
                              if(this.ahliPet[this.i].petAuthorities.id == 'SECURITY-PET-ADMIN'){
                                  this.currentUser = this.ahliPet[this.i].user;
                                  noti.setCreatedby(this.autheticatonService.getCurrentUser()),
                                    noti.setUser(this.currentUser),
                                    noti.setMaklumat("Requesting Join PET " + data.nama),
                                    noti.setStatus("New"),
                                    noti.setTindakan("/pet/approvals/"+this.pet.id)
                                    noti.setModifiedby(this.autheticatonService.getCurrentUser())
                                    
                                    this._noti.createNoti(noti).subscribe(
                                        success=>{ 
                                            console.log(success)
                                    },
                                        error=>{
                                            console.log(error)    
                                        }
                                    )
                              }
                          }
                        //   this._ahli.getByUserId()
                                    
                      }
                  )
                                   
              },
              error => {
                  console.log(error)
                  this.snackBar.open(this.errorMsg, "OK", {
                      panelClass: ['red-snackbar']
                    });
              }
          );
      }
    }
    back(){
      this._router.navigate(['/pet/request']);
  }
}
