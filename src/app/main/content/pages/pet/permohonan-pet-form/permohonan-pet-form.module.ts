import { NgModule } from '@angular/core'; 
import { RouterModule } from '@angular/router'; 
import { MatButtonModule, 
          MatCheckboxModule, 
          MatFormFieldModule, 
          MatInputModule, 
          MatIconModule, 
          MatSelectModule, 
          MatStepperModule, 
          MatSnackBarModule } from '@angular/material'; 
import { NgxDatatableModule } from '@swimlane/ngx-datatable'; 
import { FuseSharedModule } from '@fuse/shared.module'; 
import { TranslateModule } from '@ngx-translate/core'; 
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { PermohonanPetFormComponent } from './permohonan-pet-form.component'; 
 
const routes = [ 
  { 
      path  :'request-form/:id', 
      component:  PermohonanPetFormComponent 
  } 
]; 
 
@NgModule({ 
    declarations: [ 
        PermohonanPetFormComponent 
    ], 
    imports     : [ 
        RouterModule.forChild(routes), 
        TranslateModule, 
 
        MatIconModule, 
        MatButtonModule, 
        MatCheckboxModule, 
        MatFormFieldModule, 
        MatInputModule, 
        MatSelectModule, 
        MatStepperModule, 
        MatSnackBarModule, 
        SweetAlert2Module,
        NgxDatatableModule,
        FuseSharedModule 
    ], 
}) 
export class PermohonanPetFormModule 
{ 
}