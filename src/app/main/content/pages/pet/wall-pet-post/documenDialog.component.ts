import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../../profile/i18n/en';
import { locale as malay } from '../../profile/i18n/my';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { User } from '../../../model/user';
import { FileStorageService } from 'app/main/content/services/wall3/filestorage.service'
import { PetAttachment } from '../../../model/wall3/petAttachment';
import { Taksonomi } from '../../../model/wall3/taksonomi';
import { TaksonomiService } from 'app/main/content/services/taksonomi/taksonomi.service';
import { DataService } from 'app/main/content/services/wall3/data.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'documenDialog',
  templateUrl: 'documenDialog.component.html',
  animations : fuseAnimations
})
export class DocumenDialogComponent implements OnInit
{
    uploadForm: FormGroup;
    uploadFormErrors: any;
    user: User;
    taksonomiLs: any[];
    taks: any[];

    pa: PetAttachment;
    taksonomi: Taksonomi;

    //document upload
    @ViewChild('fileUpload') fileUpload;
    fileName: String;

    type: string;   
    limitTxt: string;

    searchTerm : FormControl = new FormControl();
    myTaks = <any>[];

   constructor(
       private _translate: TranslateService,
       private fuseTranslationLoader: FuseTranslationLoaderService,
       private formBuilder: FormBuilder,
       private route: ActivatedRoute,
       private _filestorage: FileStorageService,
       private _auth: AuthenticationService,
       private _dataservice: DataService,
       private _taksonomiservice: TaksonomiService,
       public snackBar: MatSnackBar,
       private _router: Router,
       public _dialogRef: MatDialogRef<DocumenDialogComponent>,
       @Inject(MAT_DIALOG_DATA) public data: any
   )
    {

    this.fuseTranslationLoader.loadTranslations(malay, english);
   
    this._translate.get('WALL.LIMIT').subscribe((res: string) => {
      this.limitTxt = res;
    });    

      this.user = this._auth.getCurrentUser();
    }

   ngOnInit()
   {
    this.uploadForm = this.formBuilder.group({
        fileUpload          : [" ", Validators.required],
        tajuk               : [ ],
        keterangan          : [ ],
        taksonomi           : [ ],
        jenis               : [ ],
        peristiwa           : [ ],
        tarikhPeristiwa     : [ ],
        lokasi              : [ ],
        
      });

      this._taksonomiservice.getTaks().subscribe(
        taks => {
          this.taksonomiLs = taks;
        }
      )

      this.searchTerm.valueChanges.subscribe(
        term => {
          if (term != '') {
            this._taksonomiservice.search(term).subscribe(
              data => {
                this.myTaks = data as any[];
                //console.log(data[0].BookName);
            })
          }
        })
   }

   upload(){

    if (document.getElementById('doc')) {
  
        let wa: PetAttachment = new PetAttachment ();
  
          wa.setTajuk(this.uploadForm.controls['tajuk'].value);
          wa.setKeterangan(this.uploadForm.controls['keterangan'].value);
          wa.setCreatedby(this.user);
          wa.setTaksonomi(this.taksonomi);
          wa.setUserid(this.user.id);
          wa.setJenisKandungan("Dokumen");
          wa.setStatus("1");

          localStorage.setItem('info', JSON.stringify(wa));
          this._dataservice.setData(this.uploadForm.get('fileUpload').value);
          this._dialogRef.close(); 

    } else if (document.getElementById('audio')) {

        let wa: PetAttachment = new PetAttachment ();
  
          wa.setTajuk(this.uploadForm.controls['tajuk'].value);
          wa.setPeristiwa(this.uploadForm.controls['peristiwa'].value);
          wa.setPeristiwadate(this.uploadForm.controls['tarikhPeristiwa'].value);
          wa.setCreatedby(this.user);
          wa.setTaksonomi(this.taksonomi);
          wa.setUserid(this.user.id);
          wa.setJenisKandungan("Audio");
          wa.setStatus("1");

          localStorage.setItem('info', JSON.stringify(wa));
          this._dataservice.setData(this.uploadForm.get('fileUpload').value);
          this._dialogRef.close(); 

    } else if (document.getElementById('foto')) {

      if (this.uploadForm.valid) {

        let wa: PetAttachment = new PetAttachment ();
  
          wa.setTajuk(this.uploadForm.controls['tajuk'].value);
          wa.setPeristiwa(this.uploadForm.controls['peristiwa'].value);
          wa.setPeristiwadate(this.uploadForm.controls['tarikhPeristiwa'].value);
          wa.setKeterangan(this.uploadForm.controls['keterangan'].value);
          wa.setLokasi(this.uploadForm.controls['lokasi'].value);
          wa.setCreatedby(this.user);
          wa.setTaksonomi(this.taksonomi);
          wa.setUserid(this.user.id);
          wa.setJenisKandungan("Foto");
          wa.setStatus("1");

          localStorage.setItem('info', JSON.stringify(wa));
          this._dataservice.setData(this.uploadForm.get('fileUpload').value);
          this._dialogRef.close(); 
        
          }
    } else if (document.getElementById('vid')){

      if (this.uploadForm.valid) {

        let wa: PetAttachment = new PetAttachment ();
  
          wa.setTajuk(this.uploadForm.controls['tajuk'].value);
          wa.setPeristiwa(this.uploadForm.controls['peristiwa'].value);
          wa.setPeristiwadate(this.uploadForm.controls['tarikhPeristiwa'].value);
          wa.setKeterangan(this.uploadForm.controls['keterangan'].value);
          wa.setLokasi(this.uploadForm.controls['lokasi'].value);
          wa.setCreatedby(this.user);
          wa.setTaksonomi(this.taksonomi);
          wa.setUserid(this.user.id);
          wa.setJenisKandungan("Video");
          wa.setStatus("1");
  
          localStorage.setItem('info', JSON.stringify(wa));
          this._dataservice.setData(this.uploadForm.get('fileUpload').value);
          this._dialogRef.close(); 
        
        }
      }
    }

      fileUploadChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        
        var pattern2 = /text-*/;
        var pattern3 = /application-*/;
        var pattern4 = /image-*/;       
        var pattern5 = /video-*/;
        var pattern6 = /audio-*/;

        var reader = new FileReader();
        
        if (document.getElementById('docUpload')){
      
          if ((file.type == pattern2 || file.type.match(pattern3)) && (file.size <= "10485760")){
            this.fileName = file.name;
            reader.readAsDataURL(file);
            this.uploadForm.get('fileUpload').setValue(file);
         
          } else {
            this.snackBar.open(this.limitTxt, "OK", {
              panelClass: ['red-snackbar']
            });
            return;
          }
      
        } else if (document.getElementById('fotoUpload')){

          if (file.type.match(pattern4) && (file.size <= "10485760")){
            this.fileName = file.name;
            reader.readAsDataURL(file);
            this.uploadForm.get('fileUpload').setValue(file);
         
          } else {
            this.snackBar.open(this.limitTxt, "OK", {
              panelClass: ['red-snackbar']
            });
            return;
          }

        } else if (document.getElementById('videoUpload')){

          if (file.type.match(pattern5) && (file.size <= "10485760")){
            this.fileName = file.name;
            reader.readAsDataURL(file);
            this.uploadForm.get('fileUpload').setValue(file);
         
          } else {
            this.snackBar.open(this.limitTxt, "OK", {
              panelClass: ['red-snackbar']
            });
            return;
          }

        } else if (document.getElementById('audioUpload')){

          if (file.type.match(pattern6) && (file.size <= "10485760")){
            this.fileName = file.name;
            reader.readAsDataURL(file);
            this.uploadForm.get('fileUpload').setValue(file);
         
          } else {
            this.snackBar.open(this.limitTxt, "OK", {
              panelClass: ['red-snackbar']
            });
            return;
          }
        }        
      }

      showForm() {
        this.type = this.uploadForm.controls['jenis'].value;
      }

      setTaksonomi(id: any) { 

        this.taks = this.myTaks.filter(value => parseInt(value.id) === parseInt(id));
        this.taksonomi = this.taks[0];
      }
}