import { Component, OnInit, ViewChild} from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { MyWallService } from 'app/main/content/services/wall3/myWall.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { WallKandungan } from '../../../model/wall3/wallKandungan';
import { Hashtag } from '../../../model/wall3/hashtag';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { User } from '../../../model/user';
import { WallKomen } from '../../../model/wall3/wallKomen';
import { FileStorageService } from 'app/main/content/services/wall3/filestorage.service'
import { PetAttachment } from '../../../model/wall3/petAttachment';
import { DataService } from 'app/main/content/services/wall3/data.service';

import { locale as english } from '../wall-pet/i18n/en';
import { locale as malay } from '../wall-pet/i18n/my';
import { ActivatedRoute, Router } from '@angular/router';
import { Pet } from '../../../model/pet/pet';
import { PetService } from '../../../services/pet/pet.service';
import { WallPetService } from '../../../services/pet/wall-pet.service';
import { PostCollection } from '../../../model/wall3/postCollection';
import { AdvancedPage } from '../../../model/util/advanced-page';
import { Notifikasi } from '../../../model/notifikasi';
import { NotifikasiService } from '../../../services/setting/notifikasi.service';
import { AhliPetService } from '../../../services/pet/ahliPet.service';
import { AhliPet } from '../../../model/pet/ahliPet';
import { WallAttachment } from '../../../model/wall3/wallAttachment';
import { DeleteDialogComponent } from './deleteDialog.component';
import { EditDialogComponent } from './editDialog.component';

@Component({
    selector   : 'wall-pet-post',
    templateUrl: './woc.component.html',
    styleUrls  : ['./woc.component.scss'],
    animations : fuseAnimations
})
export class WpetPostComponent
{

    page: AdvancedPage = new AdvancedPage;

    timeline: PostCollection[]= new Array<PostCollection>();
    _id: string;
    petObj: Pet;
    
    length: any;
    user: User;
    hashtagList: Hashtag[] = new Array<Hashtag>();
    tagList: string[] = new Array<string>();

    successTxt: string;
    existTxt: string;

    postForm: FormGroup;
    postFormErrors: any;

    ahlipet: AhliPet[];
    lenght:number;

    user2:User;
    attachment: PetAttachment;
    komenList:WallKomen[];

    id:any;
    wall: WallKandungan;
    name:string;
    imageSrc:any;
    createddate:Date;
    keterangan: string;
    userId: string;
    pet:string;
    petid:string;

    ahli:AhliPet;
    ahliUser:User;
    constructor(
        private _auth: AuthenticationService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _pet: PetService,
        private _petWall: WallPetService,
        private _wall: MyWallService,
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        public snackBar: MatSnackBar,
        public _dialog: MatDialog,
        private _filestorage: FileStorageService,
        private _dataservice: DataService,
        private _notifi:NotifikasiService,
        private _ahliPet: AhliPetService,
        private router: Router
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
        this._translate.get('WALL.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('WALL.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });


        this.user = this._auth.getCurrentUser(); 
    
    }
    ngOnInit(){
        this.id = this.route.snapshot.paramMap.get('id');
        this.getLatestTimeline()
    }

    getData(id){
        this.page.pageNumber = 0;
        this.page.size = 10;    
        
        this._id = this.route.snapshot.paramMap.get('id');
        this.postForm = this.formBuilder.group({
            post: ['', [Validators.required]],
            tagInput: ['']
        })
        this._pet.getPetById(this._id).subscribe(
            data=>{
                this.petObj = data;
                this.page.search = data.id;
                this.getLatestTimeline();
            }
        )
    }

    toggle(id: string){
        var documentid = 'komen-'+id
        var x = document.getElementById(documentid);

        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }

    }

    addTag(event: any){
        const input = event.input;
        const value = event.value;

        // Add tag
        if ( value )
        {
            this.tagList.push(value);
        }

        // Reset the input value
        if ( input )
        {
            input.value = '';
        }
    }

    removeTag(tag)
    {
        const index = this.tagList.indexOf(tag);
        if ( index >= 0 )
        {
            this.tagList.splice(index, 1);
        }
    }

    onCommentEvent(wall: any){
        
        if(this.user){
            let documentId = wall.id;
            var comment = (<HTMLInputElement>document.getElementById(documentId)).value;
            if(comment){
                let wallKomen: WallKomen = new WallKomen();
                wallKomen.setCreateddate(new Date);
                wallKomen.setKeterangan(comment);
                wallKomen.setCreatedby(this.user);
                wallKomen.setId(documentId)
                this._wall.createKomen(wallKomen).subscribe(
                    success=>{
                        this.snackBar.open(this.successTxt, "OK", {
                            panelClass: ['blue-snackbar']
                          });
                          (<HTMLInputElement>document.getElementById(documentId)).value = null;
                          this.getLatestTimeline();
                    },
                    error=>{
                        
                        this.snackBar.open(this.existTxt, "OK", {
                            panelClass: ['red-snackbar']
                          });
                    }
                )
                    this._pet.getPetById(this._id).subscribe(
                    data=>{
                        this.user = this._auth.getCurrentUser();
                        this._ahliPet.getByPetId(this._id).subscribe(
                            ahliPet=>{
                                this.ahlipet = ahliPet;
                                this.lenght = ahliPet.length;
                                 for(let i=0;i<this.length;i++){
                                    this.ahli = this.ahlipet[i];
                                    if(this.ahli){
                                        this.ahliUser = this.ahli.user;
                                            if(this.ahliUser.id != this.user.id){
                                            let noti: Notifikasi = new Notifikasi()
                                            
                                                noti.setCreatedby(this.user),
                                                noti.setUser(this.ahliUser),
                                                noti.setMaklumat("Comment in a post "+ data.namaSingkatan),
                                                noti.setStatus("New"),
                                                noti.setTindakan("/pet-post/post/"+documentId),
                                                noti.setModifiedby(this.user)
                                                
                                                this._notifi.createNoti(noti).subscribe(
                                                    success=>{

                                                    },
                                                    error=>{
                                                        
                                                    }
                                                )
                                            }
                                    }     
                                }
                            }
                        )
                    }
                )
            }
        }
    }

    deleteComment(id: string){
        let dialogRef = this._dialog.open(DeleteDialogComponent, {
            width: '500px',
            data: { 
                id: id,
                type: "komen"
            }
        });
        dialogRef.afterClosed().subscribe(
            data=>{
                this.getLatestTimeline();
            }
        )
    }
   
    editComment(id: string){
        let dialogRef = this._dialog.open(EditDialogComponent, {
            width: '750px',
            height: '350px',
            data: { 
                id: id,
                type: "komen"
            }
        });
        dialogRef.afterClosed().subscribe(
            data=>{
                this.getLatestTimeline();
            }
        )
    }

    deletePost(id: string){
        let dialogRef = this._dialog.open(DeleteDialogComponent, {
            width: '500px',
            data: { 
                id: id,
                type: "post"
            }
        });
        dialogRef.afterClosed().subscribe(
            data=>{
                this.getLatestTimeline();
                this.router.navigate(['/pet/woc/'+this.wall.petid]);
            }
        )
    }
    editPost(id: string){
        let dialogRef = this._dialog.open(EditDialogComponent, {
            width: '750px',
            height: '400px',
            data: { 
                id: id,
                type: "post"
            }
        });
        dialogRef.afterClosed().subscribe(
            data=>{
                this.getLatestTimeline();
            }
        )
    }

    getLatestTimeline(){
        this._id = this.route.snapshot.paramMap.get('id');  
        this._wall.getPostById(this.id).subscribe(
            data=>{
                this.wall = data;
                this.hashtagList = this.wall.hashtagLs;
                this.komenList = this.wall.wallKomenLs;
                this.user2 = this.wall.createdby;
                this.name = this.user2.name;
                this.imageSrc =  this.user2.image;
                this.userId = this.user2.id;
                this.createddate = this.wall.createddate;
                this.keterangan = this.wall.keterangan;
                this.id = this.wall.id;
                this.pet = this.wall.petname;
                this.petid = this.wall.petid;
            }
        )
        this._filestorage.getFilesByWallId(this.id).subscribe(
            dataAttachment =>{
                this.attachment = dataAttachment;
            }
        )
    }

    getNext(){
        if( this.length > this.timeline.length ){
            this.page.size += 10
            this._petWall.getPostAll(this.page).subscribe(
                data=>{
                    this.timeline = data.data
                }
            );
        }
        
    }

    streamFile(attachment){

        let a = document.createElement("a");
        document.body.appendChild(a);
        //a.style = "display: none"; 
        var dataFile = this.base64ToArrayBuffer(attachment.content);
        var blob = new Blob([dataFile], {type: attachment.formatType});
        var url= window.URL.createObjectURL(blob);
        a.href = url;
        a.download = attachment.filename;
        a.click();
        window.URL.revokeObjectURL(url);
    }

    base64ToArrayBuffer(base64) {
        var binaryString = window.atob(base64);
        var binaryLen = binaryString.length;
        var bytes = new Uint8Array(binaryLen);
        for (var i = 0; i < binaryLen; i++) {
           var ascii = binaryString.charCodeAt(i);
           bytes[i] = ascii;
        }
        return bytes;
    }
    goToPet(id :string){
        // this._id = this.route.snapshot.paramMap.get('id');  
        // this._wall.getPostById(this.id).subscribe(
        //     data=>{
        //         this.petid = data.petid;
        //         console.log(this.petid)
        //     }
        // )
        this.router.navigate(['/pet/woc/'+id]);
    }
}
 