import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule, 
    MatCheckboxModule,
    MatIconModule, 
    MatInputModule,
    MatChipsModule,
    MatFormFieldModule,
    MatMenuModule,
    MatSelectModule,
    MatDialogModule,
    MatStepperModule,
    MatSnackBarModule, 
    MatDividerModule,
    MatAutocompleteModule,
    MatOptionModule,
    MatDatepickerModule} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { WpetPostComponent } from './woc.component';
import { EditDialogComponent } from './editDialog.component';
import { DocumenDialogComponent } from './documenDialog.component';
import { DeleteDialogComponent } from './deleteDialog.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxEditorModule } from 'ngx-editor';


const routes = [
    {
        path     : 'post/:id',
        component: WpetPostComponent
    }
];

@NgModule({
    declarations: [
        WpetPostComponent,
        EditDialogComponent,
        DeleteDialogComponent,
        DocumenDialogComponent,
    ],
    entryComponents: [
        EditDialogComponent,
        DeleteDialogComponent,
        DocumenDialogComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatInputModule,
        
        MatStepperModule,
        MatFormFieldModule,
        MatChipsModule,
        MatMenuModule,
        MatDialogModule,
        MatSnackBarModule,
        MatSelectModule,
        MatDividerModule,
        MatAutocompleteModule,
        MatOptionModule,
        MatSelectModule,
        MatDatepickerModule,
        MatInputModule,
        NgxDatatableModule,
        NgxEditorModule,

        FuseSharedModule
    ],
    exports     : [
        WpetPostComponent
    ]
})
export class WpetPostModule
{
}