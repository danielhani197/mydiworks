export const locale = {
    lang: 'en',
    data: {
        'TERM': {
            'TITLE':'Term & Condition',
            'UPDATE_BY':'Update By',
            'DATE':'Date',
            'ERROR': 'Data Fail to Update',
            'UPDATE_BUT': 'Update',
            'CANCEL':'Cancel',
            'SEARCH':'Search',
            'VIEW':'View',
            'AGREE':'I am Agree'
        }
    }
};
