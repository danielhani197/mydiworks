export const locale = {
    lang: 'my',
    data: {
        'TERM': {
            'TITLE':'Terma & Syarat',
            'UPDATE_BY':'Kemaskini Oleh',
            'DATE':'Tarikh',
            'UPDATE_BUT':'Kemaskini',
            'CANCEL' : 'Batal',
            'ERROR': 'Gagal Kemaskini Data',
            'SUCCESS': 'Data Berjaya Di Kemaskini',
            'SEARCH' : 'Cari',
            'VIEW': 'Lihat',
            'AGREE':'Saya Setuju'
        }
    }
};
