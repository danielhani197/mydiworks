import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TermModule } from './term/term.module';
import { TermUpdateModule } from './term-update/term-update.module';

@NgModule({
    imports: [
        // Auth
        TermModule,
        TermUpdateModule,
        FormsModule
    ]
})
export class TermsModule
{
    
}
