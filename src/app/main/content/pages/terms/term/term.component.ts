import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute, Router } from "@angular/router";
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';
import { ToastrService } from 'ngx-toastr';
import { TermService } from 'app/main/content/services/setting/term.service';
import { Term } from 'app/main/content/model/term';
import { MatDialog} from '@angular/material';
import { Page } from 'app/main/content/model/util/page';
import { PagedData } from '../../../model/util/paged-data';
import { TermViewComponent } from './term-view.component';

@Component({
    selector   : 'term',
    templateUrl: './term.component.html',
    styleUrls  : ['./term.component.scss'],
    animations : fuseAnimations
})
export class TermComponent implements OnInit
{   
    data: Term[];
    rows: any[];
    temp = [];
    _id : string;
    terms: any[];
    _idToView : string;

    page = new Page();

    loadingIndicator = true;
    reorderable = true;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private http: HttpClient, 
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private route: ActivatedRoute,
        private _toastr: ToastrService,
        private _router: Router,
        private termService: TermService,
        public _dialog: MatDialog
        )
    {
        this.page.pageNumber = 0;
        this.page.size = 10;
        this.page.sortField = "date";
        this.page.sort = "desc";
        this.fuseTranslationLoader.loadTranslations(malay, english);
    }
    ngOnInit(){
        this.setPage({ offset: 0 });
    }

    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this.termService.getTermList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
                   
        }); 
    }

    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this.termService.getTermList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;
    
        this.termService.getTermList(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }
    
    update(){
        this._router.navigate(['/terms/update']);
    }
    view(id: string){
        this._idToView = id;
        let dialogRef = this._dialog.open(TermViewComponent, {
            width: '1000px',
            height: '550px',
            data: { id: this._idToView }
        });
        dialogRef.afterClosed().subscribe(result => {
            this.setPage({ offset: 0 });
        });
    }
}