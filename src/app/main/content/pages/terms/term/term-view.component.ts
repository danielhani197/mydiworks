import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router,  ActivatedRoute } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';

import { Term } from 'app/main/content/model/term';
import { TermService } from 'app/main/content/services/setting/term.service';

@Component({
    selector: 'term-view',
    templateUrl: 'term-view.component.html',
    animations : fuseAnimations
    })
    export class TermViewComponent implements OnInit
    {
        _id: string;
        term : Term;
        termForm :FormGroup;
        successTxt: string;
        termFormErrors: any;
        syarat: any;
        user: any;
        date: any;

        constructor(
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private formBuilder: FormBuilder, 
        private termService: TermService,
        private _toastr: ToastrService,
        private _router: Router,
        public _dialogRef: MatDialogRef<TermViewComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
        ) { 
            this.fuseTranslationLoader.loadTranslations(malay, english);
        }
        ngOnInit(){
            this.termForm = this.formBuilder.group({
                syarat : ['', [Validators.required]],
                username: ['', [Validators.required]],
                date: ['', [Validators.required]]
              });
         
              this.termForm.valueChanges.subscribe(() => {
                  this.onTermFormValuesChanged();
              });
         
              this._id = this.data.id;
              this.termForm = new FormGroup({
                  term : new FormControl()
              });
         
              if (this._id){
                this.termService.getTerm(this._id).subscribe(
                  term => {
                     this.term = term;
                     this.syarat = term.syarat;
                     this.user = term.user.name;
                     this.date = term.date;
                  }
                )
              }
            }
        onCloseConfirm() {
           this._dialogRef.close('CLOSE');
        }
        onTermFormValuesChanged(){
            for ( const field in this.termFormErrors )
            {
                if ( !this.termFormErrors.hasOwnProperty(field) )
                {
                    continue;
                }
     
                // Clear previous errors
                this.termFormErrors[field] = {};
     
                // Get the control
                const control = this.termForm.get(field);
     
                if ( control && control.dirty && !control.valid )
                {
                    this.termFormErrors[field] = control.errors;
                }
            }
        }
     }



    