import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule, 
    MatCheckboxModule,
    MatIconModule, 
    MatInputModule,
    MatChipsModule,
    MatFormFieldModule,
    MatMenuModule,
    MatDialogModule,
    MatSnackBarModule } from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxEditorModule } from 'ngx-editor';
import { FuseSharedModule } from '@fuse/shared.module';

import { TermComponent } from './term.component';

import { TermViewComponent } from './term-view.component';
const routes = [
    {
        path     : 'list',
        component: TermComponent
    }
];

@NgModule({
    declarations: [
        TermComponent,
        TermViewComponent
    ],
    entryComponents: [
        TermViewComponent
        ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatInputModule,
        NgxDatatableModule,
        NgxEditorModule,
        
        MatFormFieldModule,
        MatChipsModule,
        MatMenuModule,
        MatDialogModule,
        MatSnackBarModule,

        FuseSharedModule
    ],
    exports     : [
        TermComponent
    ]
})
export class TermModule
{
}