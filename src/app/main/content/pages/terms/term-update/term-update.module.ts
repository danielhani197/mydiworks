import { NgModule } from '@angular/core';
import { NgxEditorModule } from 'ngx-editor';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule, MatCheckboxModule, MatIconModule, MatInputModule } from '@angular/material';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { FuseSharedModule } from '@fuse/shared.module';

import { TermUpdateComponent } from './term-update.component';

const routes = [
    {
        path     : 'update',
        component: TermUpdateComponent
    }
];

@NgModule({
    declarations: [
        TermUpdateComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        TranslateModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatInputModule,
        NgxEditorModule,
        NgxDatatableModule,

        FuseSharedModule
    ],
    exports     : [
        TermUpdateComponent
    ]
})
export class TermUpdateModule
{
}