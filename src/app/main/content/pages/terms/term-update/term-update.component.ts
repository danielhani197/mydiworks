import { Component, OnInit } from '@angular/core';
import { Router,  ActivatedRoute } from "@angular/router";
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { MatSnackBar } from '@angular/material';
import { locale as english } from '../i18n/en';
import { locale as malay } from '../i18n/my';

import { TermService } from 'app/main/content/services/setting/term.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Term } from 'app/main/content/model/term';
import { FuseConfigService } from '@fuse/services/config.service';
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service';
import { TranslateService } from '@ngx-translate/core';



@Component({
    selector   : 'termUpdate',
    templateUrl: './term-update.component.html',
    styleUrls  : ['./term-update.component.scss'],
    animations : fuseAnimations
   
})
export class TermUpdateComponent implements OnInit
{
    // date = new FormControl(moment());
    _id : string;
    // selectedTerm : Term;
    termForm :FormGroup;

    rows: any[];
    termFormErrors: any;
    reorderable = true;

    successTxt: string;
    existTxt: string;

    term : Term;
    terms : any[];
    date: Date;
    dateConverter:string;

    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder, 
        private termService: TermService,
        private _router: Router,
        private route: ActivatedRoute,
        private _toastr: ToastrService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _translate: TranslateService,
        public snackBar: MatSnackBar,
        private autheticatonService : AuthenticationService
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
        
        this._translate.get('TERM.ERROR').subscribe((res: string) => {
            this.existTxt = res;
        });
        this._translate.get('TERM.SUCCESS').subscribe((res: string) => {
            this.successTxt = res;
        });

    }
    

    ngOnInit()
    {
        
        this.termService.latersTerm().subscribe(
            term => {
                this.term = term;
                this.termForm.setValue({
                    syarat: term.syarat
                });
                
            }
        )

        this.termForm = this.formBuilder.group({
            syarat           : ['', Validators.required],
        });

        this.termForm.valueChanges.subscribe(() => {
            this.onTermFormValuesChanged();
        });
        
    }
    onTermFormValuesChanged()
    {
        for ( const field in this.termFormErrors )
        {
            if ( !this.termFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.termFormErrors[field] = {};

            // Get the control
            const control = this.termForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.termFormErrors[field] = control.errors;
            }
        }
    }

    onSubmit(){
        
        //update term in the same term_id
        // if(this._id){
        //     let terms: Term = new Term();
        //     terms.setSyarat(this.termForm.controls['syarat'].value);
        //     terms.setUser(this.autheticatonService.getCurrentUser());
            
        //     terms.setId(this._id)

        //     this.termService.latersTerm().subscribe(
        //         success=>{
        //             this._toastr.success(this.successTxt, null, {
        //                 positionClass: 'toast-top-right'
        //             });
        //             this._router.navigate(['/terms/list']);
        //         },
        //         error => {
        //             this._toastr.error(this.existTxt, null, {
        //                 positionClass: 'toast-top-right'
        //             });
        //             this._router.navigate(['/terms/list']);
        //         }
        //     )

        // }else{ 
        // create new id term
        // this.date = new Date();
        // this.dateConverter = this.date.toDateString()
        console.log(this.dateConverter)
        let terms: Term = new Term();
            terms.setSyarat(this.termForm.controls['syarat'].value);
            terms.setUser(this.autheticatonService.getCurrentUser());
            //terms.setDate(this.dateConverter);
            
            this.termService.createTerm(terms).subscribe(
                success => {
                    
                    this.snackBar.open(this.successTxt, "OK", {
                        panelClass: ['blue-snackbar']
                      });
                    this._router.navigate(['/terms/list']);
                },
                error => {
                    this.snackBar.open(this.existTxt, "OK", {
                        panelClass: ['red-snackbar']
                      });
                }
            );
        
    }
    back(){
        this._router.navigate(['/terms/list']);
    }
}