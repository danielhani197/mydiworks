import { Component, OnInit, ViewChild } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as malay } from './i18n/my';

import { User } from 'app/main/content/model/user';
import { City } from 'app/main/content/model/city';

import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service'

import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from 'app/main/content/model/util/page';

@Component({
    selector   : 'fuse-sample',
    templateUrl: './sample.component.html',
    styleUrls  : ['./sample.component.scss']
})
export class FuseSampleComponent implements OnInit
{

    userLs: User[];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    rows: any[];
    temp = [];
    page = new Page();

    loadingIndicator = true;
    reorderable = true;

    constructor(
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _authentication: AuthenticationService
    )
    {
        this.fuseTranslationLoader.loadTranslations(malay, english);
        this.page.pageNumber = 0;
        this.page.size = 10;
        this.page.sortField = "name";
        this.page.sort = "asc";
    }

    ngOnInit(){
        this.setPage({ offset: 0 });
    }

    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._authentication.getCity(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

    onSort(event) {
        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._authentication.getCity(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;
    
        this._authentication.getCity(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
        // Whenever the filter changes, always go back to the first page
        //this.table.offset = 0;
    }

}
