import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { FuseSampleComponent } from './sample.component';

import { AuthGuard } from "app/main/content/guard/auth.guard";

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { 
    MatButtonModule, 
    MatCheckboxModule, 
    MatIconModule, 
    MatFormFieldModule, 
    MatInputModule,
} from '@angular/material';

const routes = [
    {
        path     : 'sample',
        component: FuseSampleComponent,
        canActivate: [AuthGuard],
    }
];

@NgModule({
    declarations: [
        FuseSampleComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule,
        NgxDatatableModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,MatFormFieldModule,MatInputModule,
    ],
    exports     : [
        FuseSampleComponent
    ]
})

export class FuseSampleModule
{
}
