import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Rx";
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service';
import { TokenService } from 'app/main/content/services/util/token.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
      private _router: Router,
      private _auth: AuthenticationService,
      private _token: TokenService,
      ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        let token = this._auth.getJwtToken();
        let currentUser = this._auth.getCurrentUser();

        if(token !== null && currentUser !==null){

          var status = this._token.isTokenExpired(token);

          if(status == true){
            this._router.navigate(['/auth/lock'], { queryParams: { returnUrl: state.url } });
            return false;
          }

          return true;
        }else{
          this._router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
          return false;
        }
    }
}
