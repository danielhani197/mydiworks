import { AbstractControl } from "@angular/forms";

export class PasswordValidation {

    static pattern: string = '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{11,}';

    static MatchPassword(AC: AbstractControl) {
       let password = AC.get('newpass').value; // to get value in input tag
       let confirmPassword = AC.get('confirmpass').value; // to get value in input tag
        if(password != confirmPassword) {
            AC.get('confirmpass').setErrors( {MatchPassword: true} )
        } else {
            return null
        }
    }
}