import { Component, OnInit, Inject } from '@angular/core';
import { Router,  ActivatedRoute } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { locale as english } from 'app/main/content/pages/terms/i18n/en';
import { locale as malay } from 'app/main/content/pages/terms/i18n/my';

import { TermService } from 'app/main/content/services/setting/term.service';

import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service';
import { PenggunaService } from 'app/main/content/services/pengguna/pengguna.service'
import { User } from '../model/user';

@Component({
    selector: 'term-view-login',
    templateUrl: 'term-view-login.component.html',
    styleUrls  : ['./term-view-login.component.scss'],
    animations : fuseAnimations
})
    
export class TermViewLoginComponent implements OnInit
{
    syarat: any;
    currentUser: User;

    isLogin: boolean = false;

    constructor(
        private _translate: TranslateService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private termService: TermService,
        public _dialogRef: MatDialogRef<TermViewLoginComponent>,
        private _auth: AuthenticationService,
        private _pengguna: PenggunaService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) 
    { 
        _dialogRef.disableClose = true;
        this.fuseTranslationLoader.loadTranslations(malay, english);
    }
    
    ngOnInit(){
        
        this.termService.latersTerm().subscribe(
            term => {
                this.syarat = term.syarat;
            }
        )

        this.currentUser = this._auth.getCurrentUser();
        if(this.currentUser){
            this.isLogin = true;
        }
    }

    onCloseConfirm() {
        if(this.currentUser){
            this._pengguna.updateUserTerm(this.currentUser.id).subscribe(
                success => {
                    //reset the value for current user
                    this.currentUser.termdate = new Date();
                    this._auth.setCurrentUser(this.currentUser);
                    this._dialogRef.close('CLOSE');
                }
            );
        }else{
            this._dialogRef.close('CLOSE');
        }
    }

}



    