import{User} from './user';
export class AppAuthority{

  public id: string;
  public remarks: string;
  public adminremarks: string;
  public status: string;
  public createdby: User;
  public modifiedby: User;
  public createdon: Date;
  public modifiedon: Date;
  public roleid: string;
  public user: User;
//   public authority: Authority;
 
    public getId(){
        return this.id;
    }

    public setId(id: string){
        this.id = id;
    }

    public getRemarks(){
        return this.remarks;
    }

    public setRemarks(remarks: string){
        this.remarks = remarks;
    }

    public getAdminRemarks(){
        return this.adminremarks;
    }

    public setAdminRemarks(adminremarks: string){
        this.adminremarks = adminremarks;
    }

    public getStatus(){
        return this.status;
    }

    public setStatus(status: string){
        this.status = status;
    }

    public getCreatedby(){
        return this.createdby;
    }
    
    public setCreatedby(createdby: User){
        this.createdby = createdby;
    }
    
    public getCreateddate(){
        return this.createdon;
    }
    
    public setCreateddate(createddate: Date){
        this.createdon = createddate;
    }
    
    public getModifiedBy(){
        return this.modifiedby;
    }
    
    public setModifiedby(modifiedby: User){
        this.modifiedby = modifiedby;
    }
    
    public getModifieddate(){
        return this.modifiedon;
    }
    
    public setModifieddate(modifieddate: Date){
        this.modifiedon = modifieddate;
    }

    public getRole(){
        return this.roleid;
    }

    public setRole(roleid: string){
        this.roleid = roleid;
    }

    public getUser(){
        return this.user;
    }
    
    public setUser(user: User){
        this.user = user;
    }

    



}