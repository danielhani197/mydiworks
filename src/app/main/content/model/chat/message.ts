import { User } from "../user";
import { Thread } from "./thread";

export class Message {
    public id        :    string;
    public sentAt    :    Date;
    public isRead    :    boolean;
    public text      :    string;
    public author    :    User;
    public thread    :    Thread;

    public setId(id: string){
        this.id = id;
    }

    public setSentAt(sentAt: Date){
        this.sentAt = sentAt;
    }

    public setIsRead(isRead: boolean){
        this.isRead = isRead;
    }

    public setText(text: string){
        this.text = text;
    }

    public setAuthor(author: User){
        this.author = author;
    }

    public setThread(thread: Thread){
        this.thread = thread;
    }
}