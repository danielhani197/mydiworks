export class ChatMessage {
    public who: string;
    public message: string;
    public time: string;

    public setWho(who: string){
        this.who = who;
    }

    public setMessage(message: string){
        this.message = message;
    }

    public setTime(time: string){
        this.time = time;
    }
}