import { Message } from "./message";
import { User } from "../user";


export class Thread {
    public id         :    string;
    public lastMessage:    Message;
    public user1      :    User;
    public user2      :    User;

    public setId(id: string){
        this.id = id;
    }

    public setLastMessage(lastMessage: Message){
        this.lastMessage = lastMessage;
    }

    public setUser1(user1: User){
        this.user1 = user1;
    }

    public setUser2(user2: User){
        this.user2 = user2;
    }
}