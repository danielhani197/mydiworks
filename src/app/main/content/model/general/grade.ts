export class Grade {

    public name: string;
    public seniority: number;
    public id: string;
    
    public setName(name: string){
        this.name = name;
    }

    public setSeniority(seniority: number){
        this.seniority = seniority;
    }

    public setId(id: string){
        this.id = id;
    }
}
  