import { Agensi } from './agensi';
import { Kementerian } from './kementerian';

export class Bahagian {

    public id: string;
    public name: string;
    public code: string;
    public agency: Agensi;
    public kementerian: Kementerian;
  
    public setId(id: string){
        this.id = id;
    }
    public setName(name: string){
        this.name = name;
    }
    public setCode(code: string){
        this.code = code;
    }
    public setAgensi(agency: Agensi){
        this.agency = agency;
    }
    public setKementerian(kementerian: Kementerian){
        this.kementerian = kementerian;
    }
}