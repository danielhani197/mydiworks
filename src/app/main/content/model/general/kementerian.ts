import { Bandar } from '../ref/bandar';
import { Negeri } from '../ref/negeri';

export class Kementerian {

    public name: string;
    public code: string;
    public phoneNo: string;
    public address: string;
    public city: Bandar;
    public state: Negeri;
    public postcode: number;
    public urlPortal: string;
    public id: string;

    public getName(){
        return this.name;
    }

    public setName(name: string){
        this.name = name;
    }

    public getCode(){
        return this.code;
    }

    public setCode(code: string){
        this.code = code;
    }

    public getPhoneNo(){
        return this.phoneNo;
    }

    public setPhoneNo(phoneNo: string){
        this.phoneNo = phoneNo;
    }

    public getCity(){
        return this.city;
    }

    public setCity(city: Bandar){
        this.city = city;
    }

    public getState(){
        return this.state;
    }

    public setState(state: Negeri){
        this.state = state;
    }

    public getPostcode(){
        return this.postcode;
    }

    public getAddress(){
        return this.address;
    }

    public setAddress(address: string){
        this.address = address;
    }

    public setPostcode(postcode: number){
        this.postcode = postcode;
    }

    public getUrlPortal(){
        return this.urlPortal;
    }

    public setUrlPortal(urlPortal: string){
        this.urlPortal = urlPortal;
    }

    public getId() {
        return this.id;
    }

    public setId(id: string) {
        this.id = id;
    }
}