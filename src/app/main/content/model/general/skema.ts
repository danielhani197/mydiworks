export class Skema {
    
    public name: string;
    public seniority: number;
    public id: string;
    public keterangan: string;
    
    public setName(name: string){
        this.name = name;
    }

    public setSeniority(seniority: number){
        this.seniority = seniority;
    }

    public setId(id: string){
        this.id = id;
    }

    public setKeterangan(keterangan: string){
        this.keterangan = keterangan;
    }
}