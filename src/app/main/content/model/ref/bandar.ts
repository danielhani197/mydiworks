export class Bandar {

    public id: string;
    public name: string;
    public negeri_id: string;
  
    constructor(id:string, name:string, negeri_id:string) {
        this.id = id;
        this.name = name;
        this.negeri_id = negeri_id;
    }
}