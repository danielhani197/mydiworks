export class Taksonomi {

    public id: string;
    public keterangan: string;
    public level: string;
    public parentTax: Taksonomi;
    public baseTax: Taksonomi;

    public setId(id: string){
        this.id = id;
    }

    public setKeterangan(keterangan: string){
        this.keterangan = keterangan;
    }

    public setLevel(level: string){
        this.level = level;
    }

    public setParentTax(parentTax: Taksonomi){
        this.parentTax = parentTax;
    }

    public setBaseTax(baseTax: Taksonomi){
        this.baseTax = baseTax;
    }
}