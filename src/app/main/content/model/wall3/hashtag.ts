import { WallKomen } from "./wallKomen";
import { WallKandungan } from "./wallKandungan";

export class Hashtag {
    
    public id: string;
    public nama: string;
    public wallKandungan: WallKandungan;

    public setId(id: string){
        this.id = id;
    }

    public setName(nama: string){
        this.nama = nama;
    }

    public setWallKandungan(wallKandungan: WallKandungan){
        this.wallKandungan = wallKandungan;
    }
    
}