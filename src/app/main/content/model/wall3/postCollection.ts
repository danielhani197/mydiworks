import { WallAttachment } from "./wallAttachment";
import { WallKandungan } from "./wallKandungan";

export class PostCollection {
    
    public wallAttachment: WallAttachment;
	public wallKandungan: WallKandungan;

    public setWallAttachment(wallAttachment: WallAttachment){
        this.wallAttachment = wallAttachment;
    }

    public setWallKandungan(wallKandungan: WallKandungan){
        this.wallKandungan = wallKandungan;
    }
}