import { User } from "../user";
import { Hashtag } from "./hashtag";
import { WallKomen } from "./wallKomen";

export class WallKandungan {
    
    public id: string;
    public keterangan: string;
    public hashtagLs: Hashtag[]
    public wallKomenLs: WallKomen[];
    public category: string;
    public createddate: Date;
    public createdby: User;
    public modifieddate: Date;
    public modifiedby: User;
    public userid: string;
    public bahagianid: string;
    public petid: string;
    public petname: string;

    public setId(id: string){
        this.id = id;
    }

    public setKeterangan(keterangan: string){
        this.keterangan = keterangan;
    }

    public setHashtagLs(hashtagLs: Hashtag[]){
        this.hashtagLs = hashtagLs;
    }
    
    public setWallKomenLs(wallKomenLs: WallKomen[]){
        this.wallKomenLs = wallKomenLs;
    }

    public setCategory(category: string){
        this.category = category;
    }

    public setCreateddate(createddate: Date){
        this.createddate = createddate;
    }

    public setCreatedby(createdby: User){
        this.createdby = createdby;
    }

    public setModifieddate(modifieddate: Date){
        this.modifieddate = modifieddate;
    }

    public setModifiedby(modifiedby: User){
        this.modifiedby = modifiedby;
    }

    public setUserid(userid: string){
        this.userid = userid;
    }

    public setBahagianid(bahagianid: string){
        this.bahagianid = bahagianid;
    }

    public setPetid(petid: string){
        this.petid = petid;
    }

    public setPetname(petname: string){
        this.petname = petname;
    }
}