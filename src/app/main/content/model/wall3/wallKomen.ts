import { User } from "../user";
import { WallKandungan } from "./wallKandungan";

export class WallKomen {
    
    public id: string;
    public keterangan: string;
    public createdby: User;
    public createddate: Date;
    public wallKandungan: WallKandungan;
    public modifiedby: User;
    public modifieddate: Date;

    public setId(id: string){
        this.id = id;
    }

    public setKeterangan(keterangan: string){
        this.keterangan = keterangan;
    }

    public setCreatedby(createdby: User){
        this.createdby = createdby;
    }

    public setCreateddate(createddate: Date){
        this.createddate = createddate;
    }

    public setWallKandungan(wallKandungan: WallKandungan){
        this.wallKandungan = wallKandungan;
    }

    public setModifiedby(modifiedby: User){
        this.modifiedby = modifiedby;
    }

    public setModifieddate(modifieddate: Date){
        this.modifieddate = modifieddate;
    }
}