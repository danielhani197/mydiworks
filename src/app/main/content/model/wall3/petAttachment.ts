import { User } from "../user";
import { WallKandungan } from "./wallKandungan";
import { Taksonomi } from "./taksonomi";

export class PetAttachment {
    
    public id: string;
    public tajuk: string;
    public keterangan: string;
    public peristiwa: string;
    public peristiwadate: Date;
    public createddate: Date;
    public createdby: User;
    public userid: string;
    public status: string;
    public jenisKandungan: string;
    public modifieddate: Date;
    public modifiedby: User;
    public lokasi: string;
    public filename: string;
    public wallKandungan: WallKandungan;
    public instanceid: string;
    public taksonomi: Taksonomi;
    public lockeddate: Date;
    public lockedby: User;

    public setId(id: string){
        this.id = id;
    }

    public setTajuk(tajuk: string){
        this.tajuk = tajuk;
    }

    public setKeterangan(keterangan: string){
        this.keterangan = keterangan;
    }

    public setPeristiwa(peristiwa: string){
        this.peristiwa = peristiwa;
    }

    public setPeristiwadate(peristiwadate: Date){
        this.peristiwadate = peristiwadate;
    }

    public setCreateddate(createddate: Date){
        this.createddate = createddate;
    }

    public setCreatedby(createdby: User){
        this.createdby = createdby;
    }

    public setModifieddate(modifieddate: Date){
        this.modifieddate = modifieddate;
    }

    public setModifiedby(modifiedby: User){
        this.modifiedby = modifiedby;
    }

    public setLokasi(lokasi: string){
        this.lokasi = lokasi;
    }

    public setInstanceid(instanceid: string){
        this.instanceid = instanceid;
    }

    public setFilename(filename: string){
        this.filename = filename;
    }

    public setTaksonomi(taksonomi: Taksonomi){
        this.taksonomi = taksonomi;
    }

    public setWallKandungan(wallKandungan: WallKandungan){
        this.wallKandungan = wallKandungan;
    }

    public setUserid(userid: string){
        this.userid = userid;
    }

    public setStatus(status: string){
        this.status = status;
    }

    public setJenisKandungan(jenisKandungan: string){
        this.jenisKandungan = jenisKandungan;
    }

    public setLockeddate(lockeddate: Date){
        this.lockeddate = lockeddate;
    }

    public setLockedby(lockedby: User){
        this.lockedby = lockedby;
    }
}