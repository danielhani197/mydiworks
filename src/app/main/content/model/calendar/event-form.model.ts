import { CalendarEventAction } from 'angular-calendar';
import { startOfDay, endOfDay } from 'date-fns';
import { Calendar } from 'app/main/content/model/calendar/calendar';

export class CalendarEventModel
{
    id: string;
    start: Date;
    end?: Date;
    start_datetime: Date;
    end_datetime?: Date;
    title: string;
    // calendar: Calendar;
    // color: {
    //     primary: string;
    //     secondary: string;
    // };
    // actions?: CalendarEventAction[];
    allDay?: boolean;
    
    // cssClass?: string;
    resizable?: {
        beforeStart?: boolean;
        afterEnd?: boolean;
    };
    draggable?: boolean;
    // meta?: {
    //     location: string,
    //     notes: string
    // };

    location?: string;
    notes?: string;
    calendarId?: string;
    calendarName?: string;
    calendarKind?: string;


    constructor(data?)
    {
        data = data || {};
        this.id = data.id || '';
        this.start = new Date(data.start_datetime) || startOfDay(new Date());
        this.end = new Date(data.end_datetime) || endOfDay(new Date());
        this.start_datetime = this.start;
        this.end_datetime = this.end;
        this.title = data.title || '';
        // this.calendar = data.calendar || '';
        // this.color = {
        //     primary  : data.calendar.colorcode || '#1e90ff',
        //     secondary: data.color && data.color.secondary || '#D1E8FF'
        // };
        this.draggable = data.draggable || true;
        this.resizable = {
            beforeStart: data.resizable && data.resizable.beforeStart || true,
            afterEnd   : data.resizable && data.resizable.afterEnd || true
        };
        // this.actions = data.actions || [];
        this.allDay = data.allday || false;
        // this.cssClass = data.cssClass || '';
        // this.meta = {
        //     location: data.meta && data.meta.location || '',
        //     notes   : data.meta && data.meta.notes || ''
        // };
        this.location = data.location || '';
        this.notes = data.notes || '';
        this.calendarId = data.calendarId || '';
        this.calendarName = data.calendarName || '';
        this.calendarKind = data.calendarKind || '';
    }
}
