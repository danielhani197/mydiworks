import { Calendar } from "./calendar";
import { User } from "../user";
import { Bahagian } from "../general/bahagian";
import { Agensi } from "../general/agensi";
import { Kementerian } from "../general/kementerian";
import { Pet } from "../pet/pet";

export class Event {

    public id: string;
    public title: string;
    public start: Date;
    public end: Date;
    public location: string;
    public notes: string;
    public user: User;
    public bahagian: Bahagian;
    public agensi: Agensi;
    public kementerian: Kementerian;
    public pet: Pet;
    public kind: string;
    public pic: User;
    public allDay: boolean;

    public setId(id: string){
        this.id = id;
    }
    public setTitle(title: string){
        this.title = title;
    }
    public setStart(start: Date){
        this.start = start;
    }
    public setEnd(end: Date){
        this.end = end;
    }
    public setLocation(location: string){
        this.location = location;
    }
    public setNote(notes: string){
        this.notes = notes;
    }
    public setUser(user: User){
        this.user = user;
    }
    public setBahgian(bahagian: Bahagian){
        this.bahagian = bahagian;
    }
    public setAgensi(agensi: Agensi){
        this.agensi = agensi;
    }
    public setKementerian(kementerian: Kementerian){
        this.kementerian = kementerian;
    }
    public setPet(pet: Pet){
        this.pet = pet;
    }
    public setKind(kind: string){
        this.kind = kind;
    }
    public setPic(pic: User){
        this.pic = pic;
    }
    public setAllDay(allDay: boolean){
        this.allDay = allDay
    }
}