import { User } from "../user";
import { Pet } from "../pet/pet";
import { Kementerian } from "../general/kementerian";
import { Agensi } from "../general/agensi";
import { Bahagian } from "../general/bahagian";

export class Calendar {

	public id: string;
	public user: User;
	public pet: Pet;
	public kementerian: Kementerian;
	public agensi: Agensi;
	public bahagian: Bahagian;
	public name: string;
	public colorcode: string;
	public kind: string; //USR - User's Calendar, PET - Calendar

		//public event: Event[];

	public setId(id: string) {
		this.id = id;
  	}

	public setUser(user: User) {
		this.user = user;
	}

	public setPet(pet: Pet){
		this.pet = pet;
	}

	public setKementerian(kementerian: Kementerian){
		this.kementerian = kementerian;
	}

	public setAgensi(agensi: Agensi){
		this.agensi = agensi;
	}

	public setBahagian(bahagian: Bahagian){
		this.bahagian = bahagian;
	}

	public setName(name: string) {
		this.name = name;
    }

	public setColorcode(colorcode: string) {
		this.colorcode = colorcode;
	}
	
	public setKind(kind: string) {
		this.kind = kind;
  }
}