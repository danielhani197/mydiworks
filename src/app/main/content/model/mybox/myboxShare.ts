import { User } from "../user";
import { MyBoxAttachment } from "./myboxAttachment";

export class MyBoxShare{

    public id: String;
    public createdate: Date;
    public shared: User;
    public owner: User;
    public file: MyBoxAttachment;

    public getId(){
        return this.id;
    }

    public setId(id: String){
        this.id = id;
    }

    public getShared(){
        return this.shared;
    }
    public setShared(shared: User){
        this.shared = shared;
    }

    public getCreatedDate(){
        return this.createdate;
    }

    public setCreateDate( createdate: Date){
        this.createdate = createdate;
    }

    public getOwner(){
        return this.owner;
    }

    public setOwner(owner: User){
        this.owner = owner;
    }

    public getFile(){
        return this.file;
    }

    public setFile(file: MyBoxAttachment){
        this.file = file;
    }

}