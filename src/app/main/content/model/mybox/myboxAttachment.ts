import { User } from "../user";

export class MyBoxAttachment {
    
    public id: string;   
    public createddate: Date;
    public createdby: User;
    public modifieddate: Date;
    public modifiedby: User;
    public filename: string;
    public instanceid: string;
    public userid: string;
    public folderName: string;
    public status:string;
    public fileSize: number;   
    public parentid :string;
    public type: string;
    public share: User;

    public setId(id: string){
        this.id = id;
    }

    public setCreateddate(createddate: Date){
        this.createddate = createddate;
    }

    public setCreatedby(createdby: User){
        this.createdby = createdby;
    }

    public setModifieddate(modifieddate: Date){
        this.modifieddate = modifieddate;
    }

    public setModifiedby(modifiedby: User){
        this.modifiedby = modifiedby;
    }

    public setFilename(filename: string){
        this.filename = filename;
    }

    public setFoldername (folderName: string){
        this.folderName = folderName;
    }

    public setInstanceid(instanceid: string){
        this.instanceid = instanceid;
    }
    
    public setUserid(userid: string){
        this.userid = userid;
    }

    public getStatus(){
        return status;
    }

    public setStatus(status: string){
        this.status = status;
    }

    public setFileSize(fileSize: number){
        this.fileSize = fileSize;
    }

    public setParentId(parentid: string){
        this.parentid = parentid;
    }

    public setType(type: string){
        this.type = type;
    }

    public setShare (share: User){
        this.share = share;
    }
    
}