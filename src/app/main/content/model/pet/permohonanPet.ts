import { Agensi } from '../general/agensi'; 
import { Pet } from './pet';
import { User } from '../user';

export class PermohonanPet { 
 
    public id: String; 
    public tarikhPermohonan: Date; 
    public tarikhMelulus: Date; 
    public pemohon: User; 
    public pelulus: User; 
    public statusPermohonan: number; 
    public catatan: String; 
    public pet: Pet;
 
    public getId() { 
    return this.id; 
    } 
 
    public setId(id: String) { 
    this.id = id; 
    } 
     
    public getPelulus() { 
    return this.pelulus; 
    } 
 
    public setPelulus(pelulus: User) { 
    this.pelulus = pelulus; 
    } 
 
    public getTarikhPermohonan() { 
    return this.tarikhPermohonan; 
    } 
 
    public setTarikhPermohonan( tarikhPermohonan: Date) { 
    this.tarikhPermohonan = tarikhPermohonan; 
    } 
 
    public getTarikhMelulus() { 
    return this.tarikhMelulus; 
    } 
 
    public setTarikhMelulus( tarikhMelulus: Date ) { 
    this.tarikhMelulus = tarikhMelulus; 
    } 
 
    public getPemohon() { 
    return this.pemohon; 
    } 
 
    public setPemohon( pemohon: User) { 
    this.pemohon = pemohon; 
    }
 
    public getStatusPermohonan() { 
    return this.statusPermohonan; 
    } 
 
    public setStatusPermohonan(statusPermohonan: number) { 
    this.statusPermohonan = statusPermohonan ; 
    } 
 
    public getCatatan() { 
    return this.catatan; 
    } 
 
    public setCatatan(catatan: String) { 
    this.catatan = catatan; 
    } 

    public getPet(){
        return this.pet;
    }

    public setPet(pet: Pet){
        this.pet=pet;
    }
}