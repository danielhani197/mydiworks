import { Agensi } from '../general/agensi';
import {AhliPet} from './ahliPet';
import { Kementerian } from '../general/kementerian';
import { Bahagian } from '../general/bahagian';

export class Pet {

  public id: string;
  public nama: string;
  public namaSingkatan: string;
  public tujuan: string;
  public keterangan: string;
  public catatan: string;
  public jenis: string;
  public status: string;
  public ahliPet: AhliPet[];
  public agensi: Agensi;
  public kementerian: Kementerian;
  public bahagian: Bahagian;
  public image:any;
  public enabled:  boolean;
  public createddate: Date;

  public getId(){
    return this.id;
  }

  public setId(id: string){
    this.id = id;
  }

  public getNama(){
    return this.nama;
  }

  public setNama(nama: string){
    this.nama = nama;
  }

  public getNamaSingkatan(){
    return this.namaSingkatan;
  }

  public setNamaSingkatan(namaSingkatan: string){
    this.namaSingkatan = namaSingkatan;
  }

  public getTujuan(){
    return this.tujuan;
  }

  public setTujuan(tujuan: string){
    this.tujuan = tujuan;
  }

  public getKeterangan(){
    return this.keterangan;
  }

  public setKeterangan(keterangan: string){
    this.keterangan = keterangan;
  }

  public getCatatan(){
    return this.catatan;
  }

  public setCatatan(catatan: string){
    this.catatan = catatan;
  }

  public getJenis(){
    return this.jenis;
  }

  public setJenis(jenis: string){
    this.jenis = jenis;
  }

  public getStatus(){
    return this.status;
  }

  public setStatus(status: string){
    this.status = status;
  }

  public getAhliPet(){
    return this.ahliPet;
  }

  public setAhliPet(ahliPet: AhliPet[]){
    this.ahliPet = ahliPet
  }
  
  public getAgensi(){
    return this.agensi;
  }

  public setAgensi(agensi: Agensi){
    this.agensi = agensi;
  }

  public getKementerian(){
    return this.kementerian;
  }

  public setKementerian(kementerian: Kementerian){
    this.kementerian = kementerian;
  }

  public getBahagian(){
    return this.bahagian;
  }

  public setBahagian(bahagian: Bahagian){
    this.bahagian = bahagian;
  }
  
  public getImage() {
		return this.enabled;
	}

	public setImage(image: any) {
		this.image = image;
    }
    
  public getCreateddate(){
    return this.createddate;
  }

  public setCreateddate(createddate: Date){
    this.createddate = createddate;
  }
}
