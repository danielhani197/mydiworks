import{Pet} from '../pet/pet'
import{User} from '../user'
import { PetAuthorities } from './petAuthorities';

export class AhliPet{

    public id: string;
    public petAuthorities: PetAuthorities;
    public pet:Pet;
    public user: User;
    public tempPet: Pet;
    public status: String;


    public getId(){
        return this.id;
    }
    
    public setId(id: string){
        this.id = id;
    }
    
    public getPet(){
        return this.pet;
    }
    
    public setPet(pet: Pet){
        this.pet = pet;
    }

    public getUser(){
        return this.user;
    }
    
    public setUser(user: User){
        this.user = user;
    }
     
    public getPetAuthoritiesn(){
        return this.petAuthorities;
    }
    
    public setPetAuthorities(petAuthorities: PetAuthorities){
        this.petAuthorities = petAuthorities;
    }

    public setTempPet(tempPet: Pet){
        this.tempPet = tempPet;
    }

    public getStatus(){
        return this.status;
    }

    public setStatus( status: String){
        this.status = status;
    }
    
}