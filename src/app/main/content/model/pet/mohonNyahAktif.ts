import { Pet } from './pet';
import { User } from '../user';

export class MohonNyahAktif {
    public id: string;
    public catatan: string;
    public tarikhMohon: Date;
    public tarikhLulus: Date;
    public pemohon: User;
    public pet: Pet;

    public getId() {
        return this.id;
    }

    public setId(id: string) {
        this.id = id;
    }

    public getCatatan() {
        return this.catatan;
    }

    public setCatatan(catatan: string) {
        this.catatan = catatan;
    }

    public getTarikhMOhon() {
        return this.tarikhMohon;
    }

    public setTarikhMohon(tarikhPeMohon: Date) {
        this.tarikhMohon = tarikhPeMohon;
    }

    public getTarikhLulus() {
        return this.tarikhLulus;
    }

    public setTarikhMelulus(tarikhLulus: Date) {
        this.tarikhLulus = tarikhLulus;
    }
    public getPemohon() {
        return this.pemohon;
    }

    public setPemohon(pemohon: User) {
        this.pemohon = pemohon;
    }

    public getPet() {
        return this.pet;
    }

    public setPet(pet: Pet) {
        this.pet = pet;
    }
}