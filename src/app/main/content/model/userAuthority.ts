import { User } from "./user";
import { Authorities } from "./authorities";

export class UserAuthority{
    public id:string;
    public user: User;
    public auth: Authorities;
    public createdon: Date;
    public createby: string;

    public getId(){
        return this.id;
    }

    public setId(id:string){
        this.id= id;
    }

    public getUser(){
        return this.user;
    }

    public setUser(user: User){
        this.user = user;
    }

    public getAuth(){
        return this.auth;
    }

    public setAuth(auth: Authorities){
        this.auth = auth;
    }

    public getCreatedOn(){
        return this.createdon;
    }

    public setCreatedOn(createdon: Date){
        this.createdon = createdon;
    }

    public getCreateBy(){
        return this.createby;
    }

    public setCreateBy(createby: string){
        this.createby = createby;
    }

}