import { User } from './user';
import { MyBoxShare } from './mybox/myboxShare';
 
export class Notifikasi {
    
    public id: string;
    public user: User;
    public tindakan: string;
    public createdby: User ;
    public createddate: Date;
    public modifiedby: User;
    public modifieddate: Date;
    public maklumat: string;
    public status: string;
    public share: MyBoxShare;

    public getId(){
        return this.id;
    }

    public setId(id : string){
        this.id = id;
    }

    public getUser(){
        return this.user;
    }

    public setUser(user: User){
        this.user = user;
    }

    public getCreatedby(){
        return this.createdby;
    }

    public setCreatedby(createdby: User){
        this.createdby = createdby;
    }

    public getCreateddate(){
        return this.createddate;
    }

    public setCreateddate(createddate: Date){
        this.createddate = createddate;
    }

    public getModifiedBy(){
        return this.modifiedby;
    }

    public setModifiedby(modifiedby: User){
        this.modifiedby = modifiedby;
    }

    public getModifieddate(){
        return this.modifieddate;
    }

    public setModifieddate(modifieddate: Date){
        this.modifieddate = modifieddate;
    }

    public getMaklumat(){
        return this.maklumat;
    }

    public setMaklumat(maklumat: string){
        this.maklumat = maklumat;
    }

    public getStatus(){
        return this.status;
    }

    public setStatus(status: string){
        this.status = status;
    }
    public getTindakan(){
        return this.tindakan;
    }

    public setTindakan(tindakan: string){
        this.tindakan = tindakan;
    }

    public getShare(){
        return this.tindakan;
    }

    public setShare(share: MyBoxShare){
        this.share = share;
    }

}