import { User } from './user';

export class Term {

    public id: String;
		public syarat: String;
		public user : User;
		public date: string;

    public getId() {
		return this.id;
	}

	public setId(id: String) {
		this.id = id;
    }
    
    public getSyarat() {
		return this.syarat;
	}

	public setSyarat(syarat: String) {
		this.syarat = syarat;
	}

	public getUser(){
		return this.user;
	}

	public setUser(user: User ){
		this.user = user;
	}

	public getDate(){
		return this.date;
	}

	public setDate(date : string){
		this.date = date;
	}
}
