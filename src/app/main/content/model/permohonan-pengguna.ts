import { Kementerian } from "app/main/content/model/general/kementerian";
import { Agensi } from "app/main/content/model/general/agensi";

export class PermohonanPengguna {

	public id: string;
	public username: string;
	public name: string;
	public tujuan: string;
	public email: string;
	public kementerian: Kementerian;
	public agensi: Agensi;

	public getId() {
		return this.id;
	}

	public setId(id: string) {
		this.id = id;
  }

  public getUsername() {
		return this.username;
	}

	public setUsername(username: string) {
		this.username = username;
  }

  public getName() {
		return this.name;
	}

	public setName(name: string) {
		this.name = name;
  }

  public getEmail() {
		return this.id;
	}

	public setEmail(email: string) {
		this.email = email;
  }
		
	public getKementerian() {
		return this.kementerian;
	}

	public setKementerian(kementerian: Kementerian) {
		this.kementerian = kementerian;
	}

	public getAgensi() {
		return this.agensi;
	}

	public setAgensi(agensi: Agensi) {
		this.agensi = agensi;
	}
	public getTujuan() {
		return this.tujuan;
	}

	public setTujuan(tujuan: string) {
		this.tujuan = tujuan;
	}
}
