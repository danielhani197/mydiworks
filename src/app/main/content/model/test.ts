export class Test {

    public id: string;
    public username: string;

    public getId() {
		return this.id;
	}

	public setId(id: string) {
		this.id = id;
    }
    
    public getUsername() {
		return this.username;
	}

	public setUsername(username: string) {
		this.username = username;
	}
}
