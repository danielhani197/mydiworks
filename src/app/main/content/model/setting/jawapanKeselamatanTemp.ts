export class JawapanKeselamatanTemp {

  public id: string;
  public jawapan: string;
  public rank : string;
  public userId: string;
  public soalanKeselamatanId: string;

  public setId(id: string) {
      this.id = id;
  }

  public setJawapan(jawapan: string){
      this.jawapan = jawapan;
  }

  public setRank(rank: string) {
    this.rank = rank;
  }

  public setUserId(userId: string) {
    this.userId = userId;
  }

  public setSoalanKeselamatanId(soalanKeselamatanId: string) {
    this.soalanKeselamatanId = soalanKeselamatanId;
  }
}
