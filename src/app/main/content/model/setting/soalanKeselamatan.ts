export class SoalanKeselamatan {

  public id: string;
  public keterangan: string;

  public getId() {
      return this.id;
  }

  public setId(id: string) {
      this.id = id;
  }

  public getKeterangan(){
      return this.keterangan;
  }
  public setKeterangan(keterangan: string){
      this.keterangan = keterangan;
  }
}
