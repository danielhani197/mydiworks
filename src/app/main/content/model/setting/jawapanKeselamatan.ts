import {SoalanKeselamatan} from '../setting/soalanKeselamatan';
import { User } from '../user';

export class JawapanKeselamatan {

  public id: string;
  public jawapan: string;
  public rank : string;
  public user: User;
  public soalanKeselamatan: SoalanKeselamatan;

  public getId() {
      return this.id;
  }

  public setId(id: string) {
      this.id = id;
  }

  public getJawapan(){
      return this.jawapan;
  }

  public setJawapan(jawapan: string){
      this.jawapan = jawapan;
  }

  public getRank() {
    return this.rank;
  }

  public setRank(rank: string) {
    this.rank = rank;
  }

  public getUser(){
    return this.user;
  }

  public setUser(user: User){
    this.user = user;
  }

  public getSoalanKeselamatan(){
      return this.soalanKeselamatan;
  }

  public setSoalanKeselamatan(soalanKeselamatan: SoalanKeselamatan){
      this.soalanKeselamatan = soalanKeselamatan;
  }
}
