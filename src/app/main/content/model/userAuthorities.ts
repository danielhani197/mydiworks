import { User } from "./user";
import { Authority } from "./authority";

export class UserAuthorities {
    id: string;
    user: User;
    authority: Authority;
    createdby: string;
    createdon: Date;

    public setId(id: string){
        this.id = id;
    }

    public setUser(user: User){
        this.user = user;
    }

    public setAuthority(authority: Authority){
        this.authority = authority;
    }

    public setCreatedby(createdby: string){
        this.createdby = createdby;
    }

    public setCreatedon(createdon: Date){
        this.createdon = createdon;
    }
}