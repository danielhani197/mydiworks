import { Kementerian } from "app/main/content/model/general/kementerian";
import { Agensi } from "app/main/content/model/general/agensi";
import { Bahagian } from "app/main/content/model/general/bahagian";
import { Skema } from "../model/general/skema";
import { Grade } from "../model/general/grade";
import { Bandar } from "./ref/bandar";
import { Negeri } from "./ref/negeri";
import { Authorities } from "./authorities";

export class User {

    public id: string;
    public username: string;
    public name: string;
    public email: string;
    public newPassword: string;
    public oldPassword: string;
    public address: string;
    public position: string;
    public postcode: number;
    public phoneNo: string;
    public enabled: boolean;
		public image: any;
		public kementerian: Kementerian;
		public agensi: Agensi;
		public bahagian: Bahagian;
		public termdate: Date;
		public isFirstLogin: boolean;
		public userEmail: string;
		public skema: Skema;
		public grade: Grade;
		public bandar: Bandar;
		public negeri: Negeri;
		public authorities: Authorities[];
		public roles: string[];

    public getId() {
		return this.id;
	}

	public setId(id: string) {
		this.id = id;
    }

    public getUsername() {
		return this.username;
	}

	public setUsername(username: string) {
		this.username = username;
    }

    public getName() {
		return this.name;
	}

	public setName(name: string) {
		this.name = name;
    }

    public getEmail() {
		return this.id;
	}

	public setEmail(email: string) {
		this.email = email;
    }

    public getNewPassword() {
		return this.id;
	}

	public setNewPassword(newPassword: string) {
		this.newPassword = newPassword;
    }

    public getOldPassword() {
		return this.oldPassword;
	}

	public setOldPassword(oldPassword: string) {
		this.oldPassword = oldPassword;
    }

    public getAddress() {
		return this.address;
	}

	public setAddress(address: string) {
		this.address = address;
    }

    public getPosition() {
		return this.position;
	}

	public setPosition(position: string) {
		this.position = position;
    }

    public getPostcode() {
		return this.postcode;
	}

	public setPostcode(postcode: number) {
		this.postcode = postcode;
    }

    public getPhoneNo() {
		return this.phoneNo;
	}

	public setPhoneNo(phoneNo: string) {
		this.phoneNo = phoneNo;
    }

    public getEnabled() {
		return this.enabled;
	}

	public setEnabled(enabled: boolean) {
		this.enabled = enabled;
    }

    public getImage() {
		return this.enabled;
	}

	public setImage(image: any) {
		this.image = image;
		}
		
	public getKementerian() {
		return this.kementerian;
	}

	public setKementerian(kementerian: Kementerian) {
		this.kementerian = kementerian;
	}

	public setAgensi(agensi: Agensi) {
		this.agensi = agensi;
	}

	public setBahagian(bahagian: Bahagian) {
		this.bahagian = bahagian;
	}

	public setTermdate(termdate: Date) {
		this.termdate = termdate;
	}

	public setFirstLogin(firstLogin: boolean) {
		this.isFirstLogin = firstLogin;
	}

	public getUserEmail(){
		return this.userEmail;
	}

	public setUserEmail( userEmail: string){
		this.userEmail = userEmail;
	}

	public getSkema(){
		return this.skema;
	}

	public setSkema(skema: Skema){
		this.skema= skema;
	}

	public getGrade(){
		return this.grade;
	}

	public setGrade(grade: Grade){
		this.grade = grade;
	}

	public getBandar(){
		return this.bandar;
	}

	public setBandar(bandar: Bandar){
		this.bandar = bandar;
	}

	public getNegeri(){
		return this.negeri;
	}

	public setNegeri(negeri: Negeri){
		this.negeri= negeri;
	}

	public setAuthorities(authorities: Authorities[]){
		this.authorities= authorities;
	}

	public setRoles(){
		return this.roles;
	}
}
