import { Pet } from "../pet/pet";

export class Pengumuman {

	public id: string;
    public title: string;
    public content: string;
    public startDatetime: Date;
    public endDatetime: Date;
	public createddate : Date;
	public modifieddate : Date;
	public pet: Pet;

	public getTitle() {
		return this.title;
	}

	public setTitle(title: string){
		this.title = title;
	}

	public getContent(){
		return this.content;
	}

	public setContent(content: string){
		this.content = content;
	}

	public getStart_date(){
		return this.startDatetime;
	}

	public setStart_date(startDatetime: Date){
		this.startDatetime = startDatetime;
	}

	public getEnd_date(){
		return this.endDatetime;
	}

	public setEnd_date(endDatetime: Date){
		this.endDatetime = endDatetime;
	}

	public getId(): string {
		return this.id;
	}

	public setId(id: string): void {
		this.id = id;
	}

	public setCreatedDate(createddate: Date){
		this.createddate = createddate;
	}

	public getCreatedDate(){
			return this.createddate;
	}
	public setModifiedDate(modifieddate: Date){
		this.modifieddate = this.modifieddate
	}

	public getPet() {
		return this.pet;
	}
	public setPet(pet: Pet) {
		this.pet = pet;
	}
    
}