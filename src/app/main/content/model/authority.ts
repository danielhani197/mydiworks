export class Authority {
    id: string;
    rolename: string;
    name: string;

    public setId(id: string){
        this.id = id;
    }

    public setName(name: string){
        this.name = name;
    }
}