import { Bahagian } from "../general/bahagian";
import { Agensi } from "../general/agensi";
import { Kementerian } from "../general/kementerian";
import { User } from "../user";

export class AdvancedPage {

    //The number of elements in the page
    size: number = 0;

    //The index of pages: 0,1,3...
    pageNumber: number = 0;

    //The total number of pages
    search: string;

    //Total elements
    totalElements: number = 0;

    //filters 
    bahagian: Bahagian;
    agensi: Agensi;
    kementerian: Kementerian;
    user: User;

}