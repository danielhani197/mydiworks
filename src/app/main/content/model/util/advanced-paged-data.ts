import { AdvancedPage} from "./advanced-page";

/**
 * An array of data with an associated page object used for paging
 */
export class AdvancedPagedData<T> {
    data = new Array<T>();
    page = new AdvancedPage();
}