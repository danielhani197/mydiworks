import { User } from "../user";
import { Agensi } from "../general/agensi";
import { Kementerian } from "../general/kementerian";
export class Page {
    //The number of elements in the page
    size: number = 0;
    //The total number of elements
    totalElements: number = 0;
    //The total number of pages
    totalPages: number = 0;
    //The current page number
    pageNumber: number = 0;
    //sorting field name
    sortField: string;
    //sort order
    sort: string;
    //search
    search: string;
    //status Pemohon
    status: string;
    //filters
    jenis: string;
    // User ahliPet
    userId : string[];
    // agensi
    agensi: Agensi;
    //kementerian
    kementerian: Kementerian;
    // user
    user: User;

    ministryId: string;

    agencyId: string;

    petId: String;
}
