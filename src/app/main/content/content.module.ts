import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

import { FuseContentComponent } from 'app/main/content/content.component';

import { AuthGuard } from "app/main/content/guard/auth.guard";

import { TermViewLoginComponent } from 'app/main/content/common/term-view-login.component';
import { 
    MatButtonModule,
    MatDialogModule,
    MatIconModule 
} 
    from '@angular/material';

    import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        FuseContentComponent,
        TermViewLoginComponent
    ],


    entryComponents: [
        TermViewLoginComponent
    ],
    imports     : [
        RouterModule,

        FuseSharedModule,
        MatDialogModule,
        MatButtonModule,
        TranslateModule,
        MatIconModule
    ],
    providers: [
        AuthGuard
    ],
    exports: [
        FuseContentComponent
    ]
})
export class FuseContentModule
{
}
