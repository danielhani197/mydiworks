import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
 
import { Observable, of, empty } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { User } from 'app/main/content/model/user';
import { City } from 'app/main/content/model/city';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import { PermohonanPengguna } from 'app/main/content/model/permohonan-pengguna';
import { JawapanKeselamatan } from '../../model/setting/jawapanKeselamatan';
import {environment} from '../../../../../environments/environment';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable({ providedIn: 'root' })
export class AuthenticationService {
 
  private globalUrl = `${environment.apiUrl}/user/all`;
  private signupUrl = `${environment.apiUrl}/auth/signup`;
  private testUrl = `${environment.apiUrl}/api/test/user/all`;
  private loginUrl = `${environment.apiUrl}/auth/login`;
  private userDetailUrl = `${environment.apiUrl}/auth/user`;
  private forgotUrl = `${environment.apiUrl}/auth/forgotPassword`;
  private userAppUrl = `${environment.apiUrl}/api/user/app`;
  private resUrl = `${environment.apiUrl}/auth/resPassword`;
  private checkEmailUrl = `${environment.apiUrl}/auth/checkEmail`;
 
  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }

  testGetUsers (): Observable<User[]> {
    return this.http.get<User[]>(this.testUrl)
      .pipe(
        tap(),
        catchError(this._error.handleError('getHeroes', []))
      );
  }
 
  getUsers (): Observable<User[]> {

    return this.http.get<User[]>(this.globalUrl, this._token.jwtBearer())
      .pipe(
        tap(),
        catchError(this._error.handleError('getHeroes', []))
      );
  }

  /** GET user by id. Will 404 if id not found */
  getUser(id: string): Observable<User> {
    const url = `${this.globalUrl}/${id}`;
    return this.http.get<User>(url).pipe(
      tap(),
      catchError(this._error.handleError<User>(`getUser id=${id}`))
    );
  }

  /** POST: save the user on the server */
  signUp (user: PermohonanPengguna): Observable<PermohonanPengguna> {
    return this.http.post(this.signupUrl, user, httpOptions).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

  login (user: User): Observable<User> {
    return this.http.post(this.loginUrl,  JSON.stringify({ username: user.username, password: user.oldPassword }), httpOptions)
    .pipe(
      tap( _ => {
        var token = _['token'];
        this.setJwtToken(token);
      }),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      //this.log(`${operation} failed: ${error.message}`);
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  forgot (jawapanKeselamatan: JawapanKeselamatan[], id:string): Observable<JawapanKeselamatan[]> {
    return this.http.post(this.forgotUrl+"/"+id, jawapanKeselamatan, httpOptions).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updateUser'))
    );
  }

  res (user: User): Observable<User> {
    return this.http.post(this.resUrl, user, httpOptions).pipe(
      tap(),
      catchError(this._error.handleError<any>('updateUser'))
    );
  }

  checkEmail (user: User): Observable<JawapanKeselamatan[]> {
    return this.http.post(this.checkEmailUrl, user, httpOptions).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updateUser'))
    );
  }
  
  logout() {
    // remove user from local storage to log user out
    this.removeCurrentUser();
    this.removeJwtToken();
    localStorage.clear();
  }

  getUserDetail(){
    return this.http.get<User>(this.userDetailUrl, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    )
  }

  getCity (page: Page): Observable<PagedData<City>> {
    return this.http.post(`${environment.apiUrl}/api/ref/test/state`, page, httpOptions).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

  getUserApproved (page: Page): Observable<PagedData<User>> {
    return this.http.post(this.userAppUrl, page, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

  getJwtToken() {
    return localStorage.getItem(this._token.TOKEN_KEY);
  }

  setJwtToken(token) {
      localStorage.setItem(this._token.TOKEN_KEY, token);
  }

  removeJwtToken() {
      localStorage.removeItem(this._token.TOKEN_KEY);
  }

  setCurrentUser(user:User){
    localStorage.setItem('currentUser', JSON.stringify(user));
  }

  getCurrentUser(){
    let user: User = JSON.parse(localStorage.getItem('currentUser'));
      return user;
  }

  removeCurrentUser() {
      localStorage.removeItem('currentUser');
  }


 
}
