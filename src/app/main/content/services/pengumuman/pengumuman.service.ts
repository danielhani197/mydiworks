import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Pengumuman } from 'app/main/content/model/pengumuman/pengumuman';
import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';
import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import {environment} from '../../../../../environments/environment';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable({ providedIn: 'root' })
export class PengumumanService {
 
  private globalUrl = `${environment.apiUrl}/api/pengumuman`;
  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }
 
 
// CREATE
  createPengumuman (pengumuman: Pengumuman): Observable<Pengumuman> {
    console.log(pengumuman);
    return this.http.post(this.globalUrl +'/create', pengumuman, this._token.jwtBearer()).pipe(
      tap(_ => console.log('create pengumuman')),
      catchError(this._error.handleErrorStatus<any>('createPengumuman'))
    );
  }

// LIST
  getPengumumanList (page: Page): Observable<PagedData<Pengumuman>> {
    return this.http.post(this.globalUrl +'/list', page, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
    );
  }

//DELETE
deletePengumuman(id: String): Observable<Pengumuman> {
  //console.log(pengumuman);
  return this.http.delete<Pengumuman>(this.globalUrl+'/delete/'+id, this._token.jwtBearer()).pipe(
    tap(_ => console.log('delete pengumuman')),
    catchError(this._error.handleErrorStatus<any>('deletePengumuman'))
  );
  
}
//UPDATE
updatePengumuman (pengumuman: Pengumuman): Observable<Pengumuman> {
  return this.http.put<Pengumuman>(this.globalUrl+'/edit/'+pengumuman.id, pengumuman, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updatePengumuman'))
  );
}
//LOAD by ID
getPengumumanById (id: string): Observable<Pengumuman> {
  return this.http.get<Pengumuman>(this.globalUrl+'/get/'+id, this._token.jwtBearer())
  .pipe(
      tap(),
      catchError(this._error.handleError('getPengumumanById'))
  );
}
//GET 
getAllPengumuman (): Observable<Pengumuman[]> {

  return this.http.get<Pengumuman[]>(this.globalUrl+'/all', this._token.jwtBearer())
  .pipe(
      tap(),
      catchError(this._error.handleError('getAllPengumuman', []))
  );
}
 //get latest pengumuman by user Id
 getLatestPengumuman (): Observable<Pengumuman[]> {
  return this.http.get<Pengumuman[]>(this.globalUrl+'/latestPengumuman/', this._token.jwtBearer()) 
  .pipe(
      tap(),
      catchError(this._error.handleError('getAllPengumuman', []))
  );
}
}
