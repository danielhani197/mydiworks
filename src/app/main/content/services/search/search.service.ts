import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Negeri } from '../../model/ref/negeri';
import { Bandar } from '../../model/ref/bandar';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import { Page } from '../../model/util/page';
import { AdvancedPage } from '../../model/util/advanced-page';
import { WallKandungan } from '../../model/wall3/wallKandungan';
import {environment} from '../../../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class SearchService {

    private globalUrl = `${environment.apiUrl}/api`;

    constructor(
      private http: HttpClient,
      private _token: TokenService,
      private _error: ErrorHandlerService
    ) { }


    getClassic(page: AdvancedPage): Observable<any> {
        return this.http.post(this.globalUrl+'/post/search', page, this._token.jwtBearer())
        .pipe(
          tap(),
          catchError(this._error.handleErrorStatus<any>())
        );
    }

    getPost(page: AdvancedPage): Observable<any> {
      return this.http.post(this.globalUrl+'/post/carian', page, this._token.jwtBearer())
      .pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
      );
    }

    carianAdmin(): Observable<WallKandungan[]> {
      return this.http.get<WallKandungan[]>(this.globalUrl+'/carianPostAdmin', this._token.jwtBearer())
      .pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
      );
    }
}
