import { Injectable, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';

import { Calendar } from 'app/main/content/model/calendar/calendar';
import { Event } from 'app/main/content/model/calendar/event';
import { reject } from 'q';
import { User } from 'app/main/content/model/user';
import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service'
import {environment} from '../../../../../environments/environment';
import { CalendarEvent } from '../../model/calendar/calendar-utils';
// import { CalendarEvent } from 'calendar-utils';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

@Injectable({ providedIn: 'root' })
export class CalendarService {
    
    userId: string;
    events: any;
    kalendar: Calendar[];
    @Input()
    kalendarPeribadi: Calendar;
    onEventsUpdated = new Subject<any>();

    private globalUrl = `${environment.apiUrl}/api/kalendar`;

    private _listners = new Subject<any>();
    petListen(): Observable<any> {
       return this._listners.asObservable();
    }
    personalListen(): Observable<any> {
        return this._listners.asObservable();
    }
    update() {
       this._listners.next();
    }


    constructor(
      private http: HttpClient,
      private _token: TokenService,
      private _error: ErrorHandlerService,
      private _authentication: AuthenticationService,
    ) 
    {
        this.userId = this._authentication.getCurrentUser().id;
    }

    getUserEvents(): Observable<CalendarEvent[]>{
        return this.http.get<CalendarEvent[]>(this.globalUrl+'/getListEvents/'+this.userId, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        );
    }

    createEvent(event: Event): Observable<Event>{
        return this.http.post<Event>(this.globalUrl+'/createEvent', event, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        );
    }

    getEventById(id: string): Observable<Event>{
        return this.http.get<Event>(this.globalUrl+'/getEvent/'+id, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        );
    }

    deleteEventById(id: string): Observable<any>{
        return this.http.delete<any>(this.globalUrl+'/deleteEvent/'+id, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        );
    }

    editEvent(event: Event): Observable<Event>{
        return this.http.post<Event>(this.globalUrl+'/editEvent', event, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        );
    }

    getPetEvents(petId: string): Observable<CalendarEvent[]>{
        return this.http.get<CalendarEvent[]>(this.globalUrl+'/getPetEvents/'+petId, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        );
    }

    getCurrentWeekEvents(userid: string): Observable<Event[]>{
        return this.http.get<Event[]>(this.globalUrl+'/getCurrentWeekEvents/'+userid, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        );
    }

    getPetCurrentWeekEvents(petid: string): Observable<Event[]>{
        return this.http.get<Event[]>(this.globalUrl+'/getCurrentWeekPetEvents/'+petid, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        );
    }

}
