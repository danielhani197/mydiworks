import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Event } from 'app/main/content/model/calendar/event';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import { CalendarEvent } from 'calendar-utils';
import {environment} from '../../../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class EventService {

  private globalUrl = `${environment.apiUrl}/api/kalendar`;

  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }

  // get all
  // getEvent (): Observable<Event[]> {

  //   return this.http.get<Event[]>(this.globalUrl+'/1000/events', this._token.jwtBearer())
  //     .pipe(
  //       tap(heroes => console.log(`fetched event - getEvent`)),
  //       catchError(this._error.handleErrorStatus('getAllEvents', []))
  //     );
  // }

  // get
  getEventById (id: string): Observable<Event> {
      return this.http.get<Event>(this.globalUrl+'/get/'+id, this._token.jwtBearer())
      .pipe(
          tap(heroes => console.log(`fetched event with id=${id}`)),
          catchError(this._error.handleError('getEventById'))
      );
  }

  // create
  createEvent (event: Event): Observable<Event>{
    return this.http.post<Event>(this.globalUrl+'/eventCreate', event, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus())
    );
  }

  // update
  updateEvent (event: Event): Observable<Event> {
      return this.http.put(this.globalUrl+'/edit/'+event.id, event, this._token.jwtBearer()).pipe(
          tap(_ => console.log(`update event with id=${event.id}`)),
          catchError(this._error.handleError<any>('updateEvent'))
      );
  }

  //listing
  getEventList(page: Page): Observable<PagedData<Event>> {
      return this.http.post(this.globalUrl+'/list', page, this._token.jwtBearer()).pipe(
          tap(),
          catchError(this._error.handleErrorStatus<any>())
        );
      }

  //delete
  deleteEventById (id: string):Observable<Event> {
      return this.http.delete<Event>(this.globalUrl+'/delete/'+id, this._token.jwtBearer())
      .pipe(
          tap(heroes => console.log(`deleted event with id=${id}`)),
          catchError(this._error.handleError('deleteEventById'))
      );
  }
}
