import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { JawapanKeselamatan } from 'app/main/content/model/setting/jawapanKeselamatan';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import { JawapanKeselamatanTemp } from '../../model/setting/jawapanKeselamatanTemp';
import {environment} from '../../../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class JawapanKeselamatanService {

  private globalUrl = `${environment.apiUrl}/api/jawapanKeselamatan`;
  private editUrl = `${environment.apiUrl}/api/jawapanKeselamatan/edit`;
  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }

  // get all
  getJawapanKeselamatan (): Observable<JawapanKeselamatan[]> {

    return this.http.get<JawapanKeselamatan[]>(this.globalUrl+'/all', this._token.jwtBearer())
      .pipe(
        tap(heroes => console.log(`fetched jawapanKeselamatans`)),
        catchError(this._error.handleError('getJawapanKeselamatan', []))
      );
  }
  // create
  createJawapanKeselamatan (jawapanKeselamatan: JawapanKeselamatan[]): Observable<JawapanKeselamatan[]>{
    return this.http.post<JawapanKeselamatan>(this.globalUrl+'/create', jawapanKeselamatan, this._token.jwtBearer())
    .pipe(
      tap(_ => console.log()),
      catchError(this._error.handleErrorStatus<any>('createJawapanKeselamatan'))
    );
  }

  // get jawapan
  getJawapanKeselamatanById (id: string): Observable<JawapanKeselamatan[]> {
      return this.http.get<JawapanKeselamatan[]>(this.globalUrl+'/get/'+id, this._token.jwtBearer())
      .pipe(
          tap(heroes => console.log()),
          catchError(this._error.handleErrorStatus('getSoalanKeselamatanById'))
      );
  }

  // update
  updateJawapanKeselamatan (jawapanKeselamatan: JawapanKeselamatan[]): Observable<JawapanKeselamatan[]> {
    return this.http.post<JawapanKeselamatan[]>(this.editUrl, jawapanKeselamatan, this._token.jwtBearer())
    .pipe(
      tap(_ => console.log()),
      catchError(this._error.handleErrorStatus('updateJawapanKeselamatan'))
    );
  }

  createJawapanKeselamatanTemp (ls: JawapanKeselamatanTemp[]): Observable<JawapanKeselamatan[]>{
    return this.http.post<JawapanKeselamatan>(this.globalUrl+'/create/temp', ls, httpOptions)
    .pipe(
      tap(_ => console.log()),
      catchError(this._error.handleErrorStatus<any>('createJawapanKeselamatanTemp'))
    );
  }


}
