import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
 
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Term } from 'app/main/content/model/term';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import {environment} from '../../../../../environments/environment';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable({ providedIn: 'root' })
export class TermService {
 

  private createUrl = `${environment.apiUrl}/api/tnc/create`;
  private deleteUrl = `${environment.apiUrl}/api/tnc/delete`;
  private readUrl = `${environment.apiUrl}/api/tnc/list`;
  private editUrl = `${environment.apiUrl}/api/tnc/edit/`;
  private latersUrl = `${environment.apiUrl}/api/tnc/laters/`;
  private listlUrl = `${environment.apiUrl}/api/tnc/search`;
  private viewlUrl = `${environment.apiUrl}/api/tnc/view/`;
 
  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }
 
  getTerms (): Observable<Term[]> {
    return this.http.get<Term[]>(this.readUrl, this._token.jwtBearer())
      .pipe(
        tap(heroes => console.log(`fetched term`)),
        catchError(this._error.handleError('getTerm', []))
      );
  }
  getTermList (page: Page): Observable<PagedData<Term>> {
    return this.http.post(this.listlUrl, page, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
    );
}

  /** GET user by id. Will 404 if id not found */
  getTerm(id: string): Observable<Term> {
    return this.http.get<Term>(this.viewlUrl+id, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleError<Term>(`getUser id=${id}`))
    );
  }

  /** POST: save the term on the server */
  createTerm(term: Term): Observable<Term> {
    return this.http.post(this.createUrl, term, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleError<any>('createTerm'))
    );
  }

  /* PUT : edit data term on the server */
  editTerm(term: Term): Observable<Term> {
    return this.http.put<Term>(this.editUrl+term.id, term, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleError<Term>('editTerm'))
    );
  }

  getListTerm(){
    return this.http.get<Term>(this.readUrl, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    )
  }

  deleteTerm(term: Term): Observable<Term> {
    return this.http.put<Term>(this.deleteUrl+term.id, term, httpOptions)
    .pipe(
      tap(),
      catchError(this._error.handleError<Term>('editTerm'))
    );
  }
  latersTerm(): Observable<Term> {
    return this.http.get<Term>(this.latersUrl, httpOptions)
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus<Term>('latersTerm'))
    );

  }

 
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }


 
}
