import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { SoalanKeselamatan } from 'app/main/content/model/setting/soalanKeselamatan';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';
import { throwError } from 'rxjs/internal/observable/throwError';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import {environment} from '../../../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class SoalanKeselamatanService {

  private globalUrl = `${environment.apiUrl}/api/soalanKeselamatan`;

  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }

  // get all
  getSoalanKeselamatan (): Observable<SoalanKeselamatan[]> {

    return this.http.get<SoalanKeselamatan[]>(this.globalUrl+'/all', httpOptions)
      .pipe(
        tap(),
        catchError(this._error.handleErrorStatus('getAllSoalanKeselamatans', []))
      );
  }

  // get soalan
  getSoalanKeselamatanById (id: string): Observable<SoalanKeselamatan> {
      return this.http.get<SoalanKeselamatan>(this.globalUrl+'/get/'+id, this._token.jwtBearer())
      .pipe(
          tap(heroes => console.log(`fetched soalan keselamamatan with id=${id}`)),
          catchError(this._error.handleErrorStatus('getSoalanKeselamatanById'))
      );
  }

// create
  createSoalanKeselamatan (soalanKeselamatan: SoalanKeselamatan): Observable<SoalanKeselamatan>{
    return this.http.post<SoalanKeselamatan>(this.globalUrl+'/create', soalanKeselamatan, this._token.jwtBearer())
    .pipe(
      tap(_ => console.log(`create soalanKeselamatan id=${soalanKeselamatan.keterangan}`)),
      catchError(this._error.handleErrorStatus<any>('createSoalanKeselamatan'))
    );
  }

// update
updateSoalanKeselamatan (soalanKeselamatan: SoalanKeselamatan): Observable<SoalanKeselamatan> {
    return this.http.put(this.globalUrl+'/edit/'+soalanKeselamatan.id, soalanKeselamatan, this._token.jwtBearer()).pipe(
        tap(_ => console.log(`update soalanKeselamatan with id=${soalanKeselamatan.id}`)),
        catchError(this._error.handleErrorStatus<any>('updateSoalanKeselamatan'))
    );
}

  //listing
  getSoalanKeselamatanList(page: Page): Observable<PagedData<SoalanKeselamatan>> {
      return this.http.post(this.globalUrl+'/list', page, this._token.jwtBearer()).pipe(
          tap(),
          catchError(this._error.handleErrorStatus<any>())
        );
      }

  //delete
  deleteSoalanKeselamatanById (id: string):Observable<SoalanKeselamatan> {
      return this.http.delete<SoalanKeselamatan>(this.globalUrl+'/delete/'+id, this._token.jwtBearer())
      .pipe(
          tap(heroes => console.log(`deleted soalan keselamamatan with id=${id}`)),
          catchError(this._error.handleError('deleteSoalanKeselamatanById'))
      );
  }

  // exist keterangan soalan
  isExistKeterangan (keterangan: string) {
    return this.http.get(this.globalUrl+"/exist/username/"+keterangan, httpOptions).pipe(
      tap(),
      catchError(
        error => {
          return throwError(error)
        }
      )
    );
  }


}
