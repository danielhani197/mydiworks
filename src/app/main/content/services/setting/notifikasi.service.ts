import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
 
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Notifikasi } from 'app/main/content/model/notifikasi';
 
import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';
 
import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import { AuthenticationService } from '../authentication/authentication.service';
import { User } from '../../model/user';
import {environment} from '../../../../../environments/environment';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}; 
 
@Injectable({ providedIn: 'root' })
export class NotifikasiService{
  
    private UrlList = `${environment.apiUrl}/api/notification/list`;
    private UrlCreate = `${environment.apiUrl}/api/notification/create`;
    private UrlView = `${environment.apiUrl}/api/notification/view-id/`;
    private UrlEdit = `${environment.apiUrl}/api/notification/edit/`;
    private latersUrl = `${environment.apiUrl}/api/notification/views/`;
    private listlUrl = `${environment.apiUrl}/api/notification/view/`;
    private urlOpen = `${environment.apiUrl}/api/notification/go/`;

    notifikasis:Notifikasi[];
    user:User;
    id:any;

    constructor (
      private http: HttpClient,
      private _token: TokenService,
      private _error: ErrorHandlerService,
      private _user: AuthenticationService,
  ) { 

    //this.user = this._user.getCurrentUser()
    //this.id = this.user.id;
  }

  getNotiList(): Observable<Notifikasi[]>{
      return this.http.get<Notifikasi[]>(this.UrlList, this._token.jwtBearer())
          .pipe(
              tap(),
              catchError(this._error.handleErrorStatus('getNotification'))
          );
  }

  createNoti(notifikasi: Notifikasi): Observable<Notifikasi> {
      return this.http.post(this.UrlCreate, notifikasi, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>('createNotifikasi'))
      );
    }

    updateStatusReadNoti(notifikasi: Notifikasi): Observable<Notifikasi> {
      return this.http.post<Notifikasi>(this.UrlEdit+notifikasi.id, notifikasi, this._token.jwtBearer())
      .pipe(
        tap(),
        catchError(this._error.handleErrorStatus<Notifikasi>('editNotifikasi'))
      );
    }

    getNotiId(id: string): Observable<Notifikasi> {
      return this.http.get<Notifikasi>(this.urlOpen+id, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<Notifikasi>(`getUser id=${id}`))
      );
    }

    getNotiByUserId(id: string): Observable<Notifikasi[]> {
      return this.http.get<Notifikasi[]>(this.UrlView+id, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<Notifikasi[]>(`getUser id=${id}`))
      );
    }

    laters5Noti(id:string): Observable<Notifikasi[]> {
      return this.http.get<Notifikasi[]>(this.latersUrl+id, this._token.jwtBearer())
      .pipe(
        tap(),
        catchError(this._error.handleErrorStatus<Notifikasi[]>(`laters`))
      );
  
    }

    getNotiView(page: Page, id: string): Observable<PagedData<Notifikasi>> {
      return this.http.post(this.listlUrl+id, page, this._token.jwtBearer()).pipe(
          tap(),
          catchError(this._error.handleErrorStatus<any>())
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {
        console.error(error);
        return of(result as T);
      };
    }

    refresh(){
      
      return this.notifikasis;
      
    }

    loadNoti(notifikasi){
      this.notifikasis = notifikasi;  
      console.log(this.notifikasis)
    }
  }
