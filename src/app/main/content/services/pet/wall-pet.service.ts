import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import { WallKandungan } from '../../model/wall3/wallKandungan';
import { WallKomen } from '../../model/wall3/wallKomen';
import { Pet } from '../../model/pet/pet';
import { AdvancedPagedData } from '../../model/util/advanced-paged-data';
import { AdvancedPage } from '../../model/util/advanced-page';
import { PostCollection } from '../../model/wall3/postCollection';
import { WallAttachment } from '../../model/wall3/wallAttachment';
import { PagedData } from '../../model/util/paged-data';
import { Page } from '../../model/util/page';
import { PetAttachment } from '../../model/wall3/petAttachment';
import {environment} from '../../../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class WallPetService {

  private globalUrl = `${environment.apiUrl}/api/wallPet`;

  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }


  getPostAll (page: AdvancedPage): Observable<any> {

    return this.http.post(this.globalUrl+'/getAll/', page, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus())
    );
  }

  //get project by user
  getProjectByUser( userId: string): Observable<Pet[]>{
    return this.http.get<Pet[]>(this.globalUrl+'/getProject/'+userId, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus())
    );
  }

  //get event by user
  getEventByUser( userId: string): Observable<Pet[]>{
    return this.http.get<Pet[]>(this.globalUrl+'/getEvent/'+userId, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus())
    );
  }

  //get project by user
  getTeamByUser( userId: string): Observable<Pet[]>{
    return this.http.get<Pet[]>(this.globalUrl+'/getTeam/'+userId, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus())
    );
  }

  getGallery(page: Page, petId: string): Observable<PagedData<PetAttachment>>{
    return this.http.post(this.globalUrl+'/gallery/'+petId, page, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }
  
  getGalleryDrop(page: Page, petId: string): Observable<PagedData<PetAttachment>>{
    return this.http.post(this.globalUrl+'/galleryDrop/'+petId, page, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

}
