import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of, BehaviorSubject, ObservableInput } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Pet } from 'app/main/content/model/pet/pet';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import { AuthenticationService } from '../authentication/authentication.service';
import { PenggunaService } from '../pengguna/pengguna.service';
import { User } from '../../model/user';
import {environment} from '../../../../../environments/environment';
import { MohonNyahAktif } from '../../model/pet/mohonNyahAktif';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class PetService {

  private globalUrl = `${environment.apiUrl}/api/pet`;
  private ahliPetUrl = `${environment.apiUrl}/api/user/ahliPet`;
  private deactiveUrl = `${environment.apiUrl}/api/deactive`;



  userList : User[] = [];
  selectedUser : string[] = [];
  currentList : string[] =[];
  filterBy: string;
  pagedData: PagedData<User> = new PagedData<User>();

  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService,
    private _auth: PenggunaService
  ) { }

  // get all
  getPet (): Observable<Pet[]> {

    return this.http.get<Pet[]>(this.globalUrl+'/all', this._token.jwtBearer())
      .pipe(
        tap(),
        catchError(this._error.handleErrorStatus('getPet', []))
      );
  }

  // get Pet
  getPetById (id: string): Observable<Pet> {
      return this.http.get<Pet>(this.globalUrl+'/get/'+id, this._token.jwtBearer())
      .pipe(
          tap(),
          catchError(this._error.handleErrorStatus('getPetById'))
      );
  }

  // create
  createPet (pet: Pet): Observable<Pet>{
    return this.http.post<Pet>(this.globalUrl+'/create', pet, this._token.jwtBearer())
    .pipe(
      tap(_ => console.log(`create pet id=${pet.nama}`)),
      catchError(this._error.handleErrorStatus<any>('createPet'))
    );
  }

  // update
  updatePet (pet: Pet): Observable<Pet> {
      return this.http.put(this.globalUrl+'/edit/'+pet.id, pet, this._token.jwtBearer()).pipe(
          tap(_ => console.log(`update pet with id=${pet.id}`)),
          catchError(this._error.handleErrorStatus<any>('updatePet'))
      );
  }

  updateAhliPet (pet: Pet): Observable<Pet> {
    return this.http.put(this.globalUrl+'/updateMembers/'+pet.id, pet, this._token.jwtBearer()).pipe(
        tap(_ => console.log(`update pet with id=${pet.id}`)),
        catchError(this._error.handleErrorStatus<any>('updatePet'))
    );
  }

  //diactive
  diactive(id : string): Observable<Pet>{
    return this.http.get(this.globalUrl+'/diactive/'+id, this._token.jwtBearer()).pipe(
      tap(),
          catchError(this._error.handleErrorStatus<any>('deactive'))
    );
  }

  //active
  active(id : string): Observable<Pet>{
    console.log(id)
    return this.http.get(this.globalUrl+'/active/'+id, this._token.jwtBearer()).pipe(
      tap(),
          catchError(this._error.handleErrorStatus<any>('active'))
    );
  }

  //apply deactive
  deactiveApply (mohonNyahAktif: MohonNyahAktif): Observable<MohonNyahAktif>{
    return this.http.post<Pet>(this.deactiveUrl+'/apply', mohonNyahAktif, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('applyDeactive'))
    );
  }

  // deactive list
  getDeactiveList(page: Page): Observable<PagedData<MohonNyahAktif>>{
    return this.http.post(this.deactiveUrl+'/list', page, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

  // approve deactivation
  approveDeactive(id: string): Observable<any>{
    return this.http.get(this.deactiveUrl+'/approve/'+id, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    )
  }

  // Reject Deactivation
  rejectDeactive(id:string): Observable<any> {
    return this.http.get(this.deactiveUrl+'/reject/'+id, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    )
  }

  //listing
  getPetList(page: Page): Observable<PagedData<Pet>> {
      return this.http.post(this.globalUrl+'/list', page, this._token.jwtBearer()).pipe(
          tap(),
          catchError(this._error.handleErrorStatus<any>())
        );
      }

  //listing All
  getPetListAll(page: Page): Observable<PagedData<Pet>> {
      return this.http.post(this.globalUrl+'/listAll', page, this._token.jwtBearer()).pipe(
          tap(),
          catchError(this._error.handleErrorStatus<any>())
        );
      }

  //listing User
  getPetUser(pagedData: PagedData<User>): Observable<PagedData<User>> {
    return this.http.post(this.globalUrl+'/userPet', pagedData, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
      );
    }

  //delete
  deletePetById (id: string):Observable<Pet> {
      return this.http.delete<Pet>(this.globalUrl+'/delete/'+id, this._token.jwtBearer())
      .pipe(
          tap(),
          catchError(this._error.handleErrorStatus('deletePetById'))
      );
  }

  // listing user ahliPet
  getAhliPet (page: Page): Observable<PagedData<User>> {
    return this.http.post(this.ahliPetUrl, page, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }
  updatePetProfilPhoto(obj: any): Observable<Pet>{
    return this.http.post(this.globalUrl+'/editpicture', obj, this._token.jwtFile()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updatePet'))
    )
  }
  updatePetProfil(pet: Pet): Observable<Pet> {
    return this.http.post<Pet>(this.globalUrl+'/editProfile/'+pet.id, pet, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleError<Pet>('editTerm'))
    );
  }


  // get paged data pertama
  getPagedData(){
    let pagedData: PagedData<User> = new PagedData<User>();
    pagedData.data = this.userList;

    return pagedData;
  }

  // get paged data kedua
  getPaged(){
    let pagedData: PagedData<User> = new PagedData<User>();
    pagedData.data = this.pagedData.data;

    return pagedData;
  }

  // load yang kedua tambah ahli dalam pet
  loadUser(selected){
    this.pagedData.data = selected;
    // console.log(selected)
  }

  // load yang pertama tambah ahli pet
  loadPengguna (){
   
    for( let obj of this.selectedUser){
      
    this._auth.getUserById(obj).subscribe(
        data=>{ 
         
          this.userList.push(data);
          
        }
      )
    }
    this.pagedData.data = this.userList;   
  }

  // resetUser(selected){
  //   this.selected = [];
  // }
   resetSelectedUser(){
    this.selectedUser = [];
  }
  selectUser(id: string){
      if (this.selectedUser.length > 0){
          const index = this.selectedUser.indexOf(id);
        
          if(index !== -1){
              this.selectedUser.splice(index,1);
              return;
          }
      }
      
      this.selectedUser.push(id);
      
  }

  getUsers(){
    return this.currentList;
  }

  setCurrentList(ids: string[]){
    this.currentList = ids;
  }


}
