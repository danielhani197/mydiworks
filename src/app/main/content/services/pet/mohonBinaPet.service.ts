import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of, BehaviorSubject, ObservableInput } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { MohonBinaPet } from 'app/main/content/model/pet/mohonBinaPet';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import { AuthenticationService } from '../authentication/authentication.service';
import { PenggunaService } from '../pengguna/pengguna.service';
import { User } from '../../model/user';
import {environment} from '../../../../../environments/environment';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class MohonBinaPetService {
    private globalUrl = `${environment.apiUrl}/api/mohonBinaPet`;
    private ahliPetUrl = `${environment.apiUrl}/api/user/ahliPet`;

    idUser: string;
    selectedRequest: string[] = [];
    dataSelectedBar: number;
        
    userList : User[] = [];
    selectedUser : string[] = [];
    currentList : string[] =[];
    filterBy: string;
    pagedData: PagedData<User> = new PagedData<User>();

    constructor(
        private http: HttpClient,
        private _token: TokenService,
        private _error: ErrorHandlerService,
        private _auth: PenggunaService
      ) { }


    //listing All
    getMohonList(page: Page): Observable<PagedData<MohonBinaPet>> {
        return this.http.post(this.globalUrl+'/list', page, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        );
        }

    //listing Agensi
    getMohonAgensiList(page: Page): Observable<PagedData<MohonBinaPet>> {
        return this.http.post(this.globalUrl+'/agensi/list', page, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        );
        }

    // listing user ahliPet
    getAhliPet (page: Page): Observable<PagedData<User>> {
        
        return this.http.post(this.ahliPetUrl, page, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
        );
    }

    approveMohon(id: string): Observable<any> {
        
        return this.http.get(this.globalUrl+'/lulusPermohonanPet/'+id, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        );
    }

    rejectMohon(mohonBinaPet: MohonBinaPet): Observable<MohonBinaPet> {
        
        return this.http.put(this.globalUrl+'/rejectPermohonanPet/'+mohonBinaPet.id, mohonBinaPet, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        );
    }

      // create
    createPetReq (mohonBinaPet: any): Observable<MohonBinaPet>{
        return this.http.post<MohonBinaPet>(this.globalUrl+'/createReq', mohonBinaPet, this._token.jwtFile()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
        );
    }

      //delete
    deleteById (id: string):Observable<MohonBinaPet> {
        return this.http.delete<MohonBinaPet>(this.globalUrl+'/delete/'+id, this._token.jwtBearer())
        .pipe(
            tap(heroes => console.log(`deleted mohonBinaPet with id=${id}`)),
            catchError(this._error.handleError('deleteById'))
        );
    }

    getPagedData(){
        let pagedData: PagedData<User> = new PagedData<User>();
        pagedData.data = this.userList;
        
        return pagedData;
      }
    
      loadPengguna (){
       
        for( let obj of this.selectedUser){
          
        this._auth.getUserById(obj).subscribe(
            data=>{
             
              this.userList.push(data);
              
            }
          )
        }
        this.pagedData.data = this.userList;   
      }
    
      resetSelectedUser(){
        this.selectedUser = [];
      }
      selectUser(id: string){
          if (this.selectedUser.length > 0){
              const index = this.selectedUser.indexOf(id);
            
              if(index !== -1){
                  this.selectedUser.splice(index,1);
                  return;
              }
          }
          
          this.selectedUser.push(id);
          
      }
    
      getUsers(){
        return this.currentList;
      }
    
      setCurrentList(ids: string[]){

        this.currentList = ids;
        this.userList = [];
        for (let obj of ids){
            let user = new User();
            this._auth.getUserById(obj).subscribe(
                data=>{
                    user = data;
                    this.userList.push(user);
                }
            )
        }

        console.log(this.currentList)
        console.log(this.userList)

      }


}
