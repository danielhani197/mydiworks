import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of, BehaviorSubject, ObservableInput } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { AhliPet } from 'app/main/content/model/pet/ahliPet';
import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import {environment} from '../../../../../environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  
  @Injectable({ providedIn: 'root' })
  export class AhliPetService{
    private globalUrl = `${environment.apiUrl}/api/ahliPet`;

    constructor(
        private http: HttpClient,
        private _token: TokenService,
        private _error: ErrorHandlerService,

      ) { }

    // delete ahli Pet
    deleteAhliPetById (id: string):Observable<AhliPet> {
        return this.http.delete<AhliPet>(this.globalUrl+"/delete/"+id, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        );
    }

    // update
    updatePet (ahliPet: AhliPet): Observable<AhliPet> {
        return this.http.put(this.globalUrl+'/edit/'+ahliPet.id, ahliPet, this._token.jwtBearer()).pipe(
            tap(_ => console.log(`update pet with id=${ahliPet.id}`)),
            catchError(this._error.handleErrorStatus<any>('updatePet'))
        );
    }

     //listing
    getAhliPetList(page: Page, id: string): Observable<PagedData<AhliPet>> {
        return this.http.post(this.globalUrl+'/list/'+id, page, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
      );
    }

    getByUserId(id: String): Observable<AhliPet[]> {
        return this.http.get<AhliPet[]>(this.globalUrl+'/PetUser/get/'+id, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus<AhliPet[]>('userId'))
        );
    }

    getByPetId(id: String): Observable<AhliPet[]> {
        return this.http.get<AhliPet[]>(this.globalUrl+'/PetId/get/'+id, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus<AhliPet[]>('userId'))
        );
    }

    getByAhli(): Observable<AhliPet[]> {
        return this.http.get<AhliPet[]>(this.globalUrl+'/all', this._token.jwtBearer())
        .pipe(
          tap(),
          catchError(this._error.handleError('getState', []))
        );
    }

    createAhliPet(ahliPet: AhliPet): Observable<AhliPet> {
        return this.http.post(this.globalUrl+'/create', ahliPet, this._token.jwtBearer()).pipe(
          tap(),
          catchError(this._error.handleErrorStatus<any>('createPermohonanPet'))
        );
    }
    
    updatePetStatus (id: String): Observable<any> {
        return this.http.get(this.globalUrl+'/editStatus/'+id, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>('updatePet'))
        );
    }

    updatePetStatusActive (id: String): Observable<any> {
        return this.http.get(this.globalUrl+'/editStatusActive/'+id, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>('updatePet'))
        );
    }

    isUserAdmin(petId: string, userId: string): Observable<any> {
        return this.http.get(this.globalUrl+'/isUserAdmin/'+petId+'/'+userId, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>('updatePet'))
        );
    }
  }
