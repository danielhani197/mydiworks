import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
 
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Pengumuman } from 'app/main/content/model/pengumuman/pengumuman';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import {environment} from '../../../../../environments/environment';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable({ providedIn: 'root' })
export class PengumumanPetService {
 
  // private globalUrl = `${environment.apiUrl}/api/pengumumanPet`;
  private createUrl = `${environment.apiUrl}/api/pengumuman/pet/create`;
  private deleteUrl =  `${environment.apiUrl}/api/pengumuman/pet/delete/`;
  private readUrl = `${environment.apiUrl}/api/pengumuman/pet/read`;
  private editUrl = `${environment.apiUrl}/api/pengumuman/pet/edit/`;
  private latersUrl = `${environment.apiUrl}/api/pengumuman/pet/laters/`;
  private listlUrl = `${environment.apiUrl}/api/pengumuman/pet/list`;
  private viewlUrl = `${environment.apiUrl}/api/pengumuman/pet/get/`;
  private wallPengumumanPetUrl = `${environment.apiUrl}/api/pengumuman/pet/wall`;
 
  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }
 
  getPengumumans (): Observable<Pengumuman[]> {
    return this.http.get<Pengumuman[]>(this.readUrl, this._token.jwtBearer())
      .pipe(
        tap(heroes => console.log(`fetched pengumuman`)),
        catchError(this._error.handleError('getPengumuman', []))
      );
  }
  getPengumumanList (page: Page, id: string): Observable<PagedData<Pengumuman>> {
    return this.http.post(this.listlUrl+"/"+id, page, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
    );
}

  /** GET pengumuman by id. Will 404 if id not found */
  getPengumumanByID(id: string): Observable<Pengumuman> {
    return this.http.get<Pengumuman>(this.viewlUrl+id, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleError<Pengumuman>(`getUser id=${id}`))
    );
  }

  /** POST: save the pengumuman on the server */
  createPengumuman (pengumuman: Pengumuman): Observable<Pengumuman> {
    return this.http.post(this.createUrl, pengumuman, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('createPengumuman'))
    );
  }

  /* PUT : edit data pengumuman on the server */
  editPengumuman(pengumuman: Pengumuman): Observable<Pengumuman> {
    return this.http.put<Pengumuman>(this.editUrl+pengumuman.id, pengumuman, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleError<Pengumuman>('editPengumuman'))
    );
  }

  getListPengumumanPetWall(id: string){
    return this.http.get<Pengumuman>(this.wallPengumumanPetUrl+'/'+id, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    )
  }

  // deletePengumuman(pengumuman: Pengumuman): Observable<Pengumuman> {
  //   return this.http.put<Pengumuman>(this.deleteUrl+pengumuman.id, pengumuman, httpOptions)
  //   .pipe(
  //     tap(),
  //     catchError(this._error.handleError<Pengumuman>('editPengumuman'))
  //   );
  // }

  //DELETE BY ID
  deletePengumuman(id: String): Observable<Pengumuman> {
    //console.log(pengumuman);
    return this.http.delete<Pengumuman>(this.deleteUrl+id, this._token.jwtBearer()).pipe(
      tap(_ => console.log('delete pengumuman')),
      catchError(this._error.handleErrorStatus<any>('deletePengumuman'))
    );
    
  }
  latestPengumuman(): Observable<Pengumuman> {
    return this.http.get<Pengumuman>(this.latersUrl, httpOptions)
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus<Pengumuman>('latestPengumuman'))
    );

  }

 
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }


 
}
