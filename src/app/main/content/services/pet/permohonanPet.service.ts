import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';

import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { FuseUtils } from '@fuse/utils';

import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { PermohonanPet } from 'app/main/content/model/pet/permohonanPet';
 
import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';
 
import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import { id } from '@swimlane/ngx-datatable/release/utils';
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material';
import {environment} from '../../../../../environments/environment';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable({ providedIn: 'root' })
export class  PermohonanPetService {
  
 
  private createUrl = `${environment.apiUrl}/api/petrequest/create`;
  private globalUrl = `${environment.apiUrl}/api/petrequest`;
  private readUrl = `${environment.apiUrl}/api/petrequest/list`;
  private editUrl = `${environment.apiUrl}/api/petrequest/approve`;
  private viewlUrl = `${environment.apiUrl}/api/petrequest/view/`;


  filterBy: string;
  user: any;
  searchText: string;
  
  onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
  onpermohonanPetChanged: BehaviorSubject<any> = new BehaviorSubject([]);
  onSelectedpermohonanPetChanged: BehaviorSubject<any> = new BehaviorSubject([]);
  onSearchTextChanged: Subject<any> = new Subject();

  onFilterChanged: Subject<any> = new Subject();

  selectedRequest: string[] = [];
  permohonanPet: PermohonanPet[];
  dataSelectedBar: number;


  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }
 
  //listing
  getPermohonanPetList (page: Page): Observable<PagedData<PermohonanPet>> {
    return this.http.post(this.readUrl, page, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
    );
  }
  getPermohonanPetById(id: string): Observable<PermohonanPet> {
    return this.http.get<PermohonanPet>(this.viewlUrl+id, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<PermohonanPet>(`getUser id=${id}`))
    );
  }
  createPermohonanPet(permohonanPet: PermohonanPet): Observable<PermohonanPet> {
    return this.http.post(this.createUrl, permohonanPet, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('createPermohonanPet'))
    );
  }
  editPermohonan(permohonanPet: PermohonanPet): Observable<PermohonanPet> {
    return this.http.post(this.editUrl, permohonanPet, this._token.jwtBearer())
      .pipe(
        tap(),
      catchError(this._error.handleErrorStatus<any>('editTerm'))
    );  
  }
  getByUserId(id: String):Observable<PermohonanPet> {
    return this.http.get<PermohonanPet>(this.globalUrl+'/getUser/'+ id, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<PermohonanPet>())
    )
  }

  getPermohonanPetListByPetId (page: Page, id:String): Observable<PagedData<PermohonanPet>> {
    return this.http.post(this.globalUrl+"/listBypet/"+id, page, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
    );
  }

  

  deselectPermohonanPet()
    {
        this.selectedRequest = [];

        if ( this.selectedRequest.length > 0 )
        {
            const index = this.selectedRequest.length;
           
            if ( index !== -1 )
            {
                this.selectedRequest.splice(index, 1);

                // Trigger the next event
                this.onSelectedpermohonanPetChanged.next(this.selectedRequest);
                // Return
                return;
                
            }
        }

        // Trigger the next event
        this.onSelectedpermohonanPetChanged.next(this.selectedRequest);
    }


    toggleSelectedContact(id)
    {
        
        // First, check if we already have that contact as selected...
        if ( this.selectedRequest.length > 0 )
        {
            const index = this.selectedRequest.indexOf(id);
           
            if ( index !== -1 )
            {
                this.selectedRequest.splice(index, 1);

                // Trigger the next event
                this.onSelectedpermohonanPetChanged.next(this.selectedRequest);
                // Return
                return;
                
            }
        }

        // If we don't have it, push as selected
        this.selectedRequest.push(id);
        // Trigger the next event
        
        this.onSelectedpermohonanPetChanged.next(this.selectedRequest);

    }

   
}
