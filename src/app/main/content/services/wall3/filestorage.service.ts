import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers, ResponseType } from '@angular/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { WallAttachment } from '../../model/wall3/wallAttachment';
import { PetAttachment } from '../../model/wall3/petAttachment';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import {environment} from '../../../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class FileStorageService {

  private saveUrl = `${environment.apiUrl}/api/file/save`;
  private saveMyboxUrl = `${environment.apiUrl}/api/fileMyBox/save`;
  private savePetUrl = `${environment.apiUrl}/api/petAttachment/save`;
  private fileUrl = `${environment.apiUrl}/api/getall`;
  private petAttachUrl = `${environment.apiUrl}/api/getall/attachment`;
  private downloadUrl = `${environment.apiUrl}/api/download`;
  private versionUrl = `${environment.apiUrl}/api/petAttachment/version/`;
  private updateUrl = `${environment.apiUrl}/api/petAttachment/update`;
  private AttachUrl = `${environment.apiUrl}/api/getPetAttachment`;
  savePetmboxUrl = `${environment.apiUrl}/api/fileMybox/save`;
  private wallUrl = `${environment.apiUrl}/api/findByWallId/attachment/`;

  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }

  // save wall attachment in MongoDB
  saveFile (obj: any): Observable<WallAttachment> {
    return this.http.post(this.saveUrl, obj, this._token.jwtFile()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updateUser'))
    );
  }

  // save wall attachment from mybox in WA
  saveMyBoxFile (obj: any): Observable<WallAttachment> {
    return this.http.post(this.saveMyboxUrl, obj, this._token.jwtFile()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updateUser'))
    );
  }

  // save pet attachment in MongoDB
  savePetFile (obj: any): Observable<PetAttachment> {
    return this.http.post(this.savePetUrl, obj, this._token.jwtFile()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updateUser'))
    );
  }

  // save pet attachment from mybox in PA
  savePetMyboxFile (obj: any): Observable<PetAttachment> {
    return this.http.post(this.savePetmboxUrl, obj, this._token.jwtFile()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updateUser'))
    );
  }

  // upload new version
  saveNewVersion (obj: any, petId: string): Observable<PetAttachment> {
    return this.http.post(this.versionUrl+petId, obj, this._token.jwtFile()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updateUser'))
    );
  }

  // list file
  getFiles (): Observable<WallAttachment[]> {
    return this.http.get<WallAttachment[]>(this.fileUrl, this._token.jwtBearer())
      .pipe(
        tap(),
        catchError(this._error.handleErrorStatus('getFiles', []))
      );
  }

  // list pet file
  getPetFiles (): Observable<PetAttachment[]> {
    return this.http.get<PetAttachment[]>(this.petAttachUrl, this._token.jwtBearer())
      .pipe(
        tap(),
        catchError(this._error.handleErrorStatus('getFiles', []))
      );
  }

  // list pet file by id
  getPetFilesById (id: string): Observable<PetAttachment> {
    return this.http.get<PetAttachment>(this.AttachUrl+"/"+id, this._token.jwtBearer())
      .pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
      );
  }

  // download file
  getFilesById (instanceid: string): Observable<any> {
    const url = this.downloadUrl+"/"+instanceid;
    return this.http.get<any>(this.downloadUrl+"/"+instanceid, this._token.jwtBearer())
      .pipe(
      tap(),
      catchError(this._error.handleErrorStatus<string>(`loadByInstance id=${instanceid}`))
    );
  }
  
  updateStatus (petAttachment: PetAttachment): Observable<PetAttachment> {
    return this.http.put(this.updateUrl+"/"+petAttachment.id, petAttachment, this._token.jwtBearer()).pipe(
        tap(heroes => console.log(`update attachment file with id=${petAttachment.id}`)),
        catchError(this._error.handleErrorStatus<any>())
    );
  }

  getFilesByWallId(id : string): Observable<PetAttachment>{
    return this.http.get<PetAttachment>(this.wallUrl+id, this._token.jwtBearer())
      .pipe(
        tap(),
        catchError(this._error.handleErrorStatus<PetAttachment>(`find attachment by wall id =${id}`))
      )
  }
}
