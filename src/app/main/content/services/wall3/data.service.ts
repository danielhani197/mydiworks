import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class DataService {
  public sharedData: any;
  public sharedInfo: any;

  private _listners = new Subject<any>();

  listen(): Observable<any> {
    return this._listners.asObservable();
 }

 filter() {
    this._listners.next();
  }
  constructor(){
  }

  setData (data) {
    this.sharedData = data;
  }
  getData () {
    return this.sharedData;
  }
  setInfo (info) {
    this.sharedInfo = info;
  }
  getInfo () {
    return this.sharedInfo;
  }

  clearInfo(){
    this.sharedData = null;
    this.sharedInfo = null;
  }
}