import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import { WallKandungan } from '../../model/wall3/wallKandungan';
import { WallKomen } from '../../model/wall3/wallKomen';
import { PostCollection } from '../../model/wall3/postCollection';
import { AdvancedPage } from '../../model/util/advanced-page';
import { AdvancedPagedData } from '../../model/util/advanced-paged-data';
import {environment} from '../../../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class MyWallService {

  private globalUrl = `${environment.apiUrl}/api/post`;

  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }

  getPostById( id: string ):Observable<WallKandungan> {
    return this.http.get(this.globalUrl+"/getPost/"+id, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    )  
  }

  getKomenById( id: string ):Observable<WallKomen> {
    return this.http.get(this.globalUrl+"/getKomen/"+id, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    )  
  }

  updateKomen (komen: WallKomen): Observable<WallKomen> {
    return this.http.put(this.globalUrl+'/editKomen/'+komen.id, komen, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
    );
  }
  
  updatePost (post: WallKandungan): Observable<WallKandungan> {
    return this.http.put(this.globalUrl+'/editPost/'+post.id, post, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
    );
  }

  createPost(wallKandungan: any):Observable<WallKandungan> {
    return this.http.post(this.globalUrl+"/create", wallKandungan, this._token.jwtFile()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
    )  
  }

  createKomen(wallkomen: WallKomen):Observable<WallKandungan[]> {
    return this.http.post(this.globalUrl+"/createKomen", wallkomen, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
    )  
  }

  getPostAll (userid: string): Observable<WallKandungan[]> {

    return this.http.get<WallKandungan[]>(this.globalUrl+'/getAll/'+userid, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus())
    );
  }

  getNewPostAll (page: AdvancedPage): Observable<any> {

    return this.http.post(this.globalUrl+'/getNewAll/', page, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus())
    );
  }

  deleteKomenById(id: string):Observable<WallKomen> {
    return this.http.delete(this.globalUrl+"/deleteKomen/"+id , this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
    )  
  }
  
  deletePostById(id: string):Observable<WallKandungan> {
    return this.http.delete(this.globalUrl+"/deletePost/"+id , this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
    )  
  }
}
