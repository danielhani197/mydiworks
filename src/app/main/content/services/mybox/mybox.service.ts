import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers, ResponseType } from '@angular/http';
import { Observable, of, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { MyBoxAttachment } from '../../model/mybox/myboxAttachment';
import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';
import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import { id } from '@swimlane/ngx-datatable/release/utils';
import { MyBoxShare } from '../../model/mybox/myboxShare';
import {environment} from '../../../../../environments/environment';
import { FolderMyBox } from '../../model/mybox/folderMyBox';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class MyBoxService {

    parentid: string = "home";
    event:any;
    currentEvent:any;

    private _listners = new Subject<any>();

    listen(): Observable<any> {
       return this._listners.asObservable();
    }
    filter(id: string) {
        this.parentid = id
       this._listners.next(id);
    }

    private globalUrl = `${environment.apiUrl}/api/mybox`;
    private shareUrl  = `${environment.apiUrl}/api/share`;

    constructor(
        private http: HttpClient,
        private _token: TokenService,
        private _error: ErrorHandlerService
        ) {}
    eventChange(event){
        this.currentEvent = event;
    }

    eventSideNav(){
        return this.currentEvent;
    }

    setlocation(id: string){
        this.parentid = id;
    }

    // save file
    saveFile (obj: any): Observable<MyBoxAttachment> {
        if(this.parentid=="home"){
        return this.http.post(this.globalUrl+"/doc/save", obj, this._token.jwtFile()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>('updateUser'))
            );
        } else {
            return this.http.post(this.globalUrl+"/fileFolder/save/" +this.parentid, obj, this._token.jwtFile()).pipe(
                tap(),
                catchError(this._error.handleErrorStatus<any>('updateUser'))
                );           
        }
    }

    getParentId(){
        return this.parentid
    }

    // save folder 
    saveFolder (obj: any): Observable<MyBoxAttachment> {
        if(this.parentid=="home"){
            return this.http.post(this.globalUrl+"/create/folder", obj, this._token.jwtBearer()).pipe(
                tap(),
                catchError(this._error.handleErrorStatus<any>('updateUser'))
                );
        }else{
            return this.http.post(this.globalUrl+"/create/folder/" +this.parentid, obj, this._token.jwtBearer()).pipe(
                tap(),
                catchError(this._error.handleErrorStatus<any>('updateUser'))
                );
        }   
    }

    // get all
    getMybox (): Observable<MyBoxAttachment[]> {
        return this.http.get<MyBoxAttachment[]>(this.globalUrl+'/mybox/getall', this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus('getMybox', []))
        );
    }

    // get list by userid
    getMyboxList (userid: string): Observable<MyBoxAttachment[]> {
        return this.http.get<MyBoxAttachment[]>(this.globalUrl+'/getmybox/'+userid, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus('getMybox', []))
        );
    }

    // getMyboxSharedList (userid: string): Observable<MyBoxAttachment[]> {
    //     return this.http.get<MyBoxAttachment[]>(this.globalUrl+'/getmybox/'+userid, this._token.jwtBearer())
    //     .pipe(
    //         tap(),
    //         catchError(this._error.handleErrorStatus('getMybox', []))
    //     );
    // }

    // get list by parent id
    getListInFolder (parentid: string): Observable<MyBoxAttachment[]> {
        return this.http.get<MyBoxAttachment[]>(this.globalUrl+'/getFilesFolder/'+parentid, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus('getFolder', []))
        );
    }

    getBreadCrumbsById(id: string): Observable<any>{
        return this.http.get<any>(this.globalUrl+'/getBreadCrumbs/'+id, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        );
    }

    // download file
    getMyBoxFileById (instanceid: string): Observable<any> {
        const url = this.globalUrl+'/mybox/download/'+instanceid;
        return this.http.get<any>(this.globalUrl+'/mybox/download/'+instanceid, this._token.jwtBearer())
        .pipe(
        tap(),
        catchError(this._error.handleErrorStatus<string>(`loadByInstance id=${instanceid}`))
        );
    }

    // download folder
    getMyBoxFolderById(id: string): Observable<FolderMyBox> {
        const url = this.globalUrl+'/mybox/downloadFolder/'+id;
        return this.http.get<FolderMyBox>(url, this._token.jwtBearer())
        .pipe(
        tap(),
        catchError(this._error.handleErrorStatus<FolderMyBox>(``))
        );
    }

    // delete file
    deleteMyBoxFileById(id: string):Observable<MyBoxAttachment> {
        return this.http.delete(this.globalUrl+"/mybox/delete/"+id , this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )  
    }
    shareMyBoxFile(myboxShare: MyBoxShare):Observable<MyBoxShare>{
        return this.http.post(this.shareUrl+"/share", myboxShare, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }

    getMyBoxById(id: string):Observable<any>{
        return this.http.get<any>(this.globalUrl+"/setShare/"+id, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }

    getFilterShare (page: Page): Observable<PagedData<MyBoxShare>> {
        return this.http.post(this.shareUrl+'/filter', page, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        );
    }
    getLatersShare():Observable<MyBoxShare>{
        return this.http.get(this.shareUrl+'/laters', this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }

    //listing
    getMBList(page: Page): Observable<PagedData<MyBoxAttachment>> {
        return this.http.post(this.globalUrl+'/mybox/list', page, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        );
    }

    getShareFolder(userId: string):Observable<MyBoxShare[]>{
        return this.http.get<MyBoxShare[]>(this.shareUrl+"/shared/"+userId, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<MyBoxShare[]>())
        )
    }
    getMyBoxId(myboxId: string):Observable<MyBoxAttachment>{
        return this.http.get<MyBoxAttachment>(this.globalUrl+"/getmyboxid/"+myboxId, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<MyBoxAttachment>())
        )
    }
}
