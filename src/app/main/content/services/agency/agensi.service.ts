import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { environment} from '../../../../../environments/environment';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Agensi } from '../../model/general/agensi';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class AgensiService {

    private globalUrl = `${environment.apiUrl}/api/agency`;
 
    constructor(
        private http: HttpClient,
        private _token: TokenService,
        private _error: ErrorHandlerService
    ) { }

    //Get All Agensi
    getAllAgencies (): Observable<Agensi[]> {

        return this.http.get<Agensi[]>(this.globalUrl+'/all', this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleError('getAllAgencies', []))
        );
    }

    getAgencyList (page: Page): Observable<PagedData<Agensi>> {
        return this.http.post(this.globalUrl+'/listing', page, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        );
    }

    getAgencyById (id: string): Observable<Agensi> {
        return this.http.get<Agensi>(this.globalUrl+'/get/'+id, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleError('getAgencyById'))
        );
    }

    createAgensi (agensi: Agensi): Observable<Agensi> {
        return this.http.post(this.globalUrl+'/create/', agensi, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>('createAgensi'))
        );
    }

    updateAgensi (agensi: Agensi): Observable<Agensi> {
        return this.http.put(this.globalUrl+'/edit/'+agensi.id, agensi, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>('updateAgensi'))
        );
    }

    deleteAgensiById (id: string):Observable<Agensi> {
        return this.http.delete<Agensi>(this.globalUrl+'/delete/'+id, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus('deleteAgensiById'))
        );
    }

    getAllAgenciesByMinistryId (id: string): Observable<Agensi[]> {
        return this.http.get<Agensi[]>(this.globalUrl+'/ministry/'+id, httpOptions)
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        );
    }

}
