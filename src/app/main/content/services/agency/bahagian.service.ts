import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { environment} from '../../../../../environments/environment';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Bahagian } from '../../model/general/bahagian';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class BahagianService {

    private globalUrl = `${environment.apiUrl}/api/bahagian`;
 
    constructor(
        private http: HttpClient,
        private _token: TokenService,
        private _error: ErrorHandlerService
    ) { }

    getAllBahagian (): Observable<Bahagian[]> {

        return this.http.get<Bahagian[]>(this.globalUrl+'/all', this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleError('getAllAgencies', []))
        );
    }

    getBahagianList (page: Page, type: string, id: string): Observable<PagedData<Bahagian>> {
        return this.http.post(this.globalUrl+'/listing/'+type+'/'+id, page, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        );
    }

    getBahagianById (id: string): Observable<Bahagian> {
        return this.http.get<Bahagian>(this.globalUrl+'/get/'+id, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleError('getBahagianById'))
        );
    }

    getBahagiansByAgensiId (id: string): Observable<Bahagian[]> {
        return this.http.get<Bahagian[]>(this.globalUrl+'/getByAgensi/'+id, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        );
    }

    getBahagiansByKementerianId (id: string): Observable<Bahagian[]> {
        return this.http.get<Bahagian[]>(this.globalUrl+'/getByMinistery/'+id, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        );
    }

    createBahagian (bahagian: Bahagian): Observable<Bahagian> {
        return this.http.post(this.globalUrl+'/create/', bahagian, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>('createBahagian'))
        );
    }

    updateBahagian (bahagian: Bahagian): Observable<Bahagian> {
        return this.http.put(this.globalUrl+'/edit/'+bahagian.id, bahagian, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>('updateBahagian'))
        );
    }

    deleteBahagianById (id: string):Observable<Bahagian> {
        return this.http.delete<Bahagian>(this.globalUrl+'/delete/'+id, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleError('deleteBahagianById'))
        );
    }

}
