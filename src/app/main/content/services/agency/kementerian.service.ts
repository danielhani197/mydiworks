import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Kementerian } from 'app/main/content/model/general/kementerian';
import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';
import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import {environment} from '../../../../../environments/environment';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable({ providedIn: 'root' })
export class KementerianService {
 
  private globalUrl = `${environment.apiUrl}/api/ministry`;
 
  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }
 
  getKementerians (): Observable<Kementerian[]> {

    return this.http.get<Kementerian[]>(this.globalUrl+'/all', httpOptions)
    .pipe(
      tap(),
      catchError(this._error.handleError('getKementerian', []))
    );
  }

  getKementerianList (page: Page): Observable<PagedData<Kementerian>> {
    return this.http.post(this.globalUrl+'/listing', page, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>())
    );
  }

  getKementerianById(id : string): Observable<Kementerian>{
    return this.http.get<Kementerian>(this.globalUrl+'/get/'+id, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus())
    );
  }

  createKementerian (kementerian: Kementerian): Observable<Kementerian> {
    return this.http.post(this.globalUrl+'/create/', kementerian, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('createKementerian'))
    );
  }

  updateKementerian (kementerian: Kementerian): Observable<Kementerian> {
    return this.http.put(this.globalUrl+'/edit/'+kementerian.id, kementerian, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updateKementerian'))
    );
  }

  deleteKementerianById (id: string):Observable<Kementerian> {
    return this.http.delete<Kementerian>(this.globalUrl+'/delete/'+id, this._token.jwtBearer())
    .pipe(
        tap(),
        catchError(this._error.handleErrorStatus('deleteKementerianById'))
    );
  }
 
}
