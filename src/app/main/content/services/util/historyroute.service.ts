import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class HistoryRouteService {

  static ISADMIN: string = "ADMIN";
  static ISSAM: string = "SAM";
  public previousPath: any;
  public role: any;

  private _listners = new Subject<any>();

  listen(): Observable<any> {
    return this._listners.asObservable();
 }

 filter() {
    this._listners.next();
  }
  constructor(){
  }

  setPreviousPath (path) {
    this.previousPath = path;
  }
  getPreviousPath () {
    return this.previousPath;
  }

  setRole (role) {
    this.role = role;
  }
  getRole () {
    return this.role;
  }

  clearRole(){
    this.role = null;
  }

  clearPreviousPath(){
    this.previousPath = null;
  }

  clearAll(){
    this.role = null;
    this.previousPath = null;
  }
}