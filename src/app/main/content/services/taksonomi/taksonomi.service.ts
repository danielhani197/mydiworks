import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Taksonomi } from '../../model/wall3/taksonomi';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import {environment} from '../../../../../environments/environment';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable({ providedIn: 'root' })
export class TaksonomiService {

    private globalUrl = `${environment.apiUrl}/api/taks`;

    constructor(
      private http: HttpClient,
      private _token: TokenService,
      private _error: ErrorHandlerService
    ) { }
    
    //Get All Taksonomi
    getTaks(): Observable<Taksonomi[]> {
      return this.http.get<Taksonomi[]>(this.globalUrl+'/taksonomi/all', this._token.jwtBearer())
      .pipe(
        tap(),
        catchError(this._error.handleError('getTaks', []))
      );
    }

  search(term) {
      var listOfTaks = this.http.get(this.globalUrl+'/taksonomi/search/' + term, this._token.jwtBearer())
      .pipe(
          debounceTime(500),  // WAIT FOR 500 MILISECONDS ATER EACH KEY STROKE.
          map(
              (data: any) => {
                  return (
                      data.length != 0 ? data as any[] : [{"Taksonomi": "No Record Found"} as any]
                  );
              }
      ));

      return listOfTaks;  
  }

  keyUpSearch(key: string): Observable<Taksonomi[]> {
    return this.http.get<Taksonomi[]>(this.globalUrl+'/taksonomi/search?searchterm=' + key, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus())
    )
  }
 
}
