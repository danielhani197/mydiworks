import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Grade } from 'app/main/content/model/general/grade';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';
import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import {environment} from '../../../../../environments/environment';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class GradeService {

    private globalUrl = `${environment.apiUrl}/api/grade`;
 
    constructor(
        private http: HttpClient,
        private _token: TokenService,
        private _error: ErrorHandlerService
    ) { }

    getGradeList (page: Page): Observable<PagedData<Grade>> {
        return this.http.post(this.globalUrl+'/listing', page, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        );
    }

    getGradeCn(): Observable<Grade[]>{
        return this.http.get(this.globalUrl+"/count", this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }

    getGradeCnEdit(): Observable<Grade[]>{
        return this.http.get(this.globalUrl+"/countEdit", this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }

    createGrade(grade: Grade):Observable<Grade> {

        return this.http.post(this.globalUrl+"/create", grade, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }


    getGradeById(id: string): Observable<Grade> {
        return this.http.get(this.globalUrl+"/get/"+id, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }

    updateGrade(grade: Grade):Observable<Grade> {

        return this.http.put(this.globalUrl +"/edit/"+ grade.id, grade, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }

    deleteGradeById(id: string): Observable<Grade> {

        return this.http.delete<Grade>(this.globalUrl +"/delete/"+ id, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        )
    }

    getGrade():Observable<Grade[]> {
        return this.http.get<Grade[]>(this.globalUrl+"/all", this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        )
    }
}
