import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Skema } from 'app/main/content/model/general/skema';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';
import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import {environment} from '../../../../../environments/environment'; 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class SkemaService {

    private globalUrl = `${environment.apiUrl}/api/skema`;
 
    constructor(
        private http: HttpClient,
        private _token: TokenService,
        private _error: ErrorHandlerService
    ) { }

    getSkemaList (page: Page): Observable<PagedData<Skema>> {
        return this.http.post(this.globalUrl+'/listing', page, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        );
    }
    
    getSchema(): Observable<Skema[]> {
        return this.http.get<Skema[]>(this.globalUrl+'/all', this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        )
    }

    getSchemaCn(): Observable<Skema[]>{
        return this.http.get(this.globalUrl+"/count", this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }

    getSchemaCnEdit(): Observable<Skema[]>{
        return this.http.get(this.globalUrl+"/countEdit", this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }

    createSchemas(skema: Skema):Observable<Skema> {

        return this.http.post(this.globalUrl+"/create", skema, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }


    getSchemaById(schemaId: string): Observable<Skema> {
        return this.http.get(this.globalUrl+"/get/"+schemaId, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }

    updateSchema(schema: Skema):Observable<Skema> {

        return this.http.put(this.globalUrl +"/edit/"+ schema.id,schema, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }

    deleteSchemaById(schemaId: string): Observable<Skema> {

        return this.http.delete<Skema>(this.globalUrl +"/delete/"+ schemaId, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus())
        )
    }
}
