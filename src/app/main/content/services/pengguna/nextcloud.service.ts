import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers, Response } from '@angular/http';
 
import { of, empty, Observable } from 'rxjs';
import { catchError, map, tap, filter } from 'rxjs/operators';
import { User } from 'app/main/content/model/user';
import { City } from 'app/main/content/model/city';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';

import 'rxjs/add/observable/throw';
import {environment} from '../../../../../environments/environment';
 
const httpOptions = {
  headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization':'Basic YWRtaW46UGlqZXBpamUxPw==',
      'OCS-APIRequest':'true '
    })
};

@Injectable({ providedIn: 'root' })
export class NextcloudService {

  private ncUrl = `${environment.nextCloudUrl}/cloud/users`;

  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }

  addNewUser (): Observable<User> {
    return this.http.post(this.ncUrl, JSON.stringify({ "userid": "din", "password": "Pijepije1?" }), httpOptions).pipe(
    //return this.http.post(this.ncUrl, { "userid": "din", "password": "Pijepije1?" }, httpOptions).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

}
