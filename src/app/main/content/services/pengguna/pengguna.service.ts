import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers, Response } from '@angular/http';
 
import { of, empty, Observable } from 'rxjs';
import { catchError, map, tap, filter } from 'rxjs/operators';
import { User } from 'app/main/content/model/user';
import { City } from 'app/main/content/model/city';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';

import 'rxjs/add/observable/throw';
import { throwError } from 'rxjs/internal/observable/throwError';
import {environment} from '../../../../../environments/environment';
import { UserAuthorities } from '../../model/userAuthorities';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable({ providedIn: 'root' })
export class PenggunaService {

  private userUrl = `${environment.apiUrl}/api/user`;

  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }


 getUserById (id: string): Observable<User> {
  return this.http.get(this.userUrl+"/get/"+id, this._token.jwtBearer()).pipe(
    tap(),
    catchError(this._error.handleErrorStatus<any>())
  );
}

  getUsersNew (page: Page): Observable<PagedData<User>> {
    return this.http.post(this.userUrl+"/new", page, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

  approveNewUser (id: string): Observable<User> {
    return this.http.get(this.userUrl+"/approve/"+id, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

  rejectNewUser (id: string): Observable<User> {
    return this.http.get(this.userUrl+"/reject/"+id, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

  isExistUsername (username: string) {
    return this.http.get(this.userUrl+"/exist/username/"+username, httpOptions).pipe(
      tap(),
      catchError(
        error => {
          return throwError(error)
        }
      )
    );
  }

  updateUserTerm (id: string): Observable<User> {
    return this.http.get(this.userUrl+'/update/term/'+id, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

  //update profile user
  updateUserProfilPic(obj: any): Observable<User>{
    return this.http.post(this.userUrl+'/profile/editPic', obj, this._token.jwtFile()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updateUser'))
    )
  }
  updateUserProfil(user: User): Observable<User> {
    return this.http.post<User>(this.userUrl+'/profile/edit/', user, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus<User>('editTerm'))
    );
  }

  adminkemaskiniMaklumat(user:User): Observable<User> {
    return this.http.post(this.userUrl+'/editMaklumat', user, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updateUser'))
    )
  }

  adminkemaskiniAgensi(user:User): Observable<User> {
    return this.http.post(this.userUrl+'/editAgensi', user, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updateUser'))
    )
  }

  adminKemaskiniPerhubungan(user:User): Observable<User> {
    return this.http.post(this.userUrl+'/editPerhubungan', user, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updateUser'))
    )
  }

  adminKemaskiniPeranan(userAuthorities: UserAuthorities[]): Observable<any> {
    return this.http.post(this.userUrl+'/editPeranan', userAuthorities, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updateUser'))
    )
  }

  userResetPassword (user: User): Observable<User> {
    return this.http.post(this.userUrl+'/editPassword', user, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('updateUser'))
    )
  }

  getAuthoritiesListByUserId(id: string): Observable<UserAuthorities[]>{
    return this.http.get<UserAuthorities[]>(this.userUrl+'/getUser/'+id, this._token.jwtBearer())
    .pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>('getAuthorities'))
    )
  }

  activateUser (id: string): Observable<any> {
    return this.http.get(this.userUrl+'/update/status/enabled/'+id, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

  deActivateUser (id: string): Observable<any> {
    return this.http.get(this.userUrl+'/update/status/disabled/'+id, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

  deleteUserById (id: string):Observable<any> {
    return this.http.delete(this.userUrl+'/delete/'+id, this._token.jwtBearer())
    .pipe(
        tap(),
        catchError(this._error.handleErrorStatus('deleteUserById'))
    );
  }

  getAllUser(): Observable<any> {
    return this.http.get(this.userUrl+'/findAllUsers', this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

  keyUpSearch(name: string): Observable<any> {
    return this.http.get(this.userUrl+'/findUserByName/'+name, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }
}
