import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers, Response } from '@angular/http';
 
import { of, empty, Observable } from 'rxjs';
import { catchError, map, tap, filter } from 'rxjs/operators';
import { User } from 'app/main/content/model/user';
import { City } from 'app/main/content/model/city';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';

import 'rxjs/add/observable/throw';
import { throwError } from 'rxjs/internal/observable/throwError';
import {environment} from '../../../../../environments/environment';
import { UserAuthorities } from '../../model/userAuthorities';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable({ providedIn: 'root' })
export class AdminService {

  private userUrl = `${environment.apiUrl}/api/admin/user`;

  constructor(
    private http: HttpClient,
    private _token: TokenService,
    private _error: ErrorHandlerService
  ) { }

  getUserApproved (page: Page): Observable<PagedData<User>> {
    return this.http.post(this.userUrl+"/app", page, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

  getNewUser (page: Page): Observable<PagedData<User>> {
    return this.http.post(this.userUrl+"/new", page, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }

  getAppAuth (page: Page): Observable<PagedData<User>> {
    return this.http.post(this.userUrl+"/approle", page, this._token.jwtBearer()).pipe(
      tap(),
      catchError(this._error.handleErrorStatus<any>())
    );
  }
 

}
