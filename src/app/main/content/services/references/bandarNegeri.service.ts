import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Negeri } from '../../model/ref/negeri';
import { Bandar } from '../../model/ref/bandar';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import {environment} from '../../../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable({ providedIn: 'root' })
export class BandarNegeriService {

    private globalUrl = `${environment.apiUrl}/api/ref`;

    constructor(
      private http: HttpClient,
      private _token: TokenService,
      private _error: ErrorHandlerService
    ) { }
    
    //Get All Negeri
    getState(): Observable<Negeri[]> {
      return this.http.get<Negeri[]>(this.globalUrl+'/state/all', this._token.jwtBearer())
      .pipe(
        tap(),
        catchError(this._error.handleError('getState', []))
      );
    }

    //Get Bandar by Negeri id
    getCity(stateId: string): Observable<Bandar[]> {
      const url = `${this.globalUrl}/city/state/${stateId}`;
      return this.http.get<Bandar[]>(url).pipe(
        tap(),
        catchError(this._error.handleError<Bandar[]>(`getCity with state id=${stateId}`))
      );
    }
 
}
