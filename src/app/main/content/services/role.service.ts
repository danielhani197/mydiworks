import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { AppAuthority } from 'app/main/content/model/appAuthority';

import { Page } from 'app/main/content/model/util/page';
import { PagedData } from 'app/main/content/model/util/paged-data';

import { TokenService } from 'app/main/content/services/util/token.service';
import { ErrorHandlerService } from 'app/main/content/services/util/error.handler.service';
import {environment} from '../../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class RoleService{
    private globalUrl = `${environment.apiUrl}/api/role`;
    private refUrl = `${environment.apiUrl}/api/ref`;

    constructor(
        private http: HttpClient,
        private _token: TokenService,
        private _error: ErrorHandlerService
      ) { }

    // create
    createRole (app: AppAuthority): Observable<AppAuthority>{
        return this.http.post<AppAuthority>(this.globalUrl+'/create', app, this._token.jwtBearer()).pipe(
        tap(),
        catchError(this._error.handleErrorStatus<any>('create role'))
        );
    }

    // update
    updateRole (app: AppAuthority): Observable<AppAuthority> {
        return this.http.put(this.globalUrl+'/edit/'+app.id, app, this._token.jwtBearer()).pipe(tap(),
            catchError(this._error.handleErrorStatus<any>('updateRole'))
        );
    }

    // listing
    getRoleList(page: Page): Observable<PagedData<AppAuthority>> {
        return this.http.post(this.globalUrl+'/list', page, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
          );
    }

    // get all
    getRole(): Observable<AppAuthority[]> {
        return this.http.get<AppAuthority[]>(this.refUrl+'/role', httpOptions).pipe(tap(),
            catchError(this._error.handleErrorStatus('getRole', []))
        );
    }

    // get role by id
    getRoleById (id: string): Observable<AppAuthority> {
        return this.http.get<AppAuthority>(this.globalUrl+'/get/'+id, this._token.jwtBearer())
        .pipe(
            tap(),
            catchError(this._error.handleErrorStatus('getRoleById'))
        );
    }    
}
