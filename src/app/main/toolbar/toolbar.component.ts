import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { navigation } from 'app/navigation/navigation';

import { AuthenticationService } from 'app/main/content/services/authentication/authentication.service';

import { User } from 'app/main/content/model/user';

import * as Stomp from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import { environment } from '../../../environments/environment';
import { filter, debounceTime } from 'rxjs/operators';
import { NotifikasiService } from '../content/services/setting/notifikasi.service';
import { Page } from '../content/model/util/page';
import { Notifikasi } from '../content/model/notifikasi';
import { PenggunaService } from '../content/services/pengguna/pengguna.service';
import { Authorities } from '../content/model/authorities';
import { UserAuthorities } from '../content/model/userAuthorities';

@Component({
    selector   : 'fuse-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls  : ['./toolbar.component.scss']
})

export class FuseToolbarComponent implements OnInit
{
    userStatusOptions: any[];
    languages: any;
    selectedLanguage: any;
    showLoadingBar: boolean;
    horizontalNav: boolean;
    noNav: boolean;
    navigation: any;

    page:Page;
    currentUser: User;
    user: User;
    adminAuth: boolean;
    agencyAuth: boolean;
    roles : any;

    userId: string;
    listAuthorities: UserAuthorities[] = new Array<UserAuthorities>();

    count:any;
    c:number;
    notiCount:Notifikasi[];
    dataNotifikasi:Notifikasi[];
    z:number= 0;
    list:Notifikasi[];

    imageSrc:any;

    private stompSelfSub;

    constructor(
        private router: Router,
        private fuseConfig: FuseConfigService,
        private sidebarService: FuseSidebarService,
        private translate: TranslateService,
        private _auth: AuthenticationService,
        private cdRef : ChangeDetectorRef,
        private _notification: NotifikasiService,
        private _pengguna : PenggunaService
    )
    {
        this.userStatusOptions = [
            {
                'title': 'Online',
                'icon' : 'icon-checkbox-marked-circle',
                'color': '#4CAF50'
            },
            {
                'title': 'Away',
                'icon' : 'icon-clock',
                'color': '#FFC107'
            },
            {
                'title': 'Do not Disturb',
                'icon' : 'icon-minus-circle',
                'color': '#F44336'
            },
            {
                'title': 'Invisible',
                'icon' : 'icon-checkbox-blank-circle-outline',
                'color': '#BDBDBD'
            },
            {
                'title': 'Offline',
                'icon' : 'icon-checkbox-blank-circle-outline',
                'color': '#616161'
            }
        ];

        this.languages = [
            {
                'id'   : 'my',
                'title': 'Melayu',
                'flag' : 'my'
            },
            {
                'id'   : 'en',
                'title': 'English',
                'flag' : 'uk'
            }
        ];

        this.selectedLanguage = this.languages[0];

        router.events.subscribe(
            (event) => {
                if ( event instanceof NavigationStart )
                {
                    this.showLoadingBar = true;
                }
                if ( event instanceof NavigationEnd )
                {
                    this.showLoadingBar = false;
                }
            });

        this.fuseConfig.onConfigChanged.subscribe((settings) => {
            this.horizontalNav = settings.layout.navigation === 'top';
            this.noNav = settings.layout.navigation === 'none';
        });

        this.navigation = navigation;
        this.currentUser = this._auth.getCurrentUser();

                // this._pengguna.getAuthoritiesListByUserId(this.currentUser.id).subscribe(
                //     data=>{
                //         this.listAuthorities = data;
                //         for(let obj of this.listAuthorities){
                //             if(obj.authority.name == "ROLE_SUPERADMIN"){
                //                 this.adminAuth = true;
                //             }else{
                //                 if(obj.authority.name != "ROLE_SUPERADMIN"){
                //                     this.agencyAuth = true;
                //                 }
                //             }
                            
                            
                            
                //         }

                //     }
                // )

            this.roles = this.currentUser.authorities;
            var roleArr = new Array();
            for(let roleObj of this.roles){
                let role = roleObj.authority;
                roleArr.push(role);
            }

            if(roleArr.includes('ROLE_SUPERADMIN')){
                this.adminAuth = true;
            }else if(roleArr.includes('ROLE_ADMIN')){
                this.agencyAuth = true;
            }

       
    }

    ngOnInit(){
        
        this.currentUser = this._auth.getCurrentUser();
        if(this.currentUser){
            const id = this.currentUser.id;
            this.setBadge(id);
        }
        if (this.currentUser.image) {
            this.imageSrc = "data:image/JPEG;base64," + this.currentUser.image;
        }
        if (!this.currentUser.image) {
            this.imageSrc = "assets/images/avatars/profile.jpg";
        }

        // let ws = new SockJS(`${environment.apiUrl}/socket`);
        // this.stompSelfSub = Stomp.over(ws);
        // let that = this
        // that.stompSelfSub.debug = () => {};
        // that.stompSelfSub.connect({}, function (frame) {
        //     that.stompSelfSub.subscribe("/topic/private.noti." + that.user.id, (chatlive) => {
        //         if(chatlive){
                    
        //             this._notification.getNotiByUserId(this.currentUser.id).subscribe(
        //                 data=>{
        //                     this.dataNotifikasi = data
        //                     for( let i=0; i<data.length;i++){
        //                         if(this.dataNotifikasi[i].status =="New"){
        //                             this.z = this.z + 1;
        //                         }
        //                     }
        //                     this.count = this.z;
                            
        //                 }
        //             )
        //         }
        //     });
        // });
    }
    setBadge(id: string){
        this._notification.getNotiByUserId(id).subscribe(
            data=>{
                this.dataNotifikasi = data
                for( let i=0; i<data.length;i++){
                    if(this.dataNotifikasi[i].status =="New"){
                        this.z = this.z + 1;
                    }
                }
                this.count = this.z;
                
            }
        )
    }

    toggleSidebarOpened(key)
    {
        this.sidebarService.getSidebar(key).toggleOpen();
    }

    search(value)
    {
        // Do your search here...
    }

    setLanguage(lang)
    {
        // Set the selected language for toolbar
        this.selectedLanguage = lang;

        // Use the selected language for translations
        this.translate.use(lang.id);
    }

    logout(){
        this._auth.logout();
        this.router.navigate(['/login']);
    }

    myprofile() {
        this.router.navigate(['profileEdit/edit/']);
    }


    setCountNoti(){

        this.currentUser = this._auth.getCurrentUser();
        const id = this.currentUser.id;
        
        this._notification.getNotiByUserId(id).subscribe(
            noti => {
                this.list = noti;  
                for(let i=0; i < this.list.length; i++){
                    if(this.list[i].status =="New"){
                        let noti: Notifikasi = new Notifikasi();
                        noti.setModifiedby(this.list[i].modifiedby),
                        noti.setId(this.list[i].id)
                        this._notification.updateStatusReadNoti(noti).subscribe(
                        success=>{
                            this.count = 0;
                            }
                        )
                    }
                }
               
            }
        )
    }

}
