import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of, Subject } from 'rxjs';

import { AuthenticationService } from '../content/services/authentication/authentication.service';
import { User } from '../content/model/user';
import { Thread } from '../content/model/chat/thread';
import { ErrorHandlerService } from '../content/services/util/error.handler.service';
import { TokenService } from '../content/services/util/token.service';
import { Message } from '../content/model/chat/message';
import {environment} from '../../../environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class ChatPanelService
{
    contacts: User[];
    chats: any[];
    user: any;
    
    // Default to Bahagian
    groups: string = "bahagian";
    private globalUrl = `${environment.apiUrl}/api/message`;
    
    private _listners = new Subject<any>();

    listen(): Observable<any> {
       return this._listners.asObservable();
    }

    filter(filterBy: string) {
        this.groups = filterBy;
       this._listners.next(filterBy);
    }

    public _listner2 = new Subject<any>();
    trigger(){
        this._listner2.next();
    }
    
    constructor(
        private _httpClient: HttpClient,
        private _token: TokenService,
        private _auth: AuthenticationService,
        private _error: ErrorHandlerService
    )
    {
    }

    createThread(thread: Thread):Observable<Thread>{
        return this._httpClient.post(this.globalUrl+"/createThread", thread, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }

    getThread(thread: Thread):Observable<Thread>{
        return this._httpClient.post(this.globalUrl+"/getThread", thread, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }
    
    setGroups(group: string){
        this.groups = group;
        this.getUsers().subscribe(
            data=>{
                this.contacts = data;
            }
        )
    }

    getUsers():Observable<any>{
        let user = this._auth.getCurrentUser();
        let userId = ""
        if(this.groups === "bahagian"){
            
            let bhgId = "";
            if (user == null){
                bhgId = "";
                userId = "";
            }else {
                userId = user.id;
                if (user.bahagian!=null){
                    bhgId = user.bahagian.id;
                }else{
                    bhgId = "nothing";
                }
            }
            
            
            return this._httpClient.get(this.globalUrl+"/bahagian/get/"+bhgId+"/"+userId, this._token.jwtBearer()).pipe(
                tap(),
                catchError(this._error.handleErrorStatus<any>())
            )
        }else{
            if(user != null){
                userId = user.id;
            }
            return this._httpClient.get(this.globalUrl+"/pet/get/"+this.groups+"/"+userId, this._token.jwtBearer()).pipe(
                tap(),
                catchError(this._error.handleErrorStatus<any>())
            )
        }
    }

    isThreadExist(thread: Thread):Observable<boolean>{

        return this._httpClient.post(this.globalUrl+"/threadExist", thread, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }

    getMessagesByThreadId(id: string):Observable<Message[]>{
        return this._httpClient.get(this.globalUrl+"/threads/message/"+id, this._token.jwtBearer()).pipe(
            tap(),
            catchError(this._error.handleErrorStatus<any>())
        )
    }
}
