import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { fuseAnimations } from '@fuse/animations';

import { Subject } from 'rxjs';

import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';

import * as Stomp from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import * as $ from "jquery";
import { ChatPanelService } from './chat-panel.service';
import { User } from '../content/model/user';
import { AuthenticationService } from '../content/services/authentication/authentication.service';
import { Thread } from '../content/model/chat/thread';
import { ChatMessage } from '../content/model/chat/chatMessage';
import { Message } from '../content/model/chat/message';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'chat-panel',
    templateUrl: './chat-panel.component.html',
    styleUrls: ['./chat-panel.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ChatPanelComponent implements OnInit, AfterViewInit, OnDestroy {
    private stompClient;
    private stompSelfSub;
    private tempStomp;
    contacts: any[];

    chat: ChatMessage[] = new Array<ChatMessage>();

    roomId: string = "OPEN";

    selectedContact: User;
    sidebarFolded: boolean;

    user: User;

    messageInput = document.querySelector('#message');

    username: string;

    @ViewChild('replyInput') replyInput: ElementRef;
    @ViewChild('message') messagePanel: ElementRef;


    private _unsubscribeAll: Subject<any>;

    constructor(
        private _chatPanelService: ChatPanelService,
        private _fuseSidebarService: FuseSidebarService,
        private _auth: AuthenticationService
    ) {
        // Set the defaults
        this.selectedContact = null;
        this.sidebarFolded = true;
        this.user = this._auth.getCurrentUser();
        // Set the private defaults
        this._unsubscribeAll = new Subject();

        this._chatPanelService.listen().subscribe(
            (e:any)=>{
                this.setUser();
            }
        )
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngOnInit(): void {

        this._chatPanelService.setGroups("bahagian");
        this.setUser();
        let ws = new SockJS(`${environment.apiUrl}/socket`);
        this.stompSelfSub = Stomp.over(ws);
        let that = this
        that.stompSelfSub.debug = () => {};
        that.stompSelfSub.connect({}, function (frame) {
            that.stompSelfSub.subscribe("/topic/private.noti." + that.user.id, (chatlive) => {
                if(chatlive){
                    
                    that._chatPanelService.getUsers().subscribe(
                        data => {
                            that.contacts = data;
                            
                        }
                    )
                }
            });
        });
    }

    setUser() {
        
        this._chatPanelService.getUsers().subscribe(
            data => {
                this.contacts = data;
                
            }
        )
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }



    @ViewChildren(FusePerfectScrollbarDirective)
    private _fusePerfectScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    private _chatViewScrollbar: FusePerfectScrollbarDirective;
    private _prepareChatForReplies(): void {
        setTimeout(() => {

            if (this._chatViewScrollbar) {
                this._chatViewScrollbar.update();

                setTimeout(() => {
                    this._chatViewScrollbar.scrollToBottom(0);
                });
            }
        });
    }
    ngAfterViewInit(): void {
        this._chatViewScrollbar = this._fusePerfectScrollbarDirectives.find((directive) => {
            return directive.element.nativeElement.id === 'messages';
        });
    }



    foldSidebarTemporarily(): void {
        $("chat-panel").animate({ "min-width": "60px" }, 500)
        $("chat-panel").animate({ "max-width": "60px" }, 500)
    }

    unfoldSidebarTemporarily(): void {
        $("chat-panel").animate({ "min-width": "360px" }, 500)
        $("chat-panel").animate({ "max-width": "360px" }, 500)
    }

    toggleSidebarOpen(): void {
        this._fuseSidebarService.getSidebar('chatPanel').toggleOpen();
    }

    shouldShowContactAvatar(message, i): boolean {
        return (
            message.who === this.selectedContact.id &&
            ((this.chat[i + 1] && this.chat[i + 1].who !== this.selectedContact.id) || !this.chat[i + 1])
        );
    }

    isFirstMessageOfGroup(message, i): boolean {
        return (i === 0 || this.chat[i - 1] && this.chat[i - 1].who !== message.who);
    }

    isLastMessageOfGroup(message, i): boolean {
        return (i === this.chat.length - 1 || this.chat[i + 1] && this.chat[i + 1].who !== message.who);
    }

    toggleChat(contact): void {

        let thread = new Thread()
        thread.setUser1(contact)
        thread.setUser2(this.user)



        if ((this.selectedContact)&&(contact.id === this.selectedContact.id)) {

            this.resetChat();

        } else {
            this.resetChat();

            this.unfoldSidebarTemporarily();

            this.selectedContact = contact;
            this._chatPanelService.isThreadExist(thread).subscribe(
                data => {

                    let ws = new SockJS(`${environment.apiUrl}/socket`);
                    this.tempStomp = Stomp.over(ws);
                    let that = this;
                    that.tempStomp.debug = () => {};
                    this.tempStomp.connect({}, function (frame) {
                        that.tempStomp.subscribe("/topic/private.noti." + that.selectedContact.id, (chatlive) => {
                        });
                    });

                    if (data == false) {
                        this._chatPanelService.createThread(thread).subscribe(
                            data => {
                                this.setUser();
                                this.roomId = data.id;
                                let ws = new SockJS(`${environment.apiUrl}/socket`);
                                this.stompClient = Stomp.over(ws);
                                let that = this;
                                that.stompClient.debug = () => {};
                                this.stompClient.connect({}, function (frame) {
                                    that.stompClient.subscribe("/topic/private.chat." + that.roomId, (chatlive) => {
                                        if (chatlive.body) {
                                            // Message

                                            that._prepareChatForReplies()
                                            let msjObj = JSON.parse(chatlive.body)
                                            let messageLive: ChatMessage = new ChatMessage()
                                            messageLive.setWho(msjObj.sender);
                                            messageLive.setTime(new Date().toISOString())
                                            messageLive.setMessage(msjObj.content)

                                            // Add the message to the chat
                                            that.chat.push(messageLive)

                                            // Reset the reply form
                                            that.replyInput.nativeElement.value = "";

                                            // Scroll to bottom
                                            that._prepareChatForReplies();
                                        }
                                    });
                                });
                            }
                        )
                    } else {

                        this._chatPanelService.getThread(thread).subscribe(
                            data => {
                                this.setUser();
                                this.roomId = data.id;
                                this._chatPanelService.getMessagesByThreadId(data.id).subscribe(
                                    messages => {

                                        for (let obj of messages) {

                                            let date = new Date();
                                            date = obj.sentAt

                                            let messageLive: ChatMessage = new ChatMessage()
                                            messageLive.setWho(obj.author.id);
                                            messageLive.setTime(date.toString())
                                            messageLive.setMessage(obj.text)
                                            this.chat.push(messageLive);

                                        }

                                        let ws = new SockJS(`${environment.apiUrl}/socket`);
                                        this.stompClient = Stomp.over(ws);
                                        this._prepareChatForReplies();
                                        let that = this;
                                        that.stompClient.debug = () => {};
                                        this.stompClient.connect({}, function (frame) {
                                            that.stompClient.subscribe("/topic/private.chat." + that.roomId, (chatlive) => {
                                                if (chatlive.body) {
                                                    // Message
                                                    that._prepareChatForReplies();
                                                    let msjObj = JSON.parse(chatlive.body)
                                                    let messageLive: ChatMessage = new ChatMessage()
                                                    messageLive.setWho(msjObj.sender);
                                                    messageLive.setTime(new Date().toISOString())
                                                    messageLive.setMessage(msjObj.content)

                                                    // Add the message to the chat
                                                    that.chat.push(messageLive)

                                                    // Reset the reply form
                                                    that.replyInput.nativeElement.value = "";

                                                    // Scroll to bottom
                                                    that._prepareChatForReplies();
                                                }
                                            });
                                        });
                                    }
                                )
                            }
                        )
                    }
                }
            )
        }
    }

    resetChat(): void {
        
        if (this.stompClient) {
            
            this.stompClient.unsubscribe("/topic/private.chat." + this.roomId);
            this.stompClient = null;
        }
        if(this.tempStomp&&this.selectedContact){
            
            this.tempStomp.unsubscribe("/topic/private.chat." + this.selectedContact.id);
            this.stompClient = null;
        }

        // Set the selected contact as null
        this.selectedContact = null;
        
        // Set the roomId as null
        this.roomId = "";

        // Set the chat as null
        this.chat = [];
    }

    reply(event) {
        this._chatPanelService.trigger()
        var messageContent = this.replyInput.nativeElement.value;

        if (messageContent && this.stompClient) {
            var chatMessage = {
                sender: this.user.id,
                content: this.replyInput.nativeElement.value,
                type: 'CHAT',
                threadId: this.roomId,
            };
            
            
            this.stompClient.send("/app/private.chat." + this.roomId, {}, JSON.stringify(chatMessage));
            setTimeout(
                _=>{
                    this.tempStomp.send("/app/private.noti."+this.selectedContact.id, {}, JSON.stringify(chatMessage));
                }
            )
            
        }
        
        event.preventDefault();
    }

    onMessageReceived(payload) {
        payload
    }
}
