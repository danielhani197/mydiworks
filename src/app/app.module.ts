import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatNativeDateModule } from '@angular/material';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FormsModule } from '@angular/forms';
import { fuseConfig } from './fuse-config';

import { AuthGuard } from "app/main/content/guard/auth.guard";
import { NgxEditorModule } from 'ngx-editor';
import { AppComponent } from './app.component';
import { FuseFakeDbService } from './fuse-fake-db/fuse-fake-db.service'; //temporary for testing
import { FuseMainModule } from './main/main.module';
import { FuseSampleModule } from './main/content/sample/sample.module';
import { ToastrModule } from 'ngx-toastr';
import { ProfileModule } from './main/content/pages/profile/profile.module';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { EscapeHtmlPipe } from './pipes/keep-html.pipe';
import { DatePipe } from '@angular/common';

const appRoutes: Routes = [
    {
        path        : '',
        loadChildren: './main/content/pages/authentication/auth.module#AuthModule'
    },
    {
        path        : 'user',
        loadChildren: './main/content/pages/pengguna/pengguna.module#PenggunaModule',
        canActivate: [AuthGuard],
    },
    {
        path        : 'agensi/pengguna',
        loadChildren: './main/content/pages/pengguna-admin/pengguna.module#PenggunaModule',
        canActivate: [AuthGuard],
    },
    {
        path        : 'setting',
        loadChildren: './main/content/pages/setting/setting.module#SettingModule',
        canActivate: [AuthGuard],
    },
    {
        path        : 'kementerian',
        loadChildren: './main/content/pages/general/kementerian/kementerian.module#KementerianModule',
        canActivate: [AuthGuard],
    },
    {
        path        : 'bahagian',
        loadChildren: './main/content/pages/general/bahagian/bahagian.module#BahagianModule',
        canActivate: [AuthGuard],
    },
    {
        path        : 'skema',
        loadChildren: './main/content/pages/general/skema/skema.module#SkemaModule',
        canActivate: [AuthGuard],
    },
    {
        path        : 'grade',
        loadChildren: './main/content/pages/general/grade/grade.module#GradeModule',
        canActivate: [AuthGuard],
    },
    {
        path        : 'agensi',
        loadChildren: './main/content/pages/general/agensi/agensi.module#AgensiModule',
        canActivate: [AuthGuard],
    },
    {
        path      : 'terms',
        loadChildren: './main/content/pages/terms/terms.module#TermsModule',
        canActivate: [AuthGuard],
    },
    {
        path      : 'wall',
        loadChildren: './main/content/pages/profile/profile.module#ProfileModule',
        canActivate: [AuthGuard],
    },
    {
        path      : 'post',
        loadChildren: './main/content/pages/search/search.module#SearchModule',
        canActivate: [AuthGuard],
    },
    {
        path      : 'carian',
        loadChildren: './main/content/pages/carian-post/carian-post.module#CarianPostModule',
        canActivate: [AuthGuard],
    },
    {
        path      : 'pet',
        loadChildren: './main/content/pages/pet/pet.module#PetModule',
        canActivate: [AuthGuard],
    },
    {
        path        : 'pengumuman',
        loadChildren: './main/content/pages/pengumuman/pengumuman.module#PengumumanModule',
        canActivate: [AuthGuard],
    },
    {
        path        :'account',
        loadChildren:'./main/content/pages/profile/firsttime/firsttime.module#FirsttimeModule',
        canActivate: [AuthGuard],
    },
    {
        path        :'profileEdit',
        loadChildren:'./main/content/pages/profile/edit-profile/edit-profile.module#EditProfileModule',
        canActivate: [AuthGuard],
    },
    {
        path        :'profilePet',
        loadChildren:'./main/content/pages/pet/wall-pet/edit-profile-pet/edit-profile.module#EditProfilePetModule',
        canActivate: [AuthGuard],
    },
    {
        path        :'updateMember',
        loadChildren:'./main/content/pages/pet/wall-pet/update-senarai-ahli/update-ahliPet.module#UpdateAhliPetModule',
        canActivate: [AuthGuard],
    },
    {
        path        :'notification',
        loadChildren:'./main/quick-panel/notifikasi.module#NotifikasiModule',
        canActivate: [AuthGuard],
    },
    {
        path        :'kementerianProfile',
        loadChildren:'./main/content/pages/profile-kementerian-agensi/profileKementerianAgensi.module#KementerianAgensiModule',
        canActivate: [AuthGuard],
    },   
    {
        path        : 'file-manager',
        loadChildren: './main/content/pages/file-manager/file-manager.module#FuseFileManagerModule',
        canActivate: [AuthGuard],
    },
    {
        path        : 'form',
        loadChildren: './main/content/pages/notiform/form.module#FormShareModule',
        canActivate: [AuthGuard],
    },
    {
        path        : 'pet-post',
        loadChildren: './main/content/pages/pet/wall-pet-post/woc.module#WpetPostModule',
        canActivate: [AuthGuard],
    },
  
    {
        path      : '**',
        redirectTo: 'logout',
    },

   


];

@NgModule({
    declarations: [
        AppComponent,
        EscapeHtmlPipe
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatNativeDateModule,
        RouterModule.forRoot(appRoutes),
        TranslateModule.forRoot(),
        InMemoryWebApiModule.forRoot(FuseFakeDbService, {
            delay             : 0,
            passThruUnknownUrl: true
        }),
        ToastrModule.forRoot({
            closeButton: true,
            positionClass: 'toast-top-center'
        }),
        NgxEditorModule,
        FormsModule,

        ProfileModule,
        // Fuse Main and Shared modules
        FuseModule.forRoot(fuseConfig),
        FuseSharedModule,
        FuseMainModule,
        FuseSampleModule,
        SweetAlert2Module.forRoot({
            buttonsStyling: false,
            customClass: 'modal-content',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn'
        }),
    ],
    bootstrap   : [
        AppComponent
    ],
    providers: [DatePipe]
})
export class AppModule
{
}
