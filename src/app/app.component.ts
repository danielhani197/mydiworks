import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { setTheme } from 'ngx-bootstrap/utils';

import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';

import { locale as navigationEnglish } from './navigation/i18n/en';
import { locale as navigationMalay } from './navigation/i18n/my';
import { navigation } from './navigation/navigation';
import { User } from './main/content/model/user';
import { Pet } from './main/content/model/pet/pet';
import { WallPetService } from './main/content/services/pet/wall-pet.service';
import { AuthenticationService } from './main/content/services/authentication/authentication.service';

@Component({
    selector   : 'fuse-root',
    templateUrl: './app.component.html',
    styleUrls  : ['./app.component.scss']
})
export class AppComponent implements OnInit
{
    navigation : any;
    currentUser: User;
    id         : string;
    roles      : any []; 

    projectList: Pet[] = [];
    eventList  : Pet[] = [];
    teamList   : Pet[] = [];


    constructor(
        private translate: TranslateService,
        private fuseNavigationService: FuseNavigationService,
        private fuseSplashScreen: FuseSplashScreenService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private _pet: WallPetService,
        private _auth: AuthenticationService
    )
    {
        // Add languages
        this.translate.addLangs(['en', 'my']);

        // Set the default language
        this.translate.setDefaultLang('my');

        // Set the navigation translations
        this.fuseTranslationLoader.loadTranslations(navigationEnglish, navigationMalay);

        // Use a language
        this.translate.use('my');

        setTheme('bs3'); // or 'bs4'

        this.navigation  = navigation;

    }

    ngOnInit(){
        // this.currentUser = this._auth.getCurrentUser();

        // if(this.currentUser){

        //     this.roles = this.currentUser.authorities;
            
        //     for(let roleObj of this.roles){
        //         let role = roleObj.authority;
                
        //         if(role == 'ROLE_USER'){
                    
        //         }if(role == 'ROLE_ADMIN'){
                    
        //         }if(role == 'ROLE_SUPERADMIN'){
                    
        //         }
        //     }

        //     this.id = this.currentUser.id;

            
        //     this._pet.getProjectByUser(this.id).subscribe(
        //         project=>{
        //             this.addIntoNav('project', project)
        //         }
        //     );
    
        //     this._pet.getEventByUser(this.id).subscribe(
        //         event=>{
        //             this.addIntoNav('event', event)
        //         }
        //     );
    
        //     this._pet.getTeamByUser(this.id).subscribe(
        //         team=>{
        //             this.addIntoNav('team', team)
        //         }
        //     );
        // }
    }

    addIntoNav(type: string, petList: Pet[]){
        
        const result = this.navigation[0].children.find( nav => nav.id === 'petList')
        if( type == "project"){
            const toInsert = result.children.find( nav => nav.id === 'project')
            for( let obj of petList){
                const newNavItem = {
                    id   : obj.id,
                    title: obj.nama,
                    type : 'item',
                    url  : '/pet/woc/'+obj.id
                };

                toInsert.children.push(newNavItem);
            }
        }
        if( type == "event"){
            const toInsert = result.children.find( nav => nav.id === 'event')
            for( let obj of petList){
                const newNavItem = {
                    id   : obj.id,
                    title: obj.nama,
                    type : 'item',
                    url  : '/pet/woc/'+obj.id
                };

                toInsert.children.push(newNavItem);
            }
        }
        if( type == "team"){
            const toInsert = result.children.find( nav => nav.id === 'team')
            for( let obj of petList){
                const newNavItem = {
                    id   : obj.id,
                    title: obj.nama,
                    type : 'item',
                    url  : '/pet/woc/'+obj.id
                };

                toInsert.children.push(newNavItem);
            }
        }
    }
}
