export const navigation = [
    {
        'id'      : 'applications',
        'title'   : 'Applications',
        'translate': 'NAV.APPLICATIONS',
        'type'    : 'group',
        'children': [
            // {
            //     'id'   : 'sample',
            //     'title': 'Sample',
            //     'translate': 'NAV.SAMPLE.TITLE',
            //     'type' : 'item',
            //     'icon' : 'email',
            //     'url'  : '/sample',
            //     'badge': {
            //         'title': 25,
            //         'translate': 'NAV.SAMPLE.BADGE',
            //         'bg'   : '#F44336',
            //         'fg'   : '#FFFFFF'
            //     }
            // },
            // {
            //     'id'   : 'term',
            //     'title': 'Terma & Syarat',
            //     'translate': 'NAV.TERM.TITLE',
            //     'type' : 'item',
            //     'icon' : 'view_list',
            //     'url'  : 'terms/list'
            // },
            // {
            //     'id'   : 'mywall',
            //     'title': 'My Wall',
            //     'translate': 'NAV.MYWALL.TITLE',
            //     'type' : 'item',
            //     'icon' : 'forum',
            //     'url'  : '/profile'
            // },
            // {
            //     'id'   : 'profileKementerian',
            //     'title': 'Profile Kementerian/ Agensi',
            //     'translate': 'NAV.PROFILE_kEMENTERIAN.TITLE',
            //     'type' : 'item',
            //     'icon' : 'style',
            //     'url'  : 'kementerianProfile/profileKementerian'
            // },
            // {
            //     'id'   : 'mybox',
            //     'title': 'My Box',
            //     'translate': 'NAV.MYBOX.TITLE',
            //     'type' : 'item',
            //     'icon' : 'move_to_inbox',
            //     'url'  : 'file-manager/mybox'
            // },
            {
                'id'   : 'calendar',
                'title': 'Kalendar',
                'translate': 'NAV.KALENDAR.TITLE',
                'type' : 'item',
                'icon' : 'date_range',
                'url'  : '/kalendar'
            },
            // {
            //     'id'   : 'pet',
            //     'title': 'Pet',
            //     'translate': 'NAV.PET.TITLE',
            //     'type' : 'collapse',
            //     'icon' : 'business_center',
            //     'children' :[
            //     {
            //         'id'   : 'list',
            //         'title': 'Senarai PET',
            //         'translate': 'NAV.PET.LIST',
            //         'type' : 'item',
            //         'url'  : 'pet/senarai'

            //     },
            //     {  
            //         'id'   : 'request',  
            //         'title': 'Permohonan PET',  
            //         'translate':'NAV.PET.REQUEST',  
            //         'type' : 'item',  
            //         'url'  :'pet/request'  
            //     },  
            //     {  
            //       'id'   : 'approval',  
            //       'title': 'Melulus PET',  
            //       'translate':'NAV.PET.APPROVAL',  
            //       'type' : 'item',  
            //       'url'  :'pet/approval'  
            //     },
            //     {  
            //         'id'   : 'approvalCreate',  
            //         'title': 'Melulus Bina PET',  
            //         'translate':'NAV.PET.APPROVALCREATE',  
            //         'type' : 'item',  
            //         'url'  :'pet/list/approve/createPet'  
            //     },
            //     {  
            //         'id'   : 'petCreateForm',  
            //         'title': 'Mohon Bina PET',  
            //         'translate':'NAV.PET.CREATEPET',  
            //         'type' : 'item',  
            //         'url'  :'pet/form/create'  
            //     },  

            //     ]

            // },
            // {
            //     'id'   : 'petList',
            //     'title': 'Pet',
            //     'translate': 'NAV.PET.TITLE',
            //     'type' : 'collapse',
            //     'icon' : 'apps',
            //     'children' :[
            //     {
            //         'id'   : 'project',
            //         'title': 'Project',
            //         'type' : 'collapse',
            //         'icon' : 'person_pin',
            //         'children': [
            //         ] 
            //     },
            //     {  
            //         'id'   : 'event',  
            //         'title': 'Event',    
            //         'type' : 'collapse',
            //         'icon' : 'event',
            //         'children': [
            //         ]  
            //     },  
            //     {  
            //         'id'   : 'team',  
            //         'title': 'Team', 
            //         'type' : 'collapse',
            //         'icon' : 'people',
            //         'children': [
            //         ] 
            //     }  

            //     ]
            // },
            // {
            //     'id'   : 'user',
            //     'title': 'Pengurusan Pengguna',
            //     'translate': 'NAV.USER.MANAGE',
            //     'type' : 'collapse',
            //     'icon' : 'contacts',
            //     'children' : [
            //         {
            //             'id'   : 'list',
            //             'title': 'Senarai Pengguna',
            //             'translate': 'NAV.USER.LIST',
            //             'type' : 'item',
            //             'url'  : 'user/list'
            //         },
            //         {
            //             'id'   : 'approval',
            //             'title': 'Pengesahan Pengguna',
            //             'translate': 'NAV.USER.APPROVAL',
            //             'type' : 'item',
            //             'url'  : 'user/new'
            //         },
            //         {
            //             'id'   : 'role',
            //             'title': 'Peranan Pengguna',
            //             'translate': 'NAV.USER.ROLE',
            //             'type' : 'item',
            //             'url'  : 'user/role_list'
            //         },
            //     ]

            // },
            // {
            //     'id'   : 'setting',
            //     'title': 'Tetapan',
            //     'translate': 'NAV.SETTING.MANAGE',
            //     'type' : 'collapse',
            //     'icon' : 'settings',
            //     'children' : [
            //         {
            //             'id'   : 'security_ques',
            //             'title': 'Senarai Soalan Keselamatan',
            //             'translate': 'NAV.SETTING.SECURITY',
            //             'type' : 'item',
            //             'url'  : 'setting/question_list'
            //         },
            //         {
            //             'id'   : 'security_ans',
            //             'title': 'Jawapan Keselamatan',
            //             'translate': 'NAV.SETTING.ANSWER',
            //             'type' : 'item',
            //             'url'  : 'setting/security_ans'
            //         },
            //         {
            //             'id'   : 'role_apply',
            //             'title': 'Permohonan Peranan',
            //             'translate': 'NAV.SETTING.ROLE',
            //             'type' : 'item',
            //             'url'  : 'setting/role/apply'
            //         },
            //     ]

            // },
            // {
            //     'id'   : 'carian',
            //     'title': 'Carian Post',
            //     'translate': 'NAV.CARIAN.MANAGE',
            //     'type' : 'collapse',
            //     'icon' : 'search',
            //     'children' : [
            //         {
            //             'id'   : 'carian-admin',
            //             'title': 'Carian Admin',
            //             'translate': 'NAV.CARIAN.CARIANADMIN',
            //             'type' : 'item',
            //             'icon' : 'search',
            //             'url'  : '/post/search'
            //         },
            //         {
            //             'id'   : 'carian-admin',
            //             'title': 'Carian',
            //             'type' : 'item',
            //             'translate': 'NAV.CARIAN.CARIAN',
            //             'icon' : 'search',
            //             'url'  : '/carian/post'
            //         },
            //     ]

            // },

            // {

            //     'id'   : 'general',
            //     'title': 'General',
            //     'translate': 'NAV.GENERAL.MANAGE',
            //     'type' : 'collapse',
            //     'icon' : 'account_balance',
            //     'children' : [
            //         {
            //             'id'   : 'ministryList',
            //             'title': 'Senarai Kementerian',
            //             'translate': 'NAV.GENERAL.MINISTRY.MINISTRYLIST',
            //             'type' : 'item',
            //             'icon' : 'people',
            //             'url'  : 'kementerian/list'
            //         },
            //         {
            //             'id'   : 'agencyList',
            //             'title': 'Senarai Agensi',
            //             'translate': 'NAV.GENERAL.AGENSI.AGENCYLIST',
            //             'type' : 'item',
            //             'icon' : 'supervisor_account',
            //             'url'  : 'agensi/list'
            //         },
            //         {
            //             'id'   : 'bahagianList',
            //             'title': 'Senarai Bahagian',
            //             'translate': 'NAV.GENERAL.BAHAGIAN.BAHAGIANLIST',
            //             'type' : 'item',
            //             'icon' : 'people_outline',
            //             'url'  : 'bahagian/list'
            //         },
            //         {
            //             'id'   : 'skemaList',
            //             'title': 'Senarai Skema',
            //             'translate': 'NAV.GENERAL.SKEMA.SKEMALIST',
            //             'type' : 'item',
            //             'icon' : 'star_border',
            //             'url'  : 'skema/list'
            //         },
            //         {
            //             'id'   : 'grade',
            //             'title': 'Senarai Grade',
            //             'translate': 'NAV.GENERAL.GRADE.GRADELIST',
            //             'type' : 'item',
            //             'icon' : 'star',
            //             'url'  : 'grade/list'
            //         }
            //     ]
            // },

            {

                'id'   : 'pengumuman',
                'title': 'Pengumuman',
                'translate': 'NAV.ANNOUNCEMENT.MANAGE',
                'type' : 'collapse',
                'icon' : 'account_balance',
                'children' : [
                    {
                        'id'   : 'announcementList',
                        'title': 'Senarai Pengumuman',
                        'translate': 'NAV.ANNOUNCEMENT.ANNOUNCEMENT.ANNOUNCEMENTLIST',
                        'type' : 'item',
                        'icon' : 'people',
                        'url'  : 'pengumuman/list'
                    }
                ]
            }            
            
        ]
    }
];
