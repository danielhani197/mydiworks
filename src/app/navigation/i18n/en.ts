export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'APPLICATIONS': 'Menu',
            'SAMPLE'        : {
                'TITLE': 'Sample',
                'BADGE': '25'
            },
            'REG' : {
                'TITLE': 'Register'
            },
            'USER' : {
                'MANAGE': 'User Management',
                'LIST': 'User List',
                'APPROVAL': 'User Approval',
                'ROLE' : 'User Role',
                'EDIT': 'Edit User'
            },
            'SETTING':{
                'MANAGE': 'Settings',
                'SECURITY': 'Security Question',
                'ANSWER': 'Security Answer',
                'ROLE' :   'Admin Application',

            },
            'PET' : {
                'LIST': 'Listing',
                'TITLE': 'Manage PET',
                'REQUEST': 'Request Join',
                'APPROVAL': 'Approval Member',
                'APPROVALCREATE' : 'Approval Create PET',
                'APPROVALAGENSICREATE' : 'Agency Approval Create PET',
                'CREATEPET' : 'Request Create PET',
                'DEACTIVE'  : 'Application of Pet Deactivation',
                'PET' : 'PET',
            },
            'CARIAN':{
                'MANAGE': 'Search',
                'CARIAN': 'Hashtag Search',
                'CARIANADMIN' : 'Admin Hashtag Search'


            },

            'GENERAL' : {

                'MANAGE' : 'General',

                'MINISTRY' : {
                    'MANAGE': 'Ministry',
                    'MINISTRYLIST': 'Ministry List'
                },
                'AGENSI' : {
                    'MANAGE': 'Agency',
                    'AGENCYLIST': 'Agency List',
                    'MANAGEAGENCY': 'Manage Agency'
                },
                'BAHAGIAN' : {
                    'MANAGE': 'Division',
                    'BAHAGIANLIST': 'Division List'
                },
                'SKEMA' : {
                    'MANAGE': 'Scheme',
                    'SKEMALIST': 'Scheme List'
                },
                'GRADE' : {
                    'MANAGE': 'Grade',
                    'GRADELIST': 'Grade List'
                },
            },
            'TERM' : {
                'TITLE' : 'Term & Condition'
            },
            'MYWALL' : {
                'TITLE' : 'My Wall'
            },
            'KALENDAR' : {
                'TITLE' : 'Calendar'
            },
            'ANNOUNCEMENT':{
                'MANAGE': 'Announcement',

                'ANNOUNCEMENT' : {
                    'MANAGE': 'Announcement',
                    'ANNOUNCEMENTLIST': 'Announcement List'
                },
            },
            'MYBOX' : {
                'TITLE' : 'My Box'
            },
            'PROFILE_kEMENTERIAN':{
                'TITLE': 'Ministry / Agency Profile'
            },
        }
    }
}
