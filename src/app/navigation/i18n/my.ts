export const locale = {
    lang: 'my',
    data: {
        'NAV': {
            'APPLICATIONS': 'Menu',
            'SAMPLE'        : {
                'TITLE': 'Sample',
                'BADGE': '15'
            },
            'REG' : {
                'TITLE': 'Daftar'
            },
            'USER' : {
                'MANAGE': 'Pengurusan Pengguna',
                'LIST': 'Senarai Pengguna',
                'TITLE': 'Senarai Pengguna',
                'APPROVAL': 'Pengesahan Pengguna',
                'ROLE' : 'Peranan Pengguna',
                'EDIT': 'Kemaskini Pengguna'
            },
            'PET' : {
                'LIST': 'Senarai',
                'TITLE': 'Mengurus PET',
                'REQUEST': 'Permohonan Sertai', 
                'APPROVAL': 'Melulus Ahli',
                'APPROVALCREATE' : 'Melulus Bina PET',
                'APPROVALAGENSICREATE' : 'Agensi Melulus Bina PET',
                'CREATEPET' : 'Mohon Bina PET',
                'DEACTIVE'  : 'Permohonan Nyahaktif PET',
                'PET' : 'PET',
            },
            'TERM' : {
                'TITLE' : 'Terma & Syarat'

            },
            'MYWALL' : {
                'TITLE' : 'My Wall'
            },
            'KALENDAR' : {
                'TITLE' : 'Kalendar'

            },
            'GENERAL' : {

                'MANAGE' : 'General',

                'MINISTRY' : {
                    'MANAGE': 'Kementerian',
                    'MINISTRYLIST': 'Senarai Kementerian'
                },
                'AGENSI' : {
                    'MANAGE': 'Agensi',
                    'AGENCYLIST': 'Senarai Agensi',
                    'MANAGEAGENCY': 'Pengurusan Agensi'
                },
                'BAHAGIAN' : {
                    'MANAGE': 'Bahagian',
                    'BAHAGIANLIST': 'Senarai Bahagian'
                },
                'SKEMA' : {
                    'MANAGE': 'Skim',
                    'SKEMALIST': 'Senarai Skim'
                },
                'GRADE' : {
                    'MANAGE': 'Gred',
                    'GRADELIST': 'Senarai Gred'
                },
            },
            'SETTING':{
                'MANAGE': 'Tetapan',
                'SECURITY': 'Senarai Soalan Keselamatan',
                'ANSWER': 'Jawapan Keselamatan',
                'ROLE' :   'Permohonan Admin',


            },
            'CARIAN':{
                'MANAGE': 'Carian',
                'CARIAN': 'Carian Hashtag',
                'CARIANADMIN' : 'Carian Hashtag Admin'


            },
            'ANNOUNCEMENT':{
                'MANAGE': 'Pengumuman',

                'ANNOUNCEMENT' : {
                    'MANAGE': 'Pengumuman',
                    'ANNOUNCEMENTLIST': 'Senarai Pengumuman'
                },
            },
            'MYBOX' : {
                'TITLE' : 'My Box'
            },
            'PROFILE_kEMENTERIAN':{
                'TITLE': 'Profile Kementerian/Agensi'
            },

        }
    }
};
